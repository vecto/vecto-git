# VECTO-GIT

## General

Version: 0.1

**VECTO-GIT** is a tool for storing certified component data (XML-files) and certification-related files such as the issued certificate, measurement data, measurement protocols, etc.

**VECTO-GIT** uses the version control system GIT as storage backend. This means that once data is stored in the storage backend it can not be altered or deleted. Every object stored in GIT is identified by its unique GIT-id that is derived from its content.

As GIT is a distributed version control system it is possible to use GIT's functionality for exchanging component data between component manufacturers and vehicle manufacturers, ensuring the data is not modified.

**VECTO-GIT** allows to store the following component-related data:

  - Component data (either measured or standard values, XML format)
  - Component certificate (PDF)
  - Measurement data (plain text data, e.g., csv)
  - User-defined data related to the certification (e.g, measurement protocols, information documents, etc.)

**VECTO-GIT** separates the component data and component certificate from measurement data and user-defined data. This allows to separately export and transfer the component data and certificate from one OEM to another while keeping measurement data and user-defined data private.

**VECTO-GIT** supports the following operations
  
  - Storing component data
  - Storing component certificate
  - Storing measurement data and user-defined data
  - Searching for a component by 
     - Manufacturer
     - Certification date
     - Certification number
     - Model
     - GIT-identifier
  - Export public component data (component XML and component certificate)
  - Export private component data (measurement data and user-defined data)
  - Import public component data
  - Import private component data

## First Start

When starting VECTO-GIT for the first time, the [Settings Dialog](#settings-dialog) is opened where you need to select a folder on your local PC where VECTO-GIT will create its storage repository.

![](images/VECTO-GIT_Settings.png)

## Storing Component Data 

The component data (XML) shall be stored in VECTO-GIT during the certification of a component, when the measurements have been evaluated using one of the VECTO component evaluation tools (e.g, VECTO Engine, VECTO Air Drag, or OEM-specific tools for transmission components).

The first step is to add the component XML by selecting `Save` -> `Save Component Data` from the menu. Via the button `add files` one or multiple files can be selected for storing in the VECTO-GIT database. 

It is allowed to store only one component at a time. It is possible to add all component-related files (e.g., measurement data, measurement protocol, etc.) already together with the component XML. The user interface shows a list of files selected for storing in VECTO-GIT. In this list the appropriate file type has to be selected from the drop-down list. Exactly one file needs to be of type `Component Data` and needs to be a correct XML file. The component file is used to read out all required meta data such as maufacturer, model, certification number, certification date, etc. All other selected files will be assigned to this component with the selected type and treated either as private or public data. Component-related data can be added later as well (see [Adding Component-Related Data](#adding-component-related-data)).

User interface for adding component data to VECTO-GIT:

![](images/VECTO-GIT_SaveFiles.PNG)


## Adding Component-related data

Once a component data is stored in VECTO-GIT further information can be added to a component, for example the component's certificate, measurement data or measurement protocols.

### Search Component

Selecting `Save` -> `Add Component Data` shows a dialog that allows to search for the component where further information will be added. A component can be searched by manufacturer name, model name, GIT identifier, certification number or the certification date. The terms entered for manufacturer and model do not need to be exaclty the same in the searched components, all components that contain the entered search term will be listed. The entered search terms are case insensitive. For the GIT identifier at least 2 characters need to be entered and all components whose GIT identifier starts with the entered characters will be listed. All search criteria are applied in conjunction. When clicking on the `Search` button all matching components are listed.  

![](images/VECTO-GIT_AppendComponentData.png)

### Append Data to Component

By clicking on the button `append` to the right of an entry in the list, the component's details are shown. The component data can be obtained as described in [Retrieve Component Information](#retrieve-component-information). Further files can be added in the same way as described in [Store Component Data](#storing-component-data). Only a component's certificate, measurement data or user-defined data can be added to a component. Via the `add files` button one or more files can be selected to be added into VECTO-GIT's database. In the list of selected files the appropriate file type has to be selected.

![](images/VECTO-GIT_AppendComponentCertificate.png)


## Retrieve Component Information


### Search Component

It is of course possible to view component data stored in a VECTO-GIT database. When selecting `Search` -> `Search Component Data` from the menu, a search dialog is shown. A component can be searched by manufacturer name, model name, GIT identifier, certification number or the certification date. The terms entered for manufacturer and model do not need to be exaclty the same in the searched components, all components that contain the entered search term will be listed. The entered search terms are case insensitive. For the GIT identifier at least 2 characters need to be entered and all components whose GIT identifier starts with the entered characters will be listed. All search criteria are applied in conjunction. When clicking on the `Search` button all matching components are listed.

![](images/VECTO-GIT_SearchComponent2.PNG)

### Component Details

By clicking on the button `details` to the right of an entry in the list, the component's details are shown. In the overview the important information is summarized: manufacturer name, model name, certification number, and certification name together with the GIT identifier of the component data. All this information is extracted from the component's XML when the component is added to the database. 

Below the public data of a component is listed. This at least contains the component's XML data and optionally the component's certificate. If measurement data or user-defined data is also stored for a component, this data is listed below as *Private Data*.

Via right-click on an entry (either public data - component data, certificate - or private data - measurement data, user-defined data) and selecting `Show Details` on an entry a new window opens showing the contents of the file. This viewer provides syntax highlighting for different languages, e.g. XML, HTML, ... and also displays PDF documents. The displayed document can be saved using the `Save` button in the upper part of the window.

The lower part of the component information window allows to select the public data and the private data separately to be added to the list of components for transferring to another VECTO-GIT database. Simply tick the checkbox of the data to be transferred and click `save selection` to add it for later export (see [Transfer Component Data](#transfer-component-data). 

## Transfer Component Data

VECTO-GIT allows to transfer component data (either only the public data, or the public and private data) from one VECTO-GIT database to another. For example if a component manufacturer needs to provide the component data for one of its component to a vehicle manufacturer, the component manufacturer can select one or many components to be transferred to another repository and export the selected components into a file which can be sent to the vehicle manufacturer.
The vehicle manufacturer then can import the component data into its own VECTO-GIT database and be sure that the component data is not modified. When transferring component data between repositories the GIT identifier remains the same and the GIT storage backend ensures that no data has been modified (modifications would invalidate the GIT identifier).

### Export Component Data

Before you can export component data for one or more components, you need to add the desired components to the list of components to be exported. This can be done via the *Component Details* window (see [Retrieve Component Information](#retrieve-component-information)).

![](images/VECTO-GIT_Export.png)

In the export window (via `Transfer` -> `Export Component Data` in the main menu) a list of components selected for export is shown. You can remove a component by selecting it from the list and clicking on `remove selected` or show the details of a component by clicking on the button `details` to the right of an entry in the list.

The components listed can be saved as a so-called *transfer package* by clicking on the button `create transfer package`. In the file save dialog you need to naviagate to the desired storage location and enter a filename for the export file. The transfer package is stored as a ZIP-file. However, the contents of the ZIP file is not the plain component data but a GIT-specific format.

### Import Component Data

In the import window (via `Transfer` -> `Import Component Data` in the main menu) you need to select the VECTO-GIT transfer package exported from another repository (see [Export Component Data](#export-component-data)). If the selected ZIP-file is a valid VECTO-GIT transfer package, the list of components contained in the transfer package is shown. By clicking on the button `import into repository` all components are added to the VECTO-GIT database.

Components can be stored in a VECTO-GIT database only once. If a transfer-package contains a component that is already stored in the database the according component will be skipped for import. 

![](images/VECTO-GIT_Import1.png)

## VECTO Simulations

VECTO-GIT supports to run VECTO declaration-mode simulations directly. Therefore, component data is loaded from the GIT database, only vehicle-specific parameters need to be provided.

![](images/VECTO-GIT_job.png)

###  VECTO-GIT Model Parameters and Components

The vehicle parameters can be entered or selected from a drop down list directly. By clicking on the `Browse` button next to the vehicle's component input fields the search dialog is opened on the right. Here the according component can be searched according to different criteria whereas the component-type is already preselected and cannot be modified. In the list of matching components the desired component can be added to the vehicle configuration by clicking on the `Select` button of the according component. The GIT-identifier is then shown in the component input field.

![](images/VECTO-GIT_job-search.png)

### VECTO-GIT Jobs

It is possible to save and load a VECTO-GIT based VECTO job file with the `Load` and `Save` buttons below the input fields.

Via the `Run VECTO Job` button the VECTO-simulation can be started. The output is displayed on the right.

![](images/VECTO-GIT_JobFile_Sim.png)

### XML Format

The format for saving a VECTO-GIT job is an XML format, similar to the official declaration mode structure. The only difference is that instead of the component data only the GIT identifier is stored in the job-data.

![](images/VECTO-GIT_job-xml.png) 

## Settings Dialog

In the *Settings* dialog the storage location for VECTO-GIT can be changed. Moreover, a user-name and e-mail address can be configured. This is only required for the GIT storage backend (i.e., VECTO-GIT currently does not show this information in the user interface) but it may be used to trace who added certain data to the VECTO-GIT database in future. So it is suggested to add your contact information.

![](images/VECTO-GIT_Settings.png)

