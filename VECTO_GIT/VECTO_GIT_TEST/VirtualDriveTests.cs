﻿using NUnit.Framework;
using VECTO_GIT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LibGit2Sharp;
using VECTO_GIT_TEST;

namespace VECTO_GIT.Tests
{
  [TestFixture()]
  [Category("VirtualDrive")]
  public class VirtualDriveTests
  {
    private VirtualDrive virtualDrive;
    
    [OneTimeSetUp()]
    public void OneTimeInit()
    {
      virtualDrive = new VirtualDrive();
    }
        
    [Test()]
    public void MapAndUnmapDriveTest()
    {
      var beforMapping = GetAvailableDrivers();
      var mapResult = virtualDrive.MapDrive(TestHelper.DES_REPO_PATH);
      var afterMapping = GetAvailableDrivers();
      var driveLetter = virtualDrive.DriveLetter;

      Assert.NotNull(mapResult);
      Assert.AreEqual(beforMapping.Length + 1, afterMapping.Length);
      Assert.AreEqual(virtualDrive.OriginDirectoryPath, TestHelper.DES_REPO_PATH);
      Assert.IsTrue(afterMapping.Any(obj => obj.Name[0] == driveLetter[0]));

      virtualDrive.UnmapDrive();

      var afterUnmapDrive = GetAvailableDrivers();
      Assert.AreEqual(beforMapping.Length, afterUnmapDrive.Length);
      Assert.IsFalse(afterUnmapDrive.Any(obj => obj.Name[0] == driveLetter[0]));
    }

    [Test()]
    public void LongPathWithMappingTest()
    {
      //max 254 chars 
      // 254 - 14(A:\refs\heads\) - 4(pub\) - 5(comp\) -5(2014\) - 12(hash) -2(\ \)  = 212

      var repository = GetRepository(TestHelper.DES_REPO_PATH);
      var maxBranchName = "pub/" +
        "Long_Manufacturer_Name_2154652254655456545454545645646548784545421546522546554565454545456456465487845_106/" +
        "comp/" +
        "2018/" +
        "Long_Model_Name_16546512154654564654654654654654654654654654564654216546546546546546546546546546545645_106/" +
        "vwf71ha02yhu";

      //try create branch without virtual drive
      Assert.Throws<InvalidSpecificationException>(() => TestHelper.GetExampleBranch(repository, "vecto_engine_date_sample03.xml",
        maxBranchName, CommitDataType.StandardData));
      Assert.AreEqual(0, repository.Branches.Count());


      //try with virtual drive
      repository.Dispose();
      var mapDriveResult = virtualDrive.MapDrive(TestHelper.DES_REPO_PATH);
      repository = GetRepository(virtualDrive.DriveLetter + "\\");
      var driveLetter = virtualDrive.DriveLetter + "\\";
      var newBranch = TestHelper.GetExampleBranch(repository, "vecto_engine_date_sample03.xml",
        maxBranchName, CommitDataType.StandardData);

      Assert.NotNull(newBranch);
      Assert.NotNull(mapDriveResult);
      Assert.AreEqual(1, repository.Branches.Count());
      Assert.AreEqual(repository.Branches.First().FriendlyName, maxBranchName);


      //too long branchName 
      maxBranchName = maxBranchName + "a";
      Assert.Throws<LibGit2SharpException>(() => TestHelper.GetExampleBranch(repository, "vecto_engine_date_sample04.xml",
        maxBranchName, CommitDataType.StandardData));

      Assert.AreEqual(1, repository.Branches.Count());

      repository.Dispose();
      TestHelper.DeleteFolderContent(driveLetter);

      var unmapDriveResult = virtualDrive.UnmapDrive();
      Assert.IsTrue(unmapDriveResult);
    }

    [Test]
    public void SymbolicJunctionTest()
    {
      //max 254 chars 
      // 254 - 14(A:\refs\heads\) - 4(pub\) - 5(comp\) -5(2014\) - 12(hash) -2(\ \)  = 212 - 26 = 186



      //var path = @"C:\ProgramData\VECTO_GIT\J";
      //var repository = GetRepository(path);
      //var maxBranchName = "pub/" +
      //  "Long_Manufacturer_Name_21546522546554565454545456456465487845454215465225465545654545454512_94/" +
      //  "comp/" +
      //  "2018/" +
      //  "Long_Model_Name_165465121546545646546546546546546546546546545646542165465465464654654566565_94/" +
      //  "vwf71ha02yhu";

      //var branch = TestHelper.GetExampleBranch(repository, "vecto_engine_date_sample03.xml",
      //  maxBranchName, CommitDataType.StandardData);

      //Assert.NotNull(branch);


      //var maxBranchName = "pub/" +
      //  "Long_Manufacturer_Name_215465225465545654545454564564654878454542154652254655456545454_90/" +
      //  "comp/" +
      //  "2018/" +
      //  "Long_Model_Name_16546512154654564654654654654654654654654654564654216546546546546546545_91/" +
      //  "vwf71ha02yhu766";

      //Assert.Throws<LibGit2SharpException>(() => TestHelper.GetExampleBranch(repository, "vecto_engine_date_sample03.xml",
      //  maxBranchName, CommitDataType.StandardData));




    }


    private DriveInfo[] GetAvailableDrivers()
    {
      return DriveInfo.GetDrives();
    }

    private Repository GetRepository(string path)
    {
      return Helpers.CreateOrLoadRepository(path, true);
    }

  }
}