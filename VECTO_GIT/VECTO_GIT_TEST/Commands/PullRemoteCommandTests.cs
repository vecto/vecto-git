﻿using LibGit2Sharp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT_TEST;

namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("PullRemoteCommand")]
  public class PullRemoteCommandTests
  {

    private Repository srcRepository;
    private Repository desRepository;
    private Dictionary<string, List<string>> branchInformation;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      srcRepository = TestHelper.InitEmptyRepository(RepositoryType.Source, true);
      desRepository = TestHelper.InitEmptyRepository(RepositoryType.Destination);

      SetBranchDataInfo();
      SetUpTransferBranches();
      AddTagsToBranches();
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      srcRepository.Dispose();
      desRepository.Dispose();

      TestHelper.DeleteRepository(RepositoryType.Source);
      TestHelper.DeleteRepository(RepositoryType.Destination);
    }


    [Test()]
    public void PullRemoteCommandTest()
    {
      var remotePath = srcRepository.Info.Path;

      Assert.Throws<ArgumentNullException>(() => new PullRemoteCommand(null, remotePath));
      Assert.Throws<ArgumentNullException>(() => new PullRemoteCommand(srcRepository, null));
      Assert.Throws<ArgumentException>(() => new PullRemoteCommand(srcRepository, ""));
      Assert.Throws<RepositoryNotFoundException>(() => new PullRemoteCommand(new Repository(@"/wrong/repos"), remotePath));
      Assert.DoesNotThrow(() => new PullRemoteCommand(desRepository, remotePath));
    }

    [Test()]
    public void PullRemoteResultTest()
    {
      var command = new PullRemoteCommand(desRepository, srcRepository.Info.Path);

      foreach (var branch in srcRepository.Branches)
      {
        var branchName = branch.CanonicalName.Replace("refs/heads/", "");
        var desBranch = desRepository.Branches[branchName];

        Assert.AreEqual(branch.Commits.Count(), desBranch.Commits.Count());
        Assert.AreEqual(branch.Tip.Sha, desBranch.Tip.Sha);
      }
      Assert.AreEqual(srcRepository.Branches.Count(), desRepository.Branches.Count());

      foreach (var tag in srcRepository.Tags)
      {
        var tagName = tag.FriendlyName;
        var desTag = desRepository.Tags[tagName];

        Assert.AreEqual(tag.Target.Id, desTag.Target.Id);
        Assert.AreEqual(tag.Target.Sha, desTag.Target.Sha);
        Assert.AreEqual(tag.Annotation.Message, desTag.Annotation.Message);
      }
      Assert.AreEqual(srcRepository.Tags.Count(), desRepository.Tags.Count());
    }


    private void SetUpTransferBranches()
    {
      foreach (var branchdata in branchInformation)
      {
        if (branchdata.Value.Count > 1)
        {
          TestHelper.GetExampleBranch(srcRepository, branchdata.Value, branchdata.Key, CommitDataType.TestData);
        }
        else
        {
          TestHelper.GetExampleBranch(srcRepository, branchdata.Value.First(), branchdata.Key, CommitDataType.TestData);
        }
      }
    }


    private void AddTagsToBranches()
    {
      foreach (var branch in srcRepository.Branches)
      {
        var tagName = branch.CanonicalName.Replace("refs/heads/", "") + "/test";
        var tagMessage = branch.FriendlyName;
        var tag = new CreateTagCommand(srcRepository, tagName, tagMessage, branch.Commits.First().Sha);
        tag.Result();
      }
    }

    private void SetBranchDataInfo()
    {
      branchInformation = new Dictionary<string, List<string>>
      {
        { "test/branch/01", new List<string>{ "Demo_FullLoad_Child_v1.4.csv" } },
        { "test/branch/02",
          new List<string>{ "Demo_FullLoad_Parent_v1.4.csv" , "Demo_Map_v1.4.csv"} },
        { "test/branch/03", new List<string>{ "Demo_Motoring_v1.4.csv" } }
      };
    }


  }
}