﻿using NUnit.Framework;
using System;
using System.Linq;
using VECTO_GIT_TEST;
using LibGit2Sharp;



namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("CreateOrphanBranch")]
  public class CreateOrphanBranchCommandTests
  {
    private Repository repository;
    private string fileName;


    [OneTimeSetUp]
    public void OneTimeInit()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      fileName = "Demo_FullLoad_Child_v1.4.csv";
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }
    
    [Test()]
    public void CreateOrphanBranchCommandConstructorTest()
    {
      var branchName = "Test/Branch/Name";
      var testCommit = TestHelper.GetExampleCommit(repository, fileName, CommitDataType.TestData);

      Assert.Throws<ArgumentNullException>(() => new CreateOrphanBranchCommand(null, branchName, testCommit));
      Assert.Throws<ArgumentNullException>(() => new CreateOrphanBranchCommand(repository, null, testCommit));
      Assert.Throws<ArgumentNullException>(() => new CreateOrphanBranchCommand(repository, branchName, null));
      Assert.Throws<ArgumentException>(() => new CreateOrphanBranchCommand(repository, "", testCommit));
    }


    [Test()]
    public void CreateOrphanBranchCommandResultTest()
    {
      var branchName = "Test/Branch/Name";
      var branch = TestHelper.GetExampleBranch(repository, fileName, branchName, CommitDataType.TestData);

      Assert.AreEqual(branchName, branch.FriendlyName);
      Assert.AreEqual(typeof(Branch), branch.GetType());
      Assert.AreEqual(1, repository.Branches.Count());
      Assert.AreEqual(3, repository.ObjectDatabase.Count());
      Assert.AreEqual(branch.Tip, repository.Branches[branchName].Tip);
      Assert.AreEqual(Enumerable.Empty<Commit>(), branch.Tip.Parents);
    }
  }
}