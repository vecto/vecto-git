﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT_TEST;

namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("SearchDateCommand")]
  public class SearchDateCommandTests
  {

    private ComponentDataManagement compDataManagement;
    private List<string> branchNames;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      compDataManagement = new ComponentDataManagement();
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      SetUpTestBranches();

      branchNames = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_1/abcdhksp0lxv",
        PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_2/abeflksp0lxv",
        PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_3/abghkosp0lxv",
        PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_4/abgefksp0lxv",
        PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_5/abglpksp0lxv"
      };
    }


    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      compDataManagement.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
    }


    [Test()]
    public void SearchDateCommandConstructorTest()
    {
      var searchTerms = new VectoSearchTerms();

      Assert.Throws<ArgumentNullException>(() => new SearchDateCommand(null, searchTerms));
      Assert.Throws<ArgumentNullException>(() => new SearchDateCommand(compDataManagement.CurrentRepository, null));
      Assert.DoesNotThrow(() => new SearchDateCommand(compDataManagement.CurrentRepository, searchTerms));
      Assert.NotNull(new SearchDateCommand(compDataManagement.CurrentRepository, searchTerms).Result());
    }

    [Test()]
    public void SingleResultTest()
    {
      var searchedDate = GetDateTimeInUtc(new DateTime(2017, 02, 17, 23, 0, 0));
      var searchTerms = new VectoSearchTerms
      {
        CertificateDate = searchedDate,
        IsCertificateDate = true
      };
      var command = new SearchDateCommand(compDataManagement.CurrentRepository, searchTerms);
      var result = command.Result();

      Assert.NotNull(result);
      Assert.AreEqual(1, result.Count());
      Assert.AreEqual(branchNames[0], result.First().FriendlyName);
    }

    [Test()]

    public void MultipleResultTest()
    {
      var searchedDate = GetDateTimeInUtc(new DateTime(2017, 02, 18, 18, 0, 0));
      var searchTerms = new VectoSearchTerms
      {
        CertificateDate = searchedDate,
        IsCertificateDate = true
      };
      var command = new SearchDateCommand(compDataManagement.CurrentRepository, searchTerms);

      var result = command.Result();

      Assert.AreEqual(2, result.Count());
      Assert.AreEqual(branchNames[2], result.ToArray()[0].FriendlyName);
      Assert.AreEqual(branchNames[3], result.ToArray()[1].FriendlyName);
    }


    [Test()]
    public void StandardValuesResultTest()
    {
      var searchedDate = GetDateTimeInUtc(new DateTime(2017, 03, 19, 18, 0, 0));
      var searchTerms = new VectoSearchTerms
      {
        CertificateDate = searchedDate,
        IsCertificateDate = true
      };
      var command = new SearchDateCommand(compDataManagement.CurrentRepository, searchTerms);
      var result = command.Result();

      Assert.AreEqual(1, result.Count());
      Assert.AreEqual(branchNames[4], result.First().FriendlyName);
    }

    [Test()]
    public void SearchFromToResultTest()
    {
      var searchFrom = GetDateTimeInUtc(new DateTime(2017, 2, 18, 18, 0, 0));
      var searchTo = GetDateTimeInUtc(new DateTime(2017, 2, 19, 2, 0, 0));
      var searchTerms = new VectoSearchTerms
      {
        CertificateDateFrom =  searchFrom,
        CertificateDateTo = searchTo,
        IsCertificateDateFromTo = true
      };

      var command = new SearchDateCommand(compDataManagement.CurrentRepository, searchTerms);
      var result = command.Result();

      Assert.AreEqual(3, result.Count());
      Assert.AreEqual(branchNames[1], result.ToArray()[0].FriendlyName);
      Assert.AreEqual(branchNames[2], result.ToArray()[1].FriendlyName);
      Assert.AreEqual(branchNames[3], result.ToArray()[2].FriendlyName);
    }


    private DateTime GetDateTimeInUtc(DateTime dateTime)
    {
      return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
    }


    private void SetUpTestBranches()
    {
      var repository = compDataManagement.CurrentRepository;

      for (int i = 1; i <= 4; i++)
      {
        var fileName = string.Format("vecto_engine_date_sample0{0}.xml", i);
        var filePath = TestHelper.GetTestFilePath(fileName);

        var compCommit = new ComponentDataCommit(repository, filePath, null, null);
        compCommit.VerificationCheckBeforeCommit();

        if (compCommit.ExecuteCommit() == null)
        {
          throw new Exception("SetUpTestBranches() failure");
        }
      }

      var stdFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample05.xml");
      var stdCommit = new StandardDataCommit(repository, stdFilePath);
      stdCommit.VerificationCheckBeforeCommit();

      if (stdCommit.ExecuteCommit() == null)
      {
        throw new Exception("SetUpTestBranches() failure");
      }
    }

  }
}