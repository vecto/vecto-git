﻿using LibGit2Sharp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;
using VECTO_GIT_TEST;


namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("SearchTagCommand")]
  public class SearchTagCommandTests
  {
    private Repository repository;
    private Dictionary<string, List<string>> branchInformation;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);

      SetBranchDataInfo();
      SetUpTestBranches();
      AddTagsToBranches();
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [Test()]
    public void SearchTagCommandConstructorTest01()
    {
      var pubBranches = GetPublicBranches();
      var gitIdHandler = new GitIdHandler(pubBranches.First());

      Assert.Throws<ArgumentNullException>(() => new SearchTagCommand(null, gitIdHandler));
      Assert.Throws<ArgumentNullException>(() => new SearchTagCommand(repository, (INamingHandler)null));
      Assert.DoesNotThrow(() => new SearchTagCommand(repository, gitIdHandler));
      Assert.NotNull(new SearchTagCommand(repository, gitIdHandler).Result());
    }

    [Test()]
    public void SearchTagCommandConstructorTest02()
    {
      var tagName = "someTagName";

      Assert.Throws<ArgumentNullException>(() => new SearchTagCommand(null, tagName, TagNameType.CertificateNumber));
      Assert.Throws<ArgumentNullException>(() => new SearchTagCommand(repository, null, TagNameType.CertificateNumber));
      Assert.Throws<ArgumentException>(() => new SearchTagCommand(repository, "", TagNameType.CertificateNumber));
      Assert.DoesNotThrow(() => new SearchTagCommand(repository, tagName,
       TagNameType.CertificateNumber));
      Assert.NotNull(new SearchTagCommand(repository, tagName, TagNameType.CertificateNumber).Result());
    }

    [Test()]
    public void SearchTagCommandConstructorTest03()
    {
      var branchNames = new List<string>
      { PublicBranchNaming.TREE_NAME + "/GEM/Component/2017/Generic_40t/9275da3dbb4a" };

      Assert.Throws<ArgumentNullException>(() => new SearchTagCommand(null, branchNames));
      Assert.Throws<ArgumentNullException>(() => new SearchTagCommand(repository, (List<string>)null));
      Assert.Throws<ArgumentException>(() => new SearchTagCommand(repository, new List<string> { "" }));
      Assert.DoesNotThrow(() => new SearchTagCommand(repository, branchNames));
      Assert.NotNull(new SearchTagCommand(repository, branchNames).Result());
    }

    [Test()]
    public void SearchTagCommandConstructorTest04()
    {

    }


    [Test()]
    public void SearchTagCommandResultTest01()
    {
      var branchName = PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a";
      var branch = repository.Branches[branchName];
      var gitIdHandler = new GitIdHandler(branch);
      var searchTag = new SearchTagCommand(repository, gitIdHandler);

      var fileName = branchInformation[branchName].First();
      var blob = branch.Commits.Last().Tree.First().Target as Blob;


      var fileGitId = TestHelper.GetGitHashOfData(fileName, blob.IsBinary);
      var expectedGitIdHandler = new GitIdHandler(fileGitId, false);
      var expectedResult = new List<string>()
      {
        expectedGitIdHandler.GetTagGitPathName()
      };

      var searchResult = searchTag.Result();

      Assert.True(Comparer(expectedResult, searchTag.Result()));
    }

    [Test()]
    public void SearchTagCommandResultTest02()
    {
      var certNumber = "e12*0815/8051*2017/05E0000*00";
      var xmlHash = "JWEwzKSP0lXvwRgQZTiWJm9dpdtQ72FOX0CC5Vy6f2Y";
      var certHandler = new CertificateNumberHandler(certNumber, xmlHash);
      var searchTag = new SearchTagCommand(repository, certHandler);
      var expectedResult = new List<string>
      {
        "CERT_NR/e12#0815)8051#2017)05#e/0000#00/jwewzksp0lxv"
      };

      Assert.True(Comparer(expectedResult, searchTag.Result()));
    }

    [Test()]
    public void SearchTagCommandResultTest03()
    {
      var certNumber = "e12*0815/8051*2017/05*E*0000*00";

      var searchTag = new SearchTagCommand(repository, certNumber, TagNameType.CertificateNumber);
      var expectedResult = new List<string>
      {
        "CERT_NR/e12#0815)8051#2017)05#e/0000#00/jwewzksp0lxv",
        "CERT_NR/e12#0815)8051#2017)05#e/0000#00/vwf71ha02yhu"
      };

      Assert.True(Comparer(expectedResult, searchTag.Result()));

      var branchName = PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a";
      var branch = repository.Branches[branchName];
      var fileName = branchInformation[branchName].First();
      var blob = branch.Commits.Last().Tree.First().Target as Blob;

      var fileGitId = TestHelper.GetGitHashOfData(fileName, blob.IsBinary);
      var expectedGitIdHandler = new GitIdHandler(fileGitId, false);
      expectedResult = new List<string>()
      {
        expectedGitIdHandler.GetTagGitPathName()
      };

      searchTag = new SearchTagCommand(repository, fileGitId, TagNameType.GitId);

      Assert.True(Comparer(expectedResult, searchTag.Result()));
    }

    [Test()]
    public void SearchTagCommandResultTest04()
    {
      var branchName = PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a";
      var branchNames = new List<string>
      {
        branchName
      };

      var searchTag = new SearchTagCommand(repository, branchNames);
      var branch = repository.Branches[branchName];
      var fileName = branchInformation[branchName].First();
      var blob = branch.Commits.Last().Tree.First().Target as Blob;


      var fileGitId = TestHelper.GetGitHashOfData(fileName, blob.IsBinary);
      var expectedGitIdTag = new GitIdHandler(fileGitId, false);

      var expectedResult = new List<string>
      {
        "CERT_NR/e12#0815)8051#2017)05#e/0000#00/vwf71ha02yhu",
         expectedGitIdTag.GetTagGitPathName()
      };

      Assert.True(Comparer(expectedResult, searchTag.Result()));
    }


    [Test()]
    public void CertificationTagSearchTest()
    {
      var certNumberModel = new CertificationNumberModel
      {
        Country = "12",
        CertificationAct = "0815/8051",
        AmendingAct = "2017/05",
        ComponentType = "e",
        BaseNumber = "0000",
        ExtensionNumber = "00"
      };
      
      var searchTag = new SearchTagCommand(repository, certNumberModel);
      var tags = searchTag.Result();
      
      Assert.AreEqual(2, tags.Length);
      Assert.AreEqual(tags[0].FriendlyName, "CERT_NR/e12#0815)8051#2017)05#e/0000#00/jwewzksp0lxv");
      Assert.AreEqual(tags[1].FriendlyName, "CERT_NR/e12#0815)8051#2017)05#e/0000#00/vwf71ha02yhu");
    }
    

    private bool Comparer(IEnumerable<string> expected, Tag[] result)
    {
      if (expected.Count() != result.Length)
      {
        return false;
      }

      foreach (var tag in result)
      {
        if (!expected.Contains(tag.FriendlyName))
        {
          return false;
        }
      }
      return true;
    }


    private IEnumerable<Branch> GetPublicBranches()
    {
      return repository.Branches.Where(
        obj => obj.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME));
    }


    private void SetUpTestBranches()
    {
      foreach (var branchdata in branchInformation)
      {
        if (branchdata.Value.Count > 1)
        {
          TestHelper.GetExampleBranch(repository, branchdata.Value, branchdata.Key, CommitDataType.ComponentData);
        }
        else
        {
          TestHelper.GetExampleBranch(repository, branchdata.Value.First(), branchdata.Key, CommitDataType.ComponentData);
        }
      }
    }

    private void AddTagsToBranches()
    {
      var pubBranches = GetPublicBranches();

      foreach (var branch in pubBranches)
      {
        SetGitIdTag(branch);
        SetCertificateTag(branch);
      }
    }

    private void SetGitIdTag(Branch branch)
    {
      var tagMsg = new PublicBranchNaming(branch.FriendlyName).GetJsonRepesentation();
      var gitId = new GitIdHandler(branch);
      var tagGitIdName = gitId.GetTagGitPathName();

      var gitIdTag = new CreateTagCommand(repository, tagGitIdName, tagMsg,
        branch.Commits.Last().Sha);
    }


    private void SetCertificateTag(Branch branch)
    {
      var xmlReader = new XmlContentReader();
      var blob = branch.Commits.Last().Tree.First().Target as Blob;
      if (blob != null)
      {
        var content = new StreamReader(blob.GetContentStream(), Encoding.Default);
        var certId = xmlReader.ReadOutCertificateId(content);
        var tagCertIdName = certId.GetTagGitPathName();
        var tagMsg = new PublicBranchNaming(branch.FriendlyName).GetJsonRepesentation();
        var certTag = new CreateTagCommand(repository, tagCertIdName, tagMsg,
          branch.Commits.Last().Sha);
      }
    }

    private void SetBranchDataInfo()
    {
      branchInformation = new Dictionary<string, List<string>>
      {
        { PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
          new List<string>{ "vecto_engine-sample.xml" } },
        { PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a",
          new List<string>{ "vecto_gearbox-sample.xml", "VectoInputDeclaration.1.0.pdf" } },
        { PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
          new List<string>{ "Demo_FullLoad_Parent_v1.4.csv", "Demo_FullLoad_Child_v1.4.csv", "Demo_Map_v1.4.csv" } },
        { PrivateBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t",
          new List<string>{ "Demo_Motoring_v1.4.csv" } }
      };
    }



  }
}