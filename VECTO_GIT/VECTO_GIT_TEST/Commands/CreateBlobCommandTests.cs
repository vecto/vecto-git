﻿using NUnit.Framework;
using System;
using System.Linq;
using System.IO;
using System.Text;
using VECTO_GIT_TEST;
using LibGit2Sharp;

namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("CreateBlobCommand")]
  public class CreateBlobCommandTests
  {
    private Repository repository;
    private string fileName;

    [OneTimeSetUp()]
    public void OneTimeInit()
    {      
      fileName =  "Demo_FullLoad_Child_v1.4.csv";
    }
    

    [SetUp()]
    public void Init()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
    }

    [TearDown()]
    public void CleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [Test()]
    public void CreateBlobCommandConstructorTest()
    {
      var filePath = Path.Combine(TestHelper.DATA_PATH, fileName);

      Assert.Throws<ArgumentNullException>(() => new CreateBlobCommand(null, TestHelper.DATA_PATH));
      Assert.Throws<ArgumentNullException>(() => new CreateBlobCommand(repository,(Stream) null));
      Assert.Throws<ArgumentNullException>(() => new CreateBlobCommand(repository,(string) null));
      Assert.Throws<ArgumentException>(() => new CreateBlobCommand(repository, ""));
      Assert.Throws<RepositoryNotFoundException>(() => new CreateBlobCommand(new Repository(@"/wrong/repos"), TestHelper.DATA_PATH));
      Assert.Throws<ArgumentException>(() => new CreateBlobCommand(repository, TestHelper.DATA_PATH));
      Assert.DoesNotThrow(() => new CreateBlobCommand(repository, filePath));
    }

    [Test()]
    public void CreateBlobCommandResultTest()
    {
      var blob = TestHelper.GetExampleBlob(repository, fileName);
      var fileHash = TestHelper.GetGitHashOfData(fileName, blob.IsBinary);


      Assert.AreEqual(1, repository.ObjectDatabase.Count());
      Assert.AreEqual(typeof(Blob), blob.GetType());
      Assert.AreEqual(fileHash, blob.Sha);
    }

    [Test()]
    public void CreateBlobByStreamTest()
    {
      var blob = TestHelper.GetExampleBlobByStream(repository, fileName);
      var fileHash = TestHelper.GetGitHashOfData(fileName, true);

      Assert.AreEqual(typeof(Blob), blob.GetType());
      Assert.AreEqual(fileHash, blob.Sha);
    }

  }
}