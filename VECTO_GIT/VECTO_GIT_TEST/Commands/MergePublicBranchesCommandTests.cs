﻿using LibGit2Sharp;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT_TEST;

namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  public class MergePublicBranchesCommandTests
  {
    private ComponentDataManagement compDataManagement;
    private Repository localRepository;
    private Repository remoteRepository;
    private Remote currentRemote;
    private XmlContentReader xmlReader;
    private List<Branch> localBranches;
    public List<Branch> remoteBranches;

    [OneTimeSetUp()]
    public void OneTimeInit()
    {
      SetSourceRepository();
      SetDestinationRepository();
      SetupLocalBranches();
      SetupRemoteBranches();
      AddRemoteRepository(TestHelper.DES_REPO_PATH);
      FetchRemoteSpecs();
      xmlReader = new XmlContentReader();
    }

    [OneTimeTearDown()]
    public void OneTimeCleanUp()
    {
      RemoveRemoteRepository();
      remoteRepository.Dispose();
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);
    }

    [TearDown]
    public void Init()
    {
      AddRemoteRepository(TestHelper.DES_REPO_PATH);
      FetchRemoteSpecs();
    }

    private void SetDestinationRepository()
    {
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);
      remoteRepository = TestHelper.InitEmptyRepository(RepositoryType.Destination, true);
    }

    private void SetSourceRepository()
    {
      compDataManagement = new ComponentDataManagement();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      localRepository = compDataManagement.CurrentRepository;
    }

    [Test(), Order(1)]
    public void ConstructorTest()
    {
      var mergePubBranch = new MergePublicBranchesCommand(localRepository, null, null, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();
      var messages = mergePubBranch.GetMergeResultMessages();

      Assert.Throws<ArgumentNullException>(() => new MergePublicBranchesCommand(null, null, null, TestHelper.DES_REPO_PATH));
      Assert.Throws<ArgumentNullException>(() => new MergePublicBranchesCommand(localRepository, null, null, null));
      Assert.Throws<ArgumentNullException>(() => new MergePublicBranchesCommand(localRepository, null, null, null));
      Assert.DoesNotThrow(() => new MergePublicBranchesCommand(localRepository, null, null, TestHelper.DES_REPO_PATH));
      Assert.IsNotNull(mergePubBranch);
      Assert.IsNotNull(messages);
    }

    [Test(), Order(2)]
    public void MergeComponentDataCommandTest()
    {
      var localBranch = localRepository.Branches[localBranches[0].FriendlyName];
      var remoteBranch = remoteRepository.Branches[remoteBranches[0].FriendlyName];
      var localBranchName = new List<string> { localBranches[0].FriendlyName };
      var remoteBranchName = new List<string> { remoteBranches[0].FriendlyName };
      var tagsCount = localRepository.Tags.Count();

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, localBranchName, remoteBranchName, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();

      var newLocalBranch = localRepository.Branches[remoteBranch.FriendlyName];
      var stdBlob = StandardDataCommit.GetStandardDataBlob(newLocalBranch);
      var cmpBlob = ComponentDataCommit.GetComponentDataBlob(newLocalBranch);

      RemoveRemoteRepository();

      Assert.AreEqual(localBranches.Count, localRepository.Branches.Count());

      Assert.IsNotNull(localBranch);
      Assert.AreEqual(1, localBranch.Commits.Count());
      Assert.IsTrue(StandardDataCommit.IsStandardValuesBranch(localBranch));
      Assert.IsNotNull(remoteBranch);
      Assert.AreEqual(2, remoteBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(remoteBranch));

      Assert.IsNull(localRepository.Branches[localBranchName[0]]);

      Assert.IsNotNull(newLocalBranch);
      Assert.AreEqual(2, newLocalBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(newLocalBranch));

      Assert.IsNotNull(stdBlob);
      Assert.IsNotNull(cmpBlob);
      Assert.AreEqual(StandardDataCommit.GetStandardDataBlob(localBranch).Sha, stdBlob.Sha);
      Assert.AreEqual(ComponentDataCommit.GetComponentDataBlob(remoteBranch).Sha, cmpBlob.Sha);
      Assert.AreNotEqual(stdBlob.Sha, cmpBlob.Sha);
      Assert.AreEqual(tagsCount + 2, localRepository.Tags.Count());
      Assert.IsTrue(ContainsExpectedTags(newLocalBranch));

    }

    [Test, Order(3)]
    public void MergeStdAndCertDataCommandTest()
    {
      var localBranch = localRepository.Branches[localBranches[1].FriendlyName];
      var remoteBranch = remoteRepository.Branches[remoteBranches[1].FriendlyName];
      var localBranchName = new List<string> { localBranches[1].FriendlyName };
      var remoteBranchName = new List<string> { remoteBranches[1].FriendlyName };
      var tagsCount = localRepository.Tags.Count();

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, localBranchName, remoteBranchName, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();

      var newLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      var stdBlob = StandardDataCommit.GetStandardDataBlob(newLocalBranch);
      var cmpBlob = ComponentDataCommit.GetComponentDataBlob(newLocalBranch);
      var certBlob = CertificateDataCommit.GetCertificateBlob(newLocalBranch);

      RemoveRemoteRepository();

      Assert.AreEqual(localBranches.Count, localRepository.Branches.Count());

      Assert.IsNotNull(localBranch);
      Assert.AreEqual(1, localBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(localBranch));
      Assert.IsNotNull(remoteBranch);
      Assert.AreEqual(3, remoteBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(remoteBranch));

      Assert.AreEqual(localBranch.FriendlyName, newLocalBranch.FriendlyName);

      Assert.IsNotNull(newLocalBranch);
      Assert.AreEqual(3, newLocalBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(newLocalBranch));

      Assert.IsNotNull(stdBlob);
      Assert.IsNotNull(cmpBlob);
      Assert.IsNotNull(certBlob);
      Assert.AreEqual(StandardDataCommit.GetStandardDataBlob(remoteBranch).Sha, stdBlob.Sha);
      Assert.AreEqual(ComponentDataCommit.GetComponentDataBlob(localBranch).Sha, cmpBlob.Sha);
      Assert.AreEqual(CertificateDataCommit.GetCertificateBlob(remoteBranch).Sha, certBlob.Sha);

      Assert.AreNotEqual(stdBlob.Sha, cmpBlob.Sha);
      Assert.AreEqual(tagsCount, localRepository.Tags.Count());
      Assert.IsTrue(ContainsExpectedTags(newLocalBranch));
    }

    [Test, Order(4)]
    public void MergeCertDataCommandTest()
    {
      var localBranch = localRepository.Branches[localBranches[2].FriendlyName];
      var remoteBranch = remoteRepository.Branches[remoteBranches[2].FriendlyName];
      var localBranchName = new List<string> { localBranches[2].FriendlyName };
      var remoteBranchName = new List<string> { remoteBranches[2].FriendlyName };
      var tagsCount = localRepository.Tags.Count();

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, localBranchName, remoteBranchName, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();

      var newLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      var stdBlob = StandardDataCommit.GetStandardDataBlob(newLocalBranch);
      var cmpBlob = ComponentDataCommit.GetComponentDataBlob(newLocalBranch);
      var certBlob = CertificateDataCommit.GetCertificateBlob(newLocalBranch);

      RemoveRemoteRepository();

      Assert.AreEqual(localBranches.Count, localRepository.Branches.Count());

      Assert.IsNotNull(localBranch);
      Assert.AreEqual(2, localBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(localBranch));
      Assert.IsNotNull(remoteBranch);
      Assert.AreEqual(2, remoteBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(remoteBranch));

      Assert.AreEqual(localBranch.FriendlyName, newLocalBranch.FriendlyName);

      Assert.IsNotNull(newLocalBranch);
      Assert.AreEqual(3, newLocalBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(newLocalBranch));

      Assert.IsNotNull(stdBlob);
      Assert.IsNotNull(cmpBlob);
      Assert.IsNotNull(certBlob);
      Assert.AreEqual(StandardDataCommit.GetStandardDataBlob(localBranch).Sha, stdBlob.Sha);
      Assert.AreEqual(ComponentDataCommit.GetComponentDataBlob(localBranch).Sha, cmpBlob.Sha);
      Assert.AreEqual(CertificateDataCommit.GetCertificateBlob(remoteBranch).Sha, certBlob.Sha);

      Assert.AreNotEqual(stdBlob.Sha, cmpBlob.Sha);
      Assert.AreEqual(tagsCount, localRepository.Tags.Count());
      Assert.IsTrue(ContainsExpectedTags(newLocalBranch));

    }

    [Test, Order(5)]
    public void AppendStdDataCommandTest()
    {
      var localBranch = localRepository.Branches[localBranches[3].FriendlyName];
      var remoteBranch = remoteRepository.Branches[remoteBranches[3].FriendlyName];
      var localBranchName = new List<string> { localBranches[3].FriendlyName };
      var remoteBranchName = new List<string> { remoteBranches[3].FriendlyName };
      var tagsCount = localRepository.Tags.Count();

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, localBranchName, remoteBranchName, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();

      var newLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      var stdBlob = StandardDataCommit.GetStandardDataBlob(newLocalBranch);
      var cmpBlob = ComponentDataCommit.GetComponentDataBlob(newLocalBranch);
      var certBlob = CertificateDataCommit.GetCertificateBlob(newLocalBranch);

      RemoveRemoteRepository();

      Assert.AreEqual(localBranches.Count, localRepository.Branches.Count());

      Assert.IsNotNull(localBranch);
      Assert.AreEqual(2, localBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(localBranch));
      Assert.IsNotNull(remoteBranch);
      Assert.AreEqual(1, remoteBranch.Commits.Count());
      Assert.IsTrue(StandardDataCommit.IsStandardValuesBranch(remoteBranch));

      Assert.AreEqual(localBranch.FriendlyName, newLocalBranch.FriendlyName);

      Assert.IsNotNull(newLocalBranch);
      Assert.AreEqual(3, newLocalBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(newLocalBranch));

      Assert.IsNotNull(stdBlob);
      Assert.IsNotNull(cmpBlob);
      Assert.IsNotNull(certBlob);
      Assert.AreEqual(StandardDataCommit.GetStandardDataBlob(remoteBranch).Sha, stdBlob.Sha);
      Assert.AreEqual(ComponentDataCommit.GetComponentDataBlob(localBranch).Sha, cmpBlob.Sha);
      Assert.AreEqual(CertificateDataCommit.GetCertificateBlob(localBranch).Sha, certBlob.Sha);

      Assert.AreNotEqual(stdBlob.Sha, cmpBlob.Sha);
      Assert.AreEqual(tagsCount, localRepository.Tags.Count());
      Assert.IsTrue(ContainsExpectedTags(newLocalBranch));
    }

    [Test, Order(6)]
    public void NothingToMergeCommitTest()
    {
      var localBranch = localRepository.Branches[localBranches[4].FriendlyName];
      var remoteBranch = remoteRepository.Branches[remoteBranches[4].FriendlyName];
      var localBranchName = new List<string> { localBranches[4].FriendlyName };
      var remoteBranchName = new List<string> { remoteBranches[4].FriendlyName };
      var tagsCount = localRepository.Tags.Count();

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, localBranchName, remoteBranchName, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();

      var newLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      var stdBlob = StandardDataCommit.GetStandardDataBlob(newLocalBranch);
      var cmpBlob = ComponentDataCommit.GetComponentDataBlob(newLocalBranch);
      var certBlob = CertificateDataCommit.GetCertificateBlob(newLocalBranch);

      RemoveRemoteRepository();

      Assert.AreEqual(localBranches.Count, localRepository.Branches.Count());

      Assert.IsNotNull(localBranch);
      Assert.AreEqual(3, localBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(localBranch));
      Assert.IsNotNull(remoteBranch);
      Assert.AreEqual(3, remoteBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(remoteBranch));

      Assert.AreEqual(localBranch.FriendlyName, newLocalBranch.FriendlyName);

      Assert.IsNotNull(newLocalBranch);
      Assert.AreEqual(3, newLocalBranch.Commits.Count());
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(newLocalBranch));

      Assert.IsNotNull(stdBlob);
      Assert.IsNotNull(cmpBlob);
      Assert.IsNotNull(certBlob);
      Assert.AreEqual(StandardDataCommit.GetStandardDataBlob(localBranch).Sha, stdBlob.Sha);
      Assert.AreEqual(ComponentDataCommit.GetComponentDataBlob(localBranch).Sha, cmpBlob.Sha);
      Assert.AreEqual(CertificateDataCommit.GetCertificateBlob(localBranch).Sha, certBlob.Sha);

      Assert.AreNotEqual(stdBlob.Sha, cmpBlob.Sha);
      Assert.AreEqual(tagsCount, localRepository.Tags.Count());
      Assert.IsTrue(ContainsExpectedTags(newLocalBranch));
    }

    [Test, Order(7)]
    public void PullAllBranchesFromRemoteTest()
    {
      RemoveRemoteRepository();
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      SetSourceRepository();

      var branchCounter = localRepository.Branches.Count();
      var tagCounter = localRepository.Tags.Count();

      Init();

      var remoteBranchNames = new List<string> { remoteBranches[4].FriendlyName,
        remoteBranches[0].FriendlyName , remoteBranches[2].FriendlyName };

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, null, remoteBranchNames, TestHelper.DES_REPO_PATH);
      mergePubBranch.ExecutePublicMerge();

      RemoveRemoteRepository();

      Assert.IsNotNull(mergePubBranch);
      Assert.AreEqual(0, branchCounter);
      Assert.AreEqual(0, tagCounter);
      Assert.AreEqual(3, localRepository.Branches.Count());
      Assert.AreEqual(8, localRepository.Tags.Count()); //2 tags more than happend only in test case
    }


    [Test, Order(8)]
    public void PartlyMergeBranchesFromRemoteTest()
    {
      RemoveRemoteRepository();
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      SetSourceRepository();
      Init();

      var cmpFilePath02 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml");
      var cmpBranch02 = TestHelper.SaveAsComponentData(localRepository, null, cmpFilePath02);

      var remoteBranchNames = new List<string> { remoteBranches[3].FriendlyName,
        remoteBranches[1].FriendlyName, remoteBranches[4].FriendlyName};

      var mergePubBranch =
        new MergePublicBranchesCommand(localRepository, new List<string>() {cmpBranch02.FriendlyName}, 
          remoteBranchNames, TestHelper.DES_REPO_PATH);

      mergePubBranch.ExecutePublicMerge();

      RemoveRemoteRepository();


      Assert.AreEqual(3, localRepository.Branches.Count());
      Assert.AreEqual(8, localRepository.Tags.Count());
    }


    private bool ContainsExpectedTags(Branch branch)
    {
      var compCommit = ComponentDataCommit.GetComponentDataCommit(branch);
      var tags = localRepository.Tags.Where(elem => elem.Target.Sha == compCommit.Sha).ToList();

      if (tags.Count != 2)
        return false;

      var compBlob = ComponentDataCommit.GetComponentDataBlob(compCommit);

      foreach (var tag in tags)
      {
        var originBranchName = xmlReader.ReadOutBranchNameData(compBlob.GetContentStream());

        if (tag.FriendlyName.StartsWith(ConfigValue.CERT_NR_FOLDER_NAME))
        {
          var originCertNumberHandler = xmlReader.ReadOutCertificateId(compBlob.GetContentStream());
          var certNumberHandler = new CertificateNumberHandler(tag.FriendlyName, true);

          if (originCertNumberHandler.GetTagGitPathName() != tag.FriendlyName ||
              tag.FriendlyName != certNumberHandler.GetTagGitPathName())
            return false;
        }
        else if (tag.FriendlyName.StartsWith(ConfigValue.GIT_ID_FOLDER_NAME))
        {
          var originGitNumberHandler = new GitIdHandler(branch);
          var gitNumberHandler = new GitIdHandler(tag.FriendlyName, true);

          if (originGitNumberHandler.GetTagGitPathName() != tag.FriendlyName ||
              gitNumberHandler.GetTagGitPathName() != tag.FriendlyName)
            return false;
        }

        var branchNaming = PublicBranchNaming.DeserializeBranchName(tag.Annotation.Message);
        if (branchNaming.GetGitPathBranchName() != branch.FriendlyName ||
            originBranchName.GetGitPathBranchName() != branch.FriendlyName)
          return false;
      }


      return true;
    }

    #region Setup Test Branches

    private void SetupLocalBranches()
    {
      localBranches = new List<Branch>();

      var stdFilePath01 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_std.xml");
      var stdBranch01 = TestHelper.SaveStandardValueFile(localRepository, stdFilePath01);
      localBranches.Add(stdBranch01);

      var cmpFilePath02 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml");
      var cmpBranch02 = TestHelper.SaveAsComponentData(localRepository, null, cmpFilePath02);
      localBranches.Add(cmpBranch02);

      var stdFilePath03 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_std.xml");
      var cmpFilePath03 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml");
      var cmpBranch03 = TestHelper.SaveAsComponentData(localRepository, stdFilePath03, cmpFilePath03);
      localBranches.Add(cmpBranch03);

      var cmpFilePath04 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample04_cmp.xml");
      var cmpBranch04 = TestHelper.SaveAsComponentData(localRepository, null, cmpFilePath04);
      var certFilePath04 = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var certBranch04 = TestHelper.SaveCertificateByComponentFile(localRepository, certFilePath04, cmpFilePath04);
      localBranches.Add(certBranch04);

      var stdFilePath05 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_std.xml");
      var cmpFilePath05 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_cmp.xml");
      var certFilePath05 = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var cmpBranch05 = TestHelper.SaveAsComponentData(localRepository, stdFilePath05, cmpFilePath05);
      var certBranch05 = TestHelper.SaveCertificateByComponentFile(localRepository, certFilePath05, cmpFilePath05);
      localBranches.Add(certBranch05);
    }

    private void SetupRemoteBranches()
    {
      remoteBranches = new List<Branch>();

      var stdFilePath01 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_std.xml");
      var cmpFilePath01 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_cmp.xml");
      var cmpBranch01 = TestHelper.SaveAsComponentData(remoteRepository, stdFilePath01, cmpFilePath01);
      remoteBranches.Add(cmpBranch01);

      var stdFilePath02 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_std.xml");
      var cmpFilePath02 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml");
      var certFilePath02 = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var cmpBranch02 = TestHelper.SaveAsComponentData(remoteRepository, stdFilePath02, cmpFilePath02);
      var certBranch02 = TestHelper.SaveCertificateByComponentFile(remoteRepository, certFilePath02, cmpFilePath02);
      remoteBranches.Add(certBranch02);

      var cmpFilePath03 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml");
      var certFilePath03 = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var cmpBranch03 = TestHelper.SaveAsComponentData(remoteRepository, null, cmpFilePath03);
      var certBranch03 = TestHelper.SaveCertificateByComponentFile(remoteRepository, certFilePath03, cmpFilePath03);
      remoteBranches.Add(certBranch03);

      var stdFilePath04 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample04_std.xml");
      var stdBranch04 = TestHelper.SaveStandardValueFile(remoteRepository, stdFilePath04);
      remoteBranches.Add(stdBranch04);

      var stdFilePath05 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_std.xml");
      var cmpFilePath05 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_cmp.xml");
      var certFilePath05 = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var cmpBranch05 = TestHelper.SaveAsComponentData(remoteRepository, stdFilePath05, cmpFilePath05);
      var certBranch05 = TestHelper.SaveCertificateByComponentFile(remoteRepository, certFilePath05, cmpFilePath05);
      remoteBranches.Add(certBranch05);
    }

    #endregion

    #region Set Remote Repository

    private void RemoveRemoteRepository()
    {
      localRepository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
    }

    private void AddRemoteRepository(string remotePath)
    {
      localRepository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
      currentRemote = localRepository.Network.Remotes.Add(ConfigValue.REMOTE_NAME, remotePath);
    }

    private void FetchRemoteSpecs()
    {
      var fetchOptions = new FetchOptions
      {
        TagFetchMode = TagFetchMode.None
      };

      var logMessage = string.Empty;
      var refSpecs = currentRemote.FetchRefSpecs.Select(x => x.Specification);
      LibGit2Sharp.Commands.Fetch(localRepository, ConfigValue.REMOTE_NAME, refSpecs, fetchOptions, logMessage);
    }

    #endregion

  }
}