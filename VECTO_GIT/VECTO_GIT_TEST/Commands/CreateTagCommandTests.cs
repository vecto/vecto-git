﻿using NUnit.Framework;
using VECTO_GIT.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using VECTO_GIT_TEST;
using LibGit2Sharp;


namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("CreateTagCommand")]
  public class CreateTagCommandTests
  {
    private Repository repository;
    private Commit testCommit;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      var fileName = "Demo_FullLoad_Child_v1.4.csv";

      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      testCommit = TestHelper.GetExampleCommit(repository, fileName, CommitDataType.TestData);
    }


    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [Test()]
    public void CreateTagCommandConstructorTest()
    {
      var tagName = "TagName/test";
      var tagMessage = "test message";
      var commitId = "9275da3dbb4a910179754108427d82eb61b74546";

      Assert.Throws<ArgumentNullException>(() => new CreateTagCommand(repository, tagName, tagMessage, null));
      Assert.Throws<ArgumentNullException>(() => new CreateTagCommand(repository, tagName, null, commitId));
      Assert.Throws<ArgumentNullException>(() => new CreateTagCommand(repository, null, tagMessage, commitId));
      Assert.Throws<ArgumentNullException>(() => new CreateTagCommand(null, tagName, tagMessage, commitId));
      Assert.Throws<ArgumentException>(() => new CreateTagCommand(repository, "", tagMessage, commitId));
      Assert.Throws<ArgumentException>(() => new CreateTagCommand(repository, tagName, "", commitId));
      Assert.Throws<ArgumentException>(() => new CreateTagCommand(repository, tagName, tagMessage, ""));
      Assert.Throws<ArgumentException>(() => new CreateTagCommand(repository, tagName, tagMessage, commitId.Substring(1)));
    }

    [Test()]
    public void CreateTagCommandResultTest()
    {
      var tagName01 = "TagName/test01";
      var tagName02 = "TagName/test02";
      var tagMessage = "test message";
      var commitId = testCommit.Sha;
      var branchName = "Test/Branch/Name";

      var command = new CreateOrphanBranchCommand(repository, branchName, testCommit);
      var tagCommand01 = new CreateTagCommand(repository, tagName01, tagMessage, commitId);
      var tag01 = tagCommand01.Result();
      var tagCommand02 = new CreateTagCommand(repository, tagName02, tagMessage, commitId);
      var tag02 = tagCommand02.Result();

      Assert.AreEqual(2, repository.Tags.Count());
      Assert.AreEqual(testCommit.Sha, repository.Tags[tagName01].Target.Sha);
      Assert.AreEqual(testCommit.Sha, repository.Tags[tagName02].Target.Sha);
      Assert.AreEqual(tagMessage, repository.Tags[tagName01].Annotation.Message.Replace("\n", ""));
      Assert.AreEqual(tagMessage, repository.Tags[tagName02].Annotation.Message.Replace("\n", ""));
      
      tagCommand01 = new CreateTagCommand(repository, tagName01, tagMessage, commitId);
      tag01 = tagCommand01.Result();
      Assert.AreEqual(testCommit.Sha, repository.Tags[tagName01].Target.Sha);
      Assert.AreEqual(tagMessage, repository.Tags[tagName01].Annotation.Message.Replace("\n", ""));
    }
  }
}