﻿using NUnit.Framework;
using VECTO_GIT.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using VECTO_GIT_TEST;
using LibGit2Sharp;


namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("CreateTreeCommand")]
  public class CreateTreeCommandTests
  {
    private Repository repository;
    private string fstFileName;
    private string secFileName;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      fstFileName = "Demo_FullLoad_Parent_v1.4.csv";
      secFileName = "Demo_FullLoad_Child_v1.4.csv";
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
    }


    [SetUp]
    public void Init()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
    }

    [TearDown]
    public void CleanUp()
    {
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [Test()]
    public void CreateTreeCommandConstructorTest01()
    {
      var blob = TestHelper.GetExampleBlob(repository, fstFileName);

      var testDict = new Dictionary<string, Blob>
      {
        {fstFileName, blob }
      };

      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommand(repository, null));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommand(null, testDict));
      Assert.Throws<ArgumentException>(() => new CreateTreeCommand(repository, new Dictionary<string, Blob>()));
      Assert.DoesNotThrow(() => new CreateTreeCommand(repository, testDict));
    }

    [Test()]
    public void CreateTreeCommandConstructorTest02()
    {
      var blob = TestHelper.GetExampleBlob(repository, fstFileName);
      var testCommit = TestHelper.GetExampleCommit(repository, fstFileName, CommitDataType.TestData);
      var testDict = new Dictionary<string, Blob>
      {
        {fstFileName, blob }
      };

      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommand(null, testCommit, testDict));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommand(repository, testCommit, null));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommand(repository, null, testDict));
      Assert.Throws<ArgumentException>(() => new CreateTreeCommand(repository, new Dictionary<string, Blob>()));
      Assert.DoesNotThrow(() => new CreateTreeCommand(repository, testCommit, testDict));
    }

    [Test()]
    public void CreateTreeCommandResultTest()
    {
      //First Constructor
      var fstBlob = TestHelper.GetExampleBlob(repository, fstFileName);
      var fstTree = TestHelper.GetExampleTree(repository, fstFileName, CommitDataType.TestData);

      Assert.AreEqual(typeof(Tree), fstTree.GetType());
      Assert.AreEqual(1, fstTree.Count);
      Assert.AreEqual(TestHelper.GetGitHashOfData(fstFileName, fstBlob.IsBinary), fstTree.First().Target.Sha);
      Assert.AreEqual(fstFileName, fstTree.First().Name);
      Assert.AreEqual(2, repository.ObjectDatabase.Count());


      //Second Constructor
      var testCommit = new CreateTreeCommitCommand(repository, fstTree, "commit message").Result();
      var secBlob = TestHelper.GetExampleBlob(repository, secFileName);
      var secTree = TestHelper.GetExampleTree(repository, secFileName, testCommit, CommitDataType.TestData);

      Assert.AreEqual(typeof(Tree), secTree.GetType());
      Assert.AreEqual(2, secTree.Count);
      Assert.AreEqual(TestHelper.GetGitHashOfData(secFileName, secBlob.IsBinary), secTree.First().Target.Sha);
      Assert.AreEqual(TestHelper.GetGitHashOfData(fstFileName, fstBlob.IsBinary), secTree.Last().Target.Sha);
      Assert.AreEqual(secFileName, secTree.First().Name);
      Assert.AreEqual(fstFileName, secTree.Last().Name);
      Assert.AreEqual(5, repository.ObjectDatabase.Count());
    }
  }
}