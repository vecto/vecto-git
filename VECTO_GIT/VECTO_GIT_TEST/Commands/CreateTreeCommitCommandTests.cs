﻿using NUnit.Framework;
using VECTO_GIT.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using VECTO_GIT_TEST;
using LibGit2Sharp;

namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("CreateTreeCommitCommand")]
  public class CreateTreeCommitCommandTests
  {
    private Repository repository;
    private string fstFileName;
    private string secFileName;
    private string branchName;


    [OneTimeSetUp]
    public void OneTimeInit()
    {
      fstFileName = "Demo_FullLoad_Parent_v1.4.csv";
      secFileName = "Demo_Motoring_v1.4.csv";
      branchName = "test/branch";
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
    }


    [SetUp]
    public void Init()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
    }

    [TearDown]
    public void Cleanup()
    {
      TestHelper.DeleteRepository(RepositoryType.Source);
    }


    [Test()]
    public void CreateTreeCommitConstructorTest01()
    {
      var tree = TestHelper.GetExampleTree(repository, fstFileName, CommitDataType.TestData);

      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(null, tree, ""));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(repository, null, ""));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(repository, tree, null));
      Assert.DoesNotThrow(() => new CreateTreeCommitCommand(repository, tree, ""));
    }

    [Test()]
    public void CreateTreeCommitConstructorTest02()
    {
      var branch = TestHelper.GetExampleBranch(repository, fstFileName, branchName, CommitDataType.TestData);
      var commit = TestHelper.GetExampleCommit(repository, secFileName, CommitDataType.TestData);

      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(null, commit.Tree, "commit message", branch.Reference, branch.Tip));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(repository, null, "commit message", branch.Reference, branch.Tip));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(repository, commit.Tree, null, branch.Reference, branch.Tip));
      Assert.Throws<ArgumentNullException>(() => new CreateTreeCommitCommand(repository, commit.Tree, "commit message", branch.Reference, null));
      Assert.DoesNotThrow(() => new CreateTreeCommitCommand(repository, commit.Tree, "commit message", branch.Reference, branch.Tip));
    }

    [Test()]
    public void CreateTreeCommitResultTest()
    {
      var branch = TestHelper.GetExampleBranch(repository, fstFileName, branchName, CommitDataType.TestData);
      var tree = TestHelper.GetExampleTree(repository, secFileName, branch.Tip, CommitDataType.TestData);
      var commit = new CreateTreeCommitCommand(repository, tree, "commit message", branch.Reference, branch.Tip).Result();
      var branchAfter = repository.Branches[branchName];
      
      var fstBlob = repository.Lookup(commit.Tree.First().Target.Sha, ObjectType.Blob) as Blob;
      var secBlob = repository.Lookup(commit.Tree.Last().Target.Sha, ObjectType.Blob) as Blob;

      Assert.AreEqual(typeof(Commit), commit.GetType());
      Assert.AreEqual(2, commit.Tree.Count);
      Assert.AreEqual(TestHelper.GetGitHashOfData(secFileName, secBlob.IsBinary), secBlob.Sha);
      Assert.AreEqual(TestHelper.GetGitHashOfData(fstFileName, fstBlob.IsBinary), fstBlob.Sha);
      Assert.AreEqual(2, branchAfter.Commits.Count());
      Assert.AreEqual(commit.Sha, branchAfter.Tip.Sha);
      Assert.AreEqual(secFileName, branchAfter.Tip.Tree.Last().Name);
      Assert.AreEqual(fstFileName, branchAfter.Tip.Tree.First().Name);
      Assert.AreEqual(6, repository.ObjectDatabase.Count());
    }
  }
}