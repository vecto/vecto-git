﻿using NUnit.Framework;
using VECTO_GIT.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT_TEST;
using LibGit2Sharp;
using System.IO;



namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("PushBranchCommand")]
  public class PushBranchCommandTests
  {
    private Repository srcRepository;
    private string transferRepoPath;
    private Repository transRepository;
    private Dictionary<string, List<string>> branchInformation;


    [OneTimeSetUp]
    public void OneTimeInit()
    {
      srcRepository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      
      SetBranchDataInfo();
      SetUpTestBranches();
      AddTagsToBranches();
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      srcRepository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [SetUp]
    public void Init()
    {
      var repoFolerName = string.Format("{0}_{1}", ConfigValue.TRANSFER_FOLDER_NAME,
        Guid.NewGuid());
      transferRepoPath = Path.Combine(Helpers.AppFolderPath, repoFolerName);
      Directory.CreateDirectory(transferRepoPath);
    }


    [Test()]
    public void PushBranchCommandConstructorTest01()
    {
      var branchName = PublicBranchNaming.TREE_NAME + "/test/branch/01";

      Assert.Throws<ArgumentNullException>(() => new PushBranchCommand(null, transferRepoPath, branchName));
      Assert.Throws<ArgumentNullException>(() => new PushBranchCommand(srcRepository, transferRepoPath, (string)null));
      Assert.Throws<ArgumentNullException>(() => new PushBranchCommand(srcRepository, null, branchName));

      Assert.Throws<ArgumentException>(() => new PushBranchCommand(srcRepository, transferRepoPath, ""));
      Assert.Throws<ArgumentException>(() => new PushBranchCommand(srcRepository, "wrong/folder/name", branchName));

      Assert.Throws<RepositoryNotFoundException>(() => new PushBranchCommand(new Repository(@"/wrong/repos"), transferRepoPath, branchName));

      var pushCommand = new PushBranchCommand(srcRepository, transferRepoPath, branchName);

      var resultPath = pushCommand.Result();

      Assert.NotNull(resultPath);
      Assert.AreEqual(transferRepoPath +"\\", resultPath);
      Assert.IsTrue(Directory.Exists(resultPath));
      Assert.AreEqual(0, srcRepository.Network.Remotes.Count());

      Helpers.DeleteDirectory(resultPath);
      Assert.IsFalse(Directory.Exists(resultPath));
    }

    [Test()]
    public void PushBranchCommandConstructorTest02()
    {
      var branchNames = new List<string> { PublicBranchNaming.TREE_NAME + "/test/branch/01" };
      var emptybranchNames = new List<string> { "" };

      Assert.Throws<ArgumentNullException>(() => new PushBranchCommand(null, transferRepoPath, branchNames));
      Assert.Throws<ArgumentNullException>(() => new PushBranchCommand(srcRepository, transferRepoPath, (List<string>)null));
      Assert.Throws<ArgumentNullException>(() => new PushBranchCommand(srcRepository, null, branchNames));
      Assert.Throws<ArgumentException>(() => new PushBranchCommand(srcRepository, transferRepoPath, emptybranchNames));
      Assert.Throws<ArgumentException>(() => new PushBranchCommand(srcRepository, "wrong/folder/name", branchNames));

      Assert.Throws<RepositoryNotFoundException>(() => new PushBranchCommand(new Repository(@"/wrong/repos"), transferRepoPath, branchNames));

      var pushCommand = new PushBranchCommand(srcRepository, transferRepoPath, branchNames);

      var resultPath = pushCommand.Result();

      Assert.NotNull(resultPath);
      Assert.AreEqual(transferRepoPath + "\\", resultPath);
      Assert.IsTrue(Directory.Exists(transferRepoPath));
      Assert.AreEqual(0, srcRepository.Network.Remotes.Count());

      Helpers.DeleteDirectory(transferRepoPath);
      Assert.IsFalse(Directory.Exists(transferRepoPath));
    }

    [Test()]
    public void PushBranchResultTest01()
    {
      var branchName = new List<string> { PublicBranchNaming.TREE_NAME + "/test/branch/02" };
      var pushCommand = new PushBranchCommand(srcRepository, transferRepoPath, branchName);  
      var resultPath = pushCommand.Result();
      var tagNames = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/test/branch/02/test01",
        PublicBranchNaming.TREE_NAME + "/test/branch/02/test02"
      };


      var branchToTransfer = srcRepository.Branches[branchName.First()];
      LoadTransferRepository();
      var transferedBranch = transRepository.Branches.First();
      var srcBranch = branchToTransfer.Commits.GetEnumerator();
      var transBranch = transferedBranch.Commits.GetEnumerator();

      Assert.IsNotNull(resultPath);
      Assert.IsEmpty(pushCommand.PushErrors);
      Assert.AreEqual(branchName.Count, transRepository.Branches.Count());
      Assert.AreEqual(branchName.First(), transferedBranch.FriendlyName);
      Assert.AreEqual(tagNames.Count, transRepository.Tags.Count());
      Assert.AreEqual(branchInformation[branchName.First()].Count, transferedBranch.Commits.Count());
      Assert.AreEqual(branchToTransfer, transferedBranch);


      while (srcBranch.MoveNext() && transBranch.MoveNext())
      {
        Assert.AreEqual(srcBranch.Current, transBranch.Current);
        Assert.AreEqual(srcBranch.Current.Sha, transBranch.Current.Sha);
        Assert.AreEqual(srcBranch.Current.Tree, transBranch.Current.Tree);
        Assert.AreEqual(srcBranch.Current.Tree.Count, transBranch.Current.Tree.Count);
      }

      foreach (var tagName in tagNames)
      {
        Assert.IsNotNull(transRepository.Tags[tagName]);
        Assert.AreEqual(srcRepository.Tags[tagName], transRepository.Tags[tagName]);
        Assert.AreEqual(srcRepository.Tags[tagName].Target.Sha, transRepository.Tags[tagName].Target.Sha);
        Assert.AreEqual(branchToTransfer.Commits.Last().Sha, transRepository.Tags[tagName].Target.Sha);
        Assert.AreEqual(srcRepository.Tags[tagName].Annotation.Message, transRepository.Tags[tagName].Annotation.Message);
      }

      transRepository.Dispose();
      transRepository = null;

      Assert.NotNull(transferRepoPath);
      Assert.AreEqual(transferRepoPath + "\\", resultPath);
      Assert.IsTrue(Directory.Exists(transferRepoPath));
      Assert.AreEqual(0, srcRepository.Network.Remotes.Count());

      Helpers.DeleteDirectory(transferRepoPath);
      Assert.IsFalse(Directory.Exists(transferRepoPath));
    }

    [Test()]
    public void PushBranchResultTest02()
    {
      var branchNames = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/test/branch/06",
        PublicBranchNaming.TREE_NAME + "/test/branch/01",
        PublicBranchNaming.TREE_NAME + "/test/branch/05"
      };
      var pushCommand = new PushBranchCommand(srcRepository, transferRepoPath, branchNames);
      var resultPath = pushCommand.Result();
      var tagNames = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/test/branch/06/test01",
        PublicBranchNaming.TREE_NAME + "/test/branch/06/test02",
        PublicBranchNaming.TREE_NAME + "/test/branch/01/test01",
        PublicBranchNaming.TREE_NAME + "/test/branch/01/test02",
        PublicBranchNaming.TREE_NAME + "/test/branch/05/test01",
        PublicBranchNaming.TREE_NAME + "/test/branch/05/test02"
      };

      LoadTransferRepository();

      Assert.IsNotNull(resultPath);
      Assert.IsEmpty(pushCommand.PushErrors);
      Assert.AreEqual(branchNames.Count, transRepository.Branches.Count());
      Assert.AreEqual(tagNames.Count, transRepository.Tags.Count());
      
      foreach (var branchName in branchNames)
      {
        var branchToTransfer = srcRepository.Branches[branchName];
        var transferedBranch = transRepository.Branches[branchName];
        var srcBranch = branchToTransfer.Commits.GetEnumerator();
        var transBranch = transferedBranch.Commits.GetEnumerator();

        Assert.AreEqual(branchToTransfer, transferedBranch);
        while (srcBranch.MoveNext() && transBranch.MoveNext())
        {
          Assert.AreEqual(srcBranch.Current, transBranch.Current);
          Assert.AreEqual(srcBranch.Current.Sha, transBranch.Current.Sha);
          Assert.AreEqual(srcBranch.Current.Tree, transBranch.Current.Tree);
          Assert.AreEqual(srcBranch.Current.Tree.Count, transBranch.Current.Tree.Count);
        }

        foreach (var tagName in tagNames)
        {
          Assert.IsNotNull(transRepository.Tags[tagName]);
          Assert.AreEqual(srcRepository.Tags[tagName], transRepository.Tags[tagName]);
          Assert.AreEqual(srcRepository.Tags[tagName].Target.Sha, transRepository.Tags[tagName].Target.Sha);
          Assert.AreEqual(srcRepository.Tags[tagName].Annotation.Message, transRepository.Tags[tagName].Annotation.Message);
          var transMessage = transRepository.Tags[tagName].Annotation.Message.Replace("\n", "");
          Assert.AreEqual(transRepository.Branches[transMessage].Commits.Last().Sha, transRepository.Tags[tagName].Target.Sha);
          var srcMessage = srcRepository.Tags[tagName].Annotation.Message.Replace("\n", ""); ;
          Assert.AreEqual(srcRepository.Branches[srcMessage].Commits.Last().Sha, srcRepository.Tags[tagName].Target.Sha);
        }
      }

      //to make delete possible
      transRepository.Dispose();
      transRepository = null;

      Assert.NotNull(transferRepoPath);
      Assert.AreEqual(transferRepoPath + "\\", resultPath);
      Assert.IsTrue(Directory.Exists(transferRepoPath));
      Assert.AreEqual(0, srcRepository.Network.Remotes.Count());

      Helpers.DeleteDirectory(transferRepoPath);
      Assert.IsFalse(Directory.Exists(transferRepoPath));
    }


    private void LoadTransferRepository()
    {
      if (transRepository == null)
      {
        transRepository = new Repository(transferRepoPath);
      }
    }


    private void SetUpTestBranches()
    {
      foreach (var branchdata in branchInformation)
      {
        if (branchdata.Value.Count > 1)
        {
          TestHelper.GetExampleBranch(srcRepository, branchdata.Value, branchdata.Key, CommitDataType.ComponentData);
        }
        else
        {
          TestHelper.GetExampleBranch(srcRepository, branchdata.Value.First(), branchdata.Key, CommitDataType.ComponentData);
        }
      }
    }

    private void AddTagsToBranches()
    {
      foreach (var branch in srcRepository.Branches)
      {
        var tagName = branch.CanonicalName.Replace("refs/heads/", "") + "/test01";
        var tagMessage = branch.FriendlyName;
        var tag = new CreateTagCommand(srcRepository, tagName, tagMessage, branch.Commits.Last().Sha);

        tagName = branch.CanonicalName.Replace("refs/heads/", "") + "/test02";
        tagMessage = branch.FriendlyName;
        tag = new CreateTagCommand(srcRepository, tagName, tagMessage, branch.Commits.Last().Sha);
      }
    }


    private void SetBranchDataInfo()
    {
      branchInformation = new Dictionary<string, List<string>>
      {
        { PublicBranchNaming.TREE_NAME + "/test/branch/01",
          new List<string>{ "Demo_FullLoad_Child_v1.4.csv" } },
        { PublicBranchNaming.TREE_NAME + "/test/branch/02",
          new List<string>{ "Demo_FullLoad_Parent_v1.4.csv" , "Demo_Map_v1.4.csv"} },
        { PublicBranchNaming.TREE_NAME + "/test/branch/03",
          new List<string>{ "Demo_Motoring_v1.4.csv" } },
        { PublicBranchNaming.TREE_NAME + "/test/branch/04",
          new List<string>{ "vecto_engine-sample.xml" } },
        { PublicBranchNaming.TREE_NAME + "/test/branch/05",
          new List<string>{ "vecto_gearbox-sample.xml" } },
        { PublicBranchNaming.TREE_NAME + "/test/branch/06",
          new List<string>{ "VectoComponent.xsd", "VectoDeclarationDefinitions.1.0.xsd" } }
      };
    }

  }
}