﻿using NUnit.Framework;
using VECTO_GIT.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT_TEST;
using LibGit2Sharp;
using System.IO;
using VECTO_GIT.ComponentNaming;


namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  [Category("SearchBranchCommand")]
  public class SearchBranchCommandTests
  {
    private Repository repository;
    private Dictionary<string, List<string>> branchInformation;


    [OneTimeSetUp]
    public void OneTimeInit()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);

      SetBranchDataInfo();
      SetUpTestBranches();
      AddTagsToBranches();
    }


    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }


    [Test()]
    public void SearchBranchCommandConstructorTest01()
    {
      var searchTerm = "search Term";

      Assert.Throws<ArgumentNullException>(() => new SearchBranchCommand(null, searchTerm));
      Assert.Throws<ArgumentNullException>(() => new SearchBranchCommand(repository, (string)null));
      Assert.Throws<ArgumentException>(() => new SearchBranchCommand(repository, ""));
      Assert.DoesNotThrow(() => new SearchBranchCommand(repository, searchTerm));
      Assert.NotNull(new SearchBranchCommand(repository, searchTerm).Result());
    }

    [Test()]
    public void SearchBranchCommandConstructorTest02()
    {
      var searchTerm = new string[] { "search Term" };

      Assert.Throws<ArgumentNullException>(() => new SearchBranchCommand(null, searchTerm));
      Assert.Throws<ArgumentNullException>(() => new SearchBranchCommand(repository, (string[])null));
      Assert.Throws<ArgumentException>(() => new SearchBranchCommand(repository, ""));
      Assert.DoesNotThrow(() => new SearchBranchCommand(repository, searchTerm));
      Assert.NotNull(new SearchBranchCommand(repository, searchTerm).Result());
    }

    [Test()]
    public void SearchBranchCommandConstructorTest03()
    {
      var branchNameParts = new PublicBranchNameParts
        ("GEM", "Generic_40t", "2017", "9275da3dbb4a910179754108427d82eb61b74546");
      var branchNameHandler = new PublicBranchNaming(branchNameParts);

      Assert.Throws<ArgumentNullException>(() => new SearchBranchCommand(null, branchNameHandler, SearchBy.BranchName));
      Assert.Throws<ArgumentNullException>(() => new SearchBranchCommand(repository, null, SearchBy.BranchName));
      Assert.DoesNotThrow(() => new SearchBranchCommand(repository, branchNameHandler, SearchBy.BranchName));
      Assert.NotNull(new SearchBranchCommand(repository, branchNameHandler, SearchBy.BranchName).Result());
    }

    [Test()]
    public void SearchBranchCommandResultTest01()
    {
      var searchTerm = "Generic_40t";
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
        PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t"
      };

      var searchCommand = new SearchBranchCommand(repository, searchTerm);
      var searchResult = searchCommand.Result();


      Assert.IsTrue(CompareExpectedAndSearchResult(expectedResult, searchResult));
    }

    [Test()]
    public void SearchBranchCommandResultTest02()
    {
      var searchTerms = new List<string>
      {
        "2017" , "Generic_40t"
      };
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t"
      };

      var searchCommand = new SearchBranchCommand(repository, searchTerms.ToArray());
      var searchResult = searchCommand.Result();

      Assert.IsTrue(CompareExpectedAndSearchResult(expectedResult, searchResult));
    }

    [Test()]
    public void SearchBranchCommandResultTest03()
    {
      var branchNameParts = new PublicBranchNameParts
        ("GEM", "Generic_40t", "2017", "9275da3dbb4a910179754108427d82eb61b74546");
      var branchNameHandler = new PublicBranchNaming(branchNameParts);

      Assert.IsTrue(SearchByModel(branchNameHandler));
      Assert.IsTrue(SearchByYear(branchNameHandler));
      Assert.IsTrue(SearchByManufacturer(branchNameHandler));
      Assert.IsTrue(SearchByBranchName(branchNameHandler));
      Assert.IsTrue(SearchByPrivateBranchName(branchNameHandler));
      Assert.IsTrue(SearchByComponentBranchName(branchNameHandler));
    }

    private bool SearchByModel(PublicBranchNaming branchNameHandler)
    {
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
        PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t"
      };

      var searchCommand = new SearchBranchCommand(repository, branchNameHandler, SearchBy.Model);
      var searchResult = searchCommand.Result();

      return CompareExpectedAndSearchResult(expectedResult, searchResult);
    }

    private bool SearchByYear(PublicBranchNaming branchNameHandler)
    {
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t"
      };

      var searchCommand = new SearchBranchCommand(repository, branchNameHandler, SearchBy.Year);
      var searchResult = searchCommand.Result();

      return CompareExpectedAndSearchResult(expectedResult, searchResult);
    }

    private bool SearchByManufacturer(PublicBranchNaming branchNameHandler)
    {
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
        PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t"
      };

      var searchCommand = new SearchBranchCommand(repository, branchNameHandler, SearchBy.Manufacturer);
      var searchResult = searchCommand.Result();

      return CompareExpectedAndSearchResult(expectedResult, searchResult);
    }

    private bool SearchByBranchName(PublicBranchNaming branchNameHandler)
    {
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",        
      };

      var searchCommand = new SearchBranchCommand(repository, branchNameHandler, SearchBy.BranchName);
      var searchResult = searchCommand.Result();

      return CompareExpectedAndSearchResult(expectedResult, searchResult);
    }

    private bool SearchByPrivateBranchName(PublicBranchNaming branchNameHandler)
    {
      var expectedResult = new List<string>
      {
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
      };

      var searchCommand = new SearchBranchCommand(repository, branchNameHandler, SearchBy.PrivateBranchName);
      var searchResult = searchCommand.Result();

      return CompareExpectedAndSearchResult(expectedResult, searchResult);
    }

    private bool SearchByComponentBranchName(PublicBranchNaming branchNameHandler)
    {
      var expectedResult = new List<string>
      {
        PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
        PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t"
      };

      var searchCommand = new SearchBranchCommand(repository, branchNameHandler, SearchBy.ComponentName);
      var searchResult = searchCommand.Result();

      return CompareExpectedAndSearchResult(expectedResult, searchResult);
    }

    private bool CompareExpectedAndSearchResult(List<string> expectedResult, IEnumerable<Branch> searchResult)
    {
      if (expectedResult.Count != searchResult.Count())
      {
        return false;
      }
      foreach (var branch in searchResult)
      {
        if (!expectedResult.Contains(branch.FriendlyName))
        {
          return false;
        }
      }
      return true;
    }



    private void SetUpTestBranches()
    {
      foreach (var branchdata in branchInformation)
      {
        if (branchdata.Value.Count > 1)
        {
          TestHelper.GetExampleBranch(repository, branchdata.Value, branchdata.Key, CommitDataType.ComponentData);
        }
        else
        {
          TestHelper.GetExampleBranch(repository, branchdata.Value.First(), branchdata.Key, CommitDataType.ComponentData);
        }
      }
    }

    private void AddTagsToBranches()
    {
      foreach (var branch in repository.Branches)
      {
        var tagName = branch.CanonicalName.Replace("refs/heads/", "") + "/test01";
        var tagMessage = branch.FriendlyName;
        var tag = new CreateTagCommand(repository, tagName, tagMessage, branch.Commits.Last().Sha);

        tagName = branch.CanonicalName.Replace("refs/heads/", "") + "/test02";
        tagMessage = branch.FriendlyName;
        tag = new CreateTagCommand(repository, tagName, tagMessage, branch.Commits.Last().Sha);
      }
    }


    private void SetBranchDataInfo()
    {
      branchInformation = new Dictionary<string, List<string>>
      {
        { PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
          new List<string>{ "Demo_FullLoad_Child_v1.4.csv" } },
        { PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a",
          new List<string>{ "Demo_FullLoad_Parent_v1.4.csv" , "Demo_Map_v1.4.csv"} },
        { PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
          new List<string>{"Demo_Motoring_v1.4.csv" } },
        { PrivateBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t",
          new List<string>{"vecto_engine-sample.xml" } }

      };
    }



  }
}