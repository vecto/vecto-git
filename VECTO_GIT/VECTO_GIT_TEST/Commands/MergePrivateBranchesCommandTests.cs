﻿using System;
using LibGit2Sharp;
using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT_TEST;

namespace VECTO_GIT.Commands.Tests
{
  [TestFixture()]
  public class MergePrivateBranchesCommandTests
  {

    private ComponentDataManagement compDataManagement;
    private Repository localRepository;
    private Repository remoteRepository;
    private Remote currentRemote;
    private XmlContentReader xmlReader;
    private List<string> uFiles;
    private List<string> mFiles;
    private List<string> uConflictFiles;
    private List<string> mConflictFiles;
    private List<PrivateBranchNaming> prvBranchNamings;
    private List<Branch> localBranches;
    private List<Branch> remoteBranches;


    [OneTimeSetUp()]
    public void OneTimeInit()
    {
      SetSourceRepository();
      SetDestinationRepository();
      xmlReader = new XmlContentReader();
      SetSavedFiles();
      SetupLocalBranches();
      SetupRemoteBranches();
      AddRemoteRepository(TestHelper.DES_REPO_PATH);
      FetchRemoteSpecs();
    }

    [OneTimeTearDown()]
    public void OneTimeCleanUp()
    {
      RemoveRemoteRepository();
      remoteRepository.Dispose();
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);
    }



    [TearDown]
    public void Init()
    {
      AddRemoteRepository(TestHelper.DES_REPO_PATH);
      FetchRemoteSpecs();
    }

    private void SetDestinationRepository()
    {
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);
      remoteRepository = TestHelper.InitEmptyRepository(RepositoryType.Destination, true);
    }

    private void SetSourceRepository()
    {
      compDataManagement = new ComponentDataManagement();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      localRepository = compDataManagement.CurrentRepository;
    }

    [Test()]
    public void MergePrivateConstructorTest()
    {
      Assert.Throws<ArgumentNullException>(() => new MergePrivateBranchesCommand(null));
      Assert.DoesNotThrow(() => new MergePrivateBranchesCommand(localRepository));
    }


    [Test(), Order(1)]
    public void MergePrivateStdAndMeasurementBranchTest()
    {
      var branchName = prvBranchNamings[0].GetGitPathBranchName();

      var localBranchName = new List<string> { branchName };
      var remoteBranchName = new List<string> { branchName };
      var localBranch = localBranches[0];
      var remoteBranch = remoteBranches[0];

      var mergePrvBranch = new MergePrivateBranchesCommand(localRepository);
      var mergeMessage = mergePrvBranch.GetMergeResultMessages();
      var mergeResult = mergePrvBranch.MergePrivateBranches(localBranchName, remoteBranchName);

      Assert.IsEmpty(mergeResult);
      Assert.AreEqual(2, localBranch.Commits.Count());
      Assert.AreEqual(2, remoteBranch.Commits.Count());
      Assert.IsNotEmpty(mergeMessage);
      Assert.AreEqual(1, mergeMessage.Count);
      Assert.IsTrue(mergeMessage.First().Value);
      Assert.AreEqual(0, localRepository.Tags.Count());
      Assert.AreEqual(0, remoteRepository.Tags.Count());

      var newLocalBranch = localRepository.Branches[branchName];
      var uDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, newLocalBranch);
      var oldUDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch);
      var mDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, newLocalBranch);
      var oldMDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch);

      Assert.AreEqual(4, newLocalBranch.Commits.Count());
      Assert.AreEqual("bd06eaeedef4c95d1187b17ff0c1ca259705f955", oldUDataTree.Sha);
      Assert.AreEqual("b043b1702bf8559ed57554dcb3e591b91f210a5b", oldMDataTree.Sha);
      Assert.AreEqual("a6de092b910b5577b08b5e860a7c85b46eb0c2a4", uDataTree.Sha);
      Assert.AreEqual("a6de092b910b5577b08b5e860a7c85b46eb0c2a4", mDataTree.Sha);
    }

    [Test(), Order(2)]
    public void MergePrivateBranchesTest()
    {
      var localBranch = localBranches[1];
      var remoteBranch01 = remoteBranches[1];
      var remoteBranch02 = remoteBranches[3];
      var localBranchName = new List<string> { localBranch.FriendlyName };
      var remoteBranchName = new List<string> { remoteBranch01.FriendlyName, remoteBranch02.FriendlyName};

      
      var mergePrvBranch = new MergePrivateBranchesCommand(localRepository);
      var mergeMessage = mergePrvBranch.GetMergeResultMessages();
      var mergeResult = mergePrvBranch.MergePrivateBranches(localBranchName, remoteBranchName);

      var changedLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      var newLocalBranch = localRepository.Branches[remoteBranch02.FriendlyName];

      RemoveRemoteRepository();

      Assert.IsEmpty(mergeResult);
      Assert.AreEqual(1, localBranch.Commits.Count());
      Assert.AreEqual(2, remoteBranch01.Commits.Count());
      Assert.AreEqual(1, remoteBranch02.Commits.Count());
      Assert.IsNotEmpty(mergeMessage);
      Assert.AreEqual(localBranches.Count + 1, localRepository.Branches.Count());
      Assert.AreEqual(0, localRepository.Tags.Count());
      Assert.AreEqual(0, remoteRepository.Tags.Count());

      //Test merged branch
      Assert.IsNotNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch));
      Assert.IsNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch));
      Assert.AreEqual(localBranch.FriendlyName, changedLocalBranch.FriendlyName);
      Assert.AreEqual(localBranch.FriendlyName, remoteBranch01.FriendlyName);

      Assert.AreEqual(PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME,changedLocalBranch).Sha ,
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, remoteBranch01).Sha);
      Assert.AreEqual(PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch).Sha, 
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, changedLocalBranch).Sha);

      //Test added branch
      Assert.IsNotNull(newLocalBranch);
      Assert.IsNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, newLocalBranch));
      Assert.AreEqual(newLocalBranch.FriendlyName, remoteBranch02.FriendlyName);
      Assert.AreEqual(PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, newLocalBranch).Sha,
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, remoteBranch02).Sha);
    }

    [Test(), Order(3)]
    public void MergeConflictTest()
    {
      var localBranch = localBranches[3];
      var remoteBranch = remoteBranches[4];

      var localBranchName = new List<string> { localBranch.FriendlyName };
      var remoteBranchName = new List<string> { remoteBranch.FriendlyName };

      var mergePrvBranch = new MergePrivateBranchesCommand(localRepository);
      var mergeMessage = mergePrvBranch.GetMergeResultMessages();
      var mergeResult = mergePrvBranch.MergePrivateBranches(localBranchName, remoteBranchName);

      var currentLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      
      Assert.IsNotEmpty(mergeResult);
      Assert.IsNotEmpty(mergeMessage);
      Assert.AreEqual(4, mergeResult[0].EntityConflicts.Count);
      Assert.IsNotNull(currentLocalBranch);
      Assert.AreEqual(localBranch.Commits.Count(), currentLocalBranch.Commits.Count());
      Assert.AreEqual(localBranch.Tip.Sha, currentLocalBranch.Tip.Sha);
      Assert.AreEqual(0, localRepository.Tags.Count());
    }


    [Test(), Order(4)]
    public void MergeAddUserDataTest()
    {
      var localBranch = localBranches[2];
      var remoteBranch = remoteBranches[2];

      var localBranchName = new List<string> { localBranch.FriendlyName };
      var remoteBranchName = new List<string> { remoteBranch.FriendlyName };
      
      var mergePrvBranch = new MergePrivateBranchesCommand(localRepository);
      var mergeMessage = mergePrvBranch.GetMergeResultMessages();
      var mergeResult = mergePrvBranch.MergePrivateBranches(localBranchName, remoteBranchName);

      var currentLocalBranch = localRepository.Branches[localBranch.FriendlyName];
      
      Assert.IsEmpty(mergeResult);
      Assert.IsNotEmpty(mergeMessage);
      Assert.AreEqual(localBranch.FriendlyName, currentLocalBranch.FriendlyName);
      Assert.IsNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch));
      Assert.IsNotNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, currentLocalBranch));

      Assert.IsNotNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch));
      Assert.IsNotNull(PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, currentLocalBranch));

      Assert.AreEqual(PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, remoteBranch).Sha,
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, currentLocalBranch).Sha);

      Assert.AreEqual(PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch).Sha,
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, currentLocalBranch).Sha);
    }

    [Test(), Order(5)]
    public void NothingToMergeTest()
    {
      var localBranch = localRepository.Branches[localBranches[0].FriendlyName];
      var remoteBranch = remoteBranches[0];

      var localBranchName = new List<string> { localBranch.FriendlyName };
      var remoteBranchName = new List<string> { remoteBranch.FriendlyName };

      var mergePrvBranch = new MergePrivateBranchesCommand(localRepository);
      var mergeMessage = mergePrvBranch.GetMergeResultMessages();
      var mergeResult = mergePrvBranch.MergePrivateBranches(localBranchName, remoteBranchName);

      var sameBranch = localRepository.Branches[localBranch.FriendlyName];
      
      Assert.IsEmpty(mergeResult);
      Assert.IsNotEmpty(mergeMessage);
      Assert.AreEqual(localBranch.Tip, sameBranch.Tip);
      Assert.AreEqual(localBranch.Commits.Count(), sameBranch.Commits.Count());
    }

    #region Set Private Branches

    private void SetSavedFiles()
    {
      prvBranchNamings = new List<PrivateBranchNaming>();
      uFiles = new List<string>
      {
        TestHelper.GetTestFilePath("vecto_engine_date_sample01.xml"),
        TestHelper.GetTestFilePath("vecto_engine_date_sample02.xml"),
        TestHelper.GetTestFilePath("vecto_engine_date_sample03.xml")
      };

      mFiles = new List<string>
      {
        TestHelper.GetTestFilePath("vecto_engine_date_sample04.xml"),
        TestHelper.GetTestFilePath("vecto_engine_date_sample05.xml")
      };

      uConflictFiles = new List<string>
      {
        TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01.xml"),
        TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02.xml"),
        TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03.xml")
      };

      mConflictFiles = new List<string>
      {
        TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample04.xml")
      };
    }

    private void SetupLocalBranches()
    {
      Branch localBranch = null;
      localBranches = new List<Branch>();

      var pubBranchNaming01 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_cmp.xml"));
      var prvNaming01 = new PrivateBranchNaming(pubBranchNaming01.GetGitPrivateBranchName());
      prvBranchNamings.Add(prvNaming01);

      TestHelper.SaveUserDataByPrivateBranchName(localRepository, uFiles, prvNaming01);
      localBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(localRepository, mFiles, prvNaming01);
      localBranches.Add(localBranch);

      var pubBranchNaming02 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml"));
      var prvNaming02 = new PrivateBranchNaming(pubBranchNaming02.GetGitPrivateBranchName());
      prvBranchNamings.Add(prvNaming02);

      localBranch = TestHelper.SaveUserDataByPrivateBranchName(localRepository, uFiles, prvNaming02);
      localBranches.Add(localBranch);

      var pubBranchNaming03 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml"));
      var prvNaming03 = new PrivateBranchNaming(pubBranchNaming03.GetGitPrivateBranchName());
      prvBranchNamings.Add(prvNaming03);

      localBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(localRepository, mFiles, prvNaming03);
      localBranches.Add(localBranch);
      
      var pubBranchNaming05 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_cmp.xml"));
      var prvNaming05 = new PrivateBranchNaming(pubBranchNaming05.GetGitPrivateBranchName());
      prvBranchNamings.Add(prvNaming03);

      TestHelper.SaveUserDataByPrivateBranchName(localRepository, uFiles, prvNaming05);
      localBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(localRepository, mFiles, prvNaming05);
      localBranches.Add(localBranch);
    }

    private void SetupRemoteBranches()
    {
      Branch remoteBranch = null;
      remoteBranches = new List<Branch>();

      var pubBranchNaming01 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_cmp.xml"));
      var prvNaming01 = new PrivateBranchNaming(pubBranchNaming01.GetGitPrivateBranchName());

      TestHelper.SaveUserDataByPrivateBranchName(remoteRepository, mFiles, prvNaming01);
      remoteBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(remoteRepository, uFiles, prvNaming01);
      remoteBranches.Add(remoteBranch);

      var pubBranchNaming02 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml"));
      var prvNaming02 = new PrivateBranchNaming(pubBranchNaming02.GetGitPrivateBranchName());

      TestHelper.SaveUserDataByPrivateBranchName(remoteRepository, uFiles, prvNaming02);
      remoteBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(remoteRepository, mFiles, prvNaming02);
      remoteBranches.Add(remoteBranch);

      var pubBranchNaming03 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml"));
      var prvNaming03 = new PrivateBranchNaming(pubBranchNaming03.GetGitPrivateBranchName());

      remoteBranch = TestHelper.SaveUserDataByPrivateBranchName(remoteRepository, uFiles, prvNaming03);
      remoteBranches.Add(remoteBranch);
      
      var differentData = new List<string>
      {
        TestHelper.GetTestFilePath("vecto_gearbox-sample.xml"),
        TestHelper.GetTestFilePath("vecto_gearbox-sample_02.xml"),
        TestHelper.GetTestFilePath("VectoComponent.xsd")
      };


      var pubBranchNaming04 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample04_cmp.xml"));
      var prvNaming04 = new PrivateBranchNaming(pubBranchNaming04.GetGitPrivateBranchName());
      prvBranchNamings.Add(prvNaming04);

      remoteBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(remoteRepository, differentData, prvNaming04);
      remoteBranches.Add(remoteBranch);

      var pubBranchNaming05 = xmlReader.ReadOutBranchNameData(TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_cmp.xml"));
      var prvNaming05 = new PrivateBranchNaming(pubBranchNaming05.GetGitPrivateBranchName());
      prvBranchNamings.Add(prvNaming05);

      TestHelper.SaveUserDataByPrivateBranchName(remoteRepository, uConflictFiles, prvNaming05);
      remoteBranch = TestHelper.SaveMeasurementDataByPrivateBranchName(remoteRepository, mConflictFiles, prvNaming05);

      remoteBranches.Add(remoteBranch);
    }




    #endregion


    #region Set Remote Repository

    private void RemoveRemoteRepository()
    {
      localRepository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
    }

    private void AddRemoteRepository(string remotePath)
    {
      localRepository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
      currentRemote = localRepository.Network.Remotes.Add(ConfigValue.REMOTE_NAME, remotePath);
    }

    private void FetchRemoteSpecs()
    {
      var fetchOptions = new FetchOptions
      {
        TagFetchMode = TagFetchMode.None
      };

      var logMessage = string.Empty;
      var refSpecs = currentRemote.FetchRefSpecs.Select(x => x.Specification);
      LibGit2Sharp.Commands.Fetch(localRepository, ConfigValue.REMOTE_NAME, refSpecs, fetchOptions, logMessage);
    }

    #endregion

  }
}