﻿using LibGit2Sharp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT_TEST;


namespace VECTO_GIT.ComponentCommit.Tests
{
  [TestFixture()]
  [Category("ComponentDataCommit")]
  public class ComponentDataCommitTests
  {
    private Repository repository;
    private string compFilePath;
    private string stdFilePath;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      compFilePath = TestHelper.GetTestFilePath("vecto_gearbox-sample.xml");
      stdFilePath = TestHelper.GetTestFilePath("vecto_std_engine-sample.xml");

    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [TearDown]
    public void CleanUp()
    {
      repository.Dispose();
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
    }

    [Test()]
    public void ComponentDataCommitConstructorTest()
    {
      var noXmlFile = TestHelper.GetTestFilePath("Demo_FullLoad_Child_v1.4.csv");

      Assert.Throws<ArgumentNullException>(() => new ComponentDataCommit(null, compFilePath, null, null));
      Assert.Throws<ArgumentNullException>(() => new ComponentDataCommit(repository, null, null, null));
      Assert.Throws<ArgumentException>(() => new ComponentDataCommit(repository, "", null, null));
      Assert.Throws<XmlException>(() => new ComponentDataCommit(repository, noXmlFile, null, null));

      var compCommit = new ComponentDataCommit(repository, compFilePath, null, null);
      Assert.IsTrue(compCommit.VerificationCheckBeforeCommit());
    }

    [Test()]
    public void ComponentDataCommitConstructorTest02()
    {
      var compBlob = TestHelper.GetExampleBlob(repository, compFilePath);
      var stdBranch = TestHelper.SaveStandardValueFile(repository, stdFilePath);

      Assert.Throws<ArgumentNullException>(() => new ComponentDataCommit(null, stdBranch, compBlob));
      Assert.Throws<ArgumentNullException>(() => new ComponentDataCommit(repository, null, compBlob));
      Assert.Throws<ArgumentNullException>(() => new ComponentDataCommit(repository, stdBranch, null));

      var compCommit = new ComponentDataCommit(repository, stdBranch, compBlob);
      Assert.IsTrue(compCommit.VerificationCheckBeforeCommit());

      TestHelper.DeleteBranch(repository, stdBranch);
    }

    [Test()]
    public void ExecuteCommitTest()
    {
      var expectedBranchName = PublicBranchNaming.TREE_NAME +
        "/Generic_Gearbox_Manufacturer/component/2017/" +
        "Generic_40t_Long_Haul_Truck_Gearbox/vwf71ha02yhu";

      var expectedCertTag = "CERT_NR/e12#0815)8051#2017)05#e/0000#00/vwf71ha02yhu";
      var expectedGitIdTag = "GIT_ID/fd/bcf59dde09a84ca1501ae550597c947dd71011";

      var componentDataCommit = new ComponentDataCommit(repository, compFilePath, null, null);
      var verificationResult = componentDataCommit.VerificationCheckBeforeCommit();
      var result = componentDataCommit.ExecuteCommit();
      var verificationErrors = componentDataCommit.GetVerificationErrors();

      Assert.IsNotNull(result);
      Assert.AreEqual(expectedBranchName, result.FriendlyName);
      Assert.IsTrue(verificationResult);
      Assert.IsEmpty(verificationErrors);
      Assert.AreEqual(1, repository.Branches.Count());
      Assert.IsTrue(VerifyComponentDataCommit(result, expectedCertTag, expectedGitIdTag));

      Assert.IsNull(componentDataCommit.ExecuteCommit());
      componentDataCommit.VerificationCheckBeforeCommit();
      Assert.IsNotEmpty(componentDataCommit.GetVerificationErrors());
    }


    [Test()]
    public void AppendCommitTest()
    {
      var compBlob = TestHelper.GetExampleBlob(repository, compFilePath);
      var stdBranch = TestHelper.SaveStandardValueFile(repository, stdFilePath);
      var compCommit = new ComponentDataCommit(repository, stdBranch, compBlob);
      var stdHash = TestHelper.GetGitHashOfData(Path.GetFileName(stdFilePath), false);

      var verificationResult = compCommit.VerificationCheckBeforeCommit();
      var branch = compCommit.ExecuteCommit();

      Assert.IsTrue(verificationResult);
      Assert.IsNotNull(branch);
      Assert.AreEqual(2, branch.Commits.Count());
      Assert.AreEqual(compBlob.Sha, ComponentDataCommit.GetComponentDataBlob(branch).Sha);
      Assert.AreEqual(stdHash, StandardDataCommit.GetStandardDataBlob(branch).Sha);

      TestHelper.DeleteBranch(repository, branch);
    }

    [Test()]
    public void AppendCommitWithStdFileTest()
    {
      var stdBranch = TestHelper.SaveStandardValueFile(repository, stdFilePath);
      var compCommit = new ComponentDataCommit(repository, compFilePath, null, stdFilePath);
      var verificationResult = compCommit.VerificationCheckBeforeCommit();
      var branch = compCommit.ExecuteCommit();

      Assert.IsTrue(verificationResult);
      Assert.IsNotNull(branch);
      Assert.AreEqual(2, branch.Commits.Count());
      Assert.IsNotNull(ComponentDataCommit.GetComponentDataBlob(branch).Sha);
      Assert.IsNotNull(StandardDataCommit.GetStandardDataBlob(branch).Sha);
      Assert.IsNull(repository.Branches[stdBranch.FriendlyName]);
    }

    [Test()]
    public void StaticMethodsErrorsTest()
    {
      Assert.IsNull(ComponentDataCommit.GetComponentDataBlob((Commit)null));
      Assert.Throws<ArgumentNullException>(() => ComponentDataCommit.GetComponentDataCommit((Branch)null));
    }

    private bool VerifyComponentDataCommit(Branch branch, string certTagName, string gitIdTagName)
    {
      var tags = new SearchTagCommand(repository,
        new List<string> { branch.FriendlyName }).Result();

      var branchName = new PublicBranchNaming(branch.FriendlyName);
      var gitId = new GitIdHandler(branch);

      var jsonBranchName = branchName.GetJsonRepesentation();

      var compDataCommit = ComponentDataCommit.GetComponentDataCommit(branch);
      if (compDataCommit == null)
      {
        return false;
      }

      var compDataBlob = ComponentDataCommit.GetComponentDataBlob(compDataCommit);
      if (compDataBlob == null || compDataBlob.Sha != gitId.OriginGitId)
      {
        return false;
      }

      if (tags.Count() != 2)
        return false;
      foreach (var tag in tags)
      {
        if ((!tag.FriendlyName.Replace("\n", "").Equals(certTagName) &&
            !tag.FriendlyName.Replace("\n", "").Equals(gitIdTagName)))
        {
          return false;
        }

        if (!tag.Target.Equals(compDataCommit))
          return false;

        if (!tag.Annotation.Message.Replace("\n", "").Equals(jsonBranchName))
          return false;
      }

      return true;
    }
  }
}