﻿using System;
using NUnit.Framework;
using VECTO_GIT_TEST;

namespace VECTO_GIT.ComponentCommit.Tests
{
  [TestFixture()]
  [Category("XmlValidator")]
  public class XmlValidatorTests
  {
    private string compDataFilePath01;
    private string compDataFilePath02;
    private string wrongDataFilePath;
    private string xsdNameSpace;
    private string xsdFilePath;


    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
      compDataFilePath01 = TestHelper.GetTestFilePath("vecto_engine-sample.xml");
      compDataFilePath02 = TestHelper.GetTestFilePath("vecto_gearbox-sample.xml");
      wrongDataFilePath = TestHelper.GetTestFilePath("vecto_engine-sample_wrong.xml");
      xsdNameSpace = "urn:tugraz:ivt:VectoAPI:DeclarationComponent:v1.0";
      xsdFilePath = TestHelper.GetTestFilePath("VectoComponent.xsd");
    }


    [Test()]
    public void XmlValidatorTest()
    {
      var wrongFilePath = "wrong/file/Path";
      
      Assert.Throws<ArgumentNullException>(() => new XmlValidator(null, xsdNameSpace));
      Assert.Throws<ArgumentNullException>(() => new XmlValidator(xsdFilePath, null));
      Assert.Throws<ArgumentException>(() => new XmlValidator( xsdFilePath,""));
      Assert.Throws<ArgumentException>(() => new XmlValidator( "",xsdNameSpace));
      Assert.Throws<ArgumentException>(() => new XmlValidator( wrongFilePath, xsdNameSpace));

      Assert.DoesNotThrow(() => new XmlValidator( xsdFilePath, xsdNameSpace));
      Assert.NotNull(new XmlValidator( xsdFilePath, xsdNameSpace));
    }

    [Test()]
    public void ValidateXmlTest()
    {
      var xmlValidator = new XmlValidator( xsdFilePath, xsdNameSpace);

      var result = xmlValidator.ValidateXml(compDataFilePath02);

      Assert.IsNotNull(xmlValidator);
      Assert.IsTrue(result);
      Assert.IsEmpty(xmlValidator.XmlValidationResult.Errors);
      Assert.IsEmpty(xmlValidator.XmlValidationResult.Warnings);

      result = xmlValidator.ValidateXml(compDataFilePath01);
           
      Assert.IsTrue(result);
      Assert.IsEmpty(xmlValidator.XmlValidationResult.Errors);
      Assert.IsEmpty(xmlValidator.XmlValidationResult.Warnings);
    }

    [Test()]
    public void ValidateWrongFileTypeTest()
    {
      var wrongFile = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var xmlValidator = new XmlValidator( xsdFilePath, xsdNameSpace);

      var result = xmlValidator.ValidateXml(wrongFile);
      
      Assert.IsNotNull(xmlValidator);
      Assert.IsFalse(result);
      Assert.IsNull(xmlValidator.XmlValidationResult);
    }

    [Test()]
    public void ValidateWrongXmlSchemaTest()
    {
      var xmlValidator = new XmlValidator(xsdFilePath, xsdNameSpace);

      var result = xmlValidator.ValidateXml(wrongDataFilePath);

      Assert.IsNotNull(xmlValidator);
      Assert.IsFalse(result);
      Assert.IsNotEmpty(xmlValidator.XmlValidationResult.Errors);
      Assert.IsEmpty(xmlValidator.XmlValidationResult.Warnings);
    }


  }
}