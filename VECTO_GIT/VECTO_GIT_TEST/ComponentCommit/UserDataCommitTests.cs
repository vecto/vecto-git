﻿using LibGit2Sharp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;
using VECTO_GIT_TEST;

namespace VECTO_GIT.ComponentCommit.Tests
{
  [TestFixture()]
  [Category("UserDataCommit")]
  public class UserDataCommitTests
  {

    private Repository repository;
    private Dictionary<string, List<string>> branchInformation;
    private List<Item> dataItems;
    private List<string> userDataFilePaths;
    private List<string> mDataFilePaths;
    private List<string> wrongPaths;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      mDataFilePaths = new List<string>
      {
        TestHelper.GetTestFilePath("Demo_FullLoad_Child_v1.4.csv"),
        TestHelper.GetTestFilePath("Demo_FullLoad_Parent_v1.4.csv")
      };

      dataItems = TestHelper.GetFileItems(mDataFilePaths, FileType.UserData);

      userDataFilePaths = new List<string>
      {
        TestHelper.GetTestFilePath("VectoComponent.xsd")
      };

      wrongPaths = new List<string>
      {
        TestHelper.GetTestFilePath( "Demo_FullLoad_Child_v1.4.csv"),
        "some/wrong/path"
      };

      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);

      SetBranchDataInfo();
      SetUpTestBranches();
      AddTagsToBranches();
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }


    [Test()]
    public void UserDataCommitConstructorTest01()
    {
      var branchName = "some/branch/name";

      Assert.Throws<ArgumentNullException>(() => new UserDataCommit(null, dataItems, branchName));
      Assert.Throws<NullReferenceException>(() => new UserDataCommit(repository, null, branchName));
      Assert.Throws<ArgumentNullException>(() => new UserDataCommit(repository, dataItems, null));
      Assert.DoesNotThrow(() => new UserDataCommit(repository, dataItems, branchName));
    }


    [Test()]
    public void UserDataCommitConstructorTest02()
    {
      var branchName = PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t";
      var prvBranch = repository.Branches[branchName];
      var tree = prvBranch.Tip.Tree;

      Assert.Throws<ArgumentNullException>(() => new UserDataCommit(null, tree, prvBranch));
      Assert.Throws<ArgumentNullException>(() => new UserDataCommit(repository, null, prvBranch));
      Assert.Throws<ArgumentNullException>(() => new UserDataCommit(repository, tree, null));
      Assert.DoesNotThrow(() => new UserDataCommit(repository, tree, prvBranch));
    }



    [Test()]
    public void ExecuteUserDataCommitTest01()
    {
      var filePath = TestHelper.GetTestFilePath("vecto_engine_date_sample01.xml");
      var prvBranchName = TestHelper.GetPrivateBranchName(filePath);
      var branchesCount = repository.Branches.Count();
      var prvCommit = new UserDataCommit(repository, dataItems, prvBranchName);
      var validationResult = prvCommit.VerificationCheckBeforeCommit();
      var prvBranch = prvCommit.ExecuteCommit();


      Assert.IsTrue(validationResult);
      Assert.IsNotNull(prvBranch);
      Assert.AreEqual(branchesCount + 1, repository.Branches.Count());
      Assert.AreEqual(prvBranchName, prvBranch.FriendlyName);
      Assert.AreEqual(1, prvBranch.Commits.Count());
      Assert.AreEqual(typeof(Tree), prvBranch.Tip.Tree.GetType());
      var treeContent = prvBranch.Tip.Tree.First().Target as Tree;
      Assert.AreEqual(2, treeContent.Count());
      Assert.AreEqual("f58ffa0c96425c362a923a17e51c95a87dda53bc", treeContent.Sha);
      Assert.IsNull(prvCommit.ExecuteCommit());
    }

    [Test()]
    public void ExecuteUserDataCommitTest02()
    {
      var newFiles = new List<string>
      {
        TestHelper.GetTestFilePath("VectoDeclarationDefinitions.1.0.xsd"),
        TestHelper.GetTestFilePath("VectoInput.xsd"),
        TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf")
      };
      var newFileItems = TestHelper.GetFileItems(newFiles, FileType.UserData);
      var filePath = TestHelper.GetTestFilePath("vecto_engine_date_sample01.xml");
      var prvBranchName = TestHelper.GetPrivateBranchName(filePath);
      var branchesCount = repository.Branches.Count();
      var prvCommit = new UserDataCommit(repository, newFileItems, prvBranchName);
      var validationResult = prvCommit.VerificationCheckBeforeCommit();
      var prvBranch = prvCommit.ExecuteCommit();


      Assert.IsTrue(validationResult);
      Assert.IsNotNull(prvBranch);
      Assert.AreEqual(branchesCount, repository.Branches.Count());
      Assert.AreEqual(prvBranchName, prvBranch.FriendlyName);
      Assert.AreEqual(2, prvBranch.Commits.Count());
      Assert.AreEqual(typeof(Tree), prvBranch.Tip.Tree.GetType());
      var treeContent = prvBranch.Tip.Tree.First().Target as Tree;
      Assert.AreEqual(5, treeContent.Count());
      Assert.AreEqual("917bca4133025656c0dc95ab69880bd11d7db51e", treeContent.Sha);
      Assert.IsNull(prvCommit.ExecuteCommit());
    }


    [Test()]
    public void ExecuteUserDataCommitTest03()
    {
      var filePath = TestHelper.GetTestFilePath("vecto_engine_date_sample04.xml");
      var prvBranchName = TestHelper.GetPrivateBranchName(filePath);
      var branchesCount = repository.Branches.Count();
      var prvCommit = new UserDataCommit(repository, dataItems, prvBranchName);
      var validationResult = prvCommit.VerificationCheckBeforeCommit();
      var prvBranch = prvCommit.ExecuteCommit();

      Assert.IsTrue(validationResult);
      Assert.IsNotNull(prvBranch);


      var newFiles = new List<string>
      {
        TestHelper.GetTestFilePath("vecto_std_engine-sample.xml"),
        TestHelper.GetTestFilePath("vecto_gearbox-sample.xml")
      };
      var tree = TestHelper.SaveTreeOfFiles(repository, newFiles);
      prvCommit = new UserDataCommit(repository, tree, prvBranch);
      validationResult = prvCommit.VerificationCheckBeforeCommit();
      prvBranch = prvCommit.ExecuteCommit();

      Assert.IsTrue(validationResult);
      Assert.IsNotNull(prvBranch);
      Assert.AreEqual(branchesCount + 1, repository.Branches.Count());
      Assert.AreEqual(2, prvBranch.Commits.Count());
      Assert.AreEqual(typeof(Tree), prvBranch.Tip.Tree.GetType());
      var treeContent = prvBranch.Tip.Tree.First().Target as Tree;

      Assert.AreEqual("bef50eca96ce995c4cf0c551a84ec368cbb8027c", treeContent.Sha);
      Assert.AreEqual(newFiles.Count, treeContent.Count());
    }

    #region Init Repository

    private IEnumerable<Branch> GetPublicBranches()
    {
      return repository.Branches.Where(
        obj => obj.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME));
    }


    private void SetUpTestBranches()
    {
      foreach (var branchdata in branchInformation)
      {
        if (branchdata.Value.Count > 1)
        {
          TestHelper.GetExampleBranch(repository, branchdata.Value, branchdata.Key, CommitDataType.ComponentData);
        }
        else
        {
          TestHelper.GetExampleBranch(repository, branchdata.Value.First(), branchdata.Key, CommitDataType.ComponentData);
        }
      }
    }

    private void AddTagsToBranches()
    {
      var pubBranches = GetPublicBranches();

      foreach (var branch in pubBranches)
      {
        SetGitIdTag(branch);
        SetCertificateTag(branch);
      }
    }

    private void SetGitIdTag(Branch branch)
    {
      var tagMsg = new PublicBranchNaming(branch.FriendlyName).GetJsonRepesentation();
      var gitId = new GitIdHandler(branch);
      var tagGitIdName = gitId.GetTagGitPathName();

      var gitIdTag = new CreateTagCommand(repository, tagGitIdName, tagMsg,
        branch.Commits.Last().Sha);
    }


    private void SetCertificateTag(Branch branch)
    {
      var xmlReader = new XmlContentReader();
      var blob = branch.Commits.Last().Tree.First().Target as Blob;
      if (blob != null)
      {
        var content = new StreamReader(blob.GetContentStream(), Encoding.Default);
        var certId = xmlReader.ReadOutCertificateId(content);
        var tagCertIdName = certId.GetTagGitPathName();
        var tagMsg = new PublicBranchNaming(branch.FriendlyName).GetJsonRepesentation();
        var certTag = new CreateTagCommand(repository, tagCertIdName, tagMsg,
          branch.Commits.Last().Sha);
      }
    }

    private void SetBranchDataInfo()
    {
      branchInformation = new Dictionary<string, List<string>>
      {
        { PublicBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t/9275da3dbb4a",
          new List<string>{ "vecto_engine-sample.xml" } },
        { PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/612eb9de7d8a",
          new List<string>{ "vecto_gearbox-sample.xml" } },
        { PublicBranchNaming.TREE_NAME + "/gem/component/2018/generic_40t/712eb9de7d8a",
          new List<string>{ "vecto_gearbox-sample_02.xml" } },
        { PrivateBranchNaming.TREE_NAME + "/gem/component/2017/generic_40t",
          new List<string>{ "Demo_FullLoad_Parent_v1.4.csv", "Demo_FullLoad_Child_v1.4.csv", "Demo_Map_v1.4.csv" } },
      };
    }

    #endregion
  }
}