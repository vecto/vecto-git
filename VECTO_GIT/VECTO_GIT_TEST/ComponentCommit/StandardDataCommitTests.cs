﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT_TEST;
using LibGit2Sharp;


namespace VECTO_GIT.ComponentCommit.Tests
{
  [TestFixture()]
  [Category("StandardDataCommit")]
  public class StandardDataCommitTests
  {

    private Repository repository;
    private string fileName;
    private string filePath;
    private string branchName;
    private Branch stdBranch;
    private string wrongFilePath;


    [OneTimeSetUp]
    public void OneTimeInit()
    {
      fileName = "vecto_gearbox-sample_02.xml";
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      filePath = TestHelper.GetTestFilePath(fileName);
      CreateStdBranch();
      wrongFilePath = "some/wrong/file/path";
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [TearDown]
    public void CleanUp()
    {
      repository.Dispose();
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
    }


    [Test()]
    public void StandardDataCommitConstructorTest01()
    {
      Assert.Throws<ArgumentNullException>(() => new StandardDataCommit(null, filePath));
      Assert.Throws<ArgumentNullException>(() => new StandardDataCommit(repository, null));
      
      Assert.Throws<ArgumentException>(() => new StandardDataCommit(repository, ""));
      Assert.Throws<ArgumentException>(() => new StandardDataCommit(repository, wrongFilePath));

      Assert.DoesNotThrow(() => new StandardDataCommit(repository, filePath));
      Assert.NotNull(new StandardDataCommit(repository, filePath));
    }


    [Test()]
    public void StandardDataCommitConstructorTest02()
    {
      CreateStdBranch();
      var xmlReader = new XmlContentReader();
      var commit = StandardDataCommit.GetStandardDataCommit(stdBranch);
      var branchNaming = xmlReader.ReadOutBranchNameData(filePath);

      Assert.IsNotNull(stdBranch);
      Assert.IsNotNull(commit);
      Assert.AreEqual(stdBranch.FriendlyName, branchNaming.GetGitPathBranchName());
      Assert.Throws<ArgumentNullException>(() => new StandardDataCommit(null, commit, branchNaming));
      Assert.Throws<ArgumentNullException>(() => new StandardDataCommit(repository, null, branchNaming));
      Assert.Throws<ArgumentNullException>(() => new StandardDataCommit(repository, commit, null));
      Assert.DoesNotThrow(() => new StandardDataCommit(repository, commit, branchNaming));
      Assert.NotNull(new StandardDataCommit(repository, commit, branchNaming));

      TestHelper.DeleteBranch(repository, stdBranch);
      stdBranch = null;
    }



    [Test()]
    public void ExecuteCommitTest()
    {
      TestHelper.DeleteBranch(repository, stdBranch);
      stdBranch = null;

      var command = new StandardDataCommit(repository, filePath);
      var verificationResult = command.VerificationCheckBeforeCommit();
      var result = command.ExecuteCommit();
      var hashCalculator = new GitSha1Calculation(filePath);

      Assert.IsNotNull(command);
      Assert.IsTrue(verificationResult);
      Assert.IsNotNull(result);
      Assert.IsEmpty(command.GetVerificationErrors());

      var branch = repository.Branches[branchName];
      var tree = branch.Commits.Last().Tree;
      var entry = tree.Last();
      var blob = repository.Lookup(entry.Target.Id, ObjectType.Blob) as Blob;
      var preCalculatedSha1 = TestHelper.GetGitHashOfData(fileName, blob.IsBinary);
      var calculatedSha1 = hashCalculator.BinaryFileGitHash;
      var stdBlob = StandardDataCommit.GetStandardDataBlob(branch);


      Assert.IsTrue(branch.Commits.Last().Message.Contains(CommitDateHandler.STD_VALUES_DATE_NAME));
      Assert.AreEqual(1, branch.Commits.Count());
      Assert.AreEqual(1, tree.Count());
      Assert.AreEqual(ConfigValue.STANDARD_VALUES_FILE_NAME, entry.Name);
      Assert.AreEqual(entry.Target.Sha, calculatedSha1);
      Assert.AreEqual(preCalculatedSha1, calculatedSha1);
      Assert.AreEqual(0, repository.Tags.Count());
      Assert.IsNotNull(stdBlob);
      Assert.AreEqual(preCalculatedSha1, blob.Sha);
      Assert.IsTrue(StandardDataCommit.IsStandardValuesBranch(branch));

      command = new StandardDataCommit(repository, filePath);
      verificationResult = command.VerificationCheckBeforeCommit();
      result = command.ExecuteCommit();

      Assert.IsNotNull(command);
      Assert.IsFalse(verificationResult);
      Assert.IsNotEmpty(command.GetVerificationErrors());
      Assert.IsNull(result);
    }

    [Test()]
    public void IsStandardBranchTest()
    {
      stdBranch = repository.Branches[branchName];
      if (stdBranch == null)
        stdBranch = TestHelper.SaveStandardValueFile(repository, filePath);

      var compFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample05.xml");
      var compBranch = TestHelper.SaveAsComponentData(repository, compFilePath);

      var mFilePath = new List<string> {TestHelper.GetTestFilePath("vecto_engine_date_sample03.xml")};
      var prvBranch = TestHelper.SaveMeasurementDataByComponentFile(repository, mFilePath, compFilePath);

      var certBranch = TestHelper.SaveCertificateByComponentFile(repository, compFilePath, mFilePath[0]);
      var certCommit = CertificateDataCommit.GetCertificateDataCommit(certBranch);

      Assert.IsNotNull(prvBranch);
      Assert.IsNotNull(compBranch);
      Assert.IsNotNull(stdBranch);
      Assert.IsNotNull(certBranch);
      Assert.IsNull(StandardDataCommit.GetStandardDataBlob(certCommit));
      Assert.IsNull(StandardDataCommit.GetStandardDataCommit(prvBranch));
      Assert.IsTrue(StandardDataCommit.IsStandardValuesBranch(stdBranch));
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(compBranch));
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(null));
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(prvBranch));
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(certBranch));
    }

    private void CreateStdBranch()
    {
      stdBranch = TestHelper.SaveStandardValueFile(repository, filePath);
      branchName = stdBranch.FriendlyName;
    }


  }
}