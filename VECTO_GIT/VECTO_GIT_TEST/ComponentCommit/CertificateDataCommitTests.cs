﻿using LibGit2Sharp;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VECTO_GIT_TEST;
using Version = LibGit2Sharp.Version;

namespace VECTO_GIT.ComponentCommit.Tests
{
  [TestFixture()]
  [Category("CertificateDataCommit")]
  public class CertificateDataCommitTests
  {

    private Repository repository;
    private string certFilePath;
    private string stdFilePath;
    private string compFilePath;
    private XmlContentReader xmlReader;
    private Branch compBranch;
    private string wrongFilePath;
    //    private Dictionary<string, List<string>> branchInformation;


    [OneTimeSetUp]
    public void OneTimeInit()
    {
      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      certFilePath = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      stdFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample02.xml");
      compFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample03.xml");
      xmlReader = new XmlContentReader();
      wrongFilePath = "some/wrong/file/path";
      SetupBranch();
      //      SetBranchDataInfo();
      //      SetUpTestBranches();
      //      AddTagsToBranches();
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      repository.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
    }

    [TearDown]
    public void CleanUp()
    {
      //      repository.Dispose();
      //      repository = TestHelper.InitEmptyRepository(RepositoryType.Source);
      //      SetBranchDataInfo();
      //      SetUpTestBranches();
      //      AddTagsToBranches();
    }


    [Test()]
    public void CertificateDataCommitConstructorTest01()
    {
      var pubBranchNaming = xmlReader.ReadOutBranchNameData(compFilePath);
      var certNumber = xmlReader.ReadOutCertificateId(compFilePath);

      Assert.IsNotNull(pubBranchNaming);
      Assert.IsNotNull(certNumber);
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(null, certFilePath, pubBranchNaming, compBranch, certNumber));
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(repository, null, pubBranchNaming, compBranch, certNumber));
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(repository, certFilePath, null, compBranch, certNumber));
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(repository, certFilePath, pubBranchNaming, compBranch, null));

      Assert.DoesNotThrow(() => new CertificateDataCommit(repository, certFilePath, pubBranchNaming, null, certNumber));
      Assert.DoesNotThrow(() => new CertificateDataCommit(repository, certFilePath, pubBranchNaming, compBranch, certNumber));
      Assert.NotNull(new CertificateDataCommit(repository, certFilePath, pubBranchNaming, compBranch, certNumber));
    }

    [Test()]
    public void CertificateDataCommitConstructorTest02()
    {
      var certBlob = TestHelper.GetExampleBlob(repository, Path.GetFileName(certFilePath));
      var compBlob = ComponentDataCommit.GetComponentDataBlob(compBranch);

      Assert.IsNotNull(certBlob);
      Assert.IsNotNull(compBlob);
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(null, compBranch, certBlob, compBlob));
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(repository, null, certBlob, compBlob));
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(repository, compBranch, null, compBlob));
      Assert.Throws<ArgumentNullException>(() => new CertificateDataCommit(repository, compBranch, certBlob, null));

      Assert.DoesNotThrow(() => new CertificateDataCommit(repository, compBranch, certBlob, compBlob));
      Assert.NotNull(new CertificateDataCommit(repository, compBranch, certBlob, compBlob));
    }

    [Test()]
    public void ExecuteCommitTest01()
    {
      var certNumberHandler = xmlReader.ReadOutCertificateId(compFilePath);
      var pubBranchNaming = xmlReader.ReadOutBranchNameData(compFilePath);
      var commitCount = compBranch.Commits.Count();

      var certCommitCommand =
        new CertificateDataCommit(repository, certFilePath, pubBranchNaming, compBranch, certNumberHandler);

      Assert.IsNull(certCommitCommand.ExecuteCommit());

      var validationResult = certCommitCommand.VerificationCheckBeforeCommit();
      var certBranch = certCommitCommand.ExecuteCommit();
      var certBlob = CertificateDataCommit.GetCertificateBlob(certBranch);
      var expectedSha = TestHelper.GetGitHashOfData(Path.GetFileName(certFilePath), true);

      Assert.IsTrue(validationResult);
      Assert.IsNotNull(certBranch);
      Assert.IsNotNull(certBlob);
      Assert.AreEqual(expectedSha, certBlob.Sha);
      Assert.AreEqual(commitCount + 1, certBranch.Commits.Count());
    }

    [Test()]
    public void ExecuteCommitTest02()
    {
      var compFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample01.xml");
      var compBranch = TestHelper.SaveAsComponentData(repository, null, compFilePath);
      var compBlob = ComponentDataCommit.GetComponentDataBlob(compBranch);
      var certBlob = TestHelper.GetExampleBlob(repository, certFilePath);

      var certCommit = new CertificateDataCommit(repository, compBranch, certBlob, compBlob);
      var certValidation = certCommit.VerificationCheckBeforeCommit();
      var certBranch = certCommit.ExecuteCommit();


      Assert.IsNotNull(compBranch);
      Assert.IsNotNull(certBranch);
      Assert.IsTrue(certValidation);
      Assert.AreEqual(compBlob.Sha, ComponentDataCommit.GetComponentDataBlob(certBranch).Sha);
      Assert.AreEqual(certBlob.Sha, CertificateDataCommit.GetCertificateBlob(certBranch).Sha);
      Assert.AreEqual(2, certBranch.Commits.Count());
      Assert.AreEqual(compBranch.FriendlyName, certBranch.FriendlyName);
    }

    [Test()]
    public void ExecuteCommitTest03()
    {
      var compFilePath01 = TestHelper.GetTestFilePath("vecto_engine_date_sample04.xml");
      var compBranch01 = TestHelper.SaveAsComponentData(repository, null, compFilePath01);

      var compFilePath02 = TestHelper.GetTestFilePath("vecto_engine_date_sample05.xml");
      var stdFilePath02 = TestHelper.GetTestFilePath("vecto_engine-sample.xml");
      var compBranch02 = TestHelper.SaveAsComponentData(repository, stdFilePath02, compFilePath02);

      var certBranch01 = TestHelper.SaveCertificateByComponentFile(repository, certFilePath, compFilePath01);
      var certBranch02 = repository.Branches[compBranch02.FriendlyName];
      var certSha = TestHelper.GetGitHashOfData(Path.GetFileName(certFilePath), true);
      var certBlob01 = CertificateDataCommit.GetCertificateBlob(certBranch01);
      var certBlob02 = CertificateDataCommit.GetCertificateBlob(certBranch02);

      Assert.IsNotNull(compBranch01);
      Assert.IsNotNull(compBranch02);
      Assert.IsNotNull(certBranch01);
      Assert.IsNotNull(certBranch02);
      Assert.IsNotNull(certBlob01);
      Assert.IsNotNull(certBlob02);
      Assert.AreEqual(compBranch01.Commits.Count() + 1, certBranch01.Commits.Count());
      Assert.AreEqual(compBranch02.Commits.Count() + 1, certBranch02.Commits.Count());
      Assert.IsNotNull(CertificateDataCommit.GetCertificateBlob(certBranch01));
      Assert.IsNotNull(CertificateDataCommit.GetCertificateBlob(certBranch02));
      Assert.AreEqual(certBlob01.Sha, certBlob02.Sha);
      Assert.AreEqual(certSha, certBlob01.Sha);
    }



    [Test()]
    public void StaticMethodsTest()
    {
      var compFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample03.xml");
      var branchName = xmlReader.ReadOutBranchNameData(compFilePath);
      var certBranch = GetCertBranch(compFilePath, certFilePath, branchName.GetGitPathBranchName());

      var prvBranch =
        TestHelper.SaveUserDataByComponentFile(repository, new List<string>() {certFilePath}, compFilePath);
      var expectedSha = TestHelper.GetGitHashOfData(Path.GetFileName(certFilePath), true);

      Assert.Throws<ArgumentNullException>(() => CertificateDataCommit.GetCertificateBlob((Branch)null));
      Assert.IsNull(CertificateDataCommit.GetCertificateBlob((Commit)null));
      Assert.Throws<ArgumentNullException>(() => CertificateDataCommit.GetCertificateDataCommit(null));

      Assert.IsNull(CertificateDataCommit.GetCertificateDataCommit(prvBranch));
      Assert.AreEqual(expectedSha, CertificateDataCommit.GetCertificateBlob(certBranch).Sha);
    }


    private void SetupBranch()
    {
      compBranch = TestHelper.SaveAsComponentData(repository, stdFilePath, compFilePath);
    }

    private Branch CreateCertBranch(string compFilePath, string certFilePath)
    {
      return TestHelper.SaveCertificateByComponentFile(repository, certFilePath, compFilePath);
    }

    private Branch GetCertBranch(string compFilePath, string certFilePath, string branchName)
    {
      var branch = repository.Branches[branchName];
      if (branch != null)
      {
        if (ComponentDataCommit.GetComponentDataBlob(branch) != null)
          return branch;
      }
      else
      {
        return CreateCertBranch(compFilePath, certFilePath);
      }

      return null;
    }

  }
}