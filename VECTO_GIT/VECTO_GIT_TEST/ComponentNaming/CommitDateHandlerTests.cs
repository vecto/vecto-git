﻿using NUnit.Framework;
using System;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT_TEST;

namespace VECTO_GIT.ComponentNaming.Tests
{
  [TestFixture()]
  [Category("CommitDateHandler")]
  public class CommitDateHandlerTests
  {

    private string filePath;
    private string fileName;
    private string compBranchName;
    private string stdBranchName;
    private ComponentDataManagement compDataManagement;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      fileName = "vecto_engine-sample.xml";
      filePath = TestHelper.GetTestFilePath(fileName);
      compDataManagement = new ComponentDataManagement();
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      SetUpTestBranches();
    }


    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
    }

    [Test()]
    public void CommitDateHandlerConstructorTest02()
    {
      var dateTime = DateTime.UtcNow;
      Assert.DoesNotThrow(() => new CommitDateHandler(dateTime, CommitDateType.ComponentData));
    }

    [Test()]
    public void DeserializeDateTest()
    {
      var expectedCompDataDate = new DateTime(2017, 2, 17, 11, 0, 0);
      var expectedStdValueDate = new DateTime(2017, 2, 19, 05, 0, 0);


      //2017 - 02 - 17T11: 00:00Z
      //2017 - 02 - 19T05: 00:00Z


      var compDataBranch = compDataManagement.CurrentRepository.Branches[compBranchName];
      var stdValueBranch = compDataManagement.CurrentRepository.Branches[stdBranchName];

      var compCommit = ComponentDataCommit.GetComponentDataCommit(compDataBranch);
      var stdCommit = StandardDataCommit.GetStandardDataCommit(stdValueBranch);

      var date01 = CommitDateHandler.DeserializeCommitDate(compCommit.Message);
      var date02 = CommitDateHandler.DeserializeCommitDate(stdCommit.Message);

      Assert.AreEqual(expectedCompDataDate, date01.CertificationDate);
      Assert.AreEqual(CommitDateType.ComponentData, date01.DateType);
      Assert.AreEqual(expectedStdValueDate, date02.CertificationDate);
      Assert.AreEqual(CommitDateType.StandardData, date02.DateType);
    }

    [Test()]
    public void GetJsonRepesentationTest01()
    {
      var date = new DateTime(2017, 2, 15, 11, 0, 0, DateTimeKind.Utc);

      var dateHandler = new CommitDateHandler(date, CommitDateType.ComponentData);

      var expectedJson = string.Format("{{{0}:{1}}}", CommitDateHandler.COMP_DATA_DATE_NAME, "2017-02-15T11:00:00Z");

      var jsonResult = dateHandler.GetJsonRepresentation();
      jsonResult = ReplaceUnnecessaryChars(jsonResult);

      Assert.IsNotNull(dateHandler);
      Assert.AreEqual(expectedJson, jsonResult);

      //------

      dateHandler = new CommitDateHandler(date, CommitDateType.StandardData);
      expectedJson = string.Format("{{{0}:{1}}}", CommitDateHandler.STD_VALUES_DATE_NAME, "2017-02-15T11:00:00Z");

      jsonResult = dateHandler.GetJsonRepresentation();
      jsonResult = ReplaceUnnecessaryChars(jsonResult);

      Assert.IsNotNull(dateHandler);
      Assert.AreEqual(expectedJson, jsonResult);
    }

    [Test()]
    public void GetJsonRepesentationTest02()
    {
      var dateTime = DateTime.UtcNow;
      var dateTimeString = dateTime.ToString(CommitDateHandler.DATE_FORMAT);

      var dateHandler = new CommitDateHandler(dateTime, CommitDateType.ComponentData);

      var expectedJson = string.Format("{{{0}:{1}}}", CommitDateHandler.COMP_DATA_DATE_NAME, dateTimeString);

      var jsonResult = dateHandler.GetJsonRepresentation();
      jsonResult = ReplaceUnnecessaryChars(jsonResult);

      Assert.IsNotNull(dateHandler);
      Assert.AreEqual(expectedJson, jsonResult);


      dateHandler = new CommitDateHandler(dateTime, CommitDateType.StandardData);
      expectedJson = string.Format("{{{0}:{1}}}", CommitDateHandler.STD_VALUES_DATE_NAME, dateTimeString);

      jsonResult = dateHandler.GetJsonRepresentation();
      jsonResult = ReplaceUnnecessaryChars(jsonResult);

      Assert.IsNotNull(dateHandler);
      Assert.AreEqual(expectedJson, jsonResult);
    }

    [Test()]

    public void DeserializeCommitDateTest()
    {
      var wrongString = "wrong message string";
      var resultNull = CommitDateHandler.DeserializeCommitDate(null);
      var resultWrongString = CommitDateHandler.DeserializeCommitDate(wrongString);

      Assert.IsNull(resultNull);
      Assert.IsNull(resultWrongString);
    }


    private string ReplaceUnnecessaryChars(string jsonString)
    {
      jsonString = jsonString.Replace(" ", "");
      jsonString = jsonString.Replace("\r\n", "");
      return jsonString.Replace("\"", "");
    }

    private void SetUpTestBranches()
    {
      var stdFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample02.xml");
      var compFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample01.xml");
      var stdValueCommit = new StandardDataCommit(compDataManagement.CurrentRepository, stdFilePath);
      var compCommit = new ComponentDataCommit(compDataManagement.CurrentRepository, compFilePath, null, null);

      stdValueCommit.VerificationCheckBeforeCommit();
      compCommit.VerificationCheckBeforeCommit();

      var stdValueCommitResult = stdValueCommit.ExecuteCommit();
      if (stdValueCommitResult != null)
        stdBranchName = stdValueCommitResult.FriendlyName;

      var compCommitResult = compCommit.ExecuteCommit();
      if (compCommitResult != null)
        compBranchName = compCommitResult.FriendlyName;
    }


  }
}