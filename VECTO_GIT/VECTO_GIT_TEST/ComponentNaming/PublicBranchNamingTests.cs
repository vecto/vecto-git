﻿using NUnit.Framework;
using System;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT_TEST;


namespace VECTO_GIT.ComponentNaming.Tests
{
  [TestFixture()]
  [Category("PublicBranchNaming")]
  public class PublicBranchNamingTests
  {
    private string filePath;
    private string fileName;
    private ComponentDataManagement compDataManagement;

    [OneTimeSetUp]
    public void OneTimeInit()
    {
      fileName = "vecto_engine_date_sample01.xml";
      filePath = TestHelper.GetTestFilePath(fileName);
      compDataManagement = new ComponentDataManagement();
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      SetUpTestBranch();
    }


    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
    }



    [Test()]
    public void PublicBranchNamingConstructorTest01()
    {
      var manufacturer = "manufacturer";
      var model = "model";
      var year = "year";
      var xmlHash = "124243kkskskasfll";

      var branchNameParts = new PublicBranchNameParts(manufacturer, model, year, xmlHash);

      Assert.Throws<ArgumentNullException>(() => new PublicBranchNaming((PublicBranchNameParts)null));

      Assert.Throws<ArgumentNullException>(() => new PublicBranchNameParts(null, model, year, xmlHash));
      Assert.Throws<ArgumentNullException>(() => new PublicBranchNameParts(manufacturer, null, year, xmlHash));
      Assert.Throws<ArgumentNullException>(() => new PublicBranchNameParts(manufacturer, model, null, xmlHash));
      Assert.Throws<ArgumentNullException>(() => new PublicBranchNameParts(manufacturer, model, year, null));


      Assert.Throws<ArgumentException>(() => new PublicBranchNameParts("", model, year, xmlHash));
      Assert.Throws<ArgumentException>(() => new PublicBranchNameParts(manufacturer, "", year, xmlHash));
      Assert.Throws<ArgumentException>(() => new PublicBranchNameParts(manufacturer, model, "", xmlHash));
      Assert.Throws<ArgumentException>(() => new PublicBranchNameParts(manufacturer, model, year, ""));

      Assert.DoesNotThrow(() => new PublicBranchNaming(branchNameParts));
    }

    [Test()]
    public void PublicBranchNamingContstructorTest02()
    {
      var branchName = string.Format("{0}/{1}",
        PublicBranchNaming.TREE_NAME, "some/test/branch/name/lakjsdflklallasdfl");

      Assert.Throws<ArgumentNullException>(() => new PublicBranchNaming((string)null));
      Assert.Throws<ArgumentException>(() => new PublicBranchNaming(""));
      Assert.DoesNotThrow(() => new PublicBranchNaming(branchName));
    }

    [Test()]
    public void DeserializeBranchNameTest()
    {
      var xmlReader = new XmlContentReader();
      var branchNameHandler = xmlReader.ReadOutBranchNameData(filePath);
      var expectedBranchName = PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_1/abcdhksp0lxv";
      var certTagName = xmlReader.ReadOutCertificateId(filePath).GetTagGitPathName();

      var branchName = branchNameHandler.GetGitPathBranchName();
      var branch = compDataManagement.CurrentRepository.Branches[branchName];
      var gitTagName = new GitIdHandler(branch).GetTagGitPathName();

      var certTag = compDataManagement.CurrentRepository.Tags[certTagName];
      var gitTag = compDataManagement.CurrentRepository.Tags[gitTagName];


      var deserializeNameCertTag = PublicBranchNaming.DeserializeBranchName(certTag.Annotation.Message);
      var deserializeNameGitTag = PublicBranchNaming.DeserializeBranchName(gitTag.Annotation.Message);

      Assert.AreEqual(expectedBranchName, deserializeNameCertTag.GetGitPathBranchName());
      Assert.AreEqual(expectedBranchName, deserializeNameGitTag.GetGitPathBranchName());
      Assert.AreEqual(branchName, deserializeNameGitTag.GetGitPathBranchName());
      Assert.AreEqual(branchName, deserializeNameCertTag.GetGitPathBranchName());
    }

    [Test()]
    public void GetJsonRepesentationTest()
    {
      var xmlReader = new XmlContentReader();
      var branchNameHandler = xmlReader.ReadOutBranchNameData(filePath);
      var expectedJson =
        "{\"Manufacturer\":\"Manufacturer_MAN\"," +
        "\"Year\":\"2017\"," +
        "\"Model\":\"MAN_40t_Truck_Engine_1\"," +
        "\"XmlHash\":\"abcdhksp0lxv\"}";

      var json = branchNameHandler.GetJsonRepesentation();

      Assert.AreEqual(expectedJson, json);

    }

    [Test()]
    public void GetGitPathBranchNameTest()
    {
      var xmlReader = new XmlContentReader();
      var branchNameHandler = xmlReader.ReadOutBranchNameData(filePath);
      var expectedBranchName = PublicBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_1/abcdhksp0lxv";

      Assert.AreEqual(expectedBranchName, branchNameHandler.GetGitPathBranchName());
    }

    [Test()]
    public void GetGitPrivateBranchNameTest()
    {
      var xmlReader = new XmlContentReader();
      var branchNameHandler = xmlReader.ReadOutBranchNameData(filePath);
      var expectedBranchName = PrivateBranchNaming.TREE_NAME + "/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_1";

      Assert.AreEqual(expectedBranchName, branchNameHandler.GetGitPrivateBranchName());
    }

    [Test()]
    public void GetComponentBranchNameTest()
    {
      var xmlReader = new XmlContentReader();
      var branchNameHandler = xmlReader.ReadOutBranchNameData(filePath);
      var expectedBranchName = "Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_1";

      Assert.AreEqual(expectedBranchName, branchNameHandler.GetComponentBranchName());
    }

    [Test()]
    public void OperatorTest()
    {
      var branchNaming01 = new PublicBranchNaming(
        new PublicBranchNameParts("Manufacturer", "Model", "2018", "11d50bc79f960e444933e91451ec49264ade20d2"));
      var branchNaming02 = new PublicBranchNaming(
        new PublicBranchNameParts("Manufacturer", "Model", "2018", "11d50bc79f960e444933e91451ec49264ade20d2"));
      var branchNaming03 = new PublicBranchNaming(
        new PublicBranchNameParts("Manufacturer", "Model03", "2018", "11d50bc79f960e444933e91451ec49264ade20d2"));
      
      Assert.IsTrue(branchNaming01 == branchNaming02);
      Assert.IsFalse(branchNaming01 != branchNaming02);
    }

    [Test()]
    public void ParseErrorTest()
    {
      var branchName = "some/wrong/branch/name";

      Assert.Throws<ArgumentException>(() => new PublicBranchNaming(branchName));
    }
    
    private void SetUpTestBranch()
    {
      var compFilePath = TestHelper.GetTestFilePath("vecto_engine_date_sample01.xml");
      var compDataCommit = new ComponentDataCommit(compDataManagement.CurrentRepository, compFilePath, null, null);

      compDataCommit.VerificationCheckBeforeCommit();

      compDataCommit.ExecuteCommit();
    }
  }
}