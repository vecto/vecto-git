﻿using LibGit2Sharp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VECTO_GIT.GUI.Model;
using VECTO_GIT_TEST;


namespace VECTO_GIT.ComponentSearch.Tests
{
  [TestFixture()]
  [Category("SearchVectoComponent")]
  public class SearchVectoComponentTests
  {
    private ComponentDataManagement compDataManagement;
    private Repository repository;
    private List<string> branchNames;
    private List<string> mFiles;
    private List<string> uFiles;

    [OneTimeSetUp()]
    public void OneTimeInit()
    {
      compDataManagement = new ComponentDataManagement();
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      repository = compDataManagement.CurrentRepository;
      SetBranchNames();
      SetMeasurementFiles();
      SetUserFiles();
      SetUpTestBranches();
    }

    [OneTimeTearDown]
    public void OneTimeCleanUp()
    {
      compDataManagement.Dispose();
      TestHelper.DeleteRepository(RepositoryType.Source);
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
    }

    [Test()]
    public void SearchVectoComponentConstructorTest()
    {
      var searchVecto = new SearchVectoComponent(repository);
      Assert.Throws<ArgumentNullException>(() => new SearchVectoComponent(null));
      Assert.NotNull(searchVecto);
    }


    [Test()]
    public void SearchVectoComponentWithManufacturerTermTest()
    {
      var manufacturerName = "Manufacturer MAN";

      var searchTerms = new VectoSearchTerms
      {
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.AreNotEqual(Enumerable.Empty<VectoComponent>(), result);
      Assert.AreEqual(5, result.Count());

      //1st vecto component
      var component = GetVectoComponentByBranchName(branchNames[4], result);
      var componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample01.xml", false);
      var certificateNumber = "e12*0815/8051*2017/05*e*0000*00";
      var certificateDate = new DateTime(2017, 2, 17, 11, 00, 00);

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.CertificateFile);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.NotNull(component.ComponentData);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);


      //2nd vecto component
      component = GetVectoComponentByBranchName(branchNames[5], result);
      componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample02.xml", false);
      certificateNumber = "e12*0816/8051*2017/05*e*0000*00";
      certificateDate = new DateTime(2017, 2, 19, 05, 00, 00);

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.CertificateFile);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.NotNull(component.ComponentData);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);

      //3rd vecto component
      component = GetVectoComponentByBranchName(branchNames[6], result);
      componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample03.xml", false);
      certificateNumber = "e12*0818/8051*2017/05*e*0000*00";
      certificateDate = new DateTime(2017, 2, 18, 21, 00, 00);

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.PrivateBranch);
      Assert.AreEqual(branchNames[1], component.PrivateBranch.FriendlyName);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.CertificateFile);
      Assert.NotNull(component.MeasurementData);
      Assert.IsTrue(ContainsExpectedData(mFiles, component.MeasurementData));
      Assert.NotNull(component.UserData);
      Assert.IsTrue(ContainsExpectedData(uFiles, component.UserData));
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.NotNull(component.ComponentData);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);


      //4th vecto component
      component = GetVectoComponentByBranchName(branchNames[7], result);
      componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample04.xml", false);
      certificateNumber = "e12*0818/8052*2017/05*e*0000*00";
      certificateDate = new DateTime(2017, 2, 18, 20, 00, 00);
      var certificateHash = TestHelper.GetGitHashOfData("VectoInputDeclaration.1.0.pdf", true);

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNotNull(component.CertificateFile);
      Assert.AreEqual(certificateHash, component.CertificateFile.Sha);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.NotNull(component.ComponentData);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);


      //5th vecto component
      component = GetVectoComponentByBranchName(branchNames[8], result);
      componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample05.xml", false);
      certificateNumber = "e12*0818/8052*2017/05*e*0000*00";
      certificateDate = new DateTime(2017, 3, 19, 13, 00, 00);

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNotNull(component.CertificateFile);
      Assert.AreEqual(certificateHash, component.CertificateFile.Sha);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.NotNull(component.ComponentData);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);
    }

    [Test()]
    public void SearchVectoComponentWithModelTermTest()
    {
      var modelName = "Generic 40t Long Haul Truck Engine";

      var searchTerms = new VectoSearchTerms
      {
        Model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower()
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      var component = GetVectoComponentByBranchName(branchNames[2], result);
      var componentFileHash = TestHelper.GetGitHashOfData("vecto_engine-sample.xml", false);
      var stdComponentFileHash = TestHelper.GetGitHashOfData("vecto_std_engine-sample.xml", false);
      var certificateNumber = "e12*0815/8051*2017/05*e*0000*00";
      var certificateDate = new DateTime(2017, 2, 15, 11, 00, 00);

      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.PrivateBranch);
      Assert.AreEqual(branchNames[0], component.PrivateBranch.FriendlyName);
      Assert.IsNull(component.MeasurementData);
      Assert.NotNull(component.UserData);
      Assert.IsTrue(ContainsExpectedData(uFiles, component.UserData));

      Assert.NotNull(component.StandardValues);
      Assert.AreEqual(stdComponentFileHash, component.StandardValues.Sha);
      Assert.IsNull(component.CertificateFile);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.NotNull(component.ComponentData);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);
    }


    [Test()]
    public void SearchVectoComponentWithCertificateNumberTest()
    {
      var certNumberModel = new CertificationNumberModel
      {
        Country = "12",
        CertificationAct = "0818/8052",
        AmendingAct = "2017/05",
        ComponentType = "E",
        BaseNumber = "0000",
        ExtensionNumber = "00"
      };

      var searchTerms = new VectoSearchTerms
      {
        CertificationNumber = certNumberModel
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      var component = GetVectoComponentByBranchName(branchNames[7], result);
      var componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample04.xml", false);
      var certFileHash = TestHelper.GetGitHashOfData("VectoInputDeclaration.1.0.pdf", true);
      var certificateNumber = "e12*0818/8052*2017/05*e*0000*00";
      var certificateDate = new DateTime(2017, 2, 18, 20, 00, 00);

      Assert.AreEqual(2, result.Count());

      //component 4
      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.CertificateFile);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);
      Assert.AreEqual(certFileHash, component.CertificateFile.Sha);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);

      //component 5 with same certificate number
      component = GetVectoComponentByBranchName(branchNames[8], result);
      componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample05.xml", false);
      certificateDate = new DateTime(2017, 3, 19, 13, 00, 00);

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certFileHash, component.CertificateFile.Sha);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
    }

    [Test()]
    public void SearchVectoComponentWithGitIdTest()
    {
      var gitId = TestHelper.GetGitHashOfData("vecto_engine_date_sample03.xml", false);
      var searchTerms = new VectoSearchTerms
      {
        GitID = gitId
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var component = GetVectoComponentByBranchName(branchNames[6], result);
      var certificateNumber = "e12*0818/8051*2017/05*e*0000*00";
      var certificateDate = new DateTime(2017, 2, 18, 21, 00, 00);

      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.PrivateBranch);
      Assert.NotNull(component.MeasurementData);
      Assert.NotNull(component.UserData);

      Assert.AreEqual(gitId, component.ComponentData.Sha);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.IsTrue(ContainsExpectedData(mFiles, component.MeasurementData));
      Assert.IsTrue(ContainsExpectedData(uFiles, component.UserData));

      Assert.IsNull(component.CertificateFile);
      Assert.IsNull(component.StandardValues);
    }

    [Test()]
    public void SearchVectoComponentWithGitIdAndCertificatNumberTest()
    {
      var gitId = TestHelper.GetGitHashOfData("vecto_engine_date_sample05.xml", false);
      var certificateNumber = "e12*0818/8052*2017/05*e*0000*00";

      var certNumberModel = new CertificationNumberModel
      {
        Country = "12",
        CertificationAct = "0818/8052",
        AmendingAct = "2017/05",
        ComponentType = "E",
        BaseNumber = "0000",
        ExtensionNumber = "00"
      };




      var searchTerms = new VectoSearchTerms
      {
        GitID = gitId,
        CertificationNumber = certNumberModel
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var component = GetVectoComponentByBranchName(branchNames[8], result);
      var certificateDate = new DateTime(2017, 3, 19, 13, 00, 00);
      var certFileHash = TestHelper.GetGitHashOfData("VectoInputDeclaration.1.0.pdf", true);

      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.ComponentData);
      Assert.IsNotNull(component.CertificateFile);
      Assert.AreEqual(certFileHash, component.CertificateFile.Sha);
      Assert.AreEqual(gitId, component.ComponentData.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
    }

    [Test()]
    public void SearchVectoComponentWithCertificatNumberAndModelTest()
    {
      var certificateNumber = "e12*0818/8052*2017/05*e*0000*00";
      var certNumberModel = new CertificationNumberModel
      {
        Country = "12",
        CertificationAct = "0818/8052",
        AmendingAct = "2017/05",
        ComponentType = "e",
        BaseNumber = "0000",
        ExtensionNumber = "00"
      };

      var modelName = "MAN 40t Truck Engine 4";

      var searchTerms = new VectoSearchTerms
      {
        CertificationNumber = certNumberModel,
        Model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower()
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample04.xml", false);
      var component = GetVectoComponentByBranchName(branchNames[7], result);
      var certificateDate = new DateTime(2017, 2, 18, 20, 00, 00);
      var certFileHash = TestHelper.GetGitHashOfData("VectoInputDeclaration.1.0.pdf", true);

      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.ComponentData);
      Assert.IsNotNull(component.CertificateFile);
      Assert.AreEqual(certFileHash, component.CertificateFile.Sha);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
    }


    [Test()]
    public void SearchVectoComponentWithCertificatNumberAndModelManufacturerTest()
    {
      var certificateNumber = "e12*0818/8052*2017/05*e*0000*00";
      var certNumberModel = new CertificationNumberModel
      {
        Country = "12",
        CertificationAct = "0818/8052",
        AmendingAct = "2017/05",
        ComponentType = "e",
        BaseNumber = "0000",
        ExtensionNumber = "00"
      };

      var modelName = "MAN 40t Truck Engine 5";
      var manufacturerName = "Manufacturer MAN";

      var searchTerms = new VectoSearchTerms
      {
        CertificationNumber = certNumberModel,
        Model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower(),
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var componentFileHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample05.xml", false);
      var component = GetVectoComponentByBranchName(branchNames[8], result);
      var certificateDate = new DateTime(2017, 3, 19, 13, 00, 00);
      var certFileHash = TestHelper.GetGitHashOfData("VectoInputDeclaration.1.0.pdf", true);

      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.ComponentData);
      Assert.NotNull(component.CertificateFile);

      Assert.AreEqual(certFileHash, component.CertificateFile.Sha);
      Assert.AreEqual(componentFileHash, component.ComponentData.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);

      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
    }

    [Test()]
    public void SearchVectoComponentWithGitIdAndManufacturerTest()
    {
      var gitId = TestHelper.GetGitHashOfData("vecto_engine_date_sample02.xml", false);
      var manufacturerName = "Manufacturer MAN";

      var searchTerms = new VectoSearchTerms
      {
        GitID = gitId,
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var component = GetVectoComponentByBranchName(branchNames[5], result);
      var certificateDate = new DateTime(2017, 2, 19, 05, 00, 00);
      var certificateNumber = "e12*0816/8051*2017/05*e*0000*00";

      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.ComponentData);

      Assert.AreEqual(gitId, component.ComponentData.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);

      Assert.IsNull(component.CertificateFile);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
    }


    [Test()]
    public void SearchVectoComponentWithDateAndManufacturerTest()
    {
      var certDate = new DateTime(2017, 2, 17);
      var manufacturerName = "Manufacturer MAN";

      var searchTerms = new VectoSearchTerms
      {
        CertificateDate = certDate,
        IsCertificateDate = true,
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var componentHash = TestHelper.GetGitHashOfData("vecto_engine_date_sample01.xml", false);
      var component = GetVectoComponentByBranchName(branchNames[4], result);
      var certificateDate = new DateTime(2017, 2, 17, 11, 00, 00);
      var certificateNumber = "e12*0815/8051*2017/05*e*0000*00";


      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.ComponentData);

      Assert.AreEqual(componentHash, component.ComponentData.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);

      Assert.IsNull(component.CertificateFile);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNull(component.StandardValues);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.UserData);
    }


    [Test()]
    public void SearchVectoComponentWithDateTest()
    {
      var certDate = new DateTime(2017, 2, 15);
      var searchTerms = new VectoSearchTerms
      {
        IsCertificateDate = true,
        CertificateDate = certDate,
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var componentHash = TestHelper.GetGitHashOfData("vecto_engine-sample.xml", false);
      var stdComponentHash = TestHelper.GetGitHashOfData("vecto_std_engine-sample.xml", false);
      var component = GetVectoComponentByBranchName(branchNames[2], result);
      var certificateDate = new DateTime(2017, 2, 15, 11, 00, 00);
      var certificateNumber = "e12*0815/8051*2017/05*e*0000*00";


      Assert.AreEqual(1, result.Count());

      Assert.NotNull(component);
      Assert.NotNull(component.PrivateBranch);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.ComponentData);
      Assert.NotNull(component.StandardValues);
      Assert.NotNull(component.UserData);

      Assert.AreEqual(componentHash, component.ComponentData.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.IsTrue(ContainsExpectedData(uFiles, component.UserData));

      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.CertificateFile);
    }

    [Test()]
    public void SearchVectoComponentWithoutResultTest()
    {
      var gitId = TestHelper.GetGitHashOfData("vecto_engine_date_sample03.xml", false);
      //vecto_engine_date_sample01.xml certificate number
      var certNumber = "e12*0815/8051*2017/05*E*0000*00";
      //vecto_engine-sample.xml certificate date
      var certDate = new DateTime(2017, 2, 15, 11, 0, 0);
      //vecto_engine_date_sample04.xml
      var modelName = "MAN 40t Truck Engine 4";
      //vecto_gearbox-sample.xml
      var manufacturerName = "Generic Engine Manufacturer";

      var searchTerms = new VectoSearchTerms
      {
        GitID = gitId,
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsNotEmpty(result);
      Assert.AreEqual(1, result.Count());

      var certNumberModel = new CertificationNumberModel
      {
        Country = "12",
        CertificationAct = "0815/8051",
        AmendingAct = "2017/05",
        ComponentType = "e",
        BaseNumber = "0000",
        ExtensionNumber = "00"
      };


      searchTerms = new VectoSearchTerms
      {
        CertificationNumber = certNumberModel
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsNotEmpty(result);
      Assert.AreEqual(2, result.Count());


      searchTerms = new VectoSearchTerms
      {
        IsCertificateDate = true,
        CertificateDate = certDate
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsNotEmpty(result);
      Assert.AreEqual(1, result.Count());


      searchTerms = new VectoSearchTerms
      {
        Model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower()
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsNotEmpty(result);
      Assert.AreEqual(1, result.Count());


      searchTerms = new VectoSearchTerms
      {
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);


      Assert.IsNotEmpty(result);
      Assert.AreEqual(1, result.Count());

      // following test shouldn't deliver any results
      searchTerms = new VectoSearchTerms
      {
        GitID = gitId,
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsEmpty(result);


      searchTerms = new VectoSearchTerms
      {
        CertificationNumber = certNumberModel,
        GitID = gitId
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsEmpty(result);



      searchTerms = new VectoSearchTerms
      {
        CertificationNumber = certNumberModel,
        Model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower()
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsEmpty(result);


      searchTerms = new VectoSearchTerms
      {
        CertificateDate = certDate,
        IsCertificateDate = true,
        Model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower()
      };

      command = new SearchVectoComponent(repository);
      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      Assert.IsEmpty(result);
    }


    [Test()]
    public void SearchVectoComponentIndependetStandardValuesTest()
    {
      var certDate = new DateTime(2018, 01, 11);
      var searchTerms = new VectoSearchTerms
      {
        IsCertificateDate = true,
        CertificateDate = certDate
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms);
      var stdValuesHash = TestHelper.GetGitHashOfData("vecto_gearbox-sample_02.xml", false);
      var component = GetVectoComponentByBranchName(branchNames[9], result);

      Assert.AreEqual(1, result.Count());

      //Standard values only vecto component

      Assert.NotNull(component);
      Assert.NotNull(component.StandardValues);

      Assert.AreEqual(certDate.Date, component.CertificateDate.Date);
      Assert.AreEqual(stdValuesHash, component.StandardValues.Sha);

      Assert.IsNull(component.CertificateNumber);
      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.PrivateBranch);
      Assert.IsNotNull(component.PublicBranch);
      Assert.IsNull(component.ComponentData);
      Assert.IsNull(component.UserData);
      Assert.IsNull(component.CertificateFile);



      var manufacturerName = "Generic Engine Manufacturer";
      searchTerms = new VectoSearchTerms
      {
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      result = command.SearchVectoComponentWithGivenTerms(searchTerms);

      var componentHash = TestHelper.GetGitHashOfData("vecto_engine-sample.xml", false);

      stdValuesHash = TestHelper.GetGitHashOfData("vecto_std_engine-sample.xml", false);
      var certificateDate = new DateTime(2017, 2, 15, 11, 00, 00);
      var certificateNumber = "e12*0815/8051*2017/05*e*0000*00";

      Assert.AreEqual(1, result.Count());
      component = result.First();

      Assert.NotNull(component);
      Assert.NotNull(component.StandardValues);
      Assert.NotNull(component.PublicBranch);
      Assert.NotNull(component.CertificateNumber);
      Assert.NotNull(component.PrivateBranch);
      Assert.NotNull(component.ComponentData);
      Assert.NotNull(component.UserData);

      Assert.AreEqual(componentHash, component.GetPublicGitId);
      Assert.AreEqual(stdValuesHash, component.StandardValues.Sha);
      Assert.AreEqual(certificateNumber, component.CertificateNumber);
      Assert.AreEqual(certificateDate, component.CertificateDate);
      Assert.IsTrue(ContainsExpectedData(uFiles, component.UserData));

      Assert.IsNull(component.MeasurementData);
      Assert.IsNull(component.CertificateFile);
    }


    [Test()]
    public void SearchAllComponentsTest()
    {
      var command = new SearchVectoComponent(repository);
      var result = command.SearchAllVectoComponents();

      Assert.AreEqual(7, result.Count());
    }

    [Test()]
    public void TransferPropertiesTest()
    {
      var certDate = DateTime.UtcNow;
      var searchTerms = new VectoSearchTerms
      {
        Model = "generic_40t_long_haul_truck_engine",
        Manufacturer = "generic_engine_manufacturer"
      };

      var command = new SearchVectoComponent(repository);
      var result = command.SearchVectoComponentWithGivenTerms(searchTerms).First();

      result.TransferPrivateBranch = true;

      Assert.IsTrue(result.TransferPrivateBranch);
      Assert.IsFalse(result.TransferPublicBranch);
      Assert.IsFalse(result.TransferStandardValuesBranch);

      result.TransferPrivateBranch = true;
      result.TransferPublicBranch = true;

      Assert.IsTrue(result.TransferPrivateBranch);
      Assert.IsTrue(result.TransferPublicBranch);
      Assert.IsFalse(result.TransferStandardValuesBranch);


      result.TransferPrivateBranch = true;
      result.TransferPublicBranch = true;
      result.TransferStandardValuesBranch = true;

      Assert.IsTrue(result.TransferPrivateBranch);
      Assert.IsTrue(result.TransferPublicBranch);
      Assert.IsTrue(result.TransferStandardValuesBranch);
    }

    private bool ContainsExpectedData(List<string> expectedFilePaths, List<Item> data)
    {

      var hashes = new Dictionary<string, string>();
      foreach (var filePath in expectedFilePaths)
      {
        var fileName = Path.GetFileName(filePath);
        hashes[fileName] = TestHelper.GetGitHashOfData(fileName, false);
      }

      foreach (var entry in data)
      {
        if (hashes.ContainsKey(entry.Filename))
        {
          if (hashes[entry.Filename] != entry.FilePath)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }
      return true;

    }

    private VectoComponent GetVectoComponentByBranchName(string branchName,
      IEnumerable<VectoComponent> components)
    {
      foreach (var vectoComp in components)
      {
        if (vectoComp.PublicBranch != null &&
          branchName == vectoComp.PublicBranch.FriendlyName)
        {
          return vectoComp;
        }

        if (vectoComp.StandardValues != null && vectoComp.PublicBranch != null &&
          branchName == vectoComp.PublicBranch?.FriendlyName)
        {
          return vectoComp;
        }
      }

      return null;
    }


    private void SetUpTestBranches()
    {
      var certPath = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");

      for (int i = 1; i <= 5; i++)
      {
        var fileName = string.Format("vecto_engine_date_sample0{0}.xml", i);
        var filePath = TestHelper.GetTestFilePath(fileName);

        if (TestHelper.SaveAsComponentData(repository, filePath) == null)
        {
          throw new Exception("SetUpTestBranches() failure");
        }
      }

      var compFile04 = TestHelper.GetTestFilePath("vecto_engine_date_sample04.xml");
      TestHelper.SaveCertificateByComponentFile(repository, certPath, compFile04);

      var compPath03 = TestHelper.GetTestFilePath("vecto_engine_date_sample03.xml");
      TestHelper.SaveMeasurementDataByComponentFile(repository, mFiles, compPath03);
      TestHelper.SaveUserDataByComponentFile(repository, uFiles, compPath03);

      var enginePath = TestHelper.GetTestFilePath("vecto_engine-sample.xml");
      var stdFilePath = TestHelper.GetTestFilePath("vecto_std_engine-sample.xml");
      TestHelper.SaveAsComponentData(repository, stdFilePath, enginePath);
      TestHelper.SaveUserDataByComponentFile(repository, uFiles, enginePath);

      var independentStdFilePath = TestHelper.GetTestFilePath("vecto_gearbox-sample_02.xml");
      TestHelper.SaveStandardValueFile(repository, independentStdFilePath);
    }

    private void SetBranchNames()
    {
      branchNames = new List<string>
      {
        "private/Generic_Engine_Manufacturer/component/2017/Generic_40t_Long_Haul_Truck_Engine",
        "private/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_3",
        "public/Generic_Engine_Manufacturer/component/2017/Generic_40t_Long_Haul_Truck_Engine/jwewzksp0lxv",
        "public/Generic_Engine_Manufacturer/component/2018/Generic_40t_Long_Haul_Truck_Engine/hgtklksp0lxv",
        "public/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_1/abcdhksp0lxv",
        "public/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_2/abeflksp0lxv",
        "public/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_3/abghkosp0lxv",
        "public/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_4/abgefksp0lxv",
        "public/Manufacturer_MAN/component/2017/MAN_40t_Truck_Engine_5/abglpksp0lxv",
        "public/Generic_Gearbox_Manufacturer/component/2018/Generic_40t_Long_Haul_Truck_Gearbox/vgf71ha02yhu"
      };
    }

    private void SetMeasurementFiles()
    {
      mFiles = new List<string>
      {
         TestHelper.GetTestFilePath("VectoComponent.xsd"),
         TestHelper.GetTestFilePath("VectoDeclarationDefinitions.1.0.xsd")
      };
    }

    private void SetUserFiles()
    {
      uFiles = new List<string>
      {
         TestHelper.GetTestFilePath("VectoInput.xsd"),
         TestHelper.GetTestFilePath("xmldsig-core-schema.xsd")
      };
    }


  }
}