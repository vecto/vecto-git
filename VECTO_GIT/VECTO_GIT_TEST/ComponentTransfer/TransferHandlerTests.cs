﻿using LibGit2Sharp;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.Model;
using VECTO_GIT_TEST;


namespace VECTO_GIT.ComponentTransfer.Tests
{
  [TestFixture()]
  [Category("ComponentTransfer")]
  public class TransferHandlerTests
  {
    private ComponentDataManagement compDataManagement;
    private Repository repository;
    private Repository destRepository;
    private TransferHandler transferHandler;

    private List<Branch> pubBranches;
    private List<Branch> prvBranches;
    private string archiveFilePathDest;
    private List<VectoComponent> vectoComponents;

    [OneTimeSetUp()]
    public void OneTimeInit()
    {
      transferHandler = new TransferHandler();
      SetSourceRepository();
      SetupTestBranches();
      archiveFilePathDest = Path.Combine(TestHelper.DES_REPO_PATH + @"\..\", "repoArchive.zip");
    }

    [OneTimeTearDown()]
    public void OneTimeCleanUp()
    {
      if (File.Exists(archiveFilePathDest))
      {
        File.Delete(archiveFilePathDest);
      }
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);
    }

    [TearDown()]
    public void CleanUp()
    {
      ResetVectoComponentTransferSelection();
      if (destRepository != null)
      {
        destRepository.Dispose();
        destRepository = null;
      }
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);

      if (File.Exists(archiveFilePathDest))
      {
        File.Delete(archiveFilePathDest);
      }
    }
    
    [Test()]
    public void TransferHandlerConstructorTest()
    {
      var transferHandler = new TransferHandler();
      Assert.NotNull(transferHandler);
    }
    

    [Test()]
    public void MergeExtractedArchiveIntoRepositoryTest()
    {
      vectoComponents[0].TransferPrivateBranch = true;
      vectoComponents[0].TransferPublicBranch = true;

      var archiveFilePath = GetTransferArchive(archiveFilePathDest);
      destRepository = TestHelper.InitEmptyRepository(TestHelper.DES_REPO_PATH, true);
      var extractPath = transferHandler.ExtractArchiveIntoFolder(TestHelper.DES_REPO_PATH, archiveFilePath);
      var importResult = transferHandler.MergeExtractedArchiveIntoRepository(destRepository, extractPath);

      Assert.IsNotNull(archiveFilePath);
      Assert.IsNotNull(destRepository);
      Assert.IsNotNull(extractPath);
      Assert.IsNull(importResult);
      Assert.AreEqual(2, destRepository.Branches.Count());
      Assert.AreEqual(archiveFilePath, transferHandler.CreatedArchivePath);
      Assert.IsTrue(BranchAreEqual(vectoComponents[0].PublicBranch, destRepository.Branches[vectoComponents[0].PublicBranch.FriendlyName]));
      Assert.IsTrue(BranchAreEqual(vectoComponents[0].PrivateBranch, destRepository.Branches[vectoComponents[0].PrivateBranch.FriendlyName]));
    }

    [Test()]
    public void MergeStandardBranchIntoRepositoryTest()
    {
      vectoComponents[1].TransferStandardValuesBranch = true;

      var archiveFilePath = GetTransferArchive(archiveFilePathDest);
      destRepository = TestHelper.InitEmptyRepository(TestHelper.DES_REPO_PATH, true);
      var extractPath = transferHandler.ExtractArchiveIntoFolder(TestHelper.DES_REPO_PATH, archiveFilePath);
      var importResult = transferHandler.MergeExtractedArchiveIntoRepository(destRepository, extractPath);

      Assert.IsNotNull(archiveFilePath);
      Assert.IsNotNull(destRepository);
      Assert.IsNotNull(extractPath);
      Assert.IsNull(importResult);
      Assert.AreEqual(pubBranches.Count + prvBranches.Count, repository.Branches.Count());
      Assert.AreEqual(1, destRepository.Branches.Count());
    }

    [Test()]
    public void MergeConflictsIntoRepositoryTest()
    {
      vectoComponents[1].TransferPrivateBranch = true;
      
      var archiveFilePath = GetTransferArchive(archiveFilePathDest);
      destRepository = TestHelper.InitEmptyRepository(TestHelper.DES_REPO_PATH, true);
      var extractPath = transferHandler.ExtractArchiveIntoFolder(TestHelper.DES_REPO_PATH, archiveFilePath);
      var importResult = transferHandler.MergeExtractedArchiveIntoRepository(destRepository, extractPath);

      var mergeResult = transferHandler.MergeConflictsIntoRepository(destRepository, extractPath, new List<MergePrivateBranchModel>());
      Assert.IsNull(importResult);
      Assert.IsFalse(mergeResult);
      Assert.IsNotNull(transferHandler.MergeMessages);
      Assert.AreEqual(1, destRepository.Branches.Count());
      Assert.IsTrue(BranchAreEqual(vectoComponents[1].PrivateBranch, destRepository.Branches[vectoComponents[1].PrivateBranch.FriendlyName]));
    }
    

    [Test()]
    public void TransferErrorTest()
    {
      vectoComponents[2].TransferPublicBranch = true;
      var archiveFilePath = GetTransferArchive(Path.Combine(archiveFilePathDest, "wrongFolder"));
      var mergeConflict = transferHandler.MergeConflictsIntoRepository(repository, null, null);
      var mergeResult = transferHandler.MergeExtractedArchiveIntoRepository(repository, null);
      var downloadResult = transferHandler.DownloadArchiveForTransfer(repository, null, TestHelper.DES_REPO_PATH);
      
      Assert.Throws<ArgumentNullException>(() => transferHandler.ExtractArchiveIntoFolder(null, null));
      Assert.IsNull(transferHandler.ExtractArchiveIntoFolder(TestHelper.DES_REPO_PATH, null));
      Assert.IsNull(archiveFilePath);
      Assert.IsNull(mergeResult);
      Assert.IsFalse(downloadResult);
      Assert.IsFalse(mergeConflict);
      Assert.IsTrue(transferHandler.ErrorMessages != null && transferHandler.ErrorMessages.Any());
    }


    private bool BranchAreEqual(Branch originBranch, Branch copyBranch)
    {
      if (originBranch.FriendlyName != copyBranch.FriendlyName)
      {
        return false;
      }
      if (originBranch.Tip.Sha != copyBranch.Tip.Sha)
      {
        return false;
      }
      if (originBranch.Commits.Count() != copyBranch.Commits.Count())
      {
        return false;
      }

      var originCommits = originBranch.Commits.ToList();
      var copyCommits = copyBranch.Commits.ToList();

      for (int i = 0; i < originCommits.Count; i++)
      {
        if (copyCommits[i].Sha != originCommits[i].Sha)
        {
          return false;
        }
        if (copyCommits[i].Tree != originCommits[i].Tree)
        {
          return false;
        }
        if (copyCommits[i].Tree.Count != originCommits[i].Tree.Count)
        {
          return false;
        }

        if (copyCommits[i].Message != originCommits[i].Message)
        {
          return false;
        }
      }
      return true;
    }


    private void SetSourceRepository()
    {
      compDataManagement = new ComponentDataManagement();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      repository = compDataManagement.CurrentRepository;
    }

    private string GetTransferArchive(string archiveDest)
    {
      var compTransfer = new List<ComponentForTransfer>();

      for (int i = 0; i < vectoComponents.Count; i++)
      {
        var vectoComponent = vectoComponents[i];
        if (vectoComponent.TransferPrivateBranch ||
            vectoComponent.TransferPublicBranch ||
            vectoComponent.TransferStandardValuesBranch)
        {
          var transfer = new ComponentForTransfer();
          transfer.SetComponentForTransferContent(vectoComponent, vectoComponent.TransferPublicBranch,
            vectoComponent.TransferPrivateBranch, vectoComponent.TransferStandardValuesBranch);

          compTransfer.Add(transfer);
        }
      }

      var downloadResult = transferHandler.DownloadArchiveForTransfer(repository, compTransfer, archiveDest);

      if (!downloadResult)
      {
        return null;
      }

      return archiveDest;
    }

    private void SetupTestBranches()
    {
      SetPublicBranches();
      SetPrivateBranches();
      SetVectoComponents(pubBranches, prvBranches);
    }


    private void SetPublicBranches()
    {
      pubBranches = new List<Branch>();

      for (int i = 1; i < 4; i++)
      {
        var compFilePath = TestHelper.GetFurtherTestFilePath($"vecto_engine_date_sample0{i}_cmp.xml");
        var stdFilePath = TestHelper.GetFurtherTestFilePath($"vecto_engine_date_sample0{i}_std.xml");
        var pubBranch = TestHelper.SaveAsComponentData(repository, stdFilePath, compFilePath);

        if (i == 3)
        {
          var certFilePath = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
          pubBranch = TestHelper.SaveCertificateByComponentFile(repository, certFilePath, compFilePath);
        }

        pubBranches.Add(pubBranch);
      }

      var compFilePath04 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample04_cmp.xml");
      var certFilePath04 = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var pubBranch04 = TestHelper.SaveAsComponentData(repository, null, compFilePath04);
      pubBranch04 = TestHelper.SaveCertificateByComponentFile(repository, certFilePath04, compFilePath04);
      pubBranches.Add(pubBranch04);
    }

    private void SetPrivateBranches()
    {
      prvBranches = new List<Branch>();

      var uFilePaths = new List<string>
      {
        TestHelper.GetTestFilePath("VectoComponent.xsd"),
        TestHelper.GetTestFilePath("VectoDeclarationDefinitions.1.0.xsd")
      };

      var mFilePaths = new List<string>
      {
        TestHelper.GetTestFilePath("VectoInput.xsd"),
        TestHelper.GetTestFilePath("xmldsig-core-schema.xsd"),
        TestHelper.GetTestFilePath("vecto_std_engine-sample.xml")
      };

      var compFilePath01 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_cmp.xml");
      prvBranches.Add(TestHelper.SaveUserDataByComponentFile(repository, uFilePaths, compFilePath01));

      var compFilePath02 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml");
      prvBranches.Add(TestHelper.SaveMeasurementDataByComponentFile(repository, mFilePaths, compFilePath02));

      var compFilePath03 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml");
      var prvBranch03 = TestHelper.SaveUserDataByComponentFile(repository, uFilePaths, compFilePath03);
      prvBranch03 = TestHelper.SaveMeasurementDataByComponentFile(repository, mFilePaths, compFilePath03);
      prvBranches.Add(prvBranch03);
    }


    private void SetVectoComponents(List<Branch> pubBranches, List<Branch> prvBranches)
    {
      vectoComponents = new List<VectoComponent>();

      for (int i = 0; i < 3; i++)
      {
        var pubBranch = pubBranches[i];
        var prvBranch = prvBranches[i];

        var vectoComponent = new VectoComponent(repository, pubBranch, prvBranch);
        vectoComponents.Add(vectoComponent);
      }

      var pubBranch04 = pubBranches[3];
      var vectoComponent04 = new VectoComponent(repository, pubBranch04, null);
      vectoComponents.Add(vectoComponent04);
    }

    private void ResetVectoComponentTransferSelection()
    {
      foreach (var component in vectoComponents)
      {
        component.TransferPrivateBranch = false;
        component.TransferPublicBranch = false;
        component.TransferStandardValuesBranch = false;
      }
    }


  }
}