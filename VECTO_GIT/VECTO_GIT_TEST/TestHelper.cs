﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using VECTO_GIT;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT_TEST
{
  public enum CommitDataType
  {
    MesurementData,
    UserData,
    CertificateData,
    ComponentData,
    StandardData,
    TestData
  };

  public enum RepositoryType
  {
    Source,
    Destination
  }

  public static class TestHelper
  {
    private static string BASE_DIR_PATH = AppDomain.CurrentDomain.BaseDirectory;
    private static string TEST_ENV_PATH = @"TestEnvironment\TestRepository";
    private static string TEST_DATA_PATH = @"TestEnvironment\TestData";
    private static string FURTHER_TEST_DATA_PATH = @"TestEnvironment\FurtherTestData";
    private static string SOURCE_FOLDER = "Source";
    private static string DESTINATION_FOLDER = "Destination";


    public static string SRC_REPO_PATH = Path.Combine(
      BASE_DIR_PATH,
      @"..\..\",
      TEST_ENV_PATH,
      SOURCE_FOLDER);

    public static string DES_REPO_PATH = Path.Combine(
      BASE_DIR_PATH,
      @"..\..\",
      TEST_ENV_PATH,
      DESTINATION_FOLDER);

    public static string DATA_PATH = Path.Combine(
      BASE_DIR_PATH,
      @"..\..\",
      TEST_DATA_PATH);

    public static string FURTHER_DATA_PATH = Path.Combine(
      BASE_DIR_PATH,
      @"..\..\",
      FURTHER_TEST_DATA_PATH);



    public static Repository InitEmptyRepository(RepositoryType repositoryType, bool bare = false)
    {
      var gitFolder = bare ? "" : ".git";

      var repoPath = repositoryType == RepositoryType.Source ?
        Path.Combine(SRC_REPO_PATH, gitFolder) :
        Path.Combine(DES_REPO_PATH, gitFolder);

      return InitEmptyRepository(repoPath, bare);
    }

    public static Repository InitEmptyRepository(string repositoryPath, bool bare = false)
    {
      if (Directory.Exists(repositoryPath))
      {
        DeleteDirectory(repositoryPath);
      }
      Repository.Init(repositoryPath, bare);

      return new Repository(repositoryPath);
    }


    public static void DeleteRepository(RepositoryType repositoryType)
    {
      var repoPath = repositoryType == RepositoryType.Source ?
        Path.Combine(SRC_REPO_PATH, ".git") :
        Path.Combine(DES_REPO_PATH, ".git");

      if (Directory.Exists(repoPath))
      {
        DeleteDirectory(repoPath);
        DeleteFolderContent(SRC_REPO_PATH);
        DeleteFolderContent(DES_REPO_PATH);
      }
    }

    public static void DeleteBranch(Repository repository, Branch branch)
    {
      repository.Branches.Remove(branch);
    }

    public static void DeleteFolderContent(string folderPath)
    {
      NormalizeDirectoryAttributes(folderPath);
      var directoryInfo = new DirectoryInfo(folderPath);

      foreach (FileInfo file in directoryInfo.GetFiles())
      {
        file.Delete();
      }

      foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
      {
        dir.Delete(true);
      }

    }

    public static string GetGitHashOfData(string dataFileName, bool isBinary)
    {

      Dictionary<string, string> testDataText = null;
      Dictionary<string, string> testDataBinary = null;

      if (isBinary)
      {
        testDataBinary = new Dictionary<string, string>
        {
          { "Demo_FullLoad_Child_v1.4.csv", "5ad25f20f60199c5091aabdf01240993b584bbbe" },
          { "Demo_FullLoad_Parent_v1.4.csv", "47ae1fb3f3ca80cb2e53b9849af6b75950615496" },
          { "Demo_Motoring_v1.4.csv", "197e0f02144e250e4041cff7e6b055cdb5b82803" },
          { "vecto_engine-sample.xml", "7b3c2a681ebf72cfda43f24a19ab0e33f7cf71ca" },
          { "vecto_gearbox-sample.xml", "44fc0a32f85c0e5d0a481df7170c7e6c028f0880" },
          { "vecto_gearbox-sample_02.xml", "07ab9060a1492cdcd3e774491520aa04d5b4ded3" },
          { "Demo_Map_v1.4.csv", "3be0dd3a9563b2733142e11cd0f8b3b2c9ea3f73" },
          { "VectoComponent.xsd", "aa22cc2609df40bb5b1bf4e4b86934bb5f294bd5" },
          { "VectoDeclarationDefinitions.1.0.xsd", "f5f78bfe581a583ad1f53632d7bdb73918430318" },
          { "VectoInput.xsd", "08f684d3625329fb45ebb3df2797f98c96266f98" },
          { "VectoInputDeclaration.1.0.pdf", "d11d573c8390c0bcaa09120cc3a2ea77e5d0deab" },
          { "vecto_std_engine-sample.xml", "f98b6145aafdfcc4491edfe15f075bbc598c1dc5" }
        };

        return testDataBinary[dataFileName];
      }
      else
      {
        testDataText = new Dictionary<string, string>
        {
          { "Demo_FullLoad_Child_v1.4.csv", "9275da3dbb4a910179754108427d82eb61b74546" },
          { "Demo_FullLoad_Parent_v1.4.csv", "612eb9de7d8aed4caadaef3a8cea4c6f07c28285" },
          { "Demo_Motoring_v1.4.csv", "cd6b584bc46dbfe7e038fa8ee35003876b85083e" },
          { "vecto_engine-sample.xml", "04885ccbac9f855aafb2dd61896859b4b761bd3f" },
          { "vecto_gearbox-sample.xml", "fdbcf59dde09a84ca1501ae550597c947dd71011" },
          { "vecto_gearbox-sample_02.xml", "c85904031b85ca6f76b503878a10aaa33879f132" },
          { "Demo_Map_v1.4.csv", "3c6a296af430cd9679a9a07ffe1f2c20a12e104d" },
          { "VectoComponent.xsd", "b62069047069413f0953db5a11dfed979e3015cf" },
          { "VectoDeclarationDefinitions.1.0.xsd", "6a79fb6d9297410757c06a342c2601c8aa32fda2" },
          { "VectoInput.xsd", "bafee4d8c7c551e73eaa5b7ced639252a500f9a5" },
          { "xmldsig-core-schema.xsd", "df126b30e684091b32a748cd2e408f5fb9d651a9" },
          { "VectoInputDeclaration.1.0.pdf", "d11d573c8390c0bcaa09120cc3a2ea77e5d0deab" },
          { "vecto_std_engine-sample.xml", "981ed745aeea8e30022cd0ffbb6c54f8c91ca143" },
          { "vecto_engine_date_sample01.xml", "9fd2d40bb0cdf54683379f75ebc83bb0583ae056" },
          { "vecto_engine_date_sample02.xml", "3bfaf7c8b1b10cfb1ca87eeb4c938921d4a2711e" },
          { "vecto_engine_date_sample03.xml", "1ba2394812b64ecd075dd640cc26c20065d70a7c" },
          { "vecto_engine_date_sample04.xml", "a29d7cda1d0e87bcb5cc65da75fd75eb57b68383" },
          { "vecto_engine_date_sample05.xml", "9078991fcc669ddd0b52651f97294005f4e80b1e" },
        };



        return testDataText[dataFileName];
      }
    }

    private static string GetFileName(CommitDataType dataType, string fileName)
    {

      switch (dataType)
      {
        case CommitDataType.MesurementData:
          return ConfigValue.MEASUREMENT_DATA_TREE_NAME + "/" + fileName;
        case CommitDataType.UserData:
          return ConfigValue.USER_DATA_TREE_NAME + "/" + fileName;
        case CommitDataType.CertificateData:
          return ConfigValue.CERTIFICATE_DATA_FILE_NAME;
        case CommitDataType.ComponentData:
          return ConfigValue.COMPONENT_DATA_FILE_NAME;
        case CommitDataType.StandardData:
          return ConfigValue.STANDARD_VALUES_FILE_NAME;
        case CommitDataType.TestData:
          return fileName;
      }
      return fileName;
    }

    public static Blob GetBlobFurtherTestData(Repository repository, string filename)
    {
      var filePath = GetFurtherTestFilePath(filename);
      var blob = new CreateBlobCommand(repository, filePath);
      return blob.Result();
    }

    public static Blob GetExampleBlob(Repository repository, string filename)
    {
      var filePath = GetTestFilePath(filename);
      var blob = new CreateBlobCommand(repository, filePath);
      return blob.Result();
    }

    public static Blob GetExampleBlobByStream(Repository repository, string fileName)
    {
      var filePath = GetTestFilePath(fileName);
      using (var streamReader = new StreamReader(filePath, Encoding.UTF8))
      {
        using (var stream = streamReader.BaseStream)
        {
          var blob = new CreateBlobCommand(repository, stream);
          return blob.Result();
        }
      }
    }

    public static Tree GetExampleTree(Repository repository, string fileName, CommitDataType dataType)
    {
      var filePath = GetTestFilePath(fileName);
      var blobName = GetFileName(dataType, fileName);
      var blob = GetExampleBlob(repository, filePath);
      var tree = new CreateTreeCommand(repository,
          new Dictionary<string, Blob> { { blobName, blob } });
      return tree.Result();
    }

    public static Tree GetExampleTree(Repository repository, string fileName, Commit parentCommit, CommitDataType dataType)
    {
      var filePath = GetTestFilePath(fileName);
      var blobName = GetFileName(dataType, fileName);
      var blob = GetExampleBlob(repository, filePath);
      var tree = new CreateTreeCommand(repository, parentCommit,
        new Dictionary<string, Blob> { { blobName, blob } });
      return tree.Result();
    }


    public static Commit GetExampleCommit(Repository repository, string fileName, CommitDataType dataType)
    {
      var tree = GetExampleTree(repository, fileName, dataType);
      CreateTreeCommitCommand commit;
      if (dataType == CommitDataType.ComponentData)
      {
        var dateHandler = new CommitDateHandler(DateTime.UtcNow, CommitDateType.ComponentData);

        commit = new CreateTreeCommitCommand(repository, tree, dateHandler.GetJsonRepresentation());
      }
      else
      {
        commit = new CreateTreeCommitCommand(repository, tree, "example messaage");
      }
      return commit.Result();
    }

    public static Commit GetExampleCommit(Repository repository, string fileName, Reference branchReference, Commit parentCommit,
      CommitDataType dataType)
    {
      var tree = GetExampleTree(repository, fileName, parentCommit, dataType);
      var commit = new CreateTreeCommitCommand(repository, tree, "test message", branchReference, parentCommit);
      return commit.Result();
    }


    public static Branch GetExampleBranch(Repository repository, string fileName, string branchName, CommitDataType
     dataType)
    {
      var filePath = GetTestFilePath(fileName);
      var commit = GetExampleCommit(repository, fileName, dataType);
      var branch = new CreateOrphanBranchCommand(repository, branchName, commit);
      return branch.Result();
    }

    public static Branch GetExampleBranch(Repository repository, List<string> fileNames, string branchName,
      CommitDataType dataType)
    {
      Branch branch = null;
      Commit commit = null;
      var fileCount = 0;

      foreach (var fileName in fileNames)
      {

        var commitType = DetermineCommitDataType(branchName, fileCount);

        if (branch == null)
        {
          commit = GetExampleCommit(repository, fileName, commitType);
          branch = new CreateOrphanBranchCommand(repository, branchName, commit).Result();
        }
        else
        {
          branch = repository.Branches[branchName];
          commit = GetExampleCommit(repository, fileName, branch.Reference, commit, commitType);
        }
        fileCount++;
      }
      branch = repository.Branches[branchName];
      if (branch == null)
      {
        throw new ArgumentNullException("Branch is null");
      }
      return branch;

    }

    private static CommitDataType DetermineCommitDataType(string branchName, int filePosition)
    {
      if (branchName.StartsWith(PublicBranchNaming.TREE_NAME))
      {
        if (filePosition == 0)
        {
          return CommitDataType.ComponentData;
        }
        return CommitDataType.CertificateData;
      }

      if (branchName.StartsWith(PrivateBranchNaming.TREE_NAME))
      {
        return CommitDataType.MesurementData;
      }

      return CommitDataType.TestData;
    }

    public static string GetFurtherTestFilePath(string filename)
    {
      if (!File.Exists(filename))
      {
        return Path.Combine(FURTHER_DATA_PATH, filename);
      }
      return filename;
    }

    public static string GetTestFilePath(string filename)
    {
      if (!File.Exists(filename))
      {
        return Path.Combine(DATA_PATH, filename);
      }
      return filename;
    }

    public static string GetPrivateBranchName(string filePath)
    {
      var xmlReader = new XmlContentReader();
      var pubBranchNaming = xmlReader.ReadOutBranchNameData(filePath);

      return pubBranchNaming.GetGitPrivateBranchName();
    }


    private static void DeleteDirectory(string dirPath)
    {
      NormalizeDirectoryAttributes(dirPath);
      Directory.Delete(dirPath, true);
    }

    private static void NormalizeDirectoryAttributes(string directoryPath)
    {
      var filePaths = Directory.GetFiles(directoryPath);
      var subdirectoryPaths = Directory.GetDirectories(directoryPath);

      foreach (string filePath in filePaths)
      {
        File.SetAttributes(filePath, FileAttributes.Normal);
      }
      foreach (string subdirectoryPath in subdirectoryPaths)
      {
        NormalizeDirectoryAttributes(subdirectoryPath);
      }
      File.SetAttributes(directoryPath, FileAttributes.Normal);
    }

    public static Branch SaveAsComponentData(Repository repository, string stdFilePath, string compFilePath)
    {
      Branch stdBranch = null;
      if (stdFilePath != null)
        stdBranch = SaveStandardValueFile(repository, stdFilePath);

      var compCommit = new ComponentDataCommit(repository, compFilePath, stdBranch, null);
      if (compCommit.VerificationCheckBeforeCommit())
      {
        return compCommit.ExecuteCommit();
      }

      return null;
    }

    public static Branch SaveAsComponentData(Repository repository, string filePath)
    {
      var compCommit = new ComponentDataCommit(repository, filePath, null, null);

      if (compCommit.VerificationCheckBeforeCommit())
      {
        return compCommit.ExecuteCommit();
      }

      return null;
    }

    public static Branch SaveCertificateByComponentFile(Repository repository, string certFilePath, string compFilePath)
    {
      var compBranch = GetComponentBranch(repository, compFilePath);
      if (compBranch != null)
      {
        var pubBranchNaming = new PublicBranchNaming(compBranch.FriendlyName);
        var xmlReader = new XmlContentReader();
        var certNumberHandler = xmlReader.ReadOutCertificateId(compFilePath);

        var certCommit = new CertificateDataCommit(repository, certFilePath, pubBranchNaming, compBranch, certNumberHandler);
        if (certCommit.VerificationCheckBeforeCommit())
        {
          return certCommit.ExecuteCommit();
        }

      }

      return null;
    }

    private static Branch GetComponentBranch(Repository repository, string compFilePath)
    {
      var xmlReader = new XmlContentReader();

      var pubBranchNaming = xmlReader.ReadOutBranchNameData(compFilePath);
      var compBranch = repository.Branches[pubBranchNaming.GetGitPathBranchName()];
      if (compBranch == null)
      {
        compBranch = SaveAsComponentData(repository, compFilePath);
      }

      return compBranch;
    }


    public static Branch SaveStandardValueFile(Repository repository, string stdFilePath)
    {
      var stdCommit = new StandardDataCommit(repository, stdFilePath);
      if (stdCommit.VerificationCheckBeforeCommit())
      {
        return stdCommit.ExecuteCommit();
      }

      return null;
    }

    public static Branch SaveMeasurementDataByPrivateBranchName(Repository repository, List<string> mFilePaths,
      PrivateBranchNaming privateBranchNaming)
    {
      var mFileItems = GetFileItems(mFilePaths, FileType.MeasurementData);
      var mCommit = new MeasurementDataCommit(repository, mFileItems, privateBranchNaming.GetGitPathBranchName());
      if (mCommit.VerificationCheckBeforeCommit())
      {
        return mCommit.ExecuteCommit();
      }

      return null;
    }


    public static Branch SaveMeasurementDataByComponentFile(Repository repository, List<string> mFilePaths,
      string compFilePath)
    {
      var mFileItems = GetFileItems(mFilePaths, FileType.MeasurementData);
      var xmlReader = new XmlContentReader();
      var pubBranchNaming = xmlReader.ReadOutBranchNameData(compFilePath);

      var mCommit = new MeasurementDataCommit(repository, mFileItems, pubBranchNaming.GetGitPrivateBranchName());
      if (mCommit.VerificationCheckBeforeCommit())
      {
        return mCommit.ExecuteCommit();
      }

      return null;
    }

    public static Branch SaveUserDataByComponentFile(Repository repository, string uFilePath, string compFilePath)
    {
      var ufilePaths = new List<string>() {uFilePath};
      return SaveUserDataByComponentFile(repository, ufilePaths, compFilePath);
    }

    public static Branch SaveUserDataByPrivateBranchName(Repository repository, List<string> uFilePaths,
      PrivateBranchNaming privateBranchNaming)
    {
      var uFileItems = GetFileItems(uFilePaths, FileType.UserData);
      var uCommit = new UserDataCommit(repository, uFileItems, privateBranchNaming.GetGitPathBranchName());
      if (uCommit.VerificationCheckBeforeCommit())
      {
        return uCommit.ExecuteCommit();
      }

      return null;
    }


    public static Branch SaveUserDataByComponentFile(Repository repository, List<string> uFilePaths,
      string compFilePath)
    {
      var uFileItems = GetFileItems(uFilePaths, FileType.UserData);
      var xmlReader = new XmlContentReader();
      var pubBranchNaming = xmlReader.ReadOutBranchNameData(compFilePath);

      var uCommit = new UserDataCommit(repository, uFileItems, pubBranchNaming.GetGitPrivateBranchName());
      if (uCommit.VerificationCheckBeforeCommit())
      {
        return uCommit.ExecuteCommit();
      }

      return null;
    }

    public static Tree SaveTreeOfFiles(Repository repository, List<string> filePaths)
    {
      var treeEntries = new Dictionary<string, Blob>();
      
      foreach (var filePath in filePaths)
      {
        var createBlob = new CreateBlobCommand(repository, filePath);
        var fileName = Path.GetFileName(filePath);
        treeEntries.Add(fileName, createBlob.Result());
      }

      return new CreateTreeCommand(repository, treeEntries).Result();
    }

    public static List<List<Item>> GetFileItemsToSave(List<string> filePaths, FileType fileType)
    {
      var items = new List<List<Item>>();
      foreach (var filePath in filePaths)
      {
        var item = GetFileItems(new List<string> {filePath}, fileType);
        items.Add(item);
      }

      return items;
    }

    public static List<Item> GetFileItems(List<string> filePaths, FileType fileType)
    {
      var fileItems = new List<Item>();

      for (int i = 0; i < filePaths.Count; i++)
      {
        var filePath = filePaths[i];
        var fileItem = new FileItem
        {
          FilePath = filePath,
          Filename = Path.GetFileName(filePath),
          FileType = fileType
        };
        fileItems.Add(fileItem);
      }

      return fileItems;
    }

  }
}
