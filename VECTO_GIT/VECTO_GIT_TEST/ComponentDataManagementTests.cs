﻿using NUnit.Framework;
using VECTO_GIT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using VECTO_GIT_TEST;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentCommit;
using LibGit2Sharp;
using NUnit.Framework.Internal;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.ComponentTransfer;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.Tests
{
  [TestFixture()]
  [Category("ComponentDataManagement")]
  public class ComponentDataManagementTests
  {
    private ComponentDataManagement compDataManagement;
    private Repository repository;
    private Repository destRepository;

    private List<Branch> pubBranches;
    private List<Branch> prvBranches;

    private List<VectoComponent> vectoComponents;
    private List<string> measurementDataFilePaths;
    private List<string> userDataFilePaths;

    [OneTimeSetUp()]
    public void OneTimeInit()
    {
      SetSourceRepository();
      SetupTestBranches();
      destRepository = TestHelper.InitEmptyRepository(RepositoryType.Destination, true);
    }

    [OneTimeTearDown()]
    public void OneTimeCleanUp()
    {
      destRepository.Dispose();
      compDataManagement.Dispose();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      TestHelper.DeleteFolderContent(TestHelper.DES_REPO_PATH);
    }
    
    private void SetSourceRepository()
    {
      compDataManagement = new ComponentDataManagement();
      TestHelper.DeleteFolderContent(TestHelper.SRC_REPO_PATH);
      compDataManagement.SetCurrentRepository(TestHelper.SRC_REPO_PATH);
      repository = compDataManagement.CurrentRepository;
    }


    private void SetupTestBranches()
    {
      SetPublicBranches();
      SetPrivateBranches();
      SetVectoComponents(pubBranches, prvBranches);
    }


    [Test()]
    public void VerifySaveStandardValueFileTest()
    {
      var stdFilePath = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_std.xml");
      var stdBlob = TestHelper.GetBlobFurtherTestData(destRepository, "vecto_engine_date_sample05_std.xml");
      var errorVerification = compDataManagement.VerifySaveStandardValueFile(null, true);
      var branchCount = repository.Branches.Count();
      var stdCommit = compDataManagement.VerifySaveStandardValueFile(stdFilePath, true);
      var stdBranch = stdCommit.ExecuteCommit();
      
      Assert.IsNull(errorVerification);
      Assert.IsNotNull(stdCommit);
      Assert.IsNotNull(stdBranch);
      Assert.AreEqual(branchCount +1, repository.Branches.Count());
      Assert.IsTrue(StandardDataCommit.IsStandardValuesBranch(stdBranch));
      Assert.AreEqual(stdBlob.Sha , StandardDataCommit.GetStandardDataBlob(stdBranch).Sha);
    }

    [Test()]
    public void VerifySaveStandardValueFileErrorTest()
    {
      var stdFilePath = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_std.xml");
      var stdCommit01 = compDataManagement.VerifySaveStandardValueFile(stdFilePath, true);
      var stdCommit02 = compDataManagement.VerifySaveStandardValueFile(Path.Combine(stdFilePath, "wrongPath"), false);

      Assert.IsNull(stdCommit01);
      Assert.IsNull(stdCommit02);
      Assert.AreEqual(1, compDataManagement.CommitVerificationErrors.Count());
    }

    [Test()]
    public void VerifySaveComponentDataTest()
    {
      var stdFilePath = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_std.xml");
      var compFilePath = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample05_cmp.xml");
      var errorVerification = compDataManagement.VerifySaveComponentData(null, null, stdFilePath, true);
      var compCommit = compDataManagement.VerifySaveComponentData(compFilePath, null, stdFilePath, true);
      var compBranch = compCommit.ExecuteCommit();
      
      Assert.IsNull(errorVerification);
      Assert.IsNotNull(compCommit);
      Assert.IsNotNull(compBranch);
      Assert.IsFalse(StandardDataCommit.IsStandardValuesBranch(compBranch));
      Assert.AreEqual(1, compBranch.Commits.Count());
    }

    [Test()]
    public void VerifySaveComponentDataErrorTest()
    {
      var compFilePath = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml");
      var compCommit01 = compDataManagement.VerifySaveComponentData(compFilePath, null, null, false);
      var compCommit02 = compDataManagement.VerifySaveComponentData(Path.Combine(compFilePath, "wrongPath"), null,null,false);

      Assert.IsNull(compCommit01);
      Assert.IsNull(compCommit02);
      Assert.AreEqual(1, compDataManagement.CommitVerificationErrors.Count());
    }


    [Test()]
    public void VerifySaveCertificateByComponentFileTest()
    {
      var compBranch = pubBranches[1];
      compBranch = repository.Branches[compBranch.FriendlyName];
      var certFilename = "VectoInputDeclaration.1.0.pdf";
      var certFilePath = TestHelper.GetTestFilePath(certFilename);
      var certBlob = TestHelper.GetExampleBlob(destRepository, certFilename);
      var errorVerification = compDataManagement.VerifySaveCertificateByComponentFile(null, null, compBranch);
      var certCommit = compDataManagement.VerifySaveCertificateByComponentFile(certFilePath, null, compBranch);
      var certBranch = certCommit.ExecuteCommit();

      Assert.IsNull(errorVerification);
      Assert.IsNotNull(certCommit);
      Assert.IsNotNull(certBranch);
      Assert.IsNull(CertificateDataCommit.GetCertificateBlob(compBranch));
      Assert.IsNotNull(CertificateDataCommit.GetCertificateBlob(certBranch));
      Assert.AreEqual(compBranch.Commits.Count() + 1, certBranch.Commits.Count());
      Assert.AreEqual(certBlob.Sha, CertificateDataCommit.GetCertificateBlob(certBranch).Sha);
    }


    [Test()]
    public void VerifySaveCertificateByComponentBranchTest()
    {
      var compBranch = pubBranches[3];
      var certFilename = "VectoInputDeclaration.1.0.pdf";
      var compFilename = "vecto_engine_date_sample04_cmp.xml";
      var certFilePath = TestHelper.GetTestFilePath(certFilename);
      var certBlob = TestHelper.GetExampleBlob(destRepository, certFilename);
      var compFilePath = TestHelper.GetFurtherTestFilePath(compFilename);
      var compBlob = TestHelper.GetExampleBlob(destRepository, compFilename);
      var errorVerification = compDataManagement.VerifySaveCertificateByComponentFile(null, compFilePath, null);
      var certCommit = compDataManagement.VerifySaveCertificateByComponentFile(certFilePath, compFilePath, null);
      var certBranch = certCommit.ExecuteCommit();

      Assert.IsNull(errorVerification);
      Assert.IsNotNull(certCommit);
      Assert.IsNotNull(certBranch);
      Assert.IsNull(CertificateDataCommit.GetCertificateBlob(compBranch));
      Assert.IsNotNull(CertificateDataCommit.GetCertificateBlob(certBranch));
      Assert.AreEqual(compBranch.Commits.Count() + 1, certBranch.Commits.Count());
      Assert.AreEqual(certBlob.Sha, CertificateDataCommit.GetCertificateBlob(certBranch).Sha);
      Assert.AreEqual(certBlob.Sha, CertificateDataCommit.GetCertificateBlob(certBranch).Sha);
      Assert.AreNotEqual(compBlob.Sha, CertificateDataCommit.GetCertificateBlob(certBranch).Sha);
      Assert.AreEqual(compBlob.Sha, ComponentDataCommit.GetComponentDataBlob(compBranch).Sha);
    }

    [Test()]
    public void VerifySaveCertificateErrorsTest()
    {
      var certFilePath = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
      var certBlob = TestHelper.GetExampleBlob(destRepository, "VectoInputDeclaration.1.0.pdf");
      var compFilePath = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml");
      
      var stdBranch = pubBranches[0];
      var compBranch = pubBranches[2];
      var wrongBranchResult = compDataManagement.VerifySaveCertificateByComponentFile(certFilePath, null, stdBranch);
      var certAlreadyAdded = compDataManagement.VerifySaveCertificateByComponentFile(certFilePath, compFilePath, null);
      var wrongCertFilePath = compDataManagement.VerifySaveCertificateByComponentFile(Path.Combine(certFilePath, "wrongPath"), compFilePath, null);

      Assert.IsNull(certAlreadyAdded);
      Assert.IsNull(wrongBranchResult);
      Assert.IsNull(wrongCertFilePath);
      Assert.AreEqual(3, compBranch.Commits.Count());
      Assert.AreEqual(certBlob.Sha, CertificateDataCommit.GetCertificateBlob(compBranch).Sha);
    }


    [Test()]
    public void VerifySaveMeasurementDataTest()
    {
      var measurementItems = TestHelper.GetFileItemsToSave(measurementDataFilePaths, FileType.MeasurementData);
      var componentToSave = new ComponentToSave(measurementItems, vectoComponents[0]);
      var measurementCommit = compDataManagement.VerifySaveMeasurementData(componentToSave);
      var mTreeBeforeCommit = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, vectoComponents[0].PrivateBranch);
      var prvBranch = measurementCommit.ExecuteCommit();
      var measurementTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, prvBranch);

      Assert.IsNotNull(measurementCommit);
      Assert.IsNotNull(prvBranch);
      Assert.IsNotNull(measurementTree);
      Assert.IsNull(mTreeBeforeCommit);
      Assert.AreEqual(vectoComponents[0].PrivateBranch.Commits.Count()+1, prvBranch.Commits.Count());
      Assert.AreEqual(2, prvBranch.Commits.Count());
      Assert.AreEqual(2, prvBranch.Commits.First().Tree.Count());
      Assert.IsTrue(TreeContainsExpectedFiles(measurementTree, measurementDataFilePaths));
    }


    [Test]
    public void VerifySaveMeasurementDataErrorTest()
    {
      var componentToSave01 = new ComponentToSave(null);
      componentToSave01.ComponentDataBranch = prvBranches[0];

      var componentToSave02 = new ComponentToSave(null);
      var alreadExisting = TestHelper.GetFileItems(measurementDataFilePaths, FileType.MeasurementData);
      componentToSave02.PrivateDataBranch = prvBranches[1];
      componentToSave02.SelectedMeasurementData = alreadExisting;

      var componentToSave03 = new ComponentToSave(null);
      var wrongFilePath = TestHelper.GetFileItems(new List<string>{"wrong/FilePath"}, FileType.MeasurementData);
      componentToSave03.PrivateDataBranch = prvBranches[1];
      componentToSave03.SelectedMeasurementData = wrongFilePath;

      Assert.IsNull(compDataManagement.VerifySaveMeasurementData(null));
      Assert.IsNull(compDataManagement.VerifySaveMeasurementData(componentToSave01));
      Assert.IsNull(compDataManagement.VerifySaveMeasurementData(componentToSave02));
      Assert.IsNull(compDataManagement.VerifySaveMeasurementData(componentToSave03));
    }

    [Test]
    public void VerifySaveUserDataTest()
    {
      var userItems = TestHelper.GetFileItemsToSave(userDataFilePaths, FileType.UserData);
      var componentToSave = new ComponentToSave(userItems, vectoComponents[1]);
      var userDataCommit = compDataManagement.VerifySaveUserData(componentToSave);
      var uTreeBeforeCommit = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, vectoComponents[1].PrivateBranch);
      var prvBranch = userDataCommit.ExecuteCommit();
      var userDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, prvBranch);

      Assert.IsNotNull(userDataCommit);
      Assert.IsNotNull(prvBranch);
      Assert.IsNotNull(userDataTree);
      Assert.IsNull(uTreeBeforeCommit);
      Assert.AreEqual(vectoComponents[1].PrivateBranch.Commits.Count() + 1, prvBranch.Commits.Count());
      Assert.AreEqual(2, prvBranch.Commits.Count());
      Assert.AreEqual(2, prvBranch.Commits.First().Tree.Count());
      Assert.IsTrue(TreeContainsExpectedFiles(userDataTree, userDataFilePaths));
    }

    [Test]
    public void VerifySaveUserDataErrorTest()
    {
      var componentToSave01 = new ComponentToSave(null);
      componentToSave01.ComponentDataBranch = prvBranches[1];

      var componentToSave02 = new ComponentToSave(null);
      var alreadExisting = TestHelper.GetFileItems(userDataFilePaths, FileType.UserData);
      componentToSave02.PrivateDataBranch = prvBranches[0];
      componentToSave02.SelectedUserData = alreadExisting;

      var componentToSave03 = new ComponentToSave(null);
      var wrongFilePath = TestHelper.GetFileItems(new List<string> { "wrong/FilePath" }, FileType.UserData);
      componentToSave03.PrivateDataBranch = prvBranches[1];
      componentToSave03.SelectedUserData = wrongFilePath;

      Assert.IsNull(compDataManagement.VerifySaveUserData(null));
      Assert.IsNull(compDataManagement.VerifySaveUserData(componentToSave01));
      Assert.IsNull(compDataManagement.VerifySaveUserData(componentToSave02));
      Assert.IsNull(compDataManagement.VerifySaveUserData(componentToSave03));
    }

    [Test]
    public void SearchVectoComponentsByGivenTermsTest()
    {
      var manufacturerName = "Manufacturer MAN";

      var searchTerms = new VectoSearchTerms
      {
        Manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower()
      };

      var searchResult = compDataManagement.SearchVectoComponentsByGivenTerms(searchTerms);

      Assert.IsTrue(searchResult.Any());
      Assert.AreEqual(4, searchResult.Count());
    }

    [Test]
    public void SearchAllVectoComponentsTest()
    {
      var searchResult = compDataManagement.SearchAllVectoComponents();
      Assert.IsTrue(searchResult.Any());
      Assert.AreEqual(4, searchResult.Count());
    }

    [Test]
    public void XmlValidationTest()
    {
      Assert.IsNull(compDataManagement.XmlErrors);
      Assert.IsNull(compDataManagement.XmlWarnings);

      var wrongXml = TestHelper.GetTestFilePath("vecto_engine-sample_wrong.xml");
      compDataManagement.VerifySaveComponentData(wrongXml, null, null, true);

      Assert.IsNotNull(compDataManagement.XmlErrors);
      Assert.IsEmpty(compDataManagement.XmlWarnings);
    }

    [Test]
    public void SetCurrentRepositoryTest()
    {
      Assert.Throws<ArgumentException>(() => compDataManagement.SetCurrentRepository(null));
    }


    private bool TreeContainsExpectedFiles(Tree tree, List<string> filePaths)
    {
      var fileCounter = 0;
      foreach (var filePath in filePaths)
      {
        var notFound = true;
        var fileBlob = TestHelper.GetExampleBlob(destRepository, filePath);
        var filename = Path.GetFileName(filePath);
        foreach (var treeEntry in tree)
        {
          if (treeEntry.Target.Sha == fileBlob.Sha && treeEntry.Name == Path.GetFileName(filename))
          {
            fileCounter++;
            notFound = false;
            break;
          }
        }

        if (notFound)
          return false;
      }

      if(fileCounter == filePaths.Count)
        return true;
      return false;
    }


    #region Set Test Branches

    private void SetPublicBranches()
    {
      pubBranches = new List<Branch>();

      var stdFilePath01 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_std.xml");
      var stdBranch01 = TestHelper.SaveStandardValueFile(repository, stdFilePath01);
      pubBranches.Add(stdBranch01);

      for (int i = 2; i < 4; i++)
      {
        var compFilePath = TestHelper.GetFurtherTestFilePath($"vecto_engine_date_sample0{i}_cmp.xml");
        var  stdFilePath = TestHelper.GetFurtherTestFilePath($"vecto_engine_date_sample0{i}_std.xml");
        var pubBranch = TestHelper.SaveAsComponentData(repository, stdFilePath, compFilePath);

        if (i == 3)
        {
          var certFilePath = TestHelper.GetTestFilePath("VectoInputDeclaration.1.0.pdf");
          pubBranch = TestHelper.SaveCertificateByComponentFile(repository, certFilePath, compFilePath);
        }

        pubBranches.Add(pubBranch);
      }

      var compFilePath04 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample04_cmp.xml");

      var pubBranch04 = TestHelper.SaveAsComponentData(repository, null, compFilePath04);

      pubBranches.Add(pubBranch04);
    }

    private void SetPrivateBranches()
    {
      prvBranches = new List<Branch>();

      var uFilePaths = new List<string>
      {
        TestHelper.GetTestFilePath("VectoComponent.xsd"),
        TestHelper.GetTestFilePath("VectoDeclarationDefinitions.1.0.xsd")
      };

      var mFilePaths = new List<string>
      {
        TestHelper.GetTestFilePath("VectoInput.xsd"),
        TestHelper.GetTestFilePath("xmldsig-core-schema.xsd"),
        TestHelper.GetTestFilePath("vecto_std_engine-sample.xml")
      };

      var compFilePath01 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample01_cmp.xml");
      prvBranches.Add(TestHelper.SaveUserDataByComponentFile(repository, uFilePaths, compFilePath01));

      var compFilePath02 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample02_cmp.xml");
      prvBranches.Add(TestHelper.SaveMeasurementDataByComponentFile(repository, mFilePaths, compFilePath02));

      var compFilePath03 = TestHelper.GetFurtherTestFilePath("vecto_engine_date_sample03_cmp.xml");
      var prvBranch03 = TestHelper.SaveUserDataByComponentFile(repository, uFilePaths, compFilePath03);
      prvBranch03 = TestHelper.SaveMeasurementDataByComponentFile(repository, mFilePaths, compFilePath03);
      prvBranches.Add(prvBranch03);

      measurementDataFilePaths = mFilePaths;
      userDataFilePaths = uFilePaths;
    }


    private void SetVectoComponents(List<Branch> pubBranches, List<Branch> prvBranches)
    {
      vectoComponents = new List<VectoComponent>();

      for (int i = 0; i < 3; i++)
      {
        var pubBranch = pubBranches[i];
        var prvBranch = prvBranches[i];

        var vectoComponent = new VectoComponent(repository, pubBranch, prvBranch);
        vectoComponents.Add(vectoComponent);
      }

      var pubBranch04 = pubBranches[3];
      var vectoComponent04 = new VectoComponent(repository, pubBranch04, null);
      vectoComponents.Add(vectoComponent04);
    }



    #endregion
  }
}