﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;

namespace VECTO_GIT
{
  public class CommitSignature
  {
    //ToDo Reason why the signature should always the same in case of transfer
    //https://gist.github.com/masak/2415865
    
    private static CommitSignature instance;

    private CommitSignature()
    {
    }

    public static CommitSignature Instance
    {
      get
      {
        if(instance == null)
        {
          instance = new CommitSignature();
        }
        return instance;
      }
    }

    public Signature GetSignature
    {
      get
      {
        return new Signature(CommitterName, CommitterEmail, DateTimeOffset.Now);
      }
    }

    private string CommitterName
    {
      get
      {
       
        return Properties.Settings.Default.CommitterName;
      }
    }

    private string CommitterEmail
    {
      get
      {
        return Properties.Settings.Default.CommitterEMail;
      }
    }
  }
}
