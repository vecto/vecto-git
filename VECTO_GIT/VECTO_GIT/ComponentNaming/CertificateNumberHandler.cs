﻿using System;
using System.Linq;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.ComponentNaming
{
  public class CertificateNumberHandler : INamingHandler
  {
    private const char SEPERATOR = '*';
    public const int PATH_PARTS = 8;
    private static readonly string[] PATH_SEPERATOR = { "/", "#" };
    private static readonly string[] COMPONENT_TYPES =
    {
      "g", "o", "k", "m", "e", "l", "p", "t"
    };

    private string issuingCountry;
    private string certificationAct;
    private string latestAmending;
    private string componentType;
    private string baseCertificatonNumber;
    private string extensionNumber;
    private string xmlHash;

    private string originCertificationNumber;

    private bool withXmlHash;

    #region Origin getter

    public string OriginIssuingCountry
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(issuingCountry);
      }
    }

    public string OriginCertificationAct
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(certificationAct);
      }
    }

    public string OriginLatestAmending
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(latestAmending);
      }
    }

    public string OriginComponentType
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(componentType);
      }
    }

    public string OriginBaseCertificationNumber
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(baseCertificatonNumber);
      }
    }

    public string OriginExtensionNumber
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(extensionNumber);
      }
    }

    public string OriginXmlHash
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(xmlHash);
      }
    }

    public string OriginCertificationNumber
    {
      get
      {
        return originCertificationNumber;
      }
    }

    #endregion

    #region Getter and Setter
    public string IssuingCountry
    {
      get
      {
        return issuingCountry;
      }
      set
      {
        issuingCountry = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }
    public string CertificationAct
    {
      get
      {
        return certificationAct;
      }
      set
      {
        certificationAct = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }
    public string LatestAmending
    {
      get
      {
        return latestAmending;
      }
      set
      {
        latestAmending = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }
    public string ComponentType
    {
      get
      {
        return componentType;
      }
      set
      {
        componentType = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }
    public string BaseCertificationNumber
    {
      get
      {
        return baseCertificatonNumber;
      }
      set
      {
        baseCertificatonNumber = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower() ;
      }
    }
    public string ExtensionNumber
    {
      get
      {
        return extensionNumber;
      }
      set
      {
        extensionNumber = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }
    public string XmlHash
    {
      get
      {
        return xmlHash;
      }
      set
      {
        var xmlHashCut = value.Substring(0, PublicBranchNaming.XML_HASH_LENGTH);
        xmlHash = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(xmlHashCut).ToLower();
        withXmlHash = true;
      }

    }
    #endregion

    public CertificateNumberHandler(string name, bool isTagName)
    {
      withXmlHash = false;
      if (isTagName)
      {
        ParseTagName(name);
      }
      else
      {
        SplitCertificationNumber(name.ToLower());
      }

    }


    public CertificateNumberHandler(string certificateNumber, string xmlHash)
    {
      SplitCertificationNumber(certificateNumber.ToLower());
      withXmlHash = true;
      XmlHash = xmlHash;
    }


    private void SplitCertificationNumber(string certificationNumber)
    {
      var splitResult = certificationNumber.Split(SEPERATOR);

      if (splitResult.Length >= 2)
      {
        IssuingCountry = splitResult[0];
        CertificationAct = splitResult[1];
      }

      if (splitResult.Length == 4)
      {
        AdditionalLatestAmendingSplit(splitResult[2]);
        SetExtensionNumber(splitResult[3]);
        originCertificationNumber = GetOriginOfCertificationNumber(this);
      }
      else if (splitResult.Length == 6)
      {
        LatestAmending = splitResult[2];
        ComponentType = splitResult[3];
        BaseCertificationNumber = splitResult[4];
        SetExtensionNumber(splitResult[5]);
        originCertificationNumber = GetOriginOfCertificationNumber(this);
      }
    }


    //Covers the case if the xml hash after the certification number is given
    private void SetExtensionNumber(string extensionSplitResult)
    {
      var seperator = "/";

      if (extensionSplitResult.Contains(seperator))
      {
        var splitResult = extensionSplitResult.Split(
          seperator.ToArray(), StringSplitOptions.RemoveEmptyEntries);
        if (splitResult.Length == 2)
        {
          ExtensionNumber = splitResult[0];
          XmlHash = splitResult[1];
        }
      }
      else
      {
        ExtensionNumber = extensionSplitResult;
      }
    }

    //if LastestAmending, ComponentType and BaseCertificationNumber one number without seperator
    private void AdditionalLatestAmendingSplit(string latestAmending)
    {
      var index = -1;
      var componentType = string.Empty;
      foreach (var type in COMPONENT_TYPES)
      {
        index = latestAmending.IndexOf(type);
        if (index != -1)
        {
          componentType = type;
          break;
        }
      }

      LatestAmending = latestAmending.Substring(0, index);
      ComponentType = componentType;
      BaseCertificationNumber = latestAmending.Substring(index + 1);
    }



    private string GetOriginOfCertificationNumber(CertificateNumberHandler certObj)
    {
      var certNumber = string.Format("{0}#{1}#{2}#{3}#{4}#{5}", certObj.IssuingCountry,
        certObj.CertificationAct, certObj.LatestAmending, certObj.ComponentType,
        certObj.BaseCertificationNumber, certObj.ExtensionNumber);

      return StringCharacterLookup.Instance.ConvertToOrigin(certNumber);
    }


    public void ParseTagName(string tagName)
    {
      var splitResult = tagName.Split(PATH_SEPERATOR, StringSplitOptions.RemoveEmptyEntries);

      if (splitResult.Length != PATH_PARTS || splitResult.IsNullOrEmpty()  
        ||  !splitResult[0].Equals(ConfigValue.CERT_NR_FOLDER_NAME))
      {
        throw new ArgumentException("Invalid git path!");
      }

      IssuingCountry = splitResult[1];
      CertificationAct = splitResult[2];
      LatestAmending = splitResult[3];
      ComponentType = splitResult[4];
      BaseCertificationNumber = splitResult[5];
      ExtensionNumber = splitResult[6];
      XmlHash = splitResult[7];

      originCertificationNumber = GetOriginOfCertificationNumber(this);
    }

    public string GetTagGitPathName()
    {
      if (withXmlHash)
      {
        return string.Format("{0}/{1}", GetGitPathNameWithoutXmlHash(), XmlHash);
      }
      else
      {
        return GetGitPathNameWithoutXmlHash();
      }
    }

    public string GetGitPathNameWithoutXmlHash()
    {
      return string.Format("{0}/{1}#{2}#{3}#{4}/{5}#{6}", ConfigValue.CERT_NR_FOLDER_NAME,
        IssuingCountry, CertificationAct, LatestAmending, ComponentType, BaseCertificationNumber,
        ExtensionNumber);
    }



  }
}
