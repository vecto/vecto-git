﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.ComponentNaming
{
  public interface INamingHandler
  {
    void ParseTagName(string tagName);
    string GetTagGitPathName();
  }
}
