﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.ComponentNaming
{

  public struct PrivateBranchNameParts
  {
    public string Manufacturer { private set; get; }
    public string Model { private set; get; }
    public string Year { private set; get; }


    public PrivateBranchNameParts(string manufacturer, string model,
        string year)
    {
      Manufacturer = manufacturer.CheckNullOrEmtpy("Manufacturer-Name");
      Model = model.CheckNullOrEmtpy("Model-Name");
      Year = year.CheckNullOrEmtpy("Year");
    }
  }



  public class PrivateBranchNaming : IBranchName
  {
    public const string TREE_NAME = "private";
    private const string COMPONENT = "component";
    private const string PATH_SEPARATOR = "/";
    private const int PATH_PARTS = 5;

    private string manufacturer;
    private string model;
    private string year;

    #region Properties 
    public string Manufacturer
    {
      get
      {
        return manufacturer;
      }
      set
      {
        SetManufacturerName(value);
      }
    }
    public string Year
    {
      get
      {
        return year;
      }
      set
      {
        SetYear(value);
      }
    }
    public string Model
    {
      get
      {
        return model;
      }
      set
      {
        SetModelName(value);
      }
    }
    #endregion

    
    public PrivateBranchNaming(string branchName)
    {
      ParseBranchName(branchName.CheckNullOrEmtpy("branch name"));
    }

    public string GetGitPathBranchName()
    {
      return string.Format("{0}/{1}", TREE_NAME, GetComponentBranchName());
    }

    public string GetComponentBranchName()
    {
      return string.Format("{0}/{1}/{2}/{3}", GetManufacturerName(), COMPONENT, GetYear(), GetModelName());
    }

    public string GetModelName()
    {
      return model;
    }

    public void SetModelName(string modelName)
    {
      model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName);
    }

    public string GetManufacturerName()
    {
      return manufacturer;
    }

    public void SetManufacturerName(string manufacturerName)
    {
      this.manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName);
    }

    public string GetYear()
    {
      return year;
    }

    public void SetYear(string year)
    {
      this.year = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(year);
    }

    private void ParseBranchName(string branchName)
    {
      var splitResult = branchName.Split(new[] { PATH_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);

      if (splitResult.Length != PATH_PARTS && !splitResult[0].Equals(TREE_NAME))
      {
        throw new ArgumentException("Invalid git path!");
      }

      SetManufacturerName(splitResult[1]);
      SetYear(splitResult[3]);
      SetModelName(splitResult[4]);
    }

    //ToDo ignoring year of branch name?!?
    public static bool operator ==(PrivateBranchNaming x, PrivateBranchNaming y)
    {
      return x?.Manufacturer == y?.Manufacturer &&
             x?.Model == y?.Model &&
             x?.Year == y?.Year;
    }

    public static bool operator !=(PrivateBranchNaming x, PrivateBranchNaming y)
    {
      return !(x == y);
    }



  }
}
