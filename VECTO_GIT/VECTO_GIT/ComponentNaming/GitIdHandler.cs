﻿using System;
using System.Linq;
using LibGit2Sharp;
using VECTO_GIT.ComponentCommit;

namespace VECTO_GIT.ComponentNaming
{
  public class GitIdHandler : INamingHandler
  {
    private const int FIRST_PART_LENGTH = 2;
    private const int SEC_PART_LENGTH = 38;
    private const int PATH_PARTS = 3;
    private const char PATH_SEPARATOR = '/';
    public string FirstPartId { get; set; }
    public string SecondPartId { get; set; }

    private string originGitId;
    private Branch publicBranch;


    public string OriginGitId
    {
      get
      {
        return originGitId;

      }
    }

    public GitIdHandler(string name, bool isTagName)
    {
      if (isTagName)
      {
        ParseTagName(name);
      }
      else
      {
        originGitId = name;
        SplitGitId(name);
      }
    }

    public GitIdHandler(Branch publicBranch)
    {
      if (publicBranch.IsNull())
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("public branch"));
      }
      var componentBlob = GetComponentBlobOfPublicBranch(publicBranch);
      if (componentBlob == null)
      {
        throw new ArgumentException(Helpers.ExceptionMessage("public branch"));
      }

      this.publicBranch = publicBranch;
      SplitGitId(componentBlob.Sha);
    }

    public void ParseTagName(string tagName)
    {
      var splitResult = tagName.Split(new[] { PATH_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);

      if (splitResult.Length != PATH_PARTS || splitResult.IsNullOrEmpty() ||
        !splitResult[0].Equals(ConfigValue.GIT_ID_FOLDER_NAME))
      {
        throw new ArgumentException("Invalid git path!");
      }

      FirstPartId = splitResult[1];
      SecondPartId = splitResult[2];
      originGitId = FirstPartId + SecondPartId;
    }

    public string GetTagGitPathName()
    {
      return string.Format("{0}/{1}/{2}", ConfigValue.GIT_ID_FOLDER_NAME,
        FirstPartId, SecondPartId);
    }

    private Blob GetComponentBlobOfPublicBranch(Branch publicBranch)
    {
      return ComponentDataCommit.GetComponentDataBlob(publicBranch);
    }

    private void SplitGitId(string gitId)
    {
      originGitId = gitId;

      if (gitId.Length >= FIRST_PART_LENGTH)
      {
        FirstPartId = gitId.Substring(0, FIRST_PART_LENGTH);
        SecondPartId = gitId.Substring(FIRST_PART_LENGTH);
      }



    }
  }
}
