﻿using Newtonsoft.Json;
using System;
#pragma warning disable 660,661


namespace VECTO_GIT.ComponentNaming
{
  public class PublicBranchNameParts
  {
    public string Manufacturer { get; }
    public string Model { get; }
    public string Year { get; }
    public string XmlHash { get; }

    public PublicBranchNameParts(string manufacturer, string model,
      string year, string xmlHash)
    {
      Manufacturer = manufacturer.CheckNullOrEmtpy("Manufacturer");
      Model = model.CheckNullOrEmtpy("Model");
      Year = year.CheckNullOrEmtpy("Year");
      XmlHash = xmlHash.CheckNullOrEmtpy("Xml-Hash");

      if (xmlHash.Length < PublicBranchNaming.XML_HASH_LENGTH)
      {
        throw new ArgumentException(
          string.Format("Xml-Hash must be at least {0} characters long",
          PublicBranchNaming.XML_HASH_LENGTH));
      }
    }
  }


  public class PublicBranchNaming : IBranchName
  {
    [JsonIgnore]
    public const string TREE_NAME = "public";
    [JsonIgnore]
    public const int XML_HASH_LENGTH = 12;
    [JsonIgnore]
    public const string COMPONENT = "component";
    private const string PATH_SEPARATOR = "/";
    private const int PATH_PARTS = 6;

    private string manufacturer;
    private string model;
    private string year;
    private string xmlHash;


    #region Properties 

    public string Manufacturer
    {
      get { return manufacturer; }
      set { SetManufacturerName(value); }
    }

    public string Year
    {
      get { return year; }
      set { SetYear(value); }
    }

    public string Model
    {
      get { return model; }
      set { SetModelName(value); }
    }

    public string XmlHash
    {
      get { return xmlHash; }
      set
      {
        var xmlCut = value;
        if (value.Length >= XML_HASH_LENGTH)
        {
          xmlCut = value.Substring(0, XML_HASH_LENGTH);
        }
        xmlHash = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(xmlCut).ToLower();
      }
    }

    #endregion

    public PublicBranchNaming(PublicBranchNameParts publicBranchNameParts)
    {
      publicBranchNameParts.CheckNull("branche name parts");

      SetManufacturerName(publicBranchNameParts.Manufacturer);
      SetModelName(publicBranchNameParts.Model);
      SetYear(publicBranchNameParts.Year);
      XmlHash = publicBranchNameParts.XmlHash;
    }


    public PublicBranchNaming(string branchName)
    {
      ParseBranchName(branchName.CheckNullOrEmtpy("branch name"));
    }

    public static PublicBranchNaming DeserializeBranchName(string jsonMessage)
    {
      var branch = JsonConvert.DeserializeObject<PublicBranchNameParts>(jsonMessage);
      return new PublicBranchNaming(branch);
    }

    public string GetJsonRepesentation()
    {
      return JsonConvert.SerializeObject(this);
    }

    public string GetGitPathBranchName()
    {
      return string.Format("{0}/{1}/{2}", TREE_NAME, GetComponentBranchName(), XmlHash);
    }

    public string GetGitPrivateBranchName()
    {
      return string.Format("{0}/{1}", PrivateBranchNaming.TREE_NAME, GetComponentBranchName());
    }

    public string GetComponentBranchName()
    {
      return string.Format("{0}/{1}/{2}/{3}", GetManufacturerName(), COMPONENT, GetYear(), GetModelName());
    }

    public string GetModelName()
    {
      return model;
    }

    public void SetModelName(string modelName)
    {
      model = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName);
    }

    public string GetManufacturerName()
    {
      return manufacturer;
    }

    public void SetManufacturerName(string manufacturerName)
    {
      this.manufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName);
    }

    public string GetYear()
    {
      return year;
    }

    public void SetYear(string year)
    {
      this.year = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(year);
    }

    private void ParseBranchName(string branchName)
    {
      var splitResult = branchName.Split(new[] { PATH_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);

      if (splitResult.Length != PATH_PARTS ||
        splitResult.IsEmpty() || !splitResult[0].Equals(TREE_NAME))
      {
        throw new ArgumentException("Invalid branch name!");
      }

      SetManufacturerName(splitResult[1]);
      SetYear(splitResult[3]);
      SetModelName(splitResult[4]);
      XmlHash = splitResult[5];
    }

    public static bool operator ==(PublicBranchNaming x, PublicBranchNaming y)
    {
      return x?.Manufacturer == y?.Manufacturer &&
             x?.Model == y?.Model &&
             x?.Year == y?.Year &&
             x?.XmlHash == y?.XmlHash;
    }

    public static bool operator !=(PublicBranchNaming x, PublicBranchNaming y)
    {
      return !(x == y);
    }
  }
}
