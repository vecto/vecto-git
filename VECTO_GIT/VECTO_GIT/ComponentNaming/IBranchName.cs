﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.ComponentNaming
{
  public interface IBranchName
  {
    string GetGitPathBranchName();
    string GetComponentBranchName();
    string GetModelName();
    void SetModelName(string modelName);
    string GetManufacturerName();
    void SetManufacturerName(string manufacturerName);
    string GetYear();
    void SetYear(string year);
    //IBranchName ParseBranchName(string branchName);
  }
}
