﻿using System;
using System.Collections.Generic;
using VECTO_GIT.ComponentCommit;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace VECTO_GIT.ComponentNaming
{
  public enum CommitDateType
  {
    ComponentData,
    StandardData
  }
    
  public class CommitDateHandler
  {
    [JsonIgnore]
    public CommitDateType DateType { private set; get; }
    [JsonIgnore]
    public const string COMP_DATA_DATE_NAME = "CertificationDate";
    [JsonIgnore]
    public const string STD_VALUES_DATE_NAME = "CommitDate";
    [JsonIgnore]
    public const string DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public DateTime CertificationDate { set; get; }

    public CommitDateHandler(DateTime dateTime, CommitDateType dateType)
    {
      DateType = dateType;
      CertificationDate = dateTime;
      ConvertDateToUTC();
    }

    public string GetJsonRepresentation()
    {
      switch (DateType)
      {
        case CommitDateType.ComponentData:
          return Serialize(this, false);
        case CommitDateType.StandardData:
          return Serialize(this, true);
      }

      return null;
    }

    public static CommitDateHandler DeserializeCommitDate(string jsonMessage)
    {
      if (jsonMessage.IsNullOrEmptyOrWhitespace())
        return null;

      if (jsonMessage.Contains(COMP_DATA_DATE_NAME))
      {
        var dateTime = Deserialize<CommitDateHandler>(jsonMessage, false);
        return new CommitDateHandler(dateTime.CertificationDate, CommitDateType.ComponentData);
      }
      if (jsonMessage.Contains(STD_VALUES_DATE_NAME))
      {
        var dateTime = Deserialize<CommitDateHandler>(jsonMessage, true);
        return new CommitDateHandler(dateTime.CertificationDate, CommitDateType.StandardData);
      }
      return null;
    }


    private void ConvertDateToUTC()
    {
      if (CertificationDate.Kind != DateTimeKind.Utc)
      {
        CertificationDate = CertificationDate.ToUniversalTime();
      }
    }

    private string Serialize(object obj, bool switchName)
    {
      JsonSerializerSettings settings = new JsonSerializerSettings();
      settings.Formatting = Formatting.Indented;
      settings.DateFormatString = DATE_FORMAT;
      if (switchName)
      {
        settings.ContractResolver = new SwitchNameContractResolver();
      }

      return JsonConvert.SerializeObject(obj, settings);
    }


    static T Deserialize<T>(string text, bool switchName)
    {
      JsonSerializerSettings settings = new JsonSerializerSettings();
      settings.Formatting = Formatting.Indented;
      settings.DateFormatString = DATE_FORMAT;

      if (switchName)
      {
        settings.ContractResolver = new SwitchNameContractResolver();
      }

      return JsonConvert.DeserializeObject<T>(text, settings);
    }


    class SwitchNameContractResolver : DefaultContractResolver
    {
      protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
      {
        var list = base.CreateProperties(type, memberSerialization);

        foreach (JsonProperty prop in list)
        {
          if (prop.UnderlyingName == COMP_DATA_DATE_NAME)
          {
            prop.PropertyName = STD_VALUES_DATE_NAME;
            break;
          }

          if (prop.UnderlyingName == STD_VALUES_DATE_NAME)
          {
            prop.PropertyName = COMP_DATA_DATE_NAME;
            break;
          }
        }
        return list;
      }
    }
  }  
}