﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;
namespace VECTO_GIT.Commands
{
  public class PushBranchCommand : Command, ICommandResult<string>
  {
    private readonly List<string> branchNames;
    private string transferRepoPath;
    private Remote remoteRepository;
    public List<string> PushErrors { private set; get; }

    public PushBranchCommand(Repository repository, string transferRepoPath, string branchName)
      : base(repository)
    {
      this.transferRepoPath = transferRepoPath.CheckFolderPath();
      branchNames = new List<string>
      {
        branchName.CheckNullOrEmtpy(nameof(branchName))
      };
      PushErrors = new List<string>();

      Execute();
    }

    public PushBranchCommand(Repository repository, string transferRepoPath, List<string> branchNames)
      : base(repository)
    {
      this.transferRepoPath = transferRepoPath.CheckFolderPath();
      this.branchNames = branchNames.CheckNullOrEmpty();
      PushErrors = new List<string>();

      Execute();
    }

    protected sealed override void Execute()
    {
      transferRepoPath = Repository.Init(transferRepoPath, true);
      using (var repo = new Repository(transferRepoPath))
      {
        SetRemoteRepository();
        PushBranchesToRemote();
        var tags = SearchRelatedTags();
        PushTagsToRemote(tags);
        RemoveRemoteEntry(repository);
      }
    }

    public string Result()
    {
      if (PushErrors.Count > 0)
      {
        DeleteIncorrectTransferRepo(transferRepoPath);
        return null;
      }
      return transferRepoPath;
    }

    protected void PushStatusErrorHandler(PushStatusError error)
    {
      PushErrors.Add(error.Message);
    }

    private void SetRemoteRepository()
    {
      RemoveExistingExistingRemote(ConfigValue.REMOTE_NAME);
      remoteRepository = repository.Network.Remotes.Add(ConfigValue.REMOTE_NAME, transferRepoPath);
    }

    private void RemoveExistingExistingRemote(string remoteRepositoryName)
    {
      var remotes = repository.Network.Remotes;
      if(remotes != null && remotes.Any())
      {
        foreach (var remote in remotes)
        {
          if(remote.Name == remoteRepositoryName)
          {
            if(remoteRepository != null)
            {              
              remoteRepository.Dispose();
            }
            repository.Network.Remotes.Remove(remoteRepositoryName);
            break;
          }
        }
      }
    }



    private void PushTagsToRemote(IEnumerable<Tag> tags)
    {
      if (!(tags == null || tags.Any()))
      {
        return;
      }

      var option = new PushOptions();
      option.OnPushStatusError = PushStatusErrorHandler;

      foreach (var tag in tags)
      {
        var tagPath = ConfigValue.REFERENCE_TAG_PATH + "/" + tag.FriendlyName;
        repository.Network.Push(remoteRepository, tagPath, option);
      }
    }

    private void DeleteIncorrectTransferRepo(string repositoryPath)
    {
      Helpers.DeleteDirectory(repositoryPath);
    }

    private void PushBranchesToRemote()
    {
      if (branchNames.IsNullOrEmpty())
      {
        return;
      }

      var option = new PushOptions
      {
        OnPushStatusError = PushStatusErrorHandler
      };

      foreach (var branchName in branchNames)
      {
        var branchPath = ConfigValue.REFERENCE_HEAD_PATH + "/" + branchName;
        repository.Network.Push(remoteRepository, branchPath, option);        
      }
    }

    private Tag[] SearchRelatedTags()
    {
      return new SearchTagCommand(repository, branchNames).Result();
    }

    private void RemoveRemoteEntry(Repository selectedRepository)
    {
      if(remoteRepository != null)
      {
        remoteRepository.Dispose();
      }

      selectedRepository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
    }

  }

}
