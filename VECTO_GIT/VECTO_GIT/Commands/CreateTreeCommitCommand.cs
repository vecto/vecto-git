﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;

namespace VECTO_GIT.Commands
{
  public class CreateTreeCommitCommand : Command, ICommandResult<Commit>
  {
    private readonly Tree commitTree;
    private readonly string commitMessage;
    private readonly Reference branchReference;
    private readonly IEnumerable<Commit> parentCommit;
    private Commit resultCommit;

    public CreateTreeCommitCommand(Repository repository, Tree commitTree, string commitMessage) : base(repository)
    {
      parentCommit = Enumerable.Empty<Commit>();
      this.commitTree = commitTree.CheckNull(nameof(commitTree));
      this.commitMessage = commitMessage.CheckNull(nameof(this.commitMessage));

      Execute();
    }

    public CreateTreeCommitCommand(Repository repository, Tree commitTree, string commitMessage,
      Reference branchReference, Commit parentCommit) : base(repository)
    {
      this.parentCommit = new [] { parentCommit.CheckNull(nameof(parentCommit)) };
      this.commitTree = commitTree.CheckNull(nameof(commitTree));
      this.commitMessage = commitMessage.CheckNull(nameof(commitMessage));
      this.branchReference = branchReference;

      Execute();
    }

    protected sealed override void Execute()
    {
      var signature = this.commitSignature.GetSignature;

      resultCommit = repository.ObjectDatabase.CreateCommit(signature, signature,
        commitMessage, commitTree, parentCommit, true);

      if (!parentCommit.Equals(Enumerable.Empty<Commit>()) && branchReference != null)
      {
        UpdateBranchHead();
      }
    }

    public Commit Result()
    {
      return resultCommit;
    }

    private void UpdateBranchHead()
    {
      repository.Refs.UpdateTarget(branchReference, resultCommit.Id);
    }




  }
}
