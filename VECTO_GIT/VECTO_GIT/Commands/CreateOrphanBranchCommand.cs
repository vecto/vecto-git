﻿using LibGit2Sharp;

namespace VECTO_GIT.Commands
{
  public class CreateOrphanBranchCommand : Command, ICommandResult<Branch>
  {
    private readonly string branchName;
    private readonly Commit commit;
    private Branch resultBranch;

    public CreateOrphanBranchCommand(Repository repository, string branchName, Commit commit)
      : base(repository)
    {
      this.commit = commit.CheckNull(nameof(commit));
      this.branchName = branchName.CheckNullOrEmtpy(nameof(branchName));

      Execute();
    }

    protected sealed override void Execute()
    {
      resultBranch = repository.Branches[branchName];
      if (resultBranch == null)
        resultBranch = repository.CreateBranch(branchName, commit);
    }

    public Branch Result()
    {
      return resultBranch;
    }
  }
}
