﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.Commands
{
  public class SearchDateCommand : Command, ICommandResult<IEnumerable<Branch>>
  {
    private readonly IEnumerable<Branch> browseBranches;
    private readonly VectoSearchTerms searchTerms;
    private IEnumerable<Branch> searchResult;

    public SearchDateCommand(Repository repository, VectoSearchTerms searchTerms,
      IEnumerable<Branch> browseBranches = null) : base(repository)
    {
      this.searchTerms = searchTerms.CheckNull(nameof(searchTerms));
      this.browseBranches = browseBranches ?? repository.Branches;
      Execute();
    }
    
    protected sealed override void Execute()
    {
      searchResult = FilterSearchBySearchDates();
    }
    
    private IEnumerable<Branch> FilterSearchBySearchDates()
    {
      var branches = browseBranches.ToList();
      var filteredBranches = new List<Branch>();

      for (var i = 0; i < branches.Count; i++)
      {
        var branch = branches[i];
        var isStdDataCommit = StandardDataCommit.GetStandardDataCommit(branch);
        var isCompDataCommit = ComponentDataCommit.GetComponentDataCommit(branch);

        Commit selectedCommit;
        if (isStdDataCommit != null)
        {
          selectedCommit = isStdDataCommit;
        }
        else if (isCompDataCommit != null)
        {
          selectedCommit = isCompDataCommit;
        }
        else
        {
          continue;
        }

        if (IsMatchedDate(selectedCommit))
        {
          filteredBranches.Add(branch);
        }
      }

      return filteredBranches;
    }

    private bool IsMatchedDate(Commit selectedCommit)
    {
      var commitDate = CommitDateHandler.DeserializeCommitDate(selectedCommit.Message);

      if (commitDate != null)
      {
        if (searchTerms.IsCertificateDate)
        {
          return (commitDate.CertificationDate.Date == searchTerms.CertificateDate.Date);
        }

        if (searchTerms.IsCertificateDateFromTo)
        {
          return commitDate.CertificationDate.Date >= searchTerms.CertificateDateFrom.Date &&
                 commitDate.CertificationDate.Date <= searchTerms.CertificateDateTo.Date;
        }
      }
      return false;
    }
    

    public IEnumerable<Branch> Result()
    {
      return searchResult;
    }
  }
}
