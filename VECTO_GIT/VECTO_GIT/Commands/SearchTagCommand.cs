﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.Commands
{
  public enum TagNameType
  {
    CertificateNumber,
    GitId,
    SearchTagName
  }

  public class SearchTagCommand : Command, ICommandArrayResult<Tag>
  {
    private readonly string tagName;
    private readonly TagNameType tagNameType;
    private readonly List<string> branchNames;
    private readonly INamingHandler namingHandler;
    private readonly CertificationNumberModel certificationNumber;
    private IEnumerable<Tag> foundedTags;
    
    public SearchTagCommand(Repository repository, CertificationNumberModel certificationNumber)
      : base(repository)
    {
      this.certificationNumber = certificationNumber.CheckNull(nameof(certificationNumber));
      Execute();
    }
    
    public SearchTagCommand(Repository repository, INamingHandler namingHandler)
      : base(repository)
    {
      this.namingHandler = namingHandler.CheckNull(nameof(namingHandler));
      Execute();
    }

    public SearchTagCommand(Repository repository, string tagName, TagNameType tagNameType)
      : base(repository)
    {
      namingHandler = null;
      this.tagName = tagName.CheckNullOrEmtpy(nameof(tagName));
      this.tagNameType = tagNameType.CheckNull(nameof(tagNameType));
      Execute();
    }

    public SearchTagCommand(Repository repository, List<string> branchNames) : base(repository)
    {
      namingHandler = null;
      this.branchNames = branchNames.CheckNullOrEmpty();
      Execute();
    }

    protected sealed override void Execute()
    {
      if (certificationNumber != null)
      {
        foundedTags = SearchSparseCertificationTags();
      }
      else if (namingHandler is CertificateNumberHandler || namingHandler is GitIdHandler)
      {
        foundedTags = LocateTagByNamingHandler();
      }
      else if (!string.IsNullOrWhiteSpace(tagName))
      {
        foundedTags = LocateTagsByTagName();
      }
      else
      {
        foundedTags = SelectBranchesWithTags();
      }
    }

    public Tag[] Result()
    {
      return foundedTags.ToArray();
    }

    private IEnumerable<Tag> LocateTagByNamingHandler()
    {
      var tagPathName = namingHandler.GetTagGitPathName();
      return TagsContainsString(tagPathName);
    }
    
    private IEnumerable<Tag> SearchSparseCertificationTags()
    {
      var termsCounter = certificationNumber.GetCertificateSearchPartsCount();
      if (termsCounter == 0)
      {
        return Enumerable.Empty<Tag>();
      }

      var connectionSymbol = certificationNumber.ConnectionSymbol;
      var tagSeperator = new []
      {
        "/",
        connectionSymbol
      };
      var searchResult = new List<Tag>();
      var tags = repository.Tags.ToList();

      for (int i = 0; i < tags.Count; i++)
      {
        var tag = tags[i];
        if (tag.FriendlyName.StartsWith(ConfigValue.CERT_NR_FOLDER_NAME))
        {
          var tagName = tag.FriendlyName.Substring(ConfigValue.CERT_NR_FOLDER_NAME.Length);
          if (IsCertificationTagMatch(tagName, certificationNumber, tagSeperator))
            searchResult.Add(tag);
        }
      }

      return searchResult;

    }

    private bool IsCertificationTagMatch(string tagName, CertificationNumberModel certNumber, string[] tagSeperator)
    {
      var tagParts = tagName.Split(tagSeperator, StringSplitOptions.RemoveEmptyEntries);
      var searchPartsCount = certNumber?.GetCertificateSearchPartsCount();

      if (tagParts.Length != (CertificateNumberHandler.PATH_PARTS - 1)
          || certNumber == null || searchPartsCount == 0)
      {
        return false;
      }

      var hitCounter = 0;
      var countryTag = tagParts[0];
      if (countryTag != null && countryTag.StartsWith("e") && countryTag.Length > 1)
      {
        countryTag = tagParts[0].Substring(1, tagParts[0].Length - 1);
      }

      if (certNumber.Country == countryTag)
        hitCounter++;

      if (certNumber.CertificationAct != null && tagParts[1].Contains(certNumber.CertificationAct))
        hitCounter++;

      if (certNumber.AmendingAct != null && tagParts[2].Contains(certNumber.AmendingAct))
        hitCounter++;

      if (certNumber.ComponentType == tagParts[3])
        hitCounter++;

      if (certNumber.BaseNumber != null && tagParts[4].Contains(certNumber.BaseNumber))
        hitCounter++;

      if (certNumber.ExtensionNumber != null && tagParts[5].Contains(certNumber.ExtensionNumber))
        hitCounter++;

      return hitCounter == searchPartsCount;
    }


    private IEnumerable<Tag> LocateTagsByTagName()
    {
      if (tagNameType.Equals(TagNameType.CertificateNumber))
      {
        var certPathName = new CertificateNumberHandler(tagName, false).GetTagGitPathName();
        return TagsContainsString(certPathName);
      }

      if (tagNameType.Equals(TagNameType.GitId))
      {
        var gitPathName = new GitIdHandler(tagName, false).GetTagGitPathName();
        return TagsContainsString(gitPathName);
      }

      if (tagNameType.Equals(TagNameType.SearchTagName))
      {
        return TagsContainsString(tagName);
      }

      return Enumerable.Empty<Tag>();
    }

    //http://cc.davelozinski.com/c-sharp/fastest-way-to-check-if-a-string-occurs-within-a-string
    private IEnumerable<Tag> TagsContainsString(string searchString)
    {
      var foundedTags = new List<Tag>();
      foreach (var tag in repository.Tags)
      {
        var tagName = tag.FriendlyName;
        if (ContainsString(tagName, searchString))
        {
          foundedTags.Add(tag);
        }
      }
      return foundedTags;
    }

    private bool ContainsString(string desString, string searchString)
    {
      return ((desString.Length - desString.Replace(searchString, string.Empty).Length) / searchString.Length > 0);
    }

    private IEnumerable<Tag> SelectBranchesWithTags()
    {
      var pubBranchNames = branchNames.Where(obj => obj.StartsWith(PublicBranchNaming.TREE_NAME + "/"));
      var tagCommits = new List<string>();

      foreach (var branchName in pubBranchNames)
      {
        if (!string.IsNullOrEmpty(branchName))
        {
          var commitId = GetComponentDataCommitId(repository.Branches[branchName]);
          if (commitId != null)
          {
            tagCommits.Add(commitId);
          }
        }
      }
      return FetchTagsWithCommitId(tagCommits);
    }

    private string GetComponentDataCommitId(Branch branch)
    {
      var commit = ComponentDataCommit.GetComponentDataCommit(branch);
      if (commit != null)
      {
        return commit.Sha;
      }
      return null;
    }

    private IEnumerable<Tag> FetchTagsWithCommitId(IEnumerable<string> commitIds)
    {
      var locatedTags = new List<Tag>();

      foreach (var tag in repository.Tags)
      {
        if (tag.Target is Commit)
        {
          if (commitIds.Contains(tag.Target.Sha))
          {
            locatedTags.Add(tag);
          }
        }
      }
      return locatedTags;
    }
  }
}
