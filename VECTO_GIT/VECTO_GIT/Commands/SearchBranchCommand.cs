﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.Commands
{
  public enum SearchBy
  {
    Manufacturer,
    Model,
    Year,
    BranchName,
    PrivateBranchName,
    ComponentName
  }


  public class SearchBranchCommand : Command, ICommandResult<IEnumerable<Branch>>
  {
    private readonly char[] branchNameSeperator = { '/' };
    private readonly char[] searchSeperator = { '_' };

    private readonly PublicBranchNaming branchNameHandler;
    private readonly string[] searchTerms;
    private readonly VectoSearchTerms vectoSearchTerms;

    private string searchTerm;
    private IEnumerable<Branch> browseBranches;
    private IEnumerable<Branch> searchResult;

    public SearchBranchCommand(Repository repository, VectoSearchTerms vectoSearchTerms,
      IEnumerable<Branch> browseBranches = null) : base(repository)
    {
      this.vectoSearchTerms = vectoSearchTerms;
      SetRightBranchColletion(browseBranches);
      Execute();
    }

    public SearchBranchCommand(Repository repository, string[] searchTerms,
      IEnumerable<Branch> browseBranches = null) : base(repository)
    {
      this.searchTerms = searchTerms.CheckNullOrEmpty(nameof(searchTerms));
      this.searchTerms = Array.ConvertAll(this.searchTerms, x => x.ToLower());
      SetRightBranchColletion(browseBranches);
      Execute();
    }

    public SearchBranchCommand(Repository repository, string searchTerm,
      IEnumerable<Branch> browseBranches = null) : base(repository)
    {
      this.searchTerm = searchTerm.CheckNullOrEmtpy(nameof(searchTerm)).ToLower();
      SetRightBranchColletion(browseBranches);
      Execute();
    }

    public SearchBranchCommand(Repository repository, PublicBranchNaming branchNameHandler,
      SearchBy searchBy, IEnumerable<Branch> browseBranches = null) :
      base(repository)
    {
      this.branchNameHandler = branchNameHandler.CheckNull(nameof(branchNameHandler));
      SetRightBranchColletion(browseBranches);
      SetSearchTerm(searchBy);
      Execute();
    }

    public SearchBranchCommand(Repository repository) : base(repository)
    {
      GetAllBranches();
    }

    protected sealed override void Execute()
    {
      if (vectoSearchTerms != null)
      {
        SearchPublicBranchesByVectoSearchTerms();
      }
      else if (!string.IsNullOrEmpty(searchTerm))
      {
        SelectiveSearch();
      }
      else if (!searchTerms.IsNullOrEmpty())
      {
        ExecuteSearchNew();
      }
      else
      {
        searchResult = Enumerable.Empty<Branch>();
      }
    }

    public IEnumerable<Branch> Result()
    {
      return searchResult;
    }

    private void ExecuteSearchNew()
    {
      var selectedBranches = new List<Branch>();

      foreach (var branch in browseBranches)
      {
        var branchName = branch.FriendlyName.ToLower();
        var branchNameSplit = branchName.Split(branchNameSeperator, StringSplitOptions.RemoveEmptyEntries);

        if (branchNameSplit.Distinct().Intersect(searchTerms).Count() == searchTerms.Count())
        {
          selectedBranches.Add(branch);
        }
      }

      searchResult = selectedBranches;
    }

    private void SearchPublicBranchesByVectoSearchTerms()
    {
      var branches = browseBranches.ToList();
      var locatedBranches = new List<Branch>();
      string[] selectedCertYears = null;

      if (vectoSearchTerms.AnySelectedDate)
      {
        selectedCertYears = GetSelectedCertificationYears(vectoSearchTerms);
      }

      for (int i = 0; i < branches.Count; i++)
      {
        var branchParts = branches[i].FriendlyName.Split(branchNameSeperator, StringSplitOptions.RemoveEmptyEntries);
        if (branchParts.Length >= 4 && branchParts[0] == PublicBranchNaming.TREE_NAME)
        {
          if (!vectoSearchTerms.Manufacturer.IsNullOrEmptyOrWhitespace())
          {
            if (!IsManufacturerNameMatched(branchParts[1].ToLower()))
            {
              continue;
            }
          }

          if (!vectoSearchTerms.Model.IsNullOrEmptyOrWhitespace())
          {
            if (!IsModelNameMatched(branchParts[4].ToLower()))
            {
              continue;
            }
          }

          if (vectoSearchTerms.AnySelectedDate)
          {
            if (!IsAnyCertificationDateMatch(branchParts[3], selectedCertYears))
            {
              continue;
            }
          }

          locatedBranches.Add(branches[i]);
        }
      }

      searchResult = locatedBranches;
    }


    private bool IsManufacturerNameMatched(string branchManufacturerName)
    {
      return ContainsString(branchManufacturerName, vectoSearchTerms.Manufacturer);
    }

    private bool IsModelNameMatched(string branchModelName)
    {
      return ContainsString(branchModelName, vectoSearchTerms.Model);
    }


    private bool IsAnyCertificationDateMatch(string branchYear, string[] selectedYears)
    {
      for (int i = 0; i < selectedYears.Length; i++)
      {
        var year = selectedYears[i];
        if (year.Equals(branchYear))
        {
          return true;
        }
      }
      return false;
    }

    private string[] GetSelectedCertificationYears(VectoSearchTerms searchTerms)
    {
      var result = new List<string>();


      if (!searchTerms.AnySelectedDate)
        return result.ToArray();

      if (searchTerms.IsCertificateDate)
      {
        result.Add(searchTerms.CertificateDate.Year.ToString());
      }

      if (searchTerms.IsCertificateDateFromTo)
      {
        result.Add(searchTerms.CertificateDateFrom.Year.ToString());
        result.Add(searchTerms.CertificateDateTo.Year.ToString());
      }

      return result.Distinct().ToArray();
    }

    private bool ContainsString(string desString, string searchString)
    {
      return ((desString.Length - desString.Replace(searchString, string.Empty).Length) / searchString.Length > 0 ? true : false);
    }

    private void GetAllBranches()
    {
      searchResult = repository.Branches;
    }

    private void SetSearchTerm(SearchBy searchBy)
    {
      switch (searchBy)
      {
        case SearchBy.Model:
          searchTerm = branchNameHandler.GetModelName().ToLower();
          break;
        case SearchBy.Manufacturer:
          searchTerm = branchNameHandler.GetManufacturerName().ToLower();
          break;
        case SearchBy.Year:
          searchTerm = branchNameHandler.GetYear().ToLower();
          break;
        case SearchBy.BranchName:
          searchTerm = branchNameHandler.GetGitPathBranchName().ToLower();
          break;
        case SearchBy.PrivateBranchName:
          searchTerm = branchNameHandler.GetGitPrivateBranchName().ToLower();
          break;
        case SearchBy.ComponentName:
          searchTerm = branchNameHandler.GetComponentBranchName().ToLower();
          break;
      }
    }


    private void SelectiveSearch()
    {
      searchResult = browseBranches.Where(obj => obj.FriendlyName.ToLower().Contains(searchTerm));
    }


    private void SetRightBranchColletion(IEnumerable<Branch> browseBranches)
    {
      if (browseBranches == null || !browseBranches.Any())
      {
        this.browseBranches = repository.Branches;
      }
      else
      {
        this.browseBranches = browseBranches;
      }
    }

  }
}
