﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.Commands
{
  public interface ICommandResult<T>
  {
    T Result();
  }
}
