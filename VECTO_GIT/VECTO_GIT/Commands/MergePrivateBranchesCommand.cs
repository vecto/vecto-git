﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.Commands
{
  public class MergePrivateBranchesCommand : MergeBase
  {
    #region Members
    private readonly Repository repository;
    private bool addedPrivateData;
    private bool mergeConflicts;


    #endregion

    public MergePrivateBranchesCommand(Repository repository) : base(repository)
    {
      this.repository = repository;
    }


    public List<MergePrivateBranchModel> MergePrivateBranches(List<string> localBranchNames, List<string> remoteBranchNames)
    {
      var localBranchNamings = GetPrivateBranchNamings(localBranchNames);
      var remoteBranchNamings = GetPrivateBranchNamings(remoteBranchNames);
      var branchesToRemove = new List<PrivateBranchNaming>();

      var conflicts = new List<MergePrivateBranchModel>();
      for (int l = 0; l < localBranchNamings.Count; l++)
      {
        var localBranchName = localBranchNamings[l];
        for (int r = 0; r < remoteBranchNamings.Count; r++)
        {

          if (localBranchName == remoteBranchNamings[r])
          {
            var conflict = MergePrivateBranch(localBranchName, remoteBranchNamings[r]);
            if (conflict != null)
            {
              conflicts.Add(conflict);
              mergeConflicts = true;
            }

            branchesToRemove.Add(remoteBranchNamings[r]);
          }

        }

        for (int j = branchesToRemove.Count - 1; j >= 0; --j)
        {
          remoteBranchNamings.Remove(branchesToRemove[j]);
        }
        branchesToRemove.Clear();
      }

      if (remoteBranchNamings.Count > 0)
      {
        TransferNewPrivateBranches(remoteBranchNamings);
      }

      SetMergeResultMessage();

      return conflicts;
    }

    protected override void SetMergeResultMessage()
    {
      if (addedPrivateData)
        mergeResultMessages.Add(new KeyValuePair<string, bool>("Private data successfully added!", addedPrivateData));

      if (!addedPrivateData && !mergeConflicts)
        mergeResultMessages.Add(new KeyValuePair<string, bool>("No private data too add!", !addedPrivateData));

      if (mergeConflicts)
        mergeResultMessages.Add(new KeyValuePair<string, bool>("Merge conflicts of private data to solve!", !mergeConflicts));

    }



    private List<PrivateBranchNaming> GetPrivateBranchNamings(List<string> branchNames)
    {
      var pubBranchNames = new List<PrivateBranchNaming>();
      for (int i = 0; i < branchNames.Count; i++)
      {
        pubBranchNames.Add(new PrivateBranchNaming(branchNames[i]));
      }

      return pubBranchNames;
    }


    private MergePrivateBranchModel MergePrivateBranch(PrivateBranchNaming localBranchNaming, PrivateBranchNaming remoteBranchNaming)
    {
      var localBranch = GetLocalBranch(localBranchNaming.GetGitPathBranchName());
      var remoteBranch = GetRemoteBranch(remoteBranchNaming.GetGitPathBranchName());

      if (localBranch.Tip.Tree.Sha != remoteBranch.Tip.Tree.Sha)
      {
        var measurementTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch);
        var userDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch);

        List<TreeEntry> uNewEntries;
        List<MergeEntityModel> uConflictEntries;
        var userBranchChange = MergeUserData(localBranch, remoteBranch, out uNewEntries, out uConflictEntries);
        if (userBranchChange != null)
          localBranch = userBranchChange;

        List<TreeEntry> mNewEntries;
        List<MergeEntityModel> mConflictEntries;
        var measurementBranchChange = MergeMeasurementData(localBranch, remoteBranch, out mNewEntries, out mConflictEntries);
        if (measurementBranchChange != null)
          localBranch = measurementBranchChange;

        if (userBranchChange != null || measurementBranchChange != null)
          addedPrivateData = true;

        if (uConflictEntries?.Count > 0 || mConflictEntries?.Count > 0)
        {
          var conflict = new MergePrivateBranchModel(localBranch, remoteBranch);

          conflict.SetEntityConflicts(uConflictEntries);
          conflict.SetEntityConflicts(mConflictEntries);

          var measurementTreeEntries = measurementTree?.ToList();
          var userDateTreeEntries = userDataTree?.ToList();

          conflict.NewMeasurementEntries = mNewEntries;
          conflict.NewUserDataEntries = uNewEntries;

          conflict.SetAlreadyUsedFileNames(measurementTreeEntries, FileType.MeasurementData);
          conflict.SetAlreadyUsedFileNames(mNewEntries, FileType.MeasurementData);

          conflict.SetAlreadyUsedFileNames(userDateTreeEntries, FileType.UserData);
          conflict.SetAlreadyUsedFileNames(uNewEntries, FileType.UserData);

          return conflict;
        }

        if (uNewEntries != null && uNewEntries.Count > 0 || mNewEntries != null && mNewEntries.Count > 0)
        {
          if (uNewEntries != null && uNewEntries.Count > 0)
          {
            localBranch = AddNewUserDataEntries(localBranch, uNewEntries);
          }

          if (mNewEntries != null && mNewEntries.Count > 0)
          {
            localBranch = AddNewMeasurementDataEntries(localBranch, mNewEntries);
          }
          addedPrivateData = true;
        }
      }
      return null;
    }

    private Branch AddNewMeasurementDataEntries(Branch localBranch, List<TreeEntry> mNewEntries)
    {
      var localTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch);
      var treeDefinition = TreeDefinition.From(localTree);

      for (int i = 0; i < mNewEntries.Count; i++)
      {
        treeDefinition.Add(mNewEntries[i].Name, mNewEntries[i]);
      }

      var tree = repository.ObjectDatabase.CreateTree(treeDefinition);

      var measurementDataCommit = new MeasurementDataCommit(repository, tree, localBranch);
      return measurementDataCommit.ExecuteCommit();
    }


    private Branch AddNewUserDataEntries(Branch localBranch, List<TreeEntry> uNewEntries)
    {
      var localTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch);
      var treeDefinition = TreeDefinition.From(localTree);

      for (int i = 0; i < uNewEntries.Count; i++)
      {
        treeDefinition.Add(uNewEntries[i].Name, uNewEntries[i]);
      }

      var tree = repository.ObjectDatabase.CreateTree(treeDefinition);

      var userDataCommit = new UserDataCommit(repository, tree, localBranch);
      return userDataCommit.ExecuteCommit();
    }

    private Branch MergeMeasurementData(Branch localBranch, Branch remoteBranch, out List<TreeEntry> newTreeEntries,
      out List<MergeEntityModel> conflictTreeEntries)
    {
      var localMeasurementTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, localBranch);
      var remoteMeasurementTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, remoteBranch);

      newTreeEntries = null;
      conflictTreeEntries = null;

      if (localMeasurementTree == null && remoteMeasurementTree != null)
      {
        var measurementCommit = new MeasurementDataCommit(repository, remoteMeasurementTree, localBranch);
        return measurementCommit.ExecuteCommit();
      }
      if (localMeasurementTree != null && remoteMeasurementTree != null)
      {
        if (localMeasurementTree.Sha != remoteMeasurementTree.Sha)
        {
          CompareSingleFileEntries(localMeasurementTree, remoteMeasurementTree, FileType.MeasurementData,
            out newTreeEntries, out conflictTreeEntries);
        }
      }

      return null;
    }

    private Branch MergeUserData(Branch localBranch, Branch remoteBranch, out List<TreeEntry> newTreeEntries, out List<MergeEntityModel> conflictTreeEntries)
    {
      var localUserDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, localBranch);
      var remoteUserDataTree = PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, remoteBranch);

      newTreeEntries = null;
      conflictTreeEntries = null;

      if (localUserDataTree == null && remoteUserDataTree != null)
      {
        var userDataCommit = new UserDataCommit(repository, remoteUserDataTree, localBranch);
        return userDataCommit.ExecuteCommit();
      }

      if (localUserDataTree != null && remoteUserDataTree != null)
      {
        if (localUserDataTree.Sha != remoteUserDataTree.Sha)
        {
          CompareSingleFileEntries(localUserDataTree, remoteUserDataTree, FileType.UserData
            , out newTreeEntries, out conflictTreeEntries);
        }
      }
      return null;
    }

    private void CompareSingleFileEntries(Tree localTipTree, Tree remoteTipTree, FileType dataType,
                                          out List<TreeEntry> newTreeEntries, out List<MergeEntityModel> sameNamedEntries)
    {
      newTreeEntries = new List<TreeEntry>();
      sameNamedEntries = new List<MergeEntityModel>();

      var localEntries = localTipTree.ToList();
      var remoteEntries = remoteTipTree.ToList();

      for (int i = 0; i < remoteEntries.Count; i++)
      {
        var remoteEntry = remoteEntries[i];
        var newEntry = true;
        for (int j = 0; j < localEntries.Count; j++)
        {
          if (localEntries[j].TargetType != remoteEntry.TargetType)
            continue;

          var localEntry = localEntries[j];
          if (remoteEntry.Name == localEntry.Name)
          {
            if (remoteEntry.Target.Sha != localEntry.Target.Sha)
            {
              var conflict = new MergeEntityModel(localEntry, remoteEntry, dataType);
              sameNamedEntries.Add(conflict);
            }
            newEntry = false;
            break;
          }
        }

        if (newEntry)
        {
          newTreeEntries.Add(remoteEntry);
        }

      }
    }

    private void TransferNewPrivateBranches(List<PrivateBranchNaming> branchNames)
    {
      for (int i = 0; i < branchNames.Count; i++)
      {
        var branchName = branchNames[i].GetGitPathBranchName();
        var remoteBranch = GetRemoteBranch(branchName);
        CreateRemoteBranchLocaly(branchName, remoteBranch);
      }
    }
    

    private Branch CreateRemoteBranchLocaly(string branchName, Branch remoteBranch)
    {
      return repository.CreateBranch(branchName, remoteBranch.Tip);
    }



  }
}
