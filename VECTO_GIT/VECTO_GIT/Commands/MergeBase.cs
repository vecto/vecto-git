﻿using LibGit2Sharp;
using System.Collections.Generic;

namespace VECTO_GIT.Commands
{
  public abstract class MergeBase
  {
    private readonly Repository repository;
    protected List<KeyValuePair<string, bool>> mergeResultMessages;
    
    protected MergeBase(Repository repository)
    {
      this.repository = repository.CheckNull(nameof(repository));
      mergeResultMessages = new List<KeyValuePair<string, bool>>();
    }

    public List<KeyValuePair<string, bool>> GetMergeResultMessages()
    {
      return mergeResultMessages;
    }

    protected abstract void SetMergeResultMessage();

    protected Branch GetLocalBranch(string branchName)
    {
      return repository.Branches[branchName];
    }

    protected Branch GetRemoteBranch(string branchName)
    {
      var remoteBranchName = $"{ConfigValue.REMOTE_NAME}/{branchName}";
      return repository.Branches[remoteBranchName];
    }

  }
}
