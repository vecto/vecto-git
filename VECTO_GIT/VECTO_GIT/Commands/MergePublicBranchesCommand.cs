﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;

namespace VECTO_GIT.Commands
{
  public class MergePublicBranchesCommand : MergeBase
  {
    public class PublicBranchCommit
    {
      private Commit standardValueCommit;
      private Commit componentCommit;
      private Commit certificateCommit;

      public Commit StandardValueCommit
      {
        get { return standardValueCommit; }
        set
        {
          standardValueCommit = value;
          if (value != null)
            CommitCounter++;
        }
      }

      public Commit ComponentCommit
      {
        get { return componentCommit; }
        set
        {
          componentCommit = value;
          if (value != null)
            CommitCounter++;
        }
      }

      public Commit CertificateCommit
      {
        get { return certificateCommit; }
        set
        {
          certificateCommit = value;
          if (value != null)
            CommitCounter++;
        }
      }
      public int CommitCounter { get; set; }
      public PublicBranchCommit(Branch branch)
      {
        FetchBranchBlobs(branch);
      }

      private void FetchBranchBlobs(Branch branch)
      {
        StandardValueCommit = StandardDataCommit.GetStandardDataCommit(branch);
        ComponentCommit = ComponentDataCommit.GetComponentDataCommit(branch);
        CertificateCommit = CertificateDataCommit.GetCertificateDataCommit(branch);
      }
    }



    #region Members
    private readonly Repository repository;
    private readonly List<string> remotePublicBranchNames;
    private readonly List<string> localPublicBranchNames;
    private readonly string remoteRepositoryPath;

    private int addedPublicBranches;
    private int cmpDataAdded;
    private int stdDataAdded;
    private int certDataAdded;
    #endregion

    public MergePublicBranchesCommand(Repository repository, List<string> localPublicBranchNames,
      List<string> remotePublicBranchNames, string remoteRepositoryPath) : base(repository)
    {
      this.repository = repository;
      this.remotePublicBranchNames = remotePublicBranchNames;
      this.localPublicBranchNames = localPublicBranchNames;
      this.remoteRepositoryPath = remoteRepositoryPath.CheckNull(nameof(remoteRepositoryPath));
    }


    public void ExecutePublicMerge()
    {
      if (remotePublicBranchNames != null && remotePublicBranchNames.Count > 0)
      {
        if (localPublicBranchNames != null && localPublicBranchNames.Count > 0) // 1 1
        {
          MergePublicBranches(localPublicBranchNames, remotePublicBranchNames);
        }
        else //1 0
        {
          //add all remote public branches no merge needed
          PullAllRemoteBranches(remotePublicBranchNames);
        }
      }

      SetMergeResultMessage();
    }

    protected override void SetMergeResultMessage()
    {
      KeyValuePair<string, bool> message;
      if (addedPublicBranches > 0 || stdDataAdded > 0 || cmpDataAdded > 0 || certDataAdded > 0)
        message = new KeyValuePair<string, bool>("Public data successfully added!", false);
      else
        message = new KeyValuePair<string, bool>("No public data too add!", true);
      mergeResultMessages.Add(message);
    }

    private void MergePublicBranches(List<string> localBranchNames, List<string> remoteBranchNames)
    {
      var localBranchNamings = GetSplitedPublicBranchNames(localBranchNames);
      var remoteBranchNamings = GetSplitedPublicBranchNames(remoteBranchNames);
      var branchesToRemove = new List<PublicBranchNaming>();


      for (int i = 0; i < localBranchNamings.Count; i++)
      {
        var localBranchName = localBranchNamings[i];
        for (int j = 0; j < remoteBranchNamings.Count; j++)
        {
          if (localBranchName.Manufacturer == remoteBranchNamings[j].Manufacturer &&
              localBranchName.Model == remoteBranchNamings[j].Model)
          {
            if (MergeBranches(localBranchName, remoteBranchNamings[j]))
            {
              branchesToRemove.Add(remoteBranchNamings[j]);
            }
          }
        }

        for (int j = branchesToRemove.Count - 1; j >= 0; --j)
        {
          remoteBranchNamings.Remove(branchesToRemove[j]);
        }
        branchesToRemove.Clear();
      }

      if (remoteBranchNamings.Count > 0)
      {
        TransferNewPublicBranches(remoteBranchNamings);
      }
    }


    private void TransferNewPublicBranches(List<PublicBranchNaming> branchNames)
    {

      for (int i = 0; i < branchNames.Count; i++)
      {
        var branchName = branchNames[i].GetGitPathBranchName();
        var remoteBranch = GetRemoteBranch(branchName);
        var compCommit = ComponentDataCommit.GetComponentDataCommit(remoteBranch);
        if (compCommit != null)
        {
          var tagNames = GetRelatedRemoteTagNames(compCommit.Sha);
          tagNames = NormalizeRemoteTagNames(tagNames);
          PullTagsFromRemote(tagNames);
        }
        CreateRemoteBranchLocaly(branchName, remoteBranch);

      }
    }

    private List<string> NormalizeRemoteTagNames(IEnumerable<string> relatedTagNames)
    {
      var normalizeTags = new List<string>();

      foreach (var tagname in relatedTagNames)
      {
        var normalize = tagname.Trim();
        normalize = normalize.Replace("^{}", string.Empty);
        normalizeTags.Add(normalize);
      }
      return normalizeTags;
    }


    private bool MergeBranches(PublicBranchNaming localBranchNaming, PublicBranchNaming remoteBranchNaming)
    {
      var localbranch = GetLocalBranch(localBranchNaming.GetGitPathBranchName());
      var remoteBranch = GetRemoteBranch(remoteBranchNaming.GetGitPathBranchName());
      var localCommits = new PublicBranchCommit(localbranch);
      var remoteCommits = new PublicBranchCommit(remoteBranch);
      var anyChanges = false;

      if (localBranchNaming == remoteBranchNaming)
      {
        if (IsBranchContentEqual(localCommits, remoteCommits))
        {
          return true;
        }
      }

      if (localCommits.StandardValueCommit != null && remoteCommits.StandardValueCommit != null)
      {
        if (StandardDataCommit.IsStandardValuesBranch(remoteBranch))
        {
          var localBlob = StandardDataCommit.GetStandardDataBlob(localCommits.StandardValueCommit);
          var remoteBlob = StandardDataCommit.GetStandardDataBlob(remoteCommits.StandardValueCommit);
          if (localBlob.Sha == remoteBlob.Sha)
            return true;
        }
      }

      if (localCommits.StandardValueCommit == null && remoteCommits.StandardValueCommit != null)
      {
        if (localCommits.ComponentCommit != null && remoteCommits.ComponentCommit != null)
        {
          var localBlob = ComponentDataCommit.GetComponentDataBlob(localCommits.ComponentCommit);
          var remoteBlob = ComponentDataCommit.GetComponentDataBlob(remoteCommits.ComponentCommit);
          if (localBlob != remoteBlob)
          {
            return false;
          }

          RemoveLocalTags(localCommits.ComponentCommit.Sha);
          UpdateLocalBranch(localbranch, remoteBranch);
          var tagNames = GetRelatedRemoteTagNames(remoteCommits.ComponentCommit.Sha);
          tagNames = NormalizeRemoteTagNames(tagNames);
          PullTagsFromRemote(tagNames);

          stdDataAdded++;
          cmpDataAdded++;
        }
        else if (localCommits.ComponentCommit != null && remoteCommits.ComponentCommit == null)
        {
          if (AppendStandardValues(localbranch, localCommits, remoteCommits, remoteBranchNaming) == null)
          {
            return false;
          }

          stdDataAdded++;
        }

        return true;
      }

      if (localCommits.ComponentCommit == null && remoteCommits.ComponentCommit != null)
      {
        localbranch = MergeComponentData(localbranch, localCommits, remoteCommits);
        if (localbranch == null)
          return false;

        anyChanges = true;
        cmpDataAdded++;
      }

      if (localCommits.CertificateCommit == null && remoteCommits.CertificateCommit != null)
      {
        localbranch = MergeCertificateCommit(localbranch, localCommits, remoteCommits);
        if (localbranch == null)
          return false;

        anyChanges = true;
        certDataAdded++;
      }

      return anyChanges;
    }

    private void RemoveLocalTags(string compCommitSha)
    {
      var tags = GetLocalTags(compCommitSha);
      foreach (var tag in tags)
      {
        repository.Tags.Remove(tag);
      }
    }


    private Branch AppendStandardValues(Branch localBranch, PublicBranchCommit localCommits,
        PublicBranchCommit remoteCommits, PublicBranchNaming remoteBranchNaming)
    {

      Branch compBranch = null;
      var stdCommit = new StandardDataCommit(repository, remoteCommits.StandardValueCommit, remoteBranchNaming);
      if (stdCommit.VerificationCheckBeforeCommit())
      {

        var compBlob = ComponentDataCommit.GetComponentDataBlob(localCommits.ComponentCommit);
        repository.Branches.Remove(localBranch);

        var stdBranch = stdCommit.ExecuteCommit();
        var compCommit = new ComponentDataCommit(repository, stdBranch, compBlob);
        if (compCommit.VerificationCheckBeforeCommit())
        {
          compBranch = compCommit.ExecuteCommit();
          if (localCommits.CertificateCommit != null || remoteCommits.CertificateCommit != null)
          {
            var certBlob = localCommits.CertificateCommit != null
              ? CertificateDataCommit.GetCertificateBlob(localCommits.CertificateCommit)
              : CertificateDataCommit.GetCertificateBlob(remoteCommits.CertificateCommit);

            compBranch = AddCertificateData(compBranch, certBlob, compBlob);
          }
        }
      }

      return compBranch;
    }

    private Branch MergeComponentData(Branch localBranch, PublicBranchCommit localCommits,
      PublicBranchCommit remoteCommits)
    {
      if (localCommits.StandardValueCommit != null && remoteCommits.StandardValueCommit != null)
      {
        var localBlob = StandardDataCommit.GetStandardDataBlob(localCommits.StandardValueCommit);
        var remoteBlob = StandardDataCommit.GetStandardDataBlob(remoteCommits.StandardValueCommit);
        if (localBlob.Sha != remoteBlob.Sha)
          return null;
      }

      if (localCommits.StandardValueCommit != null && remoteCommits.ComponentCommit != null)
      {
        var remoteBlob = ComponentDataCommit.GetComponentDataBlob(remoteCommits.ComponentCommit);
        var compCommit = new ComponentDataCommit(repository, localBranch, remoteBlob);
        if (compCommit.VerificationCheckBeforeCommit())
        {
          var branch = compCommit.ExecuteCommit();
          if (branch != null)
          {
            localCommits.ComponentCommit = ComponentDataCommit.GetComponentDataCommit(branch);
            return branch;
          }
        }
      }

      return null;
    }


    private Branch MergeCertificateCommit(Branch localBranch, PublicBranchCommit localCommits,
      PublicBranchCommit remoteCommits)
    {
      var certBlob = CertificateDataCommit.GetCertificateBlob(remoteCommits.CertificateCommit);
      var compBlob = ComponentDataCommit.GetComponentDataBlob(localCommits.ComponentCommit);
      return AddCertificateData(localBranch, certBlob, compBlob);
    }

    private Branch AddCertificateData(Branch localBranch, Blob certificateBlob, Blob compBlob)
    {
      var certDataCommit = new CertificateDataCommit(repository, localBranch, certificateBlob, compBlob);
      return certDataCommit.VerificationCheckBeforeCommit() ? certDataCommit.ExecuteCommit() : localBranch;
    }


    private IEnumerable<Tag> GetLocalTags(string relatedCommitId)
    {
      return repository.Tags.Where(elem => elem.Target.Sha == relatedCommitId);
    }

    private bool IsBranchContentEqual(PublicBranchCommit localCommits, PublicBranchCommit remoteCommits)
    {
      if (localCommits.CommitCounter != remoteCommits.CommitCounter)
        return false;

      if (!IsStandardValueCommitEqual(localCommits.StandardValueCommit, remoteCommits.StandardValueCommit))
        return false;

      if (!IsComponentDataCommitEqual(localCommits.ComponentCommit, remoteCommits.ComponentCommit))
        return false;

      if (!(IsCertificateCommitEqual(localCommits.CertificateCommit, remoteCommits.CertificateCommit)))
        return false;
      return true;
    }


    private bool IsStandardValueCommitEqual(Commit localStandardCommit, Commit remoteStandardCommit)
    {
      if (localStandardCommit != null && remoteStandardCommit != null)
      {
        var localBlob = StandardDataCommit.GetStandardDataBlob(localStandardCommit);
        var remoteBlob = StandardDataCommit.GetStandardDataBlob(remoteStandardCommit);
        if (localBlob.Sha != remoteBlob.Sha)
          return false;
      }
      else if (localStandardCommit != remoteStandardCommit)
      {
        return false;
      }

      return true;
    }

    private bool IsComponentDataCommitEqual(Commit localComponentDataCommit, Commit remoteComponentDataCommit)
    {
      if (localComponentDataCommit != null && remoteComponentDataCommit != null)
      {
        var localBlob = ComponentDataCommit.GetComponentDataBlob(localComponentDataCommit);
        var remoteBlob = ComponentDataCommit.GetComponentDataBlob(remoteComponentDataCommit);
        if (localBlob.Sha != remoteBlob.Sha)
          return false;
      }
      else if (localComponentDataCommit != remoteComponentDataCommit)
      {
        return false;
      }

      return true;
    }

    private bool IsCertificateCommitEqual(Commit localCertificateCommit, Commit remoteCertificateCommit)
    {
      if (localCertificateCommit != null && remoteCertificateCommit != null)
      {
        var localBlob = CertificateDataCommit.GetCertificateBlob(localCertificateCommit);
        var remoteBlob = CertificateDataCommit.GetCertificateBlob(remoteCertificateCommit);
        if (localBlob.Sha != remoteBlob.Sha)
          return false;
      }
      else if (localCertificateCommit != remoteCertificateCommit)
      {
        return false;
      }

      return true;
    }


    private List<PublicBranchNaming> GetSplitedPublicBranchNames(List<string> branchNames)
    {
      var pubBranchNames = new List<PublicBranchNaming>();
      for (int i = 0; i < branchNames.Count; i++)
      {
        pubBranchNames.Add(new PublicBranchNaming(branchNames[i]));
      }

      return pubBranchNames;
    }


    private void PullAllRemoteBranches(List<string> remoteBranchNames, bool isPrivateBranch = false)
    {
      if (!remoteBranchNames.Any())
        return;

      if (!isPrivateBranch)
      {
        var tagNames = GetRemoteTagNames();
        PullTagsFromRemote(tagNames);
      }

      foreach (var remoteName in remoteBranchNames)
      {
        var remoteBranch = GetRemoteBranch(remoteName);
        CreateRemoteBranchLocaly(remoteName, remoteBranch);
      }
    }

    private IEnumerable<string> GetRemoteTagNames()
    {
      return Repository.ListRemoteReferences(remoteRepositoryPath)
            .Where(elem => elem.IsTag && !elem.CanonicalName.EndsWith("^{}"))
            .Select(elem => elem.CanonicalName);
    }

    private void PullTagsFromRemote(IEnumerable<string> tagReferences)
    {
      if (!tagReferences.Any())
        return;

      LibGit2Sharp.Commands.Fetch(repository, ConfigValue.REMOTE_NAME, tagReferences, null, null);
    }


    private IEnumerable<string> GetRelatedRemoteTagNames(string relatedCommitId)
    {
      return Repository.ListRemoteReferences(remoteRepositoryPath)
        .Where(elem => elem.IsTag && elem.CanonicalName.EndsWith("^{}")
                       && elem.TargetIdentifier == relatedCommitId)
        .Select(elem => elem.CanonicalName);
    }

    private Branch CreateRemoteBranchLocaly(string branchName, Branch remoteBranch)
    {
      addedPublicBranches++;
      return repository.CreateBranch(branchName, remoteBranch.Tip);
    }

    private void UpdateLocalBranch(Branch localBranch, Branch remoteBranch)
    {
      repository.Refs.UpdateTarget(localBranch.Reference, remoteBranch.Tip.Id);
    }

  }
}
