﻿using LibGit2Sharp;
using System.IO;

namespace VECTO_GIT.Commands
{
  public class CreateBlobCommand : Command, ICommandResult<Blob>
  {
    private Blob resultBlob;
    private readonly Stream blobContentStream;
    private readonly string filePath;

    public CreateBlobCommand(Repository repository, string filePath) : base(repository)
    {
      this.filePath = filePath.CheckFilePath();
      Execute();
    }

    public CreateBlobCommand(Repository repository, Stream blobContentStream) : base(repository)
    {
      this.blobContentStream = blobContentStream.CheckNull(nameof(blobContentStream));
      Execute();
    }

    protected sealed override void Execute()
    {
      if (filePath != null)
      {
        resultBlob = repository.ObjectDatabase.CreateBlob(filePath);
      }
      else
      {
        resultBlob = repository.ObjectDatabase.CreateBlob(blobContentStream);
      }
    }

    public Blob Result()
    {
      return resultBlob;
    }
  }
}