﻿using LibGit2Sharp;
using System.Collections.Generic;

namespace VECTO_GIT.Commands
{
  public class CreateTreeCommand : Command, ICommandResult<Tree>
  {
    private readonly Dictionary<string, Blob> blobsToCommit;
    private readonly Commit parentCommit;
    private TreeDefinition treeDefinition;
    private Tree resultTree;

    public CreateTreeCommand(Repository repository, Dictionary<string, Blob> blobsToCommit)
      : base(repository)
    {
      this.blobsToCommit = blobsToCommit.CheckNullOrEmtpy();

      Execute();
    }

    public CreateTreeCommand(Repository repository, Commit parentCommit,
      Dictionary<string, Blob> blobsToCommit) : base(repository)
    {
      this.parentCommit = parentCommit.CheckNull(nameof(parentCommit));
      this.blobsToCommit = blobsToCommit.CheckNullOrEmtpy();

      Execute();
    }

    private void CreateTreeDefinition()
    {
      treeDefinition = (parentCommit == null) ?
        new TreeDefinition() : TreeDefinition.From(parentCommit);


      foreach (var blob in blobsToCommit)
      {
        treeDefinition.Add(blob.Key, blob.Value, Mode.NonExecutableFile);
      }

    }

    protected sealed override void Execute()
    {
      CreateTreeDefinition();
      resultTree = repository.ObjectDatabase.CreateTree(treeDefinition);
    }

    public Tree Result()
    {
      return resultTree;
    }
  }
}
