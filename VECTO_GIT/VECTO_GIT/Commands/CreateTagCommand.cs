﻿using LibGit2Sharp;
using System;

namespace VECTO_GIT.Commands
{
  public class CreateTagCommand : Command, ICommandResult<Tag>
  {
    private readonly string commitId;
    private readonly string tagName;
    private readonly string tagMessage;
    private Tag resultTag;

    public CreateTagCommand(Repository repository, string tagName, string tagMessage, string commitId)
      : base(repository)
    {
      this.commitId = commitId.CheckNullOrEmtpy(nameof(commitId));

      if (commitId.Length < ConfigValue.GIT_ID_LENGTH)
      {
        throw new ArgumentException(Helpers.ExceptionMessage(nameof(commitId)));
      }

      this.tagName = tagName.CheckNullOrEmtpy(nameof(tagName));
      this.tagMessage = tagMessage.CheckNullOrEmtpy(nameof(tagMessage));

      Execute();
    }

    protected sealed override void Execute()
    {
      resultTag = CreateTag();
    }

    public Tag Result()
    {
      return resultTag;
    }

    private Tag CreateTag()
    {
      var existingTag = repository.Tags[tagName];
      if (existingTag != null)
      {
        repository.Tags.Remove(existingTag);
      }
      return repository.ApplyTag(tagName, commitId, commitSignature.GetSignature, tagMessage);
    }

  }
}
