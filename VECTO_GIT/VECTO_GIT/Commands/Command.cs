﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;


namespace VECTO_GIT.Commands
{

  public abstract class Command
  {
    protected Repository repository;
    protected CommitSignature commitSignature;

    protected Command( Repository repository )
    {
      commitSignature = CommitSignature.Instance;
      this.repository = repository.CheckValidRepository();      
    }
    
    protected abstract void Execute();
  }
}
