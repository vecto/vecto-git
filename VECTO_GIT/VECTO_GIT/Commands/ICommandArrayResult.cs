﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT
{
  public interface ICommandArrayResult<out T>
  {
    T[] Result();
  }
}
