﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.Commands
{
  public class PullRemoteCommand : Command, ICommandResult<bool>
  {
    #region Members

    private readonly string remoteRepositoryPath;
    private readonly List<MergePrivateBranchModel> prvConflicts;
    private Remote remoteRepository;
    private bool result;
    

    #endregion


    public List<MergePrivateBranchModel> PrivateMergeConflicts { get; set; }
    public List<KeyValuePair<string, bool>> PrivateMergeMessages { get; set; }
    public List<KeyValuePair<string, bool>> PublicMergeMessages { get; set; }

    public PullRemoteCommand(Repository repository, string remoteRepositoryPath)
      : base(repository)
    {
      this.remoteRepositoryPath = remoteRepositoryPath.CheckNullOrEmtpy("remote repository path");
      AddRemoteRepository(remoteRepositoryPath);
      Execute();
    }

    public PullRemoteCommand(Repository repository, string remoteRepositoryPath,
      List<MergePrivateBranchModel> prvConflicts) : base(repository)
    {
      this.remoteRepositoryPath = remoteRepositoryPath.CheckNullOrEmtpy("remoteRepositoryPath");
      this.prvConflicts = prvConflicts;
      AddRemoteRepository(remoteRepositoryPath);
      Execute();
    }
    
    protected sealed override void Execute()
    {
      FetchRemoteSpecs();

      if (prvConflicts != null)
      {
        MergeConflicts();
      }
      else
      {
        result = SetRemoteBranches();
      }
      RemoveRemoteRepository();

    }

    public bool Result()
    {
      return result;
    }


    private bool SetRemoteBranches()
    {
      var localBranchNames = GetLocalBranchNames();
      var remoteBranchNames = GetRemoteBranchNames();

      if (localBranchNames.Count == 0 && remoteBranchNames.Count > 0)//empty dest
      {
        PullAllRemoteBranches(remoteBranchNames);
      }
      else
      {
        List<string> localPubNames;
        List<string> localPrvNames;
        SplitBranchNameTypes(localBranchNames, out localPubNames, out localPrvNames);

        List<string> remotePubNames;
        List<string> remotePrvNames;
        SplitBranchNameTypes(remoteBranchNames, out remotePubNames, out remotePrvNames);

        MergePublicBranches(localPubNames, remotePubNames);


        var conflicts = MergePrivateBranches(localPrvNames, remotePrvNames);
        if (conflicts?.Count > 0)
          PrivateMergeConflicts = conflicts;

        return !(conflicts != null && conflicts.Count > 0);
      }

      return true;
    }
    

    private List<string> GetRemoteBranchNames()
    {
      return Repository.ListRemoteReferences(remoteRepositoryPath)
        .Where(elem => elem.IsLocalBranch)
        .Select(elem => elem.CanonicalName.Replace(ConfigValue.REFERENCE_HEAD_PATH + "/", "")).ToList();
    }

    private List<string> GetLocalBranchNames()
    {
      var branchNames = new List<string>();
      foreach (var branch in repository.Branches.Where(b => !b.IsRemote))
      {
        branchNames.Add(branch.FriendlyName);
      }
      return branchNames;
    }

    private List<MergePrivateBranchModel> MergePrivateBranches(List<string> localPrvBranches, List<string> remotePrvBranches)
    {
      if (remotePrvBranches.Count > 0)
      {
        if (localPrvBranches.Count > 0)
        {
          var privateMerge = new MergePrivateBranchesCommand(repository);
          var mergeResult = privateMerge.MergePrivateBranches(localPrvBranches, remotePrvBranches);
          PrivateMergeMessages = privateMerge.GetMergeResultMessages();
          return mergeResult;
        }

        PullAllRemoteBranches(remotePrvBranches, true);
      }
      return null;
    }


    #region Merge Private Conflicts
    
    private void MergeConflicts()
    {
      for (int i = 0; i < prvConflicts.Count; i++)
      {
        SetMergedTree(prvConflicts[i]);
      }
    }

    private void SetMergedTree(MergePrivateBranchModel mergeModel)
    {
      var mDataTree =
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.MEASUREMENT_DATA_TREE_NAME, mergeModel.LocalPrivateBranch);
      var uDataTree =
        PrivateDataCommit.GetPrivateDataTree(ConfigValue.USER_DATA_TREE_NAME, mergeModel.LocalPrivateBranch);

      TreeDefinition uTreeDefinition = null;
      TreeDefinition mTreeDefinition = null;
      if (mergeModel.NewUserDataEntries != null)
      {
        uTreeDefinition = AddNewTreeEntries(uDataTree, mergeModel.NewUserDataEntries);
      }

      if (mergeModel.NewMeasurementEntries != null)
      {
        mTreeDefinition = AddNewTreeEntries(mDataTree, mergeModel.NewMeasurementEntries);
      }

      uTreeDefinition = AddConflictEntries(uTreeDefinition, uDataTree, mergeModel.EntityConflicts, FileType.UserData);
      mTreeDefinition = AddConflictEntries(mTreeDefinition, mDataTree, mergeModel.EntityConflicts, FileType.MeasurementData);

      Tree utree = null;
      Tree mTree = null;
      if (uTreeDefinition != null)
        utree = repository.ObjectDatabase.CreateTree(uTreeDefinition);
      if (mTreeDefinition != null)
        mTree = repository.ObjectDatabase.CreateTree(mTreeDefinition);


      var branch = SetUserDataCommit(utree, mergeModel.LocalPrivateBranch);

      if (branch == null)
        branch = mergeModel.LocalPrivateBranch;

      SetMeasurementData(mTree, branch);
    }

    private TreeDefinition AddNewTreeEntries(Tree srcTree, List<TreeEntry> treeEntries)
    {
      var treeDefinition = TreeDefinition.From(srcTree);
      for (int i = 0; i < treeEntries.Count; i++)
      {
        treeDefinition.Add(treeEntries[i].Name, treeEntries[i]);
      }

      return treeDefinition;
    }

    private TreeDefinition AddConflictEntries(TreeDefinition treeDefinition, Tree srcTree,
      List<MergeEntityModel> conflictEntities, FileType dataType)
    {
      if (conflictEntities.IsNullOrEmpty())
        return treeDefinition;

      if (treeDefinition == null && srcTree == null)
        return null;

      if (treeDefinition == null)
        treeDefinition = TreeDefinition.From(srcTree);

      foreach (var conflict in conflictEntities)
      {
        if (conflict.DataType == dataType)
        {
          if (conflict.MergeAction == MergeActionType.Replace)
          {
            treeDefinition.Add(conflict.RemoteTreeEntry.Name, conflict.RemoteTreeEntry);
          }

          if (conflict.MergeAction == MergeActionType.Rename)
          {
            treeDefinition.Add(conflict.FileName, conflict.RemoteTreeEntry);
          }
        }
      }

      return treeDefinition;
    }

    private Branch SetUserDataCommit(Tree tree, Branch localBranch)
    {
      if (tree == null || localBranch == null)
        return null;

      var userDataCommit = new UserDataCommit(repository, tree, localBranch);
      return userDataCommit.ExecuteCommit();
    }

    private Branch SetMeasurementData(Tree tree, Branch localBranch)
    {
      if (tree == null || localBranch == null)
        return null;

      var measurementCommit = new MeasurementDataCommit(repository, tree, localBranch);
      return measurementCommit.ExecuteCommit();
    }

    #endregion

    
    //New add all remote branches within existing empty repository
    private void PullAllRemoteBranches(List<string> remoteBranchNames, bool isPrivateBranch = false)
    {
      if (!remoteBranchNames.Any())
        return;

      if (!isPrivateBranch)
      {
        var tagNames = GetRemoteTagNames();
        PullTagsFromRemote(tagNames);
      }

      foreach (var remoteName in remoteBranchNames)
      {
        var remoteBranch = GetRemoteBranch(remoteName);
        CreateRemoteBranchLocaly(remoteName, remoteBranch);
      }

      var message = "All Branches successfully added!";
      SetPullMergeMessage(message, false, isPrivateBranch);
    }

    private IEnumerable<string> GetRemoteTagNames()
    {
      return Repository.ListRemoteReferences(remoteRepositoryPath)
            .Where(elem => elem.IsTag && !elem.CanonicalName.EndsWith("^{}"))
            .Select(elem => elem.CanonicalName);
    }


    private void MergePublicBranches(List<string> localBranchNames, List<string> remoteBranchNames)	
    {
      var mergePubBranchCommand = new MergePublicBranchesCommand(repository, localBranchNames,
        remoteBranchNames, remoteRepositoryPath);

      mergePubBranchCommand.ExecutePublicMerge();
      PublicMergeMessages = mergePubBranchCommand.GetMergeResultMessages();
    }

    private void SplitBranchNameTypes(List<string> branchNames, out List<string> pubBranchNames,
      out List<string> prvBranchNames)
    {
      pubBranchNames = new List<string>();
      prvBranchNames = new List<string>();

      foreach (var branchName in branchNames)
      {
        if (branchName.StartsWith(PublicBranchNaming.TREE_NAME))
        {
          pubBranchNames.Add(branchName);
        }
        else if (branchName.StartsWith(PrivateBranchNaming.TREE_NAME))
        {
          prvBranchNames.Add(branchName);
        }
      }
    }
    
    private void PullTagsFromRemote(IEnumerable<string> tagReferences)
    {
      if (!tagReferences.Any())
        return;

      LibGit2Sharp.Commands.Fetch(repository, ConfigValue.REMOTE_NAME, tagReferences, null, null);
    }


    private void CreateRemoteBranchLocaly(string branchName, Branch remoteBranch)
    {
      repository.CreateBranch(branchName, remoteBranch.Tip);
    }
    
    private Branch GetRemoteBranch(string branchName)
    {
      var remoteBranchName = string.Format("{0}/{1}", ConfigValue.REMOTE_NAME, branchName);
      return repository.Branches[remoteBranchName];
    }

    private void FetchRemoteSpecs()
    {
      var fetchOptions = new FetchOptions
      {
        TagFetchMode = TagFetchMode.None
      };

      var logMessage = string.Empty;
      var refSpecs = remoteRepository.FetchRefSpecs.Select(x => x.Specification);
      LibGit2Sharp.Commands.Fetch(repository, ConfigValue.REMOTE_NAME, refSpecs, fetchOptions, logMessage);
    }

    private void RemoveRemoteRepository()
    {
      repository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
      remoteRepository.Dispose();
    }

    private void AddRemoteRepository(string remotePath)
    {
      repository.Network.Remotes.Remove(ConfigValue.REMOTE_NAME);
      remoteRepository = repository.Network.Remotes.Add(ConfigValue.REMOTE_NAME, remotePath);
    }

    private void SetPullMergeMessage(string message, bool isNegative, bool isPrivateData)
    {
      if (isPrivateData)
      {
        PrivateMergeMessages = new List<KeyValuePair<string, bool>>
        {
          new KeyValuePair<string, bool>(message, isNegative)
        };
      }
      else
      {
        PublicMergeMessages = new List<KeyValuePair<string, bool>>
        {
          new KeyValuePair<string, bool>(message, isNegative)
        };
      }
    }
  }
}
