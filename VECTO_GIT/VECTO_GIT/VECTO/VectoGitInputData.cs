﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using LibGit2Sharp;
using Ninject;
using TUGraz.VectoCommon.Exceptions;
using TUGraz.VectoCommon.InputData;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Resources;
using TUGraz.VectoCommon.Utils;
using TUGraz.VectoCore;
using TUGraz.VectoCore.InputData.FileIO.XML;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Factory;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Interfaces;
using TUGraz.VectoCore.InputData.FileIO.XML.Declaration.Reader;
using TUGraz.VectoCore.InputData.Impl;
using TUGraz.VectoCore.Models.Declaration;
using TUGraz.VectoCore.Models.SimulationComponent.Data;
using TUGraz.VectoCore.Utils;
using VECTO_GIT.GUI.DI;
using XmlDocumentType = TUGraz.VectoCore.Utils.XmlDocumentType;

namespace VECTO_GIT.GUI.ViewModel
{
	public class VectoGitInputData : IDeclarationInputDataProvider, IDeclarationJobInputData,
		IVehicleDeclarationInputData, IAdvancedDriverAssistantSystemDeclarationInputData, IAuxiliariesDeclarationInputData,
		IPTOTransmissionInputData, IVehicleComponentsDeclaration, IAxlesDeclarationInputData, IXMLDeclarationVehicleData
	{
		private VectoSimulationViewModel vsm;
		private readonly INinjectFactory _ninjectFactory;
		//private IAuxiliariesDeclarationInputData _auxiliaryInputData;

		
		[Inject]
		public IDeclarationInjectFactory Factory { protected get; set; }



		public VectoGitInputData(VectoSimulationViewModel vectoSimulationViewModel, INinjectFactory factory)
		{
			vsm = vectoSimulationViewModel;
			_ninjectFactory = factory;

		}

		#region Implementation of IDeclarationInputDataProvider

		public IDeclarationJobInputData JobInputData
		{
			get { return this; }
		}

		public XElement XMLHash { get; }

		#endregion

		#region Implementation of IDeclarationJobInputData

		public DataSource DataSource
		{
			get { return new DataSource() { SourceType = DataSourceType.Missing }; }
		}

		public string Source
		{
			get { return "~~GIT~~"; }
		}

		public bool SavedInDeclarationMode
		{
			get { return true; }
		}

		public IList<IAuxiliaryDeclarationInputData> Auxiliaries
		{
			get { return vsm.Auxiliaries; }
		}

		public string Manufacturer
		{
			get { return vsm.Manufacturer; }
		}

		public string Model
		{
			get { return vsm.Model; }
		}

		public string Date
		{
			get { return ""; }
		}

		public CertificationMethod CertificationMethod
		{
			get { return CertificationMethod.NotCertified; }
		}

		public string CertificationNumber
		{
			get { return null; }
		}

		public DigestData DigestValue
		{
			get { return null; }
		}

		public IVehicleDeclarationInputData Vehicle
		{
			get { return this; }
		}

		public string JobName
		{
			get { return vsm.VIN; }
		}

		public string ShiftStrategy { get; }

		#endregion

		#region Implementation of IVehicleDeclarationInputData

		public string Identifier { get; }

		public bool ExemptedVehicle
		{
			get { return false; }
		}

		public string VIN
		{
			get { return vsm.VIN; }
		}

		public LegislativeClass LegislativeClass
		{
			get { return vsm.LegislativeClass; }
		}

		public VehicleCategory VehicleCategory
		{
			get { return vsm.VehicleCategory; }
		}

		public AxleConfiguration AxleConfiguration
		{
			get { return vsm.AxleConfiguration; }
		}

		public Kilogram CurbMassChassis
		{
			get { return vsm.CurbMassChassis.SI<Kilogram>(); }
		}

		public Kilogram GrossVehicleMassRating
		{
			get { return vsm.TecnicalPermissibleMaximumLadenMass.SI<Kilogram>(); }
		}

		public IList<ITorqueLimitInputData> TorqueLimits
		{
			get { return new List<ITorqueLimitInputData>(); }
		}

		private ITyreEngineeringInputData LoadTyre(string tyreGitId)
		{
			var xml = GetXMLDocumentFromGITRepository(tyreGitId);
			var retval = new TyreInputData();
			retval.Dimension =
				xml.XPathSelectElement(string.Format("//*[local-name()='{0}']", XMLNames.AxleWheels_Axles_Axle_Dimension))?.Value;
			retval.TyreTestLoad =
				xml.XPathSelectElement(string.Format("//*[local-name()='{0}']", XMLNames.AxleWheels_Axles_Axle_FzISO))?.Value
					.ToDouble().SI<Newton>();
			retval.RollResistanceCoefficient =
				xml.XPathSelectElement(string.Format("//*[local-name()='{0}']", XMLNames.AxleWheels_Axles_Axle_RRCDeclared))?.Value
					.ToDouble() ?? 0;
			retval.CertificationNumber =
				xml.XPathSelectElement(string.Format("//*[local-name()='{0}']", XMLNames.Component_CertificationNumber))?.Value;
			retval.DigestValue = new DigestData(
				xml.XPathSelectElement(string.Format("//*[local-name()='{0}']/..", XMLNames.DI_Signature_Reference_DigestValue)));
			retval.CertificationMethod = CertificationMethod.Measured;
			return retval;
		}

		public string ManufacturerAddress
		{
			get { return vsm.ManufacturerAddress; }
		}

		public PerSecond EngineIdleSpeed
		{
			get { return vsm.IdlingSpeed.RPMtoRad(); }
		}

		public IAuxiliariesDeclarationInputData AuxiliaryInputData
		{
			get { return this; }
		}

		public XmlElement ComponentNode { get; }
		public IXMLComponentReader ComponentReader { get; set; }
		public XmlElement PTONode { get; }
		public IXMLPTOReader PTOReader { get; set; }
		public XmlElement ADASNode { get; }
		public IXMLADASReader ADASReader { get; set; }

		public AngledriveType AngledriveType { get { return vsm.AngledriveType; } }
		public RetarderType RetarderType { get { return vsm.RetarderType; } }
		public double RetarderRatio { get { return vsm.RetarderRatio; } }

		public IPTOTransmissionInputData PTOTransmissionInputData
		{
			get { return this; }
		}

		public IAxlesDeclarationInputData AxleWheels { get { return this; } }

		public bool VocationalVehicle
		{
			get { return vsm.Vocational; }
		}

		public bool SleeperCab
		{
			get { return vsm.SleeperCab; }
		}

		public TankSystem? TankSystem
		{
			get { return null; }
		}

		public IAdvancedDriverAssistantSystemDeclarationInputData ADAS
		{
			get { return this; }
		}

		public bool ZeroEmissionVehicle
		{
			get { return false; }
		}

		public bool HybridElectricHDV
		{
			get { return false; }
		}

		public bool DualFuelVehicle
		{
			get { return false; }
		}

		public Watt MaxNetPower1
		{
			get { return null; }
		}

		public Watt MaxNetPower2
		{
			get { return null; }
		}

		public IVehicleComponentsDeclaration Components { get { return this; } }


		public IEngineDeclarationInputData EngineInputData
		{
			get {
				var componentNode = GetComponentFromGITRepository(vsm.EngineComponent);
				return Factory.CreateEngineData(XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType), this, componentNode, vsm.EngineComponent);
			}
		}

		public IGearboxDeclarationInputData GearboxInputData
		{
			get {
				var componentNode = GetComponentFromGITRepository(vsm.TransmissionComponent);
				var version = XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType);
				var gbx = Factory.CreateGearboxData(version, this, componentNode, vsm.TransmissionComponent);
				gbx.Reader = Factory.CreateGearboxReader(version, this, componentNode);
				return gbx;
			}
		}

		public ITorqueConverterDeclarationInputData TorqueConverterInputData
		{
			get {
				if (string.IsNullOrWhiteSpace(vsm.TorqueConverterComponent)) {
					return null;
				}
				var componentNode = GetComponentFromGITRepository(vsm.TorqueConverterComponent);
				return Factory.CreateTorqueconverterData(XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType), this, componentNode, vsm.TorqueConverterComponent);
			}
		}

		public IAxleGearInputData AxleGearInputData
		{
			get {
				var componentNode = GetComponentFromGITRepository(vsm.AxlegearComponent);
				return Factory.CreateAxlegearData(XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType), this, componentNode, vsm.AxlegearComponent);
			}
		}

		public IAngledriveInputData AngledriveInputData
		{
			get {
				if (string.IsNullOrWhiteSpace(vsm.AngledriveComponent)) {
					return null;
				}

				var componentNode = GetComponentFromGITRepository(vsm.AngledriveComponent);
				return Factory.CreateAngledriveData(XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType), this, componentNode, vsm.AngledriveComponent);
			}
		}

		public IRetarderInputData RetarderInputData
		{
			get {
				if (vsm.RetarderType.IsDedicatedComponent()) {
					var componentNode = GetComponentFromGITRepository(vsm.RetarderComponent);
					return Factory.CreateRetarderData(XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType), this, componentNode, vsm.RetarderComponent);
				} else {
					return new DummyRetarderDataProvider(vsm.RetarderType, double.NaN);
				}
			}
		}

		public IAirdragDeclarationInputData AirdragInputData
		{
			get {
				var componentNode = GetComponentFromGITRepository(vsm.AirdragComponent);
				return Factory.CreateAirdragData(XMLHelper.GetXsdType(componentNode.SchemaInfo.SchemaType), this, componentNode, vsm.AirdragComponent);
			}
		}

		#endregion

		#region Implementation of IAdvancedDriverAssistantSystemDeclarationInputData

		public bool EngineStopStart
		{
			get { return vsm.EngineStopStart; }
		}

		public EcoRollType EcoRoll { get {
			return EcorollTypeHelper.Get(vsm.EcoRollWithoutEngineStop, vsm.EcoRollWithEngineStop);
		} }

		
		public PredictiveCruiseControlType PredictiveCruiseControl
		{
			get { return vsm.PCCOption; }
		}

		#endregion

		#region Implementation of IPTOTransmissionInputData

		public string PTOTransmissionType
		{
			get {
				var shaftGearWheels = vsm.PTOShaftGearWheels;
				if ("none".Equals(shaftGearWheels, StringComparison.InvariantCultureIgnoreCase)) {
					return "None";
				}
				if ("only one engaged gearwheel above oil level".Equals(
					shaftGearWheels, StringComparison.CurrentCultureIgnoreCase)) {
					return "only one engaged gearwheel above oil level";
				}

				var otherElements = vsm.PTOOtherElements;
				var ptoTech = string.Format("{0} - {1}", shaftGearWheels, otherElements);
				if (DeclarationData.PTOTransmission.GetTechnologies().Contains(ptoTech)) {
					return ptoTech;
				}

				throw new VectoException("PTO Technology {0} invalid!", ptoTech);
			}
		}

		public TableData PTOLossMap
		{
			get { return null; }
		}

		public TableData PTOCycle
		{
			get { return null; }
		}

		#endregion

		private XDocument GetXMLDocumentFromGITRepository(string gitId)
		{
			var repository = _ninjectFactory.ComponentDataManagement().CurrentRepository;
			var blob = repository.Lookup<Blob>(gitId);
			var xml = XDocument.Load(new XmlTextReader(blob.GetContentStream()));
			return xml;
		}

		private XmlNode GetComponentFromGITRepository(string gitId)
		{
			var repository = _ninjectFactory.ComponentDataManagement().CurrentRepository;
			var blob = repository.Lookup<Blob>(gitId);
			var xml = new XmlDocument();
			xml.Load(new XmlTextReader(blob.GetContentStream()));
			var validator = new XMLValidator(xml);
			var valid = validator.ValidateXML(XmlDocumentType.DeclarationComponentData);
			if (!valid) {
				throw new VectoException("unsupported component XML");
			}
			var dataNode =  xml.DocumentElement?.SelectSingleNode(string.Format(".//*[local-name()='{0}']", XMLNames.ComponentDataWrapper));
			return dataNode;
		}

		#region Implementation of IAxlesDeclarationInputData

		public IList<IAxleDeclarationInputData> AxlesDeclaration { get {
			var retVal = new List<IAxleDeclarationInputData>();
			foreach (var axle in vsm.Axles) {
				retVal.Add(
					new AxleInputData() {
						AxleType = axle.AxleType,
						TwinTyres = axle.TwinTyre,
						Steered = axle.Steered,
						Tyre = LoadTyre(axle.TyreComponent)
					});
			}

			return retVal;
		} }

		#endregion

	}

	internal class DummyRetarderDataProvider : IRetarderInputData
	{
		public DummyRetarderDataProvider(RetarderType vsmRetarderType, double ratio)
		{
			Type = vsmRetarderType;
			Ratio = ratio;
		}

		#region Implementation of IComponentInputData

		public DataSource DataSource { get { return null; } }
		public bool SavedInDeclarationMode { get { return true; } }
		public string Manufacturer { get { return null; } }
		public string Model { get { return null; } }
		public string Date { get { return null; } }
		public CertificationMethod CertificationMethod { get { return CertificationMethod.NotCertified; } }
		public string CertificationNumber { get { return null; } }
		public DigestData DigestValue { get { return null; } }

		#endregion

		#region Implementation of IRetarderInputData

		public RetarderType Type { get; }
		public double Ratio { get; }
		public TableData LossMap { get { return null; } }

		#endregion
	}
}
