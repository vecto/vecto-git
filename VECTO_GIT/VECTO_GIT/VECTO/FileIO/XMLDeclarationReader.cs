﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using TUGraz.VectoCommon.Exceptions;
using TUGraz.VectoCommon.InputData;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Resources;
using TUGraz.VectoCommon.Utils;
using TUGraz.VectoCore.Configuration;
using TUGraz.VectoCore.InputData.Impl;
using TUGraz.VectoCore.Utils;

namespace VECTO_GIT.GUI.ViewModel {
	internal class XMLDeclarationReader
	{
		private XPathHelper Helper;
		private XPathNavigator Navigator;
		private XmlNamespaceManager Manager;

		private string XBasePath;

		public XMLDeclarationReader(string filename)
		{
			var document = new XPathDocument(new XmlTextReader(File.OpenRead(filename)));
			Navigator = document.CreateNavigator();
			Manager = new XmlNamespaceManager(Navigator.NameTable ?? new NameTable());
			Helper = new XPathHelper(ExecutionMode.Declaration);
			//Helper.AddNamespaces(Manager);
			Manager.AddNamespace(Constants.XML.DeclarationNSPrefix, Constants.XML.VectoDeclarationDefinitionsNS);
			Manager.AddNamespace(Constants.XML.RootNSPrefix, "urn:tugraz:ivt:VectoAPI:DeclarationInput:v1.0a");

			XBasePath = Helper.QueryAbs(
				Helper.NSPrefix(XMLNames.VectoInputDeclaration, Constants.XML.RootNSPrefix),
				XMLNames.Component_Vehicle
			);
		}

		public void ReadVectoJob(VectoSimulationViewModel vsm)
		{
			vsm.Manufacturer = Manufacturer;
			vsm.ManufacturerAddress = ManufacturerAddress;
			vsm.Model = Model;
			vsm.VIN = VIN;
			vsm.LegislativeClass = LegislativeClass;
			vsm.VehicleCategory = VehicleCategory;
			vsm.AxleConfiguration = AxleConfiguration;
			vsm.CurbMassChassis = CurbMassChassis.Value();
			vsm.TecnicalPermissibleMaximumLadenMass = GrossVehicleMassRating.Value();
			vsm.IdlingSpeed = EngineIdleSpeed.AsRPM;
			vsm.RetarderType = RetarderType;
			if (RetarderType.IsDedicatedComponent()) {
				vsm.RetarderRatio = RetarderRatio;
			}
			vsm.AngledriveType = AngulargearType;
			vsm.Vocational = VocationalVehicle;
			vsm.SleeperCab = SleeperCab;
			vsm.EngineStopStart = EngineStopStart;
			vsm.EcoRollWithoutEngineStop = EcoRollWitoutEngineStop;
			vsm.EcoRollWithEngineStop = EcoRollWithEngineStop;
			vsm.PCCOption = PredictiveCruiseControl;

			vsm.EngineComponent = GetAttributeValue(XMLNames.Component_Engine, "gitId");
			vsm.TransmissionComponent = GetAttributeValue(XMLNames.Component_Gearbox, "gitId");
			if (ElementExists(XMLNames.Component_TorqueConverter)) {
				vsm.TorqueConverterComponent = GetAttributeValue(XMLNames.Component_TorqueConverter, "gitId");
			}
			if (vsm.AngledriveType == AngledriveType.SeparateAngledrive) {
				vsm.AngledriveComponent = GetAttributeValue(XMLNames.Component_Angledrive, "gitId");
			}
			vsm.AxlegearComponent = GetAttributeValue(XMLNames.Component_Axlegear, "gitId");
			if (vsm.RetarderType.IsDedicatedComponent()) {
				vsm.RetarderComponent = GetAttributeValue(XMLNames.Component_Retarder, "gitId");
			}
			if (ElementExists(XMLNames.Component_AirDrag)) {
				vsm.AirdragComponent = GetAttributeValue(XMLNames.Component_AirDrag, "gitId");
			}

			vsm.PTOShaftGearWheels = GetElementValue(Helper.Query(XMLNames.Vehicle_PTO, XMLNames.Vehicle_PTO_ShaftsGearWheels));
			vsm.PTOOtherElements = GetElementValue(Helper.Query(XMLNames.Vehicle_PTO, XMLNames.Vehicle_PTO_OtherElements));

			var aux = Auxiliaries;
			vsm.AuxElectricSysteTechnology = aux.First(x => x.Type == AuxiliaryType.ElectricSystem).Technology.First();
			vsm.AuxSteeringPumpTechnology = aux.First(x => x.Type == AuxiliaryType.SteeringPump).Technology.First();
			vsm.AuxPneumaticSystemTechnology = aux.First(x => x.Type == AuxiliaryType.PneumaticSystem).Technology.First();
			vsm.AuxHVACTechnology = aux.First(x => x.Type == AuxiliaryType.HVAC).Technology.First();
			vsm.AuxFanTechnology = aux.First(x => x.Type == AuxiliaryType.Fan).Technology.First();

			var axles = Axles;
			for (var i = 0; i < Math.Min(axles.Count, vsm.Axles.Count); i++) {
				vsm.Axles[i].AxleType = axles[i].AxleType;
				vsm.Axles[i].Steered = axles[i].Steered;
				vsm.Axles[i].TwinTyre = axles[i].TwinTyres;
				vsm.Axles[i].TyreComponent = axles[i].Tyre.Dimension;
			}
		}


		public virtual string Manufacturer
		{
			get { return GetElementValue(XMLNames.Component_Manufacturer); }
		}

		public virtual string Model
		{
			get { return GetElementValue(XMLNames.Component_Model); }
		}


		public string VIN
		{
			get { return GetElementValue(XMLNames.Vehicle_VIN); }
		}

		public LegislativeClass LegislativeClass
		{
			get { return GetElementValue(XMLNames.Vehicle_LegislativeClass).ParseEnum<LegislativeClass>(); }
		}

		public VehicleCategory VehicleCategory
		{
			get {
				var val = GetElementValue(XMLNames.Vehicle_VehicleCategory);
				if ("Rigid Lorry".Equals(val, StringComparison.InvariantCultureIgnoreCase)) {
					return VehicleCategory.RigidTruck;
				}
				return val.ParseEnum<VehicleCategory>();
			}
		}

		

		public AxleConfiguration AxleConfiguration
		{
			get { return AxleConfigurationHelper.Parse(GetElementValue(XMLNames.Vehicle_AxleConfiguration)); }
		}


		public Kilogram CurbMassChassis
		{
			get { return GetDoubleElementValue(XMLNames.Vehicle_CurbMassChassis).SI<Kilogram>(); }
		}


		public Kilogram GrossVehicleMassRating
		{
			get { return GetDoubleElementValue(XMLNames.Vehicle_GrossVehicleMass).SI<Kilogram>(); }
		}

		public string ManufacturerAddress
		{
			get { return GetElementValue(XMLNames.Component_ManufacturerAddress); }
		}

		public PerSecond EngineIdleSpeed
		{
			get { return GetDoubleElementValue(XMLNames.Vehicle_IdlingSpeed).RPMtoRad(); }
		}

		public double RetarderRatio
		{
			get { return GetDoubleElementValue(XMLNames.Vehicle_RetarderRatio); }
		}

		public RetarderType RetarderType
		{
			get {
				var value = GetElementValue(XMLNames.Vehicle_RetarderType); //.ParseEnum<RetarderType>(); 
				switch (value) {
					case "None":
						return RetarderType.None;
					case "Losses included in Gearbox":
						return RetarderType.LossesIncludedInTransmission;
					case "Engine Retarder":
						return RetarderType.EngineRetarder;
					case "Transmission Input Retarder":
						return RetarderType.TransmissionInputRetarder;
					case "Transmission Output Retarder":
						return RetarderType.TransmissionOutputRetarder;
				}
				throw new ArgumentOutOfRangeException("RetarderType", value);
			}
		}

		public AngledriveType AngulargearType
		{
			get { return GetElementValue(XMLNames.Vehicle_AngledriveType).ParseEnum<AngledriveType>(); }
		}

		public bool VocationalVehicle
		{
			get {
				return XmlConvert.ToBoolean(GetElementValue(XMLNames.Vehicle_VocationalVehicle));
			}
		}
		public bool SleeperCab
		{
			get {
				return XmlConvert.ToBoolean(GetElementValue(XMLNames.Vehicle_SleeperCab));
			}
		}

		public bool EngineStopStart
		{
			get {
				return XmlConvert.ToBoolean(GetElementValue(Helper.Query(XMLNames.Vehicle_ADAS, XMLNames.Vehicle_ADAS_EngineStopStart)));
			}
		}
		public bool EcoRollWitoutEngineStop
		{
			get {
				return XmlConvert.ToBoolean(GetElementValue(Helper.Query(XMLNames.Vehicle_ADAS, XMLNames.Vehicle_ADAS_EcoRollWithoutEngineStop)));
			}
		}
		public bool EcoRollWithEngineStop
		{
			get {
				return XmlConvert.ToBoolean(GetElementValue(Helper.Query(XMLNames.Vehicle_ADAS, XMLNames.Vehicle_ADAS_EcoRollWithEngineStopStart)));
			}
		}

		public PredictiveCruiseControlType PredictiveCruiseControl
		{
			get {
				return PredictiveCruiseControlTypeHelper.Parse(GetElementValue(Helper.Query(XMLNames.Vehicle_ADAS, XMLNames.Vehicle_ADAS_PCC)));
			}
		}

		public IList<IAuxiliaryDeclarationInputData> Auxiliaries
		{
			get {
				var retVal = new List<IAuxiliaryDeclarationInputData>();
				var auxiliaries = Navigator.Select(Helper.Query(Helper.Query(XBasePath,
																			XMLNames.Vehicle_Components,
																			XMLNames.Component_Auxiliaries,
																			XMLNames.ComponentDataWrapper),
																Helper.QueryConstraint("*",
																						Helper.NSPrefix(XMLNames.Auxiliaries_Auxiliary_Technology), null, "")),
													Manager);

				while (auxiliaries.MoveNext()) {
					var techlistNodes = auxiliaries.Current.Select(Helper.NSPrefix(XMLNames.Auxiliaries_Auxiliary_Technology), Manager);
					var technologyList = new List<string>();
					while (techlistNodes.MoveNext()) {
						technologyList.Add(techlistNodes.Current.Value);
					}
					retVal.Add(new AuxiliaryDataInputData {
						Type = auxiliaries.Current.Name.ParseEnum<AuxiliaryType>(),
						Technology = technologyList,
					});
				}
				return retVal;
			}
		}


		public IList<AxleInputData> Axles
		{
			get {
				var axles = Navigator.Select(Helper.Query(XBasePath, XMLNames.Vehicle_Components, XMLNames.Component_AxleWheels,
					XMLNames.ComponentDataWrapper, XMLNames.AxleWheels_Axles, XMLNames.AxleWheels_Axles_Axle), Manager);

				var retVal = new AxleInputData[axles.Count];
				while (axles.MoveNext()) {
					var axleNumber = axles.Current.GetAttribute(XMLNames.AxleWheels_Axles_Axle_AxleNumber_Attr, "").ToInt();
					if (axleNumber < 1 || axleNumber > retVal.Length) {
						throw new VectoException("Axle #{0} exceeds axle count", axleNumber);
					}
					if (retVal[axleNumber - 1] != null) {
						throw new VectoException("Axle #{0} defined multiple times!", axleNumber);
					}
					var axleType = axles.Current.SelectSingleNode(Helper.NSPrefix(XMLNames.AxleWheels_Axles_Axle_AxleType), Manager);
					var twinTyres = axles.Current.SelectSingleNode(Helper.NSPrefix(XMLNames.AxleWheels_Axles_Axle_TwinTyres), Manager);
					var steered = axles.Current.SelectSingleNode(Helper.NSPrefix(XMLNames.AxleWheels_Axles_Axle_Steered), Manager);
					var tyre =
						axles.Current.SelectSingleNode(Helper.Query(XMLNames.AxleWheels_Axles_Axle_Tyre),
							Manager);
					if (tyre == null) {
						throw new VectoException("Axle #{0} contains no tyre definition", axleNumber);
					}
					retVal[axleNumber - 1] = new AxleInputData {
						AxleType = axleType?.Value.ParseEnum<AxleType>() ?? AxleType.VehicleNonDriven,
						TwinTyres = twinTyres != null && XmlConvert.ToBoolean(twinTyres.Value),
						Steered = steered != null && XmlConvert.ToBoolean(steered.Value),
						Tyre = new TyreInputData {
							Dimension = tyre.GetAttribute("gitId", "")
						}
					};
				}
				return retVal;
			}
		}

		// ======================


		protected double GetDoubleElementValue(string relativePath)
		{
			return GetElementValue(relativePath).ToDouble();
		}

		protected string GetElementValue(string relativePath)
		{
			var path = Helper.Query(XBasePath, relativePath.Any() ? relativePath : null);

			var node = Navigator.SelectSingleNode(path, Manager);
			if (node == null) {
				throw new VectoException("Node {0} not found in input data", path);
			}
			return node.InnerXml;
		}

		private bool ElementExists(string relativePath)
		{
			var path = Helper.Query(XBasePath, relativePath.Any() ? relativePath : null);

			var node = Navigator.SelectSingleNode(path, Manager);
			return node == null;
		}


		protected string GetAttributeValue(string relativePath, string attrName)
		{
			var nodes =
				Navigator.Select(string.IsNullOrWhiteSpace(relativePath) ? XBasePath : Helper.Query(XBasePath, XMLNames.Vehicle_Components, relativePath),
								Manager);
			if (nodes.Count == 0) {
				return null;
			}
			nodes.MoveNext();
			return nodes.Current.GetAttribute(attrName, "");
		}

	}
}
