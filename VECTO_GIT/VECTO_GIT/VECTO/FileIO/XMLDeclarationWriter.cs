﻿/*
* This file is part of VECTO.
*
* Copyright © 2012-2019 European Union
*
* Developed by Graz University of Technology,
*              Institute of Internal Combustion Engines and Thermodynamics,
*              Institute of Technical Informatics
*
* VECTO is licensed under the EUPL, Version 1.1 or - as soon they will be approved
* by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use VECTO except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/community/eupl/og_page/eupl
*
* Unless required by applicable law or agreed to in writing, VECTO
* distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and
* limitations under the Licence.
*
* Authors:
*   Stefan Hausberger, hausberger@ivt.tugraz.at, IVT, Graz University of Technology
*   Christian Kreiner, christian.kreiner@tugraz.at, ITI, Graz University of Technology
*   Michael Krisper, michael.krisper@tugraz.at, ITI, Graz University of Technology
*   Raphael Luz, luz@ivt.tugraz.at, IVT, Graz University of Technology
*   Markus Quaritsch, markus.quaritsch@tugraz.at, IVT, Graz University of Technology
*   Martin Rexeis, rexeis@ivt.tugraz.at, IVT, Graz University of Technology
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using TUGraz.IVT.VectoXML;
using TUGraz.IVT.VectoXML.Writer;
using TUGraz.VectoCommon.InputData;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Resources;
using TUGraz.VectoCommon.Utils;
using TUGraz.VectoCore.InputData.Reader;
using TUGraz.VectoCore.InputData.Reader.ComponentData;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.VECTO.FileIO
{
	public class XMLDeclarationWriter
	{
		private readonly XNamespace _componentNamespace;
		private readonly XNamespace _tns;
		private readonly XNamespace _rootNamespace;
		private readonly XNamespace _di;
		private readonly string _schemaVersion;
		private readonly string _schemaLocationBaseUrl;

		public XMLDeclarationWriter()
		{
			_tns = "urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v1.0";
			_rootNamespace = "urn:tugraz:ivt:VectoAPI:DeclarationInput:v1.0a";
			_componentNamespace = "urn:tugraz:ivt:VectoAPI:DeclarationComponent:v1.0";
			_di = "http://www.w3.org/2000/09/xmldsig#";
			_schemaLocationBaseUrl = "";
			_schemaVersion = "1.0";
		}

		public XDocument GenerateVectoJob(VectoSimulationViewModel data)
		{
			var xsi = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");

			//<tns:VectoInputDeclaration 
			//  xmlns="urn:tugraz:ivt:VectoAPI:DeclarationDefinitions:v0.6" 
			//  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" schemaVersion="0.6" 
			//  xmlns:tns="urn:tugraz:ivt:VectoAPI:DeclarationInput:v0.6" 
			//  xsi:schemaLocation="urn:tugraz:ivt:VectoAPI:DeclarationInput:v0.6 http://markus.quaritsch.at/VECTO/VectoInput.xsd">
			var job = new XDocument();
			job.Add(new XElement(_rootNamespace + XMLNames.VectoInputDeclaration,
				new XAttribute("schemaVersion", _schemaVersion),
				new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
				new XAttribute("xmlns", _tns),
				new XAttribute(XNamespace.Xmlns + "tns", _rootNamespace),
				new XAttribute(xsi + "schemaLocation",
					string.Format("{0} {1}VectoInput.xsd", _rootNamespace, _schemaLocationBaseUrl)),
				CreateDeclarationJob(data))
				);
			return job;
		}

		
		protected XElement[] CreateDeclarationJob(VectoSimulationViewModel data)
		{
			return new[] {
				CreateVehicle(data)
			};
		}

		protected XElement CreateVehicle(VectoSimulationViewModel vehicle)
		{
			//var retarder = data.JobInputData.Vehicle.RetarderInputData;
			//var gearbox = data.JobInputData.Vehicle.GearboxInputData;
			////var vehicle = data.JobInputData.Vehicle;
			//var engine = data.JobInputData.Vehicle.EngineInputData;
			//var angledrive = data.JobInputData.Vehicle.AngledriveInputData;


			var id = CreateIdString("VEH-" + vehicle.Model);

			return new XElement(_tns + XMLNames.Component_Vehicle,
				new XAttribute(XMLNames.Component_ID_Attr, id),
				GetDefaultComponentElements(vehicle.VIN, vehicle.Model, vehicle.ManufacturerAddress, vehicle.Manufacturer),
				new XElement(_tns + XMLNames.Vehicle_LegislativeClass, "N3"),
				new XElement(_tns + XMLNames.Vehicle_VehicleCategory, vehicle.VehicleCategory.ToXMLFormat()),
				new XElement(_tns + XMLNames.Vehicle_AxleConfiguration, vehicle.AxleConfiguration.GetName()),
				new XElement(_tns + XMLNames.Vehicle_CurbMassChassis, vehicle.CurbMassChassis.ToXMLFormat(0)),
				new XElement(_tns + XMLNames.Vehicle_GrossVehicleMass, vehicle.TecnicalPermissibleMaximumLadenMass.ToXMLFormat(0)),
				new XElement(_tns + XMLNames.Vehicle_IdlingSpeed, vehicle.IdlingSpeed.ToXMLFormat(0)),
				new XElement(_tns + XMLNames.Vehicle_RetarderType,vehicle.RetarderType.ToXMLFormat()),
					vehicle.RetarderType.IsDedicatedComponent()
					? new XElement(_tns + XMLNames.Vehicle_RetarderRatio, vehicle.RetarderRatio.ToXMLFormat(3))
					: null,
				new XElement(_tns + XMLNames.Vehicle_AngledriveType, vehicle.AngledriveType.ToXMLFormat()),
				new XElement(_tns + XMLNames.Vehicle_PTO,
					new XElement(_tns + XMLNames.Vehicle_PTO_ShaftsGearWheels, vehicle.PTOShaftGearWheels),
					new XElement(_tns + XMLNames.Vehicle_PTO_OtherElements, vehicle.PTOOtherElements)),
				new XElement(_tns + XMLNames.Vehicle_ZeroEmissionVehicle, false),
				new XElement(_tns+XMLNames.Vehicle_VocationalVehicle, vehicle.Vocational),
				new XElement(_tns + XMLNames.Vehicle_SleeperCab, vehicle.SleeperCab),
				new XElement(_tns + XMLNames.Vehicle_ADAS,
					new XElement(_tns + XMLNames.Vehicle_ADAS_EngineStopStart, vehicle.EngineStopStart),
					new XElement(_tns + XMLNames.Vehicle_ADAS_EcoRollWithoutEngineStop, vehicle.EcoRollWithoutEngineStop),
					new XElement(_tns + XMLNames.Vehicle_ADAS_EcoRollWithEngineStopStart, vehicle.EcoRollWithEngineStop),
					new XElement(_tns + XMLNames.Vehicle_ADAS_PCC, vehicle.PCCOption.ToXMLFormat())
				),
				//CreateTorqueLimits(vehicle),
				new XElement(_tns + XMLNames.Vehicle_Components,
					new XElement(_tns + XMLNames.Component_Engine, new XAttribute("gitId", vehicle.EngineComponent)),
					new XElement(_tns + XMLNames.Component_Gearbox, new XAttribute("gitId", vehicle.TransmissionComponent)),
					string.IsNullOrWhiteSpace(vehicle.TorqueConverterComponent) ? null : new XElement(_tns + XMLNames.Component_TorqueConverter, new XAttribute("gitId", vehicle.TorqueConverterComponent)),
					vehicle.AngledriveType == AngledriveType.SeparateAngledrive ?  new XElement(_tns + XMLNames.Component_Angledrive, new XAttribute("gitId", vehicle.AngledriveComponent)) : null,
					vehicle.RetarderType.IsDedicatedComponent() ? new XElement(_tns + XMLNames.Component_Retarder, new XAttribute("gitId", vehicle.RetarderComponent)) : null,
					new XElement(_tns + XMLNames.Component_Axlegear, new XAttribute("gitId", vehicle.AxlegearComponent)),
					CreateAxleWheels(vehicle),
					CreateAuxiliaries(vehicle),
					string.IsNullOrWhiteSpace(vehicle.AirdragComponent) ? null : new XElement(_tns + XMLNames.Component_AirDrag, new XAttribute("gitId", vehicle.AirdragComponent))					
					)
				);
		}

		


		public XElement CreateAxleWheels(VectoSimulationViewModel data)
		{
			var axleData = data.Axles;
			var numAxles = axleData.Count;
			var axles = new List<XElement>(numAxles);
			for (var i = 0; i < numAxles; i++) {
				var axle = axleData[i];
				axles.Add(new XElement(_tns + XMLNames.AxleWheels_Axles_Axle,
					new XAttribute(XMLNames.AxleWheels_Axles_Axle_AxleNumber_Attr, i + 1),
					new XElement(_tns + XMLNames.AxleWheels_Axles_Axle_AxleType_Attr,
						i == 1 ? AxleType.VehicleDriven.ToString() : AxleType.VehicleNonDriven.ToString()),
					new XElement(_tns + XMLNames.AxleWheels_Axles_Axle_TwinTyres_Attr, axle.TwinTyre),
					new XElement(_tns + XMLNames.AxleWheels_Axles_Axle_Steered, i == 0),
					new XElement(_tns + "Tyre", new XAttribute("gitId", axle.TyreComponent))
					));
			}

			return new XElement(_tns + XMLNames.Component_AxleWheels,
				new XElement(_tns + XMLNames.ComponentDataWrapper,
					new XElement(_tns + XMLNames.AxleWheels_Axles, axles))
				);
		}

		
		public XElement CreateAuxiliaries(VectoSimulationViewModel data)
		{
			var auxList = new Dictionary<AuxiliaryType, XElement>();
			foreach (var auxData in data.Auxiliaries) {
				var entry = new XElement(_tns + AuxTypeToXML(auxData.Type),
					auxData.Technology.Select(x => new XElement(_tns + XMLNames.Auxiliaries_Auxiliary_Technology, x)).ToArray<object>());
				auxList[auxData.Type] = entry;
			}
			var aux = new XElement(_tns + XMLNames.ComponentDataWrapper);
			foreach (
				var key in
					new[] {
						AuxiliaryType.Fan, AuxiliaryType.SteeringPump, AuxiliaryType.ElectricSystem, AuxiliaryType.PneumaticSystem,
						AuxiliaryType.HVAC
					}) {
				aux.Add(auxList[key]);
			}
			return new XElement(_tns + XMLNames.Component_Auxiliaries, aux);
		}

		
		private string AuxTypeToXML(AuxiliaryType type)
		{
			return type.ToString();
		}

		private XElement AddSignatureDummy(string id)
		{
			return new XElement(_tns + XMLNames.DI_Signature,
				new XElement(_di + XMLNames.DI_Signature_Reference,
					new XAttribute(XMLNames.DI_Signature_Reference_URI_Attr, "#" + id),
					new XElement(_di + XMLNames.DI_Signature_Reference_Transforms,
						new XElement(_di + XMLNames.DI_Signature_Reference_Transforms_Transform,
							new XAttribute(XMLNames.DI_Signature_Algorithm_Attr,
								"http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithoutComments")),
						new XElement(_di + XMLNames.DI_Signature_Reference_Transforms_Transform,
							new XAttribute(XMLNames.DI_Signature_Algorithm_Attr, "urn:vecto:xml:2017:canonicalization"))
						),
					new XElement(_di + XMLNames.DI_Signature_Reference_DigestMethod,
						new XAttribute(XMLNames.DI_Signature_Algorithm_Attr, "http://www.w3.org/2001/04/xmlenc#sha256")),
					new XElement(_di + XMLNames.DI_Signature_Reference_DigestValue, "")
					)
				);
		}

		

		protected XElement[] GetDefaultComponentElements(string vin, string makeAndModel, string address, string manufacturer)
		{
			return new[] {
				new XElement(_tns + XMLNames.Component_Manufacturer, manufacturer),
				new XElement(_tns + XMLNames.Component_ManufacturerAddress, address),
				new XElement(_tns + XMLNames.Component_Model, makeAndModel),
				new XElement(_tns + XMLNames.Vehicle_VIN, vin),
				new XElement(_tns + XMLNames.Component_Date, XmlConvert.ToString(DateTime.Now, XmlDateTimeSerializationMode.Utc)),
			};
		}

		private string CreateIdString(string id)
		{
			var regexp = new Regex("[^a-zA-Z0-9_.-]");
			return regexp.Replace(id, "");
		}
	}
}
