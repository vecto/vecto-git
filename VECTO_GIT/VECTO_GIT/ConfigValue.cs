﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT
{
  public static class  ConfigValue
  {
    public static string REMOTE_NAME = "VECTO_GIT_REMOTE";
    public static string SIGNATURE_NAME = "VECTO_GIT";
    public static string SIGNATURE_E_MAIL = "vectogit@tugraz.at";
    public static int SIGNATURE_YEAR = 2017;
    public static int SIGNATURE_MONTH = 11;
    public static int SIGNATURE_DAY = 13;
    public static int SIGNATURE_HOUR = 0;
    public static int SIGNATURE_MINUTE = 0;
    public static int SIGNATURE_SECOND = 0;

    public static string STANDARD_VALUES_FILE_NAME = "STANDARD_VALUES";
    public static string COMPONENT_DATA_FILE_NAME = "COMPONENT_DATA";
    public static string CERTIFICATE_DATA_FILE_NAME = "CERTIFICATE_DATA";

    public static string MEASUREMENT_DATA_TREE_NAME = "Measurement_Data";
    public static string USER_DATA_TREE_NAME = "User_Data";

    public static int GIT_ID_LENGTH = 40;

    public static string REFERENCE_HEAD_PATH = "refs/heads";
    public static string REFERENCE_TAG_PATH = "refs/tags";
    

    public static string GIT_ID_FOLDER_NAME = "GIT_ID";
    public static string CERT_NR_FOLDER_NAME = "CERT_NR";

    public static string EXTRACT_FOLDER_NAME = "Extracted_Repository";
    public static string TRANSFER_FOLDER_NAME = "TransferFolder";
    public static string TRANSFER_FOLDER_SETTINGS = "TransferSettings";
    public static string TRANSFER_ARCHIVE_FOLDER_NAME = "Tra";
    public static string EXTRACT_ARCHIVE_FOLDER_NAME = "Ext";

    public static string TRANSFER_XML_FOLDER = "XMLTransfer";
    //public const string MSG_CERT_DATE = "CertificationDate";
    //public const string MSG_STD_DATE = "CommitDate";

  }

}
