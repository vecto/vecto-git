﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.ComponentSearch
{
  public class VectoComponent
  {
    #region Members

    private CertificateNumberHandler certNumberHandler;
    private CommitDateHandler dateHandler;
    private Repository repository;

    #endregion

    #region Properties
    public string CertificateNumber
    {
      get
      {
        return (certNumberHandler == null) ?
          null : certNumberHandler.OriginCertificationNumber;
      }
    }
    public DateTime CertificateDate
    {
      get
      {
        return (dateHandler == null) ?
          default(DateTime) : dateHandler.CertificationDate;
      }
    }

    public Blob ComponentData { get; private set; }
    public Blob StandardValues { get; private set; }
    public Blob CertificateFile { get; private set; }

    public List<Item> MeasurementData { get; private set; }

    public List<Item> UserData { get; private set; }

    public Branch PrivateBranch { get; private set; }
    public bool TransferPrivateBranch { get; set; }

    public Branch PublicBranch { get; private set; }
    public bool TransferPublicBranch { get; set; }

    public bool TransferStandardValuesBranch { get; set; }

    public PublicBranchNaming PublicBranchNaming { get; private set; }
    public PrivateBranchNaming PrivateBranchNaming { get; private set; }

    public string GetPublicGitId
    {
      get
      {
        if (ComponentData != null)
        {
          return ComponentData.Sha;
        }
        return string.Empty;
      }
    }

    #endregion

    public VectoComponent(Repository repository, Branch publicBranch, Branch privateBranch)
    {
      this.repository = repository.CheckValidRepository();

      if (publicBranch != null)
      {
        PublicBranch = publicBranch;
        FetchDataInPublicBranch();
      }

      if (privateBranch != null)
      {
        PrivateBranch = privateBranch;
        FetchDataInPrivateBranch();
      }
    }

    public void ReloadDataFromPublicBranch(Branch publicBranch)
    {
      PublicBranch = publicBranch;
      FetchDataInPublicBranch();
    }

    public void ReloadDataFromPrivateBranch(Branch privateBranch)
    {
      PrivateBranch = privateBranch;
      FetchDataInPrivateBranch();
    }


    #region Fetch Private Data

    private void FetchDataInPrivateBranch()
    {
      PrivateBranchNaming = new PrivateBranchNaming(PrivateBranch.FriendlyName);
      FetchMeasurementData();
      FetchUserData();
    }

    private void FetchUserData()
    {
      if (PrivateBranch == null)
      {
        return;
      }
      var tree = PrivateDataCommit.GetPrivateDataTree(
        ConfigValue.USER_DATA_TREE_NAME, PrivateBranch);
      UserData = FetchPrivateData(tree, FileType.UserData);
    }


    private void FetchMeasurementData()
    {
      if (PrivateBranch == null)
      {
        return;
      }
      var tree = PrivateDataCommit.GetPrivateDataTree(
        ConfigValue.MEASUREMENT_DATA_TREE_NAME, PrivateBranch);
      MeasurementData = FetchPrivateData(tree, FileType.MeasurementData);
    }


    private List<Item> FetchPrivateData(Tree tree, FileType fileType)
    {
      if (tree == null)
        return null;

      var items = new List<Item>();
      foreach (var entry in tree)
      {
        if (entry.TargetType == TreeEntryTargetType.Tree)
        {
          var dir = new DirectoryItem
          {
            FileType = fileType,
            Filename = entry.Name,
            FilePath = entry.Target.Sha,
            Items = FetchPrivateData((Tree)entry.Target, fileType)
          };
          items.Add(dir);
        }

        if (entry.TargetType == TreeEntryTargetType.Blob)
        {
          var item = new FileItem
          {
            FileType = fileType,
            Filename = entry.Name,
            FilePath = entry.Target.Sha
          };
          items.Add(item);
        }
      }
      return items;
    }

    #endregion

    #region Fetch Public Data
    private void FetchDataInPublicBranch()
    {
      PublicBranchNaming = new PublicBranchNaming(PublicBranch.FriendlyName);

      FetchCertificateBlob();
      FetchComponentBlob();
      FetchStandardValueBlob();

      if (ComponentData != null)
      {
        FetchCertificateNumberHandler(ComponentData);
      }
    }

    private void FetchCertificateBlob()
    {
      CertificateFile = CertificateDataCommit.GetCertificateBlob(PublicBranch);
    }


    private void FetchComponentBlob()
    {
      var compCommit = ComponentDataCommit.GetComponentDataCommit(PublicBranch);
      if (compCommit != null)
      {
        ComponentData = ComponentDataCommit.GetComponentDataBlob(compCommit);
        dateHandler = CommitDateHandler.DeserializeCommitDate(compCommit.Message);
      }
    }

    private void FetchStandardValueBlob()
    {
      var stdCommit = StandardDataCommit.GetStandardDataCommit(PublicBranch);
      if (stdCommit != null && dateHandler == null)
      {
        dateHandler = CommitDateHandler.DeserializeCommitDate(stdCommit.Message);
      }

      if (stdCommit != null)
      {
        StandardValues = StandardDataCommit.GetStandardDataBlob(stdCommit);
      }
    }

    private void FetchCertificateNumberHandler(Blob componentBlob)
    {
      using (var content = new StreamReader(componentBlob.GetContentStream(), Encoding.Default))
      {
        var xmlReader = new XmlContentReader();
        certNumberHandler = xmlReader.ReadOutCertificateId(content).CheckNull("certificate number read");
      }
    }

    #endregion
  }
}
