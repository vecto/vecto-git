﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.ComponentSearch
{

  public class VectoSearchTerms
  {

    public bool AnySelectedDate
    {
      get { return IsCertificateDate || IsCertificateDateFromTo; }
    }
    public bool IsCertificateDate { get; set; }
    public bool IsCertificateDateFromTo { get; set; }


    public string GitID { get; set; }
    public CertificationNumberModel CertificationNumber { get; set; }
    public string Manufacturer { get; set; }
    public string Model { get; set; }

    public DateTime CertificateDate { get; set; }
    public DateTime CertificateDateFrom { get; set; }
    public DateTime CertificateDateTo { get; set; }

    public VectoSearchTerms()
    {
      CertificateDate = default(DateTime);
      CertificateDateFrom = default(DateTime);
      CertificateDateTo = default(DateTime);
    }
  }

  public class SearchVectoComponent
  {
    private readonly Repository repository;

    public SearchVectoComponent(Repository repository)
    {
      this.repository = repository.CheckValidRepository();
    }


    #region Search VectoComponents By Given Terms

    public IEnumerable<VectoComponent> SearchVectoComponentWithGivenTerms(VectoSearchTerms givenTerms)
    {
      var foundedBranches = SearchPublicBranches(givenTerms);
      var vectoComponents = Enumerable.Empty<VectoComponent>();

      if (foundedBranches != null && foundedBranches.Any())
      {
        vectoComponents = CreateVectoComponentList(foundedBranches);
      }

      return vectoComponents;
    }

    private IEnumerable<VectoComponent> CreateVectoComponentList(IEnumerable<Branch> foundedBranches)
    {
      var vectoComponents = new List<VectoComponent>();

      foreach (var pubBranch in foundedBranches)
      {
        var branchName = new PublicBranchNaming(pubBranch.FriendlyName);
        var prvBranch = new SearchBranchCommand(repository, branchName, SearchBy.PrivateBranchName).Result();
        if (prvBranch.Any())
        {
          vectoComponents.Add(new VectoComponent(repository, pubBranch, prvBranch.First()));
        }
        else
        {
          vectoComponents.Add(new VectoComponent(repository, pubBranch, null));
        }
      }

      return vectoComponents;
    }

    private IEnumerable<Branch> SearchPublicBranches(VectoSearchTerms searchTerms)
    {
      var branches = Enumerable.Empty<Branch>();

      if (!searchTerms.GitID.IsNullOrEmptyOrWhitespace())
      {
        branches = SearchWithGitId(searchTerms);
      }

      if (searchTerms.CertificationNumber != null &&
          searchTerms.CertificationNumber.AnyCertificateSearchParts())
      {
        branches = SearchWithCertificationNumber(searchTerms, branches);
      }

      if (!searchTerms.Manufacturer.IsNullOrEmptyOrWhitespace() ||
          !searchTerms.Model.IsNullOrEmptyOrWhitespace() ||
          searchTerms.AnySelectedDate)
      {
        branches = SearchWithNameParts(searchTerms, branches);
      }

      if (searchTerms.AnySelectedDate)
      {
        branches = SearchWithCertificationDate(searchTerms, branches);
      }

      return branches;
    }


    private IEnumerable<Branch> SearchWithGitId(VectoSearchTerms searchTerms)
    {
      return SearchBranchesByGitId(searchTerms.GitID);
    }


    private IEnumerable<Branch> SearchBranchesByGitId(string gitId)
    {
      var gitIdHandler = new GitIdHandler(gitId, false);
      return SearchBranchesByTagNaming(gitIdHandler);
    }


    private IEnumerable<Branch> SearchBranchesByTagNaming(INamingHandler tagNaming,
      IEnumerable<Branch> locatedGitIdBranches = null)
    {
      var locatedTags = new SearchTagCommand(repository, tagNaming).Result();
      return FetchTagRelatedBranches(locatedTags, locatedGitIdBranches);
    }


    private IEnumerable<Branch> SearchWithCertificationNumber(VectoSearchTerms searchTerms, IEnumerable<Branch> branches)
    {
      return SearchBranchesByCertificateNumber(searchTerms.CertificationNumber, branches);
    }


    private IEnumerable<Branch> SearchBranchesByCertificateNumber(CertificationNumberModel certificationNumber,
        IEnumerable<Branch> locatedGitIdBranches)
    {
      return SearchBranchBySparseCertificationNumber(certificationNumber, locatedGitIdBranches);
    }


    private IEnumerable<Branch> SearchBranchBySparseCertificationNumber(CertificationNumberModel certificationNumber,
      IEnumerable<Branch> locatedGitIdBranches)
    {
      var locatedTags = new SearchTagCommand(repository, certificationNumber).Result();
      return FetchTagRelatedBranches(locatedTags, locatedGitIdBranches);
    }


    private IEnumerable<Branch> SearchWithNameParts(VectoSearchTerms searchTerms, IEnumerable<Branch> branches)
    {
      return SearchBranchesByName(searchTerms, branches);
    }


    private IEnumerable<Branch> SearchBranchesByName(VectoSearchTerms givenTerms,
      IEnumerable<Branch> currentSelectedBranches)
    {
      return new SearchBranchCommand(repository, givenTerms, currentSelectedBranches).Result();
    }


    private IEnumerable<Branch> SearchWithCertificationDate(VectoSearchTerms searchTerms, IEnumerable<Branch> branches)
    {
      return SearchBranchesByCertificateDate(searchTerms, branches);
    }


    private IEnumerable<Branch> SearchBranchesByCertificateDate(VectoSearchTerms givenTerms,
      IEnumerable<Branch> currentSelectedBranches)
    {
      var foundedBranches = Enumerable.Empty<Branch>();

      if (currentSelectedBranches != null && currentSelectedBranches.Any())
      {
        foundedBranches = new SearchDateCommand(repository, givenTerms, currentSelectedBranches).Result();
      }

      return foundedBranches;
    }

    #endregion


    #region Search All VectoComponents

    public IEnumerable<VectoComponent> SearchAllVectoComponents()
    {
      var branches = GetAllBranches();
      return GetVectoComponentList(branches);
    }

    private List<Branch> GetAllBranches()
    {
      return new SearchBranchCommand(repository).Result().ToList();
    }

    private IEnumerable<VectoComponent> GetVectoComponentList(IEnumerable<Branch> branches)
    {
      var pubBranches = new List<Branch>();
      var prvBranches = new List<Branch>();

      SplitupBranches(branches, ref pubBranches, ref prvBranches);

      return CreateVectoComponentList(pubBranches, prvBranches);
    }


    private void SplitupBranches(IEnumerable<Branch> branches, ref List<Branch> pubBranches,
      ref List<Branch> prvBranches)
    {
      foreach (var branch in branches)
      {
        if (branch.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME))
        {
          pubBranches.Add(branch);
        }
        else if (branch.FriendlyName.StartsWith(PrivateBranchNaming.TREE_NAME))
        {
          prvBranches.Add(branch);
        }
      }
    }


    private IEnumerable<VectoComponent> CreateVectoComponentList(List<Branch> pubBranches, List<Branch> prvBranches)
    {
      var vectoComponents = new List<VectoComponent>();

      for (int i = 0; i < pubBranches.Count; i++)
      {
        var pubBranch = pubBranches[i];
        Branch prvBranch = null;

        var branchNaming = new PublicBranchNaming(pubBranch.FriendlyName);

        var prvBranchName = branchNaming.GetGitPrivateBranchName();
        for (int j = 0; j < prvBranches.Count; j++)
        {
          if (prvBranchName == prvBranches[j].FriendlyName)
          {
            prvBranch = prvBranches[j];
            break;
          }
        }

        if (prvBranchName != null)
        {
          prvBranches.Remove(prvBranch);
        }

        vectoComponents.Add(new VectoComponent(repository, pubBranch, prvBranch));
      }

      if (prvBranches.Count > 0)
        SetBranchesAsVectoComponent(vectoComponents, prvBranches);

      return vectoComponents;
    }


    private void SetBranchesAsVectoComponent(List<VectoComponent> vectoComponents, List<Branch> branches)
    {
      for (int i = 0; i < branches.Count; i++)
      {
        var vectoComponent = new VectoComponent(repository, null, branches[i]);
        vectoComponents.Add(vectoComponent);
      }
    }


    #endregion


    private IEnumerable<Branch> FetchTagRelatedBranches(Tag[] locatedTags, IEnumerable<Branch> locatedGitIdBranch = null)
    {
      if (locatedTags.Length > 0)
      {
        var locatedBranches = new List<Branch>();

        foreach (var tag in locatedTags)
        {
          var branchName = PublicBranchNaming.DeserializeBranchName(tag.Annotation.Message);
          var locatedBranch = new SearchBranchCommand(repository, branchName, SearchBy.BranchName,
            locatedGitIdBranch).Result();
          if (locatedBranch != null && locatedBranch.Any())
          {
            locatedBranches.Add(locatedBranch.First());
          }
        }
        return locatedBranches;
      }
      return Enumerable.Empty<Branch>();
    }
  }
}
