﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VECTO_GIT.Commands;
using LibGit2Sharp;
using System.IO;
using VECTO_GIT.GUI.Model;
using System.Globalization;

namespace VECTO_GIT
{
  public static class Extensions
  {

    public static bool IsNull<T>(this T obj)
    {
      return obj == null;
    }

    public static bool IsNullOrEmpty<T>(this ICollection<T> collection)
    {
      return (collection == null || collection.Count == 0);
    }

    public static bool IsEmpty<T>(this ICollection<T> collection)
    {
      if (collection == null)
        throw new ArgumentNullException("Collection is null");

      return collection.Count == 0;
    }

    public static bool IsExistingFilePath(this string path)
    {
      return File.Exists(path);
    }


    public static bool IsExistingFolderPath(this string folderPath)
    {
      if (folderPath == null)
      {
        return false;
      }

      return Directory.Exists(folderPath);
    }

    public static bool IsNullOrEmptyOrWhitespace(this string value)
    {
      return string.IsNullOrWhiteSpace(value) || value.Length == 0; 
    }



    public static void AddIfNotNull<T>(this ICollection<T> collection, T value)
    {
      if (collection == null)
        throw new ArgumentNullException("Collection is null");

      if (value != null)
      {
        collection.Add(value);
      }
    }

    public static Repository CheckValidRepository(this Repository repository)
    {
      if (repository == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("repository"));
      }

      if (Repository.IsValid(Path.Combine(repository.Info.Path, ".git")))
      {
        throw new ArgumentException(Helpers.ExceptionMessage("repository"));
      }
      return repository;
    }

    public static T CheckNull<T>(this T value, string paramName)
    {
      if (value == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage(paramName));
      }
      return value;
    }

    public static string CheckNullOrEmtpy(this string value, string paramName)
    {
      if (value == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage(paramName));
      }

      if (string.IsNullOrWhiteSpace(value) || value.Length == 0)
      {
        throw new ArgumentException(Helpers.ExceptionMessage(paramName));
      }
      return value;
    }

    public static string CheckFolderPath(this string folderPath)
    {
      if (folderPath == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("folder path"));
      }

      if (!Directory.Exists(folderPath))
      {
        throw new ArgumentException(Helpers.ExceptionMessage("folder path"));
      }
      return folderPath;
    }

    public static string CheckFilePath(this string filePath)
    {
      if (filePath == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("filePath"));
      }

      if (!File.Exists(filePath))
      {
        throw new ArgumentException(Helpers.ExceptionMessage("filePath"));
      }
      return filePath;
    }

    public static Dictionary<TKey, TValue> CheckNullOrEmtpy<TKey, TValue>(this Dictionary<TKey, TValue> collection)
    {

      if (collection == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("collection"));
      }

      if (collection.Count == 0)
      {
        throw new ArgumentException(Helpers.ExceptionMessage("collection"));
      }

      return collection;
    }

    public static List<string> CheckNullOrEmpty(this List<string> collection)
    {
      if (collection == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("collection"));
      }
      if (collection.Count == 0 || !IsValidStrings(collection))
      {
        throw new ArgumentException(Helpers.ExceptionMessage("collection"));
      }
      return collection;
    }

    public static List<string> CheckPathNullOrEmpty(this List<string> collection)
    {
      if (collection == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage("collection"));
      }

      if (collection.Count == 0 || !IsValidStrings(collection))
      {
        throw new ArgumentException(Helpers.ExceptionMessage("collection"));
      }

      if (!collection.All(obj => File.Exists(obj)))
      {
        throw new ArgumentException(Helpers.ExceptionMessage("file path"));
      }
      return collection;
    }

    public static List<Item> CheckForNullEntry(this List<Item> collection)
    {
      for (int i = 0; i < collection.Count; i++)
      {
        if(collection[i] == null)
        {
          throw new ArgumentException(Helpers.ExceptionMessage("collection"));
        }
      }
      return collection;
    }

    private static bool IsValidStrings(List<string> collection)
    {
      foreach (var entry in collection)
      {
        if ((string.IsNullOrWhiteSpace(entry) || entry.Length == 0))
        {
          return false;
        }
      }
      return true;
    }

    public static string[] CheckNullOrEmpty(this string[] collection, string name)
    {
      if (collection == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage(name));
      }
      if (collection.Length == 0 || !IsValidStrings(collection))
      {
        throw new ArgumentException(Helpers.ExceptionMessage(name));
      }
      return collection;
    }

    public static IEnumerable<string> CheckNullOrEmpty(this IEnumerable<string> collection, string name)
    {
      if (collection == null)
      {
        throw new ArgumentNullException(Helpers.ExceptionMessage(name));
      }
      if (collection.Any() || !IsValidStrings(collection.ToList()))
      {
        throw new ArgumentException(Helpers.ExceptionMessage(name));
      }
      return collection;
    }

    private static bool IsValidStrings(string[] collection)
    {
      foreach (var entry in collection)
      {
        if ((string.IsNullOrWhiteSpace(entry) || entry.Length == 0))
        {
          return false;
        }
      }
      return true;
    }




  }
}
