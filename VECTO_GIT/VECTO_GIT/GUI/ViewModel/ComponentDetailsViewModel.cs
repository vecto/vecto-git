﻿using System.Windows.Input;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;


namespace VECTO_GIT.GUI.ViewModel
{

  public class ComponentDetailsViewModel : ViewModelBase
  {
    #region Members

    private readonly INinjectFactory ninjectFactory;
    private readonly object previousViewModel;
    private readonly SearchResultModel selectedComponent;

    private object componentDataContentViewer;

    private bool transferPublicData;
    private bool transferPrivateData;
    private bool transferStandardData;
    private bool selectablePublicData;
    private bool selectablePrivateData;
    private bool selectableStandardData;

    private VectoComponent locatedVectoComponent;

    private ICommand saveTransferCommand;
    private ICommand backToSearchCommand;

    #endregion

    #region Properties

    public VectoComponent LocatedVectoComponent
    {
      get { return locatedVectoComponent; }
      set { SetProperty(ref locatedVectoComponent, value); }
    }

    public bool TransferPublicData
    {
      get { return transferPublicData; }
      set { SetProperty(ref transferPublicData, value); }
    }

    public bool TransferPrivateData
    {
      get { return transferPrivateData; }
      set { SetProperty(ref transferPrivateData, value); }
    }

    public bool TransferStandardData
    {
      get { return transferStandardData; }
      set { SetProperty(ref transferStandardData, value); }
    }

    public bool SelectablePublicData
    {
      get { return selectablePublicData; }
      set { SetProperty(ref selectablePublicData, value); }
    }

    public bool SelectablePrivateData
    {
      get { return selectablePrivateData; }
      set { SetProperty(ref selectablePrivateData, value); }
    }

    public bool SelectableStandardData
    {
      get { return selectableStandardData; }
      set { SetProperty(ref selectableStandardData, value); }
    }

    public object ComponentDataContentViewer
    {
      get { return componentDataContentViewer; }
      set { SetProperty(ref componentDataContentViewer, value); }
    }

    #endregion

    #region Commands


    public ICommand BackToSearchCommand
    {
      get
      {
        if (backToSearchCommand == null)
        {
          backToSearchCommand = new DelegateCommand(
            param => ExecBackToSearchCommand());
        }
        return backToSearchCommand;
      }
    }

    public ICommand SaveTransferCommand
    {
      get
      {
        if (saveTransferCommand == null)
        {
          saveTransferCommand = new DelegateCommand(
            param => ExecSaveTransferCommand());
        }

        return saveTransferCommand;
      }
    }


    #endregion

    public ComponentDetailsViewModel(INinjectFactory ninjectFactory,
      SearchResultModel selectedComponent, object previousViewModel)
    {
      this.ninjectFactory = ninjectFactory;
      this.selectedComponent = selectedComponent;
      this.previousViewModel = previousViewModel;

      LocatedVectoComponent = selectedComponent.VectoComponent;

      SetTransferSelection();
      SetComponentContentViewer();
    }

    private void SetComponentContentViewer()
    {
      ComponentDataContentViewer = new ComponentDataContentViewModel(ninjectFactory, selectedComponent);
    }


    private void SetTransferSelection()
    {
      TransferPrivateData = selectedComponent.VectoComponent.TransferPrivateBranch;
      SelectablePrivateData = selectedComponent.VectoComponent.PrivateBranch != null;

      TransferPublicData = selectedComponent.VectoComponent.TransferPublicBranch;
      SelectablePublicData = selectedComponent.VectoComponent.ComponentData != null;

      TransferStandardData = selectedComponent.VectoComponent.TransferStandardValuesBranch;
      SelectableStandardData = selectedComponent.VectoComponent.StandardValues != null;
    }


    private void ExecBackToSearchCommand()
    {
      var exportViewModel = previousViewModel as ExportTransmissionPacketViewModel;
      exportViewModel?.UpdateTransferFiles();

      ninjectFactory.NavigationViewModel().CurrentViewModel = previousViewModel;
    }

    private bool AnySelectedData()
    {
      return TransferPrivateData || TransferPublicData || TransferStandardData;
    }


    private void ExecSaveTransferCommand()
    {
      var transferFile = GenerateComponentForTransfer();
      var repositoryPath = ninjectFactory.ComponentDataManagement().VirtualDrive.VirtualPath;
      var xmlHandler = ninjectFactory.TransferXmlHandler();

      if (AnySelectedData())
      {
        xmlHandler.SerializeTransferComponent(transferFile, repositoryPath);
      }
      else
      {
        xmlHandler.DeletTransferComponent(transferFile, repositoryPath);
      }
      SetTransferSelection(transferFile);

      xmlHandler.UpdateTransferList(repositoryPath);
    }


    private void SetTransferSelection(ComponentForTransfer componentForTransfer)
    {
      var currentComponent = selectedComponent.VectoComponent;
      currentComponent.TransferStandardValuesBranch = componentForTransfer.TransferStandardBranch;
      currentComponent.TransferPublicBranch = componentForTransfer.TransferPublicBranch;
      currentComponent.TransferPrivateBranch = componentForTransfer.TransferPrivateBranch;
    }

    private ComponentForTransfer GenerateComponentForTransfer()
    {
      var transferComponent = new ComponentForTransfer();
      transferComponent.SetComponentForTransferContent(locatedVectoComponent, TransferPublicData,
          TransferPrivateData, TransferStandardData);

      return transferComponent;
    }
  }

}