﻿using System;
using System.Collections.Generic;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.Helper;
using System.Windows.Input;
using System.Windows.Forms;
using System.ComponentModel;
using System.Net.Mail;
using System.IO;
using MahApps.Metro.Controls.Dialogs;
using Ninject;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.View;

namespace VECTO_GIT.GUI.ViewModel
{
  public class SettingsViewModel : ViewModelBase, IDataErrorInfo
  {

    #region Members
    private ICommand saveSettingsCommand;
    private ICommand repositoryDialogPathCommand;
    private ICommand settingsClosed;

    
    private SettingsModel settingsModel;

    private string repositoryPath;
    private string committerName;
    private string committerEMail;
    private Dictionary<string, string> errors;


    private readonly INinjectFactory ninjectFactory;

    #endregion

    #region Commands

    public ICommand SelectDialogCommand
    {
      get
      {
        if (repositoryDialogPathCommand == null)
        {
          repositoryDialogPathCommand = new DelegateCommand(
            p => ExecuteSelectRepositoryDialog());
        }
        return repositoryDialogPathCommand;
      }
    }
    public ICommand SaveSettingsCommand
    {
      get
      {
        if (saveSettingsCommand == null)
        {
          saveSettingsCommand = new DelegateCommand(
           p => CanExcuteSave(),
           p => SaveSettings(p));
        }
        return saveSettingsCommand;
      }
    }
    public ICommand ClosedSettings
    {
      get
      {
        if (settingsClosed == null)
        {
          settingsClosed = new DelegateCommand(
            p => ClosedSettingsWindow(p));
        }
        return settingsClosed;
      }
    }

    #endregion

    #region Properties

    public string RepositoryPath
    {
      get
      {
        return repositoryPath;
      }
      set
      {
        SetProperty(ref repositoryPath, value);
      }
    }

    public string CommitterName
    {
      get
      {
        return committerName;
      }
      set
      {
        SetProperty(ref committerName, value);
      }
    }

    public string CommitterEMail
    {
      get
      {
        return committerEMail;
      }
      set
      {
        SetProperty(ref committerEMail, value);
      }
    }

    #endregion

    #region Input Validation
    public string Error
    {
      get;
    }

    public string this[string columnName]
    {
      get
      {
        var errorMessage = string.Empty;
        switch (columnName)
        {
          case "CommitterName":
            errorMessage = CheckCommitterName(columnName);
            break;
          case "CommitterEMail":
            errorMessage = CheckCommitterEMail(columnName);
            break;
          case "RepositoryPath":
            errorMessage = CheckRepositoryPath(columnName);
            break;
          default:
            break;
        }
        return errorMessage;
      }
    }

    private string CheckCommitterName(string propertyName)
    {
      var errorMessage = string.Empty;

      if (string.IsNullOrEmpty(CommitterName))
      {
        errorMessage = "Please enter the committer name";
        AddError(propertyName, errorMessage);
      }
      else
      {
        RemoveError(propertyName);
      }
      return errorMessage;
    }

    private string CheckCommitterEMail(string propertyName)
    {
      var errorMessage = string.Empty;

      if (!IsValidMailAddress(CommitterEMail))
      {
        errorMessage = "Invalid E-Mail address";
        AddError(propertyName, errorMessage);
      }
      else
      {
        RemoveError(propertyName);
      }
      return errorMessage;
    }

    private string CheckRepositoryPath(string propertyName)
    {
      var errorMessage = string.Empty;

      if (!IsValidDirectory(RepositoryPath))
      {
        errorMessage = "Invalid repository path";
        AddError(propertyName, errorMessage);
      }
      else
      {
        RemoveError(propertyName);
      }
      return errorMessage;
    }

    private void AddError(string propertyName, string message)
    {
      if (!errors.ContainsKey(propertyName))
      {
        errors[propertyName] = message;
      }
    }

    private void RemoveError(string propertyName)
    {
      if (errors.ContainsKey(propertyName))
      {
        errors.Remove(propertyName);
      }
    }

    #endregion


    public SettingsViewModel(INinjectFactory ninjectFactory)
    {
      this.ninjectFactory = ninjectFactory;
      settingsModel = ninjectFactory.SettingsModel();
      repositoryPath = settingsModel.RepositoryPath;
      committerName = settingsModel.CommitterName;
      committerEMail = settingsModel.CommitterEMail;
      errors = new Dictionary<string, string>();
    }

    private void ExecuteSelectRepositoryDialog()
    {
      using (var dialog = new FolderBrowserDialog())
      {
        dialog.Description = "Select a folder where the Repository should be saved";

        if (IsValidDirectory(RepositoryPath))
        {
          dialog.SelectedPath = RepositoryPath;
        }
        dialog.ShowDialog();

        RepositoryPath = dialog.SelectedPath;
      }
    }

    private void SaveSettings(object commandParameter)
    {
      settingsModel.RepositoryPath = repositoryPath;
      settingsModel.CommitterName = committerName;
      settingsModel.CommitterEMail = committerEMail;
      ((SettingsWindow)commandParameter).Close();
      
      //ToDo Comment out virtual drive for the moment
      var compDataManagement = ninjectFactory.ComponentDataManagement();
      var repository = compDataManagement.SetCurrentRepository(repositoryPath);

      if (repository != null)
        ResetToStartView();
    }

    private void ResetToStartView()
    {
      ninjectFactory.NavigationViewModel().ExecBackToStartViewCommand();
    }


    private void ClosedSettingsWindow(object closeEvent)
    {
      if (errors.Count > 0)
      {
        CloseDialogOnError((CancelEventArgs)closeEvent);
      }

      repositoryPath = settingsModel.RepositoryPath;
      committerName = settingsModel.CommitterName;
      committerEMail = settingsModel.CommitterEMail;
    }

    private void CloseDialogOnError(CancelEventArgs cancelEvent)
    {
      var mySettings = new MetroDialogSettings()
      {
        AffirmativeButtonText = "Ok",
        NegativeButtonText = "Cancel",
        AnimateShow = true,
        AnimateHide = true,
      };

      var coordinator = DialogCoordinator.Instance;
      var result = coordinator.ShowModalMessageExternal(this, "", "Do you really want to close?",
        MessageDialogStyle.AffirmativeAndNegative, mySettings);

      if (result == MessageDialogResult.Negative)
      {
        cancelEvent.Cancel = true;
      }
    }

    private bool CanExcuteSave()
    {
      return !(errors.Count > 0);
    }

    private bool IsValidMailAddress(string eMail)
    {
      try
      {
        var mailaddress = new MailAddress(eMail);
        return true;
      }
      catch
      {
        return false;
      }
    }

    private bool IsValidDirectory(string dirPath)
    {
      return Directory.Exists(dirPath);
    }
  }
}
