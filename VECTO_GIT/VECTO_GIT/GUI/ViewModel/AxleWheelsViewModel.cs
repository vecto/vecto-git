﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Utils;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel
{
	public class AxleWheelsViewModel : ViewModelBase
	{
		private AxleType _axleType;
		private bool _twinTyre;
		private bool _steered;
		private string _tyreComponent;
		private ICommand _browseTyreComponent;
		private int _idx;

		public AxleWheelsViewModel(int idx)
		{
			_idx = idx;

			AxleTypesList = new ObservableCollection<Tuple<AxleType, string>>();
			foreach (var axleType in EnumHelper.GetValues<AxleType>()) {
				if (axleType != AxleType.Trailer) {
					AxleTypesList.Add(Tuple.Create(axleType, axleType.GetLabel()));
				}
			}
		}

		public ObservableCollection<Tuple<AxleType, string>> AxleTypesList { get; }

		public AxleType AxleType
		{
			get { return _axleType; }
			set { SetProperty(ref _axleType, value); }
		}

		public bool Steered
		{
			get { return _steered; }
			set { SetProperty(ref _steered, value); }
		}

		public bool TwinTyre
		{
			get { return _twinTyre; }
			set { SetProperty(ref _twinTyre, value); }
		}

		public string TyreComponent
		{
			get { return _tyreComponent; }
			set { SetProperty(ref _tyreComponent, value); }
		}

		public ICommand BrowseTyreComponent
		{
			get { return _browseTyreComponent ?? (_browseTyreComponent = new DelegateCommand(p => ExecBrowseTyre())); }
			
		}

		private void ExecBrowseTyre()
		{
			SimulationViewModel.ExecBrowseCommand(VectoSimulationViewModel.Components.Wheels, _idx);
		}

		public VectoSimulationViewModel SimulationViewModel { get; set; }
	}
}
