﻿using System;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Model;
using System.Windows.Input;
using Microsoft.Win32;
using VECTO_GIT.ComponentTransfer;
using LibGit2Sharp;
using VECTO_GIT.ComponentSearch;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace VECTO_GIT.GUI.ViewModel
{
  public class ImportTransmissionPacketViewModel : ViewModelBase
  {
    #region Members

    private readonly INinjectFactory ninjectFactory;
    private readonly TransferHandler transferHandler;
    private readonly ComponentDataManagement dataManagement;

    private string archivePath;
    private string extractedArchivePath;
    private List<KeyValuePair<string, bool>> resultMessages;
    private bool expandImportInformation;
    private bool showImportListing;
    private Brush messageForeground;
    private ICommand selectArchiveFileCommand;
    private ICommand importCommand;
    private ICommand cancelCommand;

    #endregion

    #region Properties

    public ObservableCollection<SearchResultModel> SearchResults { get; set; }

    public string ArchivePath
    {
      get { return archivePath; }
      set { SetProperty(ref archivePath, value); }
    }

    public bool ShowImportListing
    {
      get { return showImportListing; }
      set { SetProperty(ref showImportListing, value); }
    }

    public List<KeyValuePair<string, bool>> ResultMessages
    {
      get { return resultMessages; }
      set { SetProperty(ref resultMessages, value); }
    }

    public bool ExpandImportInformation
    {
      get { return expandImportInformation; }
      set { SetProperty(ref expandImportInformation, value); }
    }

    public Brush MessageForeground
    {
      get { return messageForeground; }
      set { SetProperty(ref messageForeground, value); }
    }
    #endregion

    #region Commands

    public ICommand SelectArchiveFileCommand
    {
      get
      {
        if (selectArchiveFileCommand == null)
        {
          selectArchiveFileCommand = new DelegateCommand(
            param => ExecSelectArchiveFileCommand());
        }
        return selectArchiveFileCommand;
      }
    }
    
    public ICommand ImportCommand
    {
      get
      {
        if (importCommand == null)
        {
          importCommand = new DelegateCommand(param => CanImportCommand(),
            param => ExecImportCommand());
        }
        return importCommand;
      }
    }

    public ICommand CancelCommand
    {
      get
      {
        if (cancelCommand == null)
        {
          cancelCommand = ninjectFactory.NavigationViewModel().BackToStartViewCommand; 
        }
        return cancelCommand;
      }
    }

    #endregion

    public ImportTransmissionPacketViewModel(INinjectFactory ninjectFactory, ComponentDataManagement dataManagement)
    {
      this.ninjectFactory = ninjectFactory;
      this.dataManagement = dataManagement;
      transferHandler = new TransferHandler();
    }

    private void ExecSelectArchiveFileCommand()
    {
      ArchivePath = SelectArchiveDialog();
      if(ArchivePath != null)
      {
        ExtractSelectedArchive(ArchivePath);
      }
      else
      {
        SetDefaultView();
      }
    }

    private bool CanImportCommand()
    {
      return SearchResults != null && SearchResults.Count > 0;
    }

    //ToDo add notifictaion on success and error!!
    private void ExecImportCommand()
    {
      var result = transferHandler.MergeExtractedArchiveIntoRepository(dataManagement.CurrentRepository, extractedArchivePath);
      if (result != null)
      {
        var mergeConflicts = ShowMergeWindow(result);
        if (mergeConflicts != null)
        {
         var mergeResult =  transferHandler.MergeConflictsIntoRepository(dataManagement.CurrentRepository,
            extractedArchivePath, mergeConflicts);
        }
      }
      else
      {
        SetDefaultView();
      }

      SetImportDataInformation(transferHandler.MergeMessages);
    }

    private List<MergePrivateBranchModel> ShowMergeWindow(List<MergePrivateBranchModel> mergeConflicts)
    {
      var mergeWindow = ninjectFactory.MergeDataWindow(mergeConflicts);
      mergeWindow.CenterToSrcWindow(Application.Current.MainWindow);

      if ((bool)mergeWindow.ShowDialog())
      {
        return mergeConflicts;
      }
      return null;
    }
    

    private string SelectArchiveDialog()
    {
      var selectArchiveDialog = new OpenFileDialog()
      {
        Filter = "Archive (*.zip)|*.zip",
        Multiselect = false,
        InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
      };

      return selectArchiveDialog.ShowDialog() == true ? selectArchiveDialog.FileName : null;
    }

    private void ExtractSelectedArchive(string selectedArchivePath)
    {
      var repositoryPath = dataManagement.CurrentRepository.Info.Path;
      ClearExtractedArchiveFolder(repositoryPath);

      var extractPath = transferHandler.ExtractArchiveIntoFolder(repositoryPath, selectedArchivePath);

      var extractRepository = LoadExtractedArchive(extractPath);
      if(extractRepository != null)
      {
        extractedArchivePath = extractPath;
        SetAllComponentsOfRepository(extractRepository);
        extractRepository.Dispose();
      }
    }

    private void ClearExtractedArchiveFolder(string repositoryPath)
    {
      var destPath = Path.Combine(repositoryPath, ConfigValue.EXTRACT_ARCHIVE_FOLDER_NAME);
      Helpers.DeleteDirectory(destPath);
    }


    private void SetAllComponentsOfRepository(Repository extractRepository)
    {
      var searchComponents = new SearchVectoComponent(extractRepository);
      var searchResult = searchComponents.SearchAllVectoComponents();

      SearchResults = GUIHelpers.SummarizeComponentSearchResult(searchResult);
      OnPropertyChanged("SearchResults");

      ShowImportListing = SearchResults.Count > 0;
    }


    private Repository LoadExtractedArchive(string extractPath)
    {
      var extractedRepository = Helpers.LoadRepository(extractPath, true);
      
      if(extractedRepository != null)
      {
        if(extractedRepository.Branches.Any()) //zip file doesn't contain any bare repository
        {
          return extractedRepository;
        }
        extractedRepository.Dispose();
      }
            
      return null;
    }

    private void SetDefaultView()
    {
      ArchivePath = null;
      ShowImportListing = false;
      SearchResults = null;
    }

    private void SetImportDataInformation(List<KeyValuePair<string, bool>> importInformation)
    {
      ResultMessages = importInformation;
      ExpandImportInformation = ResultMessages != null && ResultMessages.Count > 0;
    }

  }
}


