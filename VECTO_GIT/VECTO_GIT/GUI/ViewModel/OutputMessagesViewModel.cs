﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel
{
  public class OutputMessagesViewModel : ViewModelBase
  {
    #region Members

    private List<KeyValuePair<string, bool>> messages;
    
    #endregion

    #region Properties

    public List<KeyValuePair<string, bool>> Messages
    {
      get { return messages; }
      set { SetProperty(ref messages, value); }
    }

    #endregion
    
    public void SetMessages(List<KeyValuePair<string, bool>> currentMessages)
    {
      Messages = currentMessages;
    }
  }
}
