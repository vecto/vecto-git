﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;


namespace VECTO_GIT.GUI.ViewModel
{
  public enum SearchMode
  {
    NormalMode,
    AppendMode
  }

  public class SearchComponentViewModel : ViewModelBase
  {
    #region Members
    private const string DETAILED_BUTTON_TEXT = "Use Detailed Date Search";
    private const string SIMPLE_BUTTON_TEXT = "Use Simple Date Search";
    private const string DETAIL_BUTTON_TEXT = "Details";
    private const string APPEND_BUTTON_TEXT = "Append";

    private readonly CertificationNumberModel certificationNumber;
    private readonly INinjectFactory ninjectFactory;

    private bool expandSearchSettings;
    private bool expandSearchResult;
    private bool simpleDateSearch;
    private bool detailedDateSearch;
    private bool ignoreCertificateDate;
    private bool certDateSwitchButton;
    private SearchMode currentSearchMode;
    private string showDateButtonText;
    private string searchButtonText;

    private string manufacturerName;
    private string modelName;
    private string gitIdentifier;
    private string certCountry;
    private string certCertificationAct;
    private string certAmendingAct;
    private string certComponentType;
    private string certBaseNumber;
    private string certExtensionNumber;
    private DateTime certDate;
    private DateTime certDateFrom;
    private DateTime certDateTo;

    private VectoSearchTerms vectoSearchTerms;

    private ICommand searchCommand;
    private ICommand cancelCommand;
    private ICommand selectedActionCommand;
    private ICommand showDetailedDateCommand;

    #endregion

    #region Properties

    public ObservableCollection<SearchResultModel> SearchResults { get; set; }

    public string ManufacturerName
    {
      get { return manufacturerName; }
      set
      {
        if (SetProperty(ref manufacturerName, value))
        {
          vectoSearchTerms.Manufacturer =
            StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName).ToLower();
        }
      }
    }

    public string ModelName
    {
      get { return modelName; }
      set
      {
        if (SetProperty(ref modelName, value))
        {
          vectoSearchTerms.Model =
            StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName).ToLower();
        }
      }
    }

    public string GitIdentifier
    {
      get { return gitIdentifier; }
      set
      {
        if (SetProperty(ref gitIdentifier, value))
        {
          vectoSearchTerms.GitID = GitIdentifier;
        }
      }
    }

    public DateTime CertDate
    {
      get { return certDate; }
      set { SetProperty(ref certDate, value); }
    }

    public DateTime CertDateFrom
    {
      get { return certDateFrom; }
      set { SetProperty(ref certDateFrom, value); }
    }

    public DateTime CertDateTo
    {
      get { return certDateTo; }
      set { SetProperty(ref certDateTo, value); }
    }

    public bool ExpandSearchSettings
    {
      get { return expandSearchSettings; }
      set { SetProperty(ref expandSearchSettings, value); }
    }

    public bool ExpandSearchResult
    {
      get { return expandSearchResult; }
      set { SetProperty(ref expandSearchResult, value); }
    }

    public string CertCountry
    {
      get { return certCountry; }
      set
      {
        if (SetProperty(ref certCountry, value))
        {
          certificationNumber.Country = value;
        }
      }
    }

    public string CertCertificationAct
    {
      get { return certCertificationAct; }
      set
      {
        if (SetProperty(ref certCertificationAct, value))
        {
          certificationNumber.CertificationAct = value;
        }
      }
    }

    public string CertAmendingAct
    {
      get { return certAmendingAct; }
      set
      {
        if (SetProperty(ref certAmendingAct, value))
        {
          certificationNumber.AmendingAct = value;
        }
      }
    }

    public string CertComponentType
    {
      get { return certComponentType; }
      set
      {
        if (SetProperty(ref certComponentType, value))
        {
          certificationNumber.ComponentType = value;
        }
      }
    }
    public string CertBaseNumber
    {
      get { return certBaseNumber; }
      set
      {
        if (SetProperty(ref certBaseNumber, value))
        {
          certificationNumber.BaseNumber = value;
        }
      }
    }

    public string CertExtensionNumber
    {
      get { return certExtensionNumber; }
      set
      {
        if (SetProperty(ref certExtensionNumber, value))
        {
          certificationNumber.ExtensionNumber = value;
        }
      }
    }

    public SearchMode CurrentSearchMode
    {
      get { return currentSearchMode; }
      set { SetProperty(ref currentSearchMode, value); }
    }

    public bool SimpleDateSearch
    {
      get { return simpleDateSearch; }
      set
      {
        SetProperty(ref simpleDateSearch, value);
        if (value)
        {
          ShowDateButtonText = DETAILED_BUTTON_TEXT;
        }
        else
        {
          ShowDateButtonText = SIMPLE_BUTTON_TEXT;
        }
      }
    }

    public bool DetailedDateSearch
    {
      get { return detailedDateSearch; }
      set { SetProperty(ref detailedDateSearch, value); }
    }

    public string ShowDateButtonText
    {
      get { return showDateButtonText; }
      set { SetProperty(ref showDateButtonText, value); }
    }

    public bool IgnoreCertificateDate
    {
      get { return ignoreCertificateDate; }
      set
      {
        SetProperty(ref ignoreCertificateDate, value);
        if (value)
        {
          SimpleDateSearch = false;
          DetailedDateSearch = false;
          CertDateSwitchButton = false;
        }
        else
        {
          SimpleDateSearch = true;
          DetailedDateSearch = false;
          CertDateSwitchButton = true;
        }
      }
    }

    public bool CertDateSwitchButton
    {
      get { return certDateSwitchButton; }
      set { SetProperty(ref certDateSwitchButton, value); }
    }

    public string SearchButtonText
    {
      get { return searchButtonText; }
      set { SetProperty(ref searchButtonText, value); }
    }


    #endregion

    #region Commands

    public ICommand SearchCommand
    {
      get
      {
        if (searchCommand == null)
        {
          searchCommand = new DelegateCommand(param => ExecSearchCommand(param));
        }
        return searchCommand;
      }
    }

    public ICommand CancelCommand
    {
      get
      {
        if (cancelCommand == null)
        {
          cancelCommand = ninjectFactory.NavigationViewModel().BackToStartViewCommand;
        }
        return cancelCommand;
      }
    }

    public ICommand SelectedActionCommand
    {
      get
      {
        if (selectedActionCommand == null)
        {
          selectedActionCommand = new DelegateCommand(param => ExecSelectedActionCommand(param));
        }

        return selectedActionCommand;
      }
    }

    public ICommand ShowDetailedDateCommand
    {
      get
      {
        if (showDetailedDateCommand == null)
        {
          showDetailedDateCommand = new DelegateCommand(param => ExecShowDetailedDateCommand());
        }

        return showDetailedDateCommand;
      }
    }

    #endregion

    public SearchComponentViewModel(INinjectFactory ninjectFactory, bool isAppendSearch)
    {
      this.ninjectFactory = ninjectFactory;
      certificationNumber = new CertificationNumberModel();

      SetTypeOfSearchResult(isAppendSearch);
      DefaultInit();
    }


    private void SetTypeOfSearchResult(bool isAppendSearch)
    {
      if (isAppendSearch)
      {
        SearchButtonText = APPEND_BUTTON_TEXT;
        CurrentSearchMode = SearchMode.AppendMode;
      }
      else
      {
        SearchButtonText = DETAIL_BUTTON_TEXT;
        CurrentSearchMode = SearchMode.NormalMode;
      }
    }

    private void DefaultInit()
    {
      vectoSearchTerms = new VectoSearchTerms();
      var startDateTime = DateTime.Now;
      certDate = startDateTime;
      certDateFrom = DateTime.Now.AddDays(-7);
      certDateTo = startDateTime;
      ExpandSearchSettings = true;
      IgnoreCertificateDate = true;
    }


    #region Command Implementations
    
    private void ExecSearchCommand(object parameter)
    {
      ExpandSearchSettings = false;
      ExpandSearchResult = true;

      SetSearchResult();
    }

    private void ExecSelectedActionCommand(object parameter)
    {
      var component = parameter as SearchResultModel;
      if (component != null)
      {
        if (SearchButtonText == APPEND_BUTTON_TEXT)
        {
          ninjectFactory.NavigationViewModel().CurrentViewModel
            = ninjectFactory.AppendDataToComponentViewModel(component, this);
        }

        if (SearchButtonText == DETAIL_BUTTON_TEXT)
        {
          ninjectFactory.NavigationViewModel().CurrentViewModel
            = ninjectFactory.ComponentDetailsViewModel(component, this);
        }
      }
    }
    

    private void ExecShowDetailedDateCommand()
    {
      SimpleDateSearch = SimpleDateSearch != true;
      DetailedDateSearch = DetailedDateSearch != true;
    }


    #endregion

    private IEnumerable<VectoComponent> StartSearch()
    {
      var compDataManagement = ninjectFactory.ComponentDataManagement();
      SetCertificationSearchDate();

      if (IsNothingSelected())
      {
        return compDataManagement.SearchAllVectoComponents();
      }
      else
      {
        vectoSearchTerms.CertificationNumber = certificationNumber;
        return compDataManagement.SearchVectoComponentsByGivenTerms(vectoSearchTerms);
      }
    }

    private void SetCertificationSearchDate()
    {
      if (!IgnoreCertificateDate)
      {
        vectoSearchTerms.IsCertificateDate = SimpleDateSearch;
        vectoSearchTerms.IsCertificateDateFromTo = DetailedDateSearch;

        if (SimpleDateSearch)
        {
          vectoSearchTerms.CertificateDate = CertDate;
        }

        if (DetailedDateSearch)
        {
          vectoSearchTerms.CertificateDateFrom = CertDateFrom;
          vectoSearchTerms.CertificateDateTo = CertDateTo;
        }

      }
      else
      {
        vectoSearchTerms.IsCertificateDate = false;
        vectoSearchTerms.IsCertificateDateFromTo = false;
      }
    }

    private bool IsNothingSelected()
    {
      return manufacturerName.IsNullOrEmptyOrWhitespace() &&
             modelName.IsNullOrEmptyOrWhitespace() &&
             gitIdentifier.IsNullOrEmptyOrWhitespace() &&
             !certificationNumber.AnyCertificateSearchParts() &&
             IgnoreCertificateDate;
    }

    private void SetSearchResult()
    {
      var searchResult = StartSearch();
      SearchResults = GUIHelpers.SummarizeComponentSearchResult(searchResult);
      if (currentSearchMode == SearchMode.NormalMode)
      {
        SetSearchTransferSelection(searchResult);
      }
      OnPropertyChanged("SearchResults");
    }

    private void SetSearchTransferSelection(IEnumerable<VectoComponent> searchResult)
    {
      var repositoryPath = ninjectFactory.ComponentDataManagement().VirtualDrive.VirtualPath;
      var transferselection = ninjectFactory.TransferXmlHandler();
      transferselection.UpdateTransferList(repositoryPath);

      var selection = transferselection.TransferList;
      var vectoComponents = searchResult as List<VectoComponent> ?? searchResult.ToList();

      for (var i = 0; i < selection.Count; i++)
      {
        var currentSelection = selection[i];
        var manufacturer = currentSelection.ManufacturerName;
        var model = currentSelection.ModelName;

        for (var j = 0; j < vectoComponents.Count; j++)
        {
          var vectoComponent = vectoComponents[j];

          if (vectoComponent.PublicBranchNaming != null)
          {
            if (vectoComponent.PublicBranchNaming.Manufacturer == manufacturer &&
                vectoComponent.PublicBranchNaming.Model == model)
            {
              SetPublicTransferBranches(vectoComponent.PublicBranchNaming.GetGitPathBranchName(), vectoComponent, currentSelection);
            }
          }

          if (vectoComponent.PrivateBranchNaming != null)
          {
            if (vectoComponent.PrivateBranchNaming.Manufacturer == manufacturer &&
                vectoComponent.PrivateBranchNaming.Model == model)
            {
              SetPrivateTransferBranch(vectoComponent.PrivateBranchNaming.GetGitPathBranchName(), vectoComponent,
                currentSelection);
            }
          }
        }
      }
    }

    private void SetPublicTransferBranches(string branchName, VectoComponent component,
      ComponentForTransfer transferComponent)
    {
      if (transferComponent.PublicBranch != null && transferComponent.PublicBranch.TransferBranch)
      {
        if (transferComponent.PublicBranch.BranchName == branchName)
        {
          component.TransferPublicBranch = true;
        }
      }

      if (transferComponent.StandardBranch != null && transferComponent.StandardBranch.TransferBranch)
      {
        component.TransferStandardValuesBranch = true;
      }
    }


    private void SetPrivateTransferBranch(string branchName, VectoComponent component,
      ComponentForTransfer transferComponent)
    {
      if (transferComponent.PrivatBranch != null && transferComponent.PrivatBranch.TransferBranch)
      {
        if (transferComponent.PrivatBranch.BranchName == branchName)
        {
          component.TransferPrivateBranch = true;
        }
      }
    }
  }
}
