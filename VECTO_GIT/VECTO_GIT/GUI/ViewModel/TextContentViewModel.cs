﻿using System.Collections.Generic;
using LibGit2Sharp;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Highlighting;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel
{
  public class TextContentViewModel : ViewModelBase
  {
    #region Members

    private string textContent;
    private Blob blobContent;
    private string fileName;

    private bool showLineNumbers;
    private string syntaxHighlightType;
    private List<string> syntaxHighlightTypes;

    private ICommand saveCommand;
    private ICommand selectedHighlightCommand;
    private ICommand showLineNumbersCommand;
    private ICommand userControlLoadedCommand;
    #endregion

    #region Properties

    public string TextContent
    {
      get { return textContent; }
      set { SetProperty(ref textContent, value); }
    }

    public string SyntaxHighlightType
    {
      get { return syntaxHighlightType; }
      set { SetProperty(ref syntaxHighlightType, value); }
    }

    public List<string> SyntaxHighlightTypes
    {
      get { return syntaxHighlightTypes; }
      set { SetProperty(ref syntaxHighlightTypes, value); }
    }
    
    public bool ShowLineNumbers
    {
      get { return showLineNumbers; }
      set { SetProperty(ref showLineNumbers, value); }
    }

    #endregion

    #region Commands

    public ICommand SaveCommand
    {
      get
      {
        if (saveCommand == null)
        {
          saveCommand = new DelegateCommand(param => ExecSaveCommand());
        }

        return saveCommand;
      }
    }

    public ICommand SelectedHighlightCommand
    {
      get
      {
        if (selectedHighlightCommand == null)
        {
          selectedHighlightCommand = new DelegateCommand(param => ExecSelectedHighlightCommand(param));
        }

        return selectedHighlightCommand;
      }
    }

    public ICommand ShowLineNumbersCommand
    {
      get
      {
        if (showLineNumbersCommand == null)
        {
          showLineNumbersCommand = new DelegateCommand(param => ExecShowLineNumbersCommand(param));
        }

        return showLineNumbersCommand;
      }
    }

    public ICommand UserControlLoadedCommand
    {
      get
      {
        if (userControlLoadedCommand == null)
        {
          userControlLoadedCommand = new DelegateCommand(param => ExecUserControlLoadedCommand(param));
        }

        return userControlLoadedCommand;
      }
    }

    #endregion

    public TextContentViewModel()
    {
      SetupSyntaxHighlightTypes();
    }
    
    private void SetupSyntaxHighlightTypes()
    {
      var types = new List<string>
      {
        "XML",
        "HTML",
        "ASP/XHTML",
        "TeX",
        "CSS",
       "MarkDown"
      };
      SyntaxHighlightTypes = types;
    }
    
    public void LoadBlobContent(KeyValuePair<string, Blob> currentBlob)
    {
      fileName = currentBlob.Key;
      blobContent = currentBlob.Value;

      using (var stream = blobContent.GetContentStream())
      {
        using (var streamReader = new StreamReader(stream, Encoding.UTF8))
        {
          TextContent = streamReader.ReadToEnd();
        }
      }
    }

    private void ExecSelectedHighlightCommand(object param)
    {
      var convert = param as TextEditor;
      var highlighting = HighlightingManager.Instance.GetDefinition(SyntaxHighlightType);
      if (convert != null)
      { 
        convert.SyntaxHighlighting = highlighting;
      }
    }

    private void ExecShowLineNumbersCommand(object param)
    {
      var convert = param as TextEditor;
      if (convert != null)
      {
        convert.ShowLineNumbers = ShowLineNumbers;
      }
    }

    private void ExecUserControlLoadedCommand(object param)
    {
      var convert = param as TextEditor;
      if (convert != null)
      {
        if (SyntaxHighlightTypes != null && SyntaxHighlightTypes.Count > 0)
        {
          SyntaxHighlightType = SyntaxHighlightTypes[0];
          var highlighting = HighlightingManager.Instance.GetDefinition(SyntaxHighlightType);
          convert.SyntaxHighlighting = highlighting;
        }
      }
    }


    private void ExecSaveCommand()
    {
      GUIHelpers.SaveFileDialog(blobContent, string.Empty);
    }
  }
}
