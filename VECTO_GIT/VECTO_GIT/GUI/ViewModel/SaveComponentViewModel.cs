﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows.Input;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.ViewModel
{
  public class SaveComponentViewModel : ViewModelBase
  {
    #region Members

    private const string XSD_FILENAME = "VectoComponent.xsd";
    private const string XSD_NAMESPACE = "urn:tugraz:ivt:VectoAPI:DeclarationComponent:v1.0";

    private ICommand cancelCommand;
    private ICommand addFilesCommand;
    private ICommand removeSelectedCommand;
    private ICommand removeAllCommand;
    private ICommand saveCommand;
    private ICommand selectionChangedCommand;
    private ICommand addFolderCommand;
    private ICommand menuRemoveCommand;
    private ICommand menuEditCommand;

    private readonly INinjectFactory ninjectFactory;
    private readonly ValidateComponentDataHelper componentDataValidator;

    private bool expandSelectedFiles;
    private bool expandSaveInformation;
    private bool validForSave;

    private List<KeyValuePair<string, bool>> resultMessages;
    private List<ComponentCoreCommit> commitsToExecute;
    private List<List<CommitVerificationError>> verificationErrors;
    private List<Item> rightClickSelectedItem;

    private int selectedDataGridRow;



    #endregion

    #region Properties

    public IEnumerable<FileType> FileDataTypes { get; set; }

    public ObservableCollection<List<Item>> SelectedData { get; set; }

    public int SelectedDataGridRow
    {
      get { return selectedDataGridRow; }
      set { SetProperty(ref selectedDataGridRow, value); }
    }

    public bool ExpandSelectedFiles
    {
      get { return expandSelectedFiles; }
      set { SetProperty(ref expandSelectedFiles, value); }
    }

    public List<Item> RightClickSelectedItem
    {
      get { return rightClickSelectedItem; }
      set { SetProperty(ref rightClickSelectedItem, value); }
    }

    public List<KeyValuePair<string, bool>> ResultMessages
    {
      get { return resultMessages; }
      set { SetProperty(ref resultMessages, value); }
    }

    public bool ExpandSaveInformation
    {
      get { return expandSaveInformation; }
      set { SetProperty(ref expandSaveInformation, value); }
    }


    #endregion

    #region Commands
    public ICommand CancelCommand
    {
      get
      {
        if (cancelCommand == null)
        {
          ExecRemoveAllCommand();
          cancelCommand =
            ninjectFactory.NavigationViewModel().BackToStartViewCommand;
        }
        return cancelCommand;
      }
    }

    public ICommand AddFilesCommand
    {
      get
      {
        if (addFilesCommand == null)
        {
          addFilesCommand = new DelegateCommand(
            p => ExecFileDialogCommand());
        }
        return addFilesCommand;
      }
    }

    public ICommand RemoveSelectedCommand
    {
      get
      {
        if (removeSelectedCommand == null)
        {
          removeSelectedCommand = new DelegateCommand(
            p => CanRemoveSeledctedCommand(),
            p => ExecRemoveSelectedCommand());
        }
        return removeSelectedCommand;
      }
    }

    public ICommand RemoveAllCommand
    {
      get
      {
        if (removeAllCommand == null)
        {
          removeAllCommand = new DelegateCommand(
            p => CanRemoveAllCommand(),
            p => ExecRemoveAllCommand());
        }
        return removeAllCommand;
      }
    }

    public ICommand SaveCommand
    {
      get
      {
        if (saveCommand == null)
        {
          saveCommand = new DelegateCommand(
            p => ExecCanSaveCommand(),
            p => ExecSaveCommand());
        }
        return saveCommand;
      }
    }

    public ICommand SelectionChangedCommand
    {
      get
      {
        if (selectionChangedCommand == null)
        {
          selectionChangedCommand = new DelegateCommand
            (param => ExecSelectionChangedCommand());
        }
        return selectionChangedCommand;
      }
    }

    public ICommand AddFolderCommand
    {
      get
      {
        if (addFolderCommand == null)
        {
          addFolderCommand = new DelegateCommand(
            param => ExecAddFolderCommand());
        }
        return addFolderCommand;
      }

    }


    public ICommand MenuRemoveCommand
    {
      get
      {
        if (menuRemoveCommand == null)
        {
          menuRemoveCommand = new DelegateCommand(
            param => ExecMenuRemoveCommand());
        }
        return menuRemoveCommand;
      }
    }

    public ICommand MenuEditCommand
    {
      get
      {
        if (menuEditCommand == null)
        {
          menuEditCommand = new DelegateCommand(
            param => ExecMenuEditCommand());
        }
        return menuEditCommand;
      }
    }



    #endregion

    public SaveComponentViewModel(INinjectFactory ninjectFactory)
    {
      this.ninjectFactory = ninjectFactory;
      SetFileDataTypes();

      componentDataValidator = new ValidateComponentDataHelper();
      SelectedData = new ObservableCollection<List<Item>>();
      SelectedData.CollectionChanged += CollectionChangedMethod;

    }

    private void SetFileDataTypes()
    {
      var fileDataTypes = new List<FileType>();

      foreach (var type in Enum.GetValues(typeof(FileType)).Cast<FileType>())
      {
        if (type == FileType.NothingSelected)
        {
          continue;
        }
        fileDataTypes.Add(type);
      }

      FileDataTypes = fileDataTypes;
    }

    private void CollectionChangedMethod(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          ValidateComponentData();
          ExpandSelectedFiles = true;
          break;
        case NotifyCollectionChangedAction.Remove:
          ValidateComponentData();
          if (SelectedData.Count == 0)
            ExpandSelectedFiles = false;
          break;
        case NotifyCollectionChangedAction.Reset:
          if (!validForSave)
          {
            ValidateComponentData();
          }
          ExpandSelectedFiles = false;

          break;
      }
    }


    #region Command Methods


    private void ExecFileDialogCommand()
    {
      var selectedFiles = GUIHelpers.ShowSelectFilesDialog(true);
      if (selectedFiles != null && selectedFiles.Length > 0)
      {
        AddFilesToSelectedData(selectedFiles);
      }
    }

    private bool CanRemoveSeledctedCommand()
    {
      return SelectedDataGridRow != -1 && SelectedData.Count > 0;
    }

    private void ExecRemoveSelectedCommand()
    {
      if (SelectedData.Count > 0 && SelectedDataGridRow < SelectedData.Count)
      {
        SelectedData.RemoveAt(selectedDataGridRow);
      }
    }

    private bool CanRemoveAllCommand()
    {
      return SelectedData != null && SelectedData.Count > 0;
    }

    private void ExecRemoveAllCommand()
    {
      SelectedData?.Clear();
    }

    private bool ExecCanSaveCommand()
    {
      return validForSave;
    }

    private void ExecSaveCommand()
    {
      if (ValidXmlFiles())
      {
        if (VerifyCommitBeforeSave())
        {
          SortCommits();
          ExecuteCommitsToSaveComponent();
          SaveComponentDataInformation(new List<string> { "Component successfully saved!" }, false);
          SelectedData.Clear();
        }
        else
        {
          var errorMessages = new List<string>();
          for (int i = 0; i < verificationErrors.Count; i++)
          {
            var errors = verificationErrors[i];
            for (int j = 0; j < errors.Count; j++)
            {
              var error = errors[j];
              errorMessages.Add(error.GetErrorMessage());
            }
          }

          if (!errorMessages.IsNullOrEmpty())
            SaveComponentDataInformation(errorMessages, true);
        }
      }
    }


    private void ExecSelectionChangedCommand()
    {
      ValidateComponentData();
    }

    private void ExecAddFolderCommand()
    {
      var dirPath = GUIHelpers.ShowSelectDirectoryDialog();
      if (!dirPath.IsNullOrEmptyOrWhitespace())
      {
        var treeItem = GUIHelpers.GetFolderTreeViewItemFromPath(dirPath);
        SelectedData.Add(treeItem);
      }
    }

    private void ExecMenuRemoveCommand()
    {
      SelectedData.Remove(RightClickSelectedItem);
    }

    private void ExecMenuEditCommand()
    {
      if (RightClickSelectedItem != null && RightClickSelectedItem.Count > 0)
      {
        var rootItem = RightClickSelectedItem[0];
        if (rootItem is FileItem)
        {
          var folderPath = Path.GetDirectoryName(rootItem.FilePath);
          var fileSelections = GUIHelpers.ShowSelectFilesDialog(false, folderPath);
          if (fileSelections != null && fileSelections.Length > 0)
          {
            var oldFilePath = RightClickSelectedItem[0].FilePath;
            GUIHelpers.InterchangeFileItem(fileSelections[0], oldFilePath, SelectedData);
          }
        }
        else if (rootItem is DirectoryItem)
        {
          var folderPath = Path.GetDirectoryName(rootItem.FilePath);
          var folderSelection = GUIHelpers.ShowSelectDirectoryDialog(folderPath);
          if (folderSelection != null && !folderSelection.IsNullOrEmptyOrWhitespace())
          {
            var oldDirPath = RightClickSelectedItem[0].FilePath;
            GUIHelpers.InterchangeDirectoryItem(folderSelection, oldDirPath, SelectedData);
          }
        }
      }
    }


    #endregion

    private void SaveComponentDataInformation(List<string> messages, bool isNegative)
    {
      SetOutputMessage(messages, isNegative);
      ExpandSaveInformation = ResultMessages != null && ResultMessages.Count > 0;
      validForSave = !isNegative;
    }

    private void ValidateComponentData()
    {
      componentDataValidator.Validate(SelectedData);
      SetOutputMessage(componentDataValidator.ErrorMessages, true);
      ExpandSaveInformation = ResultMessages != null && ResultMessages.Count > 0;
      validForSave = !ExpandSaveInformation;
    }

    private void SetOutputMessage(List<string> messages, bool isNegative)
    {
      if (messages != null)
      {
        var outputMessages = new List<KeyValuePair<string, bool>>();
        for (var i = 0; i < messages.Count; i++)
        {
          outputMessages.Add(new KeyValuePair<string, bool>(messages[i], isNegative));
        }

        ResultMessages = outputMessages;
      }
      else
      {
        ResultMessages = null;
      }
    }

    private bool ValidXmlFiles()
    {
      var xmlValidationResults = ValidateGivenXmlFiles();
      if (!xmlValidationResults.IsNullOrEmpty())
      {
        var validationErrors = new List<string>();
        for (int i = 0; i < xmlValidationResults.Count; i++)
        {
          var validationResult = xmlValidationResults[i];
          validationErrors.AddRange(validationResult.Errors);
        }

        if (!validationErrors.IsNullOrEmpty())
          SaveComponentDataInformation(validationErrors, true);

        return false;
      }

      return true;
    }


    private void AddFilesToSelectedData(string[] filePaths)
    {
      for (int i = 0; i < filePaths.Length; i++)
      {
        var filePath = filePaths[i];
        AddFilesToSelectedData(filePath);
      }
    }

    private void AddFilesToSelectedData(string filePath)
    {
      if (SelectedData.Any(p => p[0].FilePath == filePath))
      {
        return;
      }

      var newSelectedFile = GUIHelpers.GetGUIFileItem(filePath);
      SelectedData.Add(newSelectedFile);
    }


    private List<XmlValidatorResult> ValidateGivenXmlFiles()
    {
      var xmlValidationResults = new List<XmlValidatorResult>();

      foreach (var file in SelectedData)
      {
        XmlValidatorResult result = null;

        switch (file[0].FileType)
        {
          case FileType.ComponentData:
            result = ValidateXmlFile(file[0] as FileItem);
            break;
          case FileType.StandardValues:
            result = ValidateXmlFile(file[0] as FileItem);
            break;
          default:
            continue;
        }

        if (result != null)
        {
          xmlValidationResults.Add(result);
        }
      }
      return xmlValidationResults;
    }

    private XmlValidatorResult ValidateXmlFile(FileItem file)
    {
      var xsdPath = Path.Combine(Helpers.XSDFolderPath, XSD_FILENAME);
      var xmlValidator = ninjectFactory.XmlValidator(xsdPath, XSD_NAMESPACE);

      if (!xmlValidator.ValidateXml(file.FilePath))
      {
        return xmlValidator.XmlValidationResult;
      }
      return null;
    }


    private bool VerifyCommitBeforeSave()
    {
      var compDataManagement = ninjectFactory.ComponentDataManagement();
      var componentToSave = new ComponentToSave(SelectedData);
      var verifyCommit = new ComponentCommitVerification(compDataManagement);
      verifyCommit.VerifyCommitsBeforeSave(componentToSave);

      commitsToExecute = verifyCommit.commitsToExecute;
      verificationErrors = verifyCommit.verificationErrors;

      return !(verificationErrors.Count > 0);
    }

    private void ExecuteCommitsToSaveComponent()
    {
      for (int i = 0; i < commitsToExecute.Count; i++)
      {
        var commit = commitsToExecute[i];
        commit.ExecuteCommit();
      }
    }

    private void SortCommits()
    {
      StandardDataCommit stdDataCommit = null;
      ComponentDataCommit compDataCommit = null;
      CertificateDataCommit certDataCommit = null;
      var remainingCommits = new List<ComponentCoreCommit>();

      for (int i = 0; i < commitsToExecute.Count; i++)
      {
        var commit = commitsToExecute[i];
        if (commit is StandardDataCommit)
        {
          stdDataCommit = commit as StandardDataCommit;
        }
        else if (commit is ComponentDataCommit)
        {
          compDataCommit = commit as ComponentDataCommit;
        }
        else if (commit is CertificateDataCommit)
        {
          certDataCommit = commit as CertificateDataCommit;
        }
        else
        {
          remainingCommits.Add(commit);
        }
      }

      commitsToExecute.Clear();
      if (stdDataCommit != null)
        commitsToExecute.Add(stdDataCommit);
      if (compDataCommit != null)
        commitsToExecute.Add(compDataCommit);
      if (certDataCommit != null)
        commitsToExecute.Add(certDataCommit);
      if (remainingCommits.Count > 0)
        commitsToExecute.AddRange(remainingCommits);
    }

  }
}
