﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel {
	public class VectoSimulationProgressViewModel :ViewModelBase {
		private int _progress;
		private string _progressMsg;
		private ICommand _closeCommand;

		public VectoSimulationProgressViewModel()
		{
			Output = new ObservableCollection<string>();
		}

		public void Report(string message, string link)
		{
			Output.Add(message);
		}

		public void Message(string message)
		{
			Output.Add(message);
		}

		public void Error(string message)
		{
			Output.Add(message);
		}

		public ObservableCollection<string> Output { get; set; }

		public int Progress
		{
			get { return _progress; }
			set { SetProperty(ref _progress, value); }
		}

		public string ProgressMsg
		{
			get { return _progressMsg; }
			set { SetProperty(ref _progressMsg, value); }
		}

		public ICommand CloseCommand
		{
			get { return _closeCommand ?? (_closeCommand = new DelegateCommand(x => !SimulationActive(), x => VectoSimulationViewModel.CancelSearchCommand.Execute(x))); }
		}

		public VectoSimulationViewModel VectoSimulationViewModel { get; set; }
		public Func<bool> SimulationActive { get; set; }
	}
}