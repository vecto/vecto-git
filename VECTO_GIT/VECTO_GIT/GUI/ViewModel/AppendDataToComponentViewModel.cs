﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;


namespace VECTO_GIT.GUI.ViewModel
{
  public class AppendDataToComponentViewModel : ViewModelBase
  {
    #region Members

    private readonly SearchResultModel selectedComponent;
    private readonly INinjectFactory ninjectFactory;
    private readonly SearchComponentViewModel searchViewModel;
    private readonly ValidateComponentDataHelper componentDataValidator;


    private List<Item> selectedData;
    private bool expandAppendData;
    private bool stdValueToAppend;

    private ICommand saveCommand;
    private ICommand cancelCommand;
    private ICommand backToSearchCommand;
    private ICommand addFileCommand;
    private ICommand addFolderCommand;
    private ICommand selectionChangedCommand;
    private ICommand removeSelectedCommand;
    private ICommand removeAllCommand;
    private ICommand menuRemoveCommand;
    private ICommand menuEditCommand;

    private VectoComponent locatedVectoComponent;

    private List<ComponentCoreCommit> commitsToExecute;
    private List<List<CommitVerificationError>> verificationErrors;
    private List<Item> rightClickSelectedItem;

    private object componentDataContentViewer;

    private List<KeyValuePair<string, bool>> resultMessages;
    private bool expandAppendInfo;

    private List<FileType> usedFileTypes;
    private ComponentToSave currentComponentToSave;
    #endregion

    #region Properties

    public ObservableCollection<List<Item>> SelectedData { get; set; }
    public IEnumerable<FileType> SelectableDataTypes { get; set; }

    public VectoComponent LocatedVectoComponent
    {
      get { return locatedVectoComponent; }
      set { SetProperty(ref locatedVectoComponent, value); }
    }

    public bool ExpandAppendData
    {
      get { return expandAppendData; }
      set { SetProperty(ref expandAppendData, value); }
    }

    public List<Item> RightClickSelectedItem
    {
      get { return rightClickSelectedItem; }
      set { SetProperty(ref rightClickSelectedItem, value); }
    }

    public object ComponentDataContentViewer
    {
      get { return componentDataContentViewer; }
      set { SetProperty(ref componentDataContentViewer, value); }
    }

    public List<KeyValuePair<string, bool>> ResultMessages
    {
      get { return resultMessages; }
      set { SetProperty(ref resultMessages, value); }
    }

    public bool ExpandAppendInfo
    {
      get { return expandAppendInfo; }
      set { SetProperty(ref expandAppendInfo, value); }
    }

    #endregion

    #region Commands

    public ICommand BackToSearchCommand
    {
      get
      {
        if (backToSearchCommand == null)
        {
          backToSearchCommand = new DelegateCommand(
            param => ExecBackToSearchCommand());
        }
        return backToSearchCommand;
      }
    }

    public ICommand CancelCommand
    {
      get
      {
        if (cancelCommand == null)
        {
          cancelCommand =
            ninjectFactory.NavigationViewModel().BackToStartViewCommand;
        }
        return cancelCommand;
      }
    }

    public ICommand SaveCommand
    {
      get
      {
        if (saveCommand == null)
        {
          saveCommand = new DelegateCommand(param => ExecSaveCommand());
        }
        return saveCommand;
      }
    }

    public ICommand AddFileCommand
    {
      get
      {
        if (addFileCommand == null)
        {
          addFileCommand = new DelegateCommand(
            p => ExecFileDialogCommand());
        }
        return addFileCommand;
      }
    }

    public ICommand AddFolderCommand
    {
      get
      {
        if (addFolderCommand == null)
        {
          addFolderCommand = new DelegateCommand(
            param => ExecAddFolderCommand());
        }
        return addFolderCommand;
      }

    }



    public ICommand RemoveSelectedCommand
    {
      get
      {
        if (removeSelectedCommand == null)
        {
          removeSelectedCommand = new DelegateCommand(
            p => CanRemoveSelectedCommand(),
            p => ExecRemoveSelectedCommand());
        }
        return removeSelectedCommand;
      }
    }

    public ICommand RemoveAllCommand
    {
      get
      {
        if (removeAllCommand == null)
        {
          removeAllCommand = new DelegateCommand(
            p => CanRemoveAllCommand(),
            p => ExecRemoveAllCommand());
        }
        return removeAllCommand;
      }
    }

    public ICommand SelectionChangedCommand
    {
      get
      {
        if (selectionChangedCommand == null)
        {
          selectionChangedCommand = new DelegateCommand(p => ExecSelectionChangedCommand());
        }
        return selectionChangedCommand;
      }
    }

    public ICommand MenuRemoveCommand
    {
      get
      {
        if (menuRemoveCommand == null)
        {
          menuRemoveCommand = new DelegateCommand(
            param => ExecMenuRemoveCommand());
        }
        return menuRemoveCommand;
      }
    }

    public ICommand MenuEditCommand
    {
      get
      {
        if (menuEditCommand == null)
        {
          menuEditCommand = new DelegateCommand(
            param => ExecMenuEditCommand());
        }
        return menuEditCommand;
      }
    }


    #endregion

    public AppendDataToComponentViewModel(INinjectFactory ninjectFactory, SearchResultModel selectedComponent,
      SearchComponentViewModel searchViewModel)
    {
      this.ninjectFactory = ninjectFactory;
      this.selectedComponent = selectedComponent;
      this.searchViewModel = searchViewModel;

      ExpandAppendData = false;

      SelectedData = new ObservableCollection<List<Item>>();
      LocatedVectoComponent = selectedComponent.VectoComponent;

      SetFileDataTypeSelection();
      componentDataValidator = new ValidateComponentDataHelper(usedFileTypes);

      SetComponentContentViewer();
    }

    private void SetComponentContentViewer()
    {
      ComponentDataContentViewer = new ComponentDataContentViewModel(ninjectFactory, selectedComponent);
    }

    private void SetFileDataTypeSelection()
    {
      var fileDataTypes = new List<FileType>();
      usedFileTypes = new List<FileType>();
      var component = selectedComponent.VectoComponent;

      foreach (var type in Enum.GetValues(typeof(FileType)).Cast<FileType>())
      {
        switch (type)
        {
          case FileType.NothingSelected:
            break;
          case FileType.StandardValues:
            if (component.StandardValues == null)
            {
              fileDataTypes.Add(type);
            }
            else
            {
              usedFileTypes.Add(type);
            }
            break;
          case FileType.ComponentData:
            if (component.ComponentData == null)
            {
              fileDataTypes.Add(type);
            }
            else
            {
              usedFileTypes.Add(type);
            }
            break;
          case FileType.CertificateData:
            if (component.CertificateFile == null)
            {
              fileDataTypes.Add(type);
            }
            else
            {
              usedFileTypes.Add(type);
            }
            break;
          case FileType.MeasurementData:
            fileDataTypes.Add(type);
            break;
          case FileType.UserData:
            fileDataTypes.Add(type);
            break;
        }
      }

      SelectableDataTypes = fileDataTypes;
    }


    private void ExecBackToSearchCommand()
    {
      ninjectFactory.NavigationViewModel().CurrentViewModel = searchViewModel;

    }

    private void ExecSaveCommand()
    {
      if (VerifyCommitBeforeSave())
      {
        ExecuteCommits();
      }
      else
      {
        SetFailureAppendMessage();
      }
    }

    private bool VerifyCommitBeforeSave()
    {
      var compDataManagement = ninjectFactory.ComponentDataManagement();
      var verifyCommit = new ComponentCommitVerification(compDataManagement);

      currentComponentToSave = new ComponentToSave(SelectedData, LocatedVectoComponent);
      CheckForStdValueToAdd(currentComponentToSave);
      
      verifyCommit.VerifyCommitsBeforeSave(currentComponentToSave);
      commitsToExecute = verifyCommit.commitsToExecute;
      verificationErrors = verifyCommit.verificationErrors;

      return !(verificationErrors.Count > 0);
    }


    private void CheckForStdValueToAdd(ComponentToSave componentToSave)
    {
      stdValueToAppend = false;
      if (componentToSave.ComponentDataBranch != null && componentToSave.StandardValuesFilePath != null)
      {
        stdValueToAppend = true;
      }
    }


    //ToDo Any Rollback handling ?!?
    private void ExecuteCommits()
    {
      if (stdValueToAppend)
      {
        AppendStdValueToCompData();
      }

      for (var i = 0; i < commitsToExecute.Count; i++)
      {
        var commit = commitsToExecute[i];
        var branch = commit.ExecuteCommit();
        var type = commit.GetType();
        UpdateBrancheAfterCommit(type, branch);
      }

      UpdateComponentDetailsView();
      SetFileDataTypeSelection();
      SetSuccessfullAppendMessage();
    }

    private void AppendStdValueToCompData()
    {
      StandardDataCommit stdCommit = null;
      var stdIndex = 0;
      for (int i = 0; i < commitsToExecute.Count; i++)
      {
        if (commitsToExecute[i] is StandardDataCommit)
        {
          stdIndex = i;
          stdCommit = (StandardDataCommit) commitsToExecute[i];
          break;
        }
      }

      if (stdCommit != null)
      {
        SetStandardValueCommit(stdCommit, stdIndex);
      }
    }

    private void SetStandardValueCommit(StandardDataCommit stdValueCommit, int stdIndex)
    {
      var stdBranch = stdValueCommit.ExecuteCommit();
      var repository = ninjectFactory.ComponentDataManagement().CurrentRepository;

      var compBlob = ComponentDataCommit.GetComponentDataBlob(currentComponentToSave.ComponentDataBranch);
      var certBlob = CertificateDataCommit.GetCertificateBlob(currentComponentToSave.ComponentDataBranch);
      repository.Branches.Remove(currentComponentToSave.ComponentDataBranch);

      var compCommit = new ComponentDataCommit(repository, stdBranch, compBlob);
      compCommit.VerificationCheckBeforeCommit();

      if (certBlob == null)
      {
        commitsToExecute[stdIndex] = compCommit;
      }
      else
      {
        var compBranch = compCommit.ExecuteCommit();
        commitsToExecute[stdIndex] = GetCertificateCommit(repository, compBranch, certBlob, compBlob);
      }
    }


    private ComponentCoreCommit GetCertificateCommit(Repository repository, Branch compBranch, Blob certificateBlob, Blob compBlob)
    {
      var certCommit = new CertificateDataCommit(repository,  compBranch, certificateBlob, compBlob);
      certCommit.VerificationCheckBeforeCommit();
      return certCommit;
    }


    private void UpdateComponentDetailsView()
    {
      var repositoryPath = ninjectFactory.ComponentDataManagement().VirtualDrive.VirtualPath;

      var transferComponent = GetRelatedTransferComponent(repositoryPath);

      UpdateTransferComponent(transferComponent, repositoryPath);

      selectedComponent.VectoComponent = LocatedVectoComponent;
      ComponentDataContentViewer = new ComponentDataContentViewModel(ninjectFactory, selectedComponent);
    }

    private ComponentForTransfer GetRelatedTransferComponent(string repositoryPath)
    {
      var xmlHandler = ninjectFactory.TransferXmlHandler();

      xmlHandler.UpdateTransferList(repositoryPath);

      return xmlHandler.GetRelatedComponentForTransfer(repositoryPath, selectedComponent.Manufacturer,
          selectedComponent.Model);
    }

    private void UpdateTransferComponent(ComponentForTransfer componentForTransfer, string repositoryPath)
    {
      if (componentForTransfer != null)
      {
        var comp = new ComponentForTransfer();
        var transferPub = componentForTransfer.PublicBranch?.TransferBranch ?? false;
        var transferStd = componentForTransfer.StandardBranch?.TransferBranch ?? false;
        var transferPrv = componentForTransfer.PrivatBranch?.TransferBranch ?? false;

        comp.SetComponentForTransferContent(LocatedVectoComponent, transferPub, transferPrv, transferStd);

        var xmlHandler = ninjectFactory.TransferXmlHandler();

        xmlHandler.DeletTransferComponent(componentForTransfer, repositoryPath);
        xmlHandler.SerializeTransferComponent(comp, repositoryPath);
      }
    }

    private void UpdateBrancheAfterCommit(Type type, Branch branch)
    {
      if (type == typeof(CertificateDataCommit) || type == typeof(ComponentDataCommit) || type == typeof(StandardDataCommit))
      {
        LocatedVectoComponent.ReloadDataFromPublicBranch(branch);
        UpdateComponentDetails();
      }
      else if (type == typeof(MeasurementDataCommit) || type == typeof(UserDataCommit))
      {
        LocatedVectoComponent.ReloadDataFromPrivateBranch(branch);
      }
      OnPropertyChanged("LocatedVectoComponent");
    }

    private void UpdateComponentDetails()
    {
      selectedComponent.Manufacturer = LocatedVectoComponent.PublicBranchNaming.Manufacturer;
      selectedComponent.Model = LocatedVectoComponent.PublicBranchNaming.Model;
      selectedComponent.CertificateDate = LocatedVectoComponent.CertificateDate;
      selectedComponent.CertificateNumber = LocatedVectoComponent.CertificateNumber;
      selectedComponent.GitId = LocatedVectoComponent.GetPublicGitId;
    }

    private void SetSuccessfullAppendMessage()
    {
      var message = new List<string> { "The selected data was successfully appended to the branch!" };
      AppendComponentDataInformation(message, false);

      ExecRemoveAllCommand();
    }

    private void SetFailureAppendMessage()
    {

      if (verificationErrors == null)
        return;

      var errorMessages = new List<string>();
      for (int i = 0; i < verificationErrors.Count; i++)
      {
        var errors = verificationErrors[i];
        for (int j = 0; j < errors.Count; j++)
        {
          var error = errors[j];
          if (error != null)
            errorMessages.Add(error.GetErrorMessage());
        }
      }

      if (!errorMessages.IsNullOrEmpty())
        AppendComponentDataInformation(errorMessages, true);
    }

    #region Command Methods

    private void ExecFileDialogCommand()
    {
      var selectedFiles = GUIHelpers.ShowSelectFilesDialog(true);
      if (selectedFiles != null && selectedFiles.Length > 0)
      {
        AddFilesToSelectedData(selectedFiles);
      }

    }

    private void ExecAddFolderCommand()
    {
      var dirPath = GUIHelpers.ShowSelectDirectoryDialog();
      if (!dirPath.IsNullOrEmptyOrWhitespace())
      {
        var treeItem = GUIHelpers.GetFolderTreeViewItemFromPath(dirPath);
        SelectedData.Add(treeItem);
        ExpandAppendData = true;

        ValidateComponentData();
      }
    }

    private void ExecSelectionChangedCommand()
    {
      ValidateComponentData();
    }

    private bool CanRemoveSelectedCommand()
    {
      return selectedData != null && SelectedData.Count > 0;
    }

    private void ExecRemoveSelectedCommand()
    {
      if (SelectedData.Count > 0 && selectedData != null)
      {
        SelectedData.Remove(selectedData);
      }
    }

    private bool CanRemoveAllCommand()
    {
      return selectedData != null && SelectedData.Count > 0;
    }

    private void ExecRemoveAllCommand()
    {
      if (SelectedData != null)
      {
        SelectedData.Clear();
      }
    }

    private void ExecMenuRemoveCommand()
    {
      SelectedData.Remove(RightClickSelectedItem);
    }

    private void ExecMenuEditCommand()
    {
      if (RightClickSelectedItem != null && RightClickSelectedItem.Count > 0)
      {
        var rootItem = RightClickSelectedItem[0];
        if (rootItem is FileItem)
        {
          var folderPath = Path.GetDirectoryName(rootItem.FilePath);
          var fileSelections = GUIHelpers.ShowSelectFilesDialog(false, folderPath);
          if (fileSelections != null && fileSelections.Length > 0)
          {
            var oldFilePath = RightClickSelectedItem[0].FilePath;
            GUIHelpers.InterchangeFileItem(fileSelections[0], oldFilePath, SelectedData);
          }
        }
        else if (rootItem is DirectoryItem)
        {
          var folderPath = Path.GetDirectoryName(rootItem.FilePath);
          var folderSelection = GUIHelpers.ShowSelectDirectoryDialog(folderPath);
          if (folderSelection != null && !folderSelection.IsNullOrEmptyOrWhitespace())
          {
            var oldDirPath = RightClickSelectedItem[0].FilePath;
            GUIHelpers.InterchangeDirectoryItem(folderSelection, oldDirPath, SelectedData);
          }
        }
      }
    }



    #endregion

    private void AddFilesToSelectedData(string[] filePaths)
    {
      for (int i = 0; i < filePaths.Length; i++)
      {
        var filePath = filePaths[i];
        AddFilesToSelectedData(filePath, false);
      }

      ExpandAppendData = true;
      ValidateComponentData();
    }

    private void AddFilesToSelectedData(string filePath, bool singleFile = true)
    {
      if (SelectedData.Any(p => p[0].FilePath == filePath))
      {
        return;
      }

      var newSelectedFile = GUIHelpers.GetGUIFileItem(filePath);
      SelectedData.Add(newSelectedFile);

      if (singleFile)
      {
        ExpandAppendData = true;
        ValidateComponentData();
      }
    }


    private void AppendComponentDataInformation(List<string> messages, bool isNegative)
    {
      SetOutputMessage(messages, isNegative);
      ExpandAppendInfo = ResultMessages != null && ResultMessages.Count > 0;
    }

    private void SetOutputMessage(List<string> messages, bool isNegative)
    {
      if (messages != null)
      {
        var outputMessages = new List<KeyValuePair<string, bool>>();
        for (int i = 0; i < messages.Count; i++)
        {
          outputMessages.Add(new KeyValuePair<string, bool>(messages[i], isNegative));
        }

        ResultMessages = outputMessages;
      }
      else
      {
        ResultMessages = null;
      }
    }



    private void ValidateComponentData()
    {
      componentDataValidator.Validate(SelectedData);
      var isNegative = componentDataValidator.ErrorMessages?.Count > 0;
      AppendComponentDataInformation(componentDataValidator.ErrorMessages, isNegative);
    }

  }
}
