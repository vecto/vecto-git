﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using TUGraz.VectoCommon.InputData;
using TUGraz.VectoCommon.Models;
using TUGraz.VectoCommon.Utils;
using TUGraz.VectoCore.InputData.Impl;
using TUGraz.VectoCore.Models.Declaration;
using TUGraz.VectoCore.Models.Simulation.Impl;
using TUGraz.VectoCore.OutputData;
using TUGraz.VectoCore.OutputData.FileIO;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.VECTO.FileIO;

namespace VECTO_GIT.GUI.ViewModel
{
	public class VectoSimulationViewModel : ViewModelBase
	{
		private object _browseViewModel;
		private string _engineComponent;
		private ICommand _browseEngine;
		private INinjectFactory _ninjectFactory;
		private ICommand _browseTransmission;
		private string _transmissionComponent;
		private ICommand _cancelSearchCommand;
		private bool _hasAngledrive;
		private string _angledriveComponent;
		private ICommand _browseAngledrive;
		private bool _hasTorqueConverter;
		private string _torqueConverterComponent;
		private ICommand _browseTorqueConverter;
		private string _axlegearComponent;
		private ICommand _browseAxlegear;
		private string _airdragComponent;
		private ICommand _browseAirdrag;
		private bool _hasRetarder;
		private string _retarderComponent;
		private ICommand _browseRetarder;
		private string _manufacturer;
		private string _manufacturerAddress;
		private string _vin;
		private string _legislativeClassList;
		private VehicleCategory _vehicleCategory;
		private double _curbMassChassis;
		private double _tecnicalPermissibleMaximumLadenMass;
		private double _idlingSpeed;
		private LegislativeClass _legislativeClass;
		private AxleConfiguration _axleConfiguration;
		private RetarderType _retarderType;
		private AngledriveType _angledriveType;
		private string _ptoShaftGearWheels;
		private string _ptoOtherElements;
		private bool _engineStopStart;
		private bool _ecoRollWithoutEngineStop;
		private bool _ecoRollWithEngineStop;
		private PredictiveCruiseControlType _pccOption;
		private string _auxFanTechnology;
		private string _auxSteeringPumpTechnology;
		private string _auxElectricSysteTechnology;
		private string _auxPneumaticSystemTechnology;
		private string _auxHvacTechnology;
		private ICommand _runVectoJob;
		private ICommand _loadGitJob;
		private ICommand _saveGitJob;
		private string _model;
		private double _retarderRatio;
		private bool _sleeperCab;
		private bool _vocational;
		private BackgroundWorker VectoWorker;

		public enum Components
		{
			Engine = 1,
			Transmission,
			TorqueConverter,
			Axlegear,
			Angledrive,
			Retarder,
			Wheels,
			AirDrag,
		}

		public VectoSimulationViewModel(INinjectFactory ninjectFactory)
		{
			_ninjectFactory = ninjectFactory;

			AxleconfigurationList = new ObservableCollection<Tuple<AxleConfiguration, string>>();
			VehicleCategoryList = new ObservableCollection<Tuple<VehicleCategory, string>>();
			LegislativeClassList = new ObservableCollection<Tuple<LegislativeClass, string>>();
			AngledriveTypeList = new ObservableCollection<Tuple<AngledriveType, string>>();
			RetarderTypeList = new ObservableCollection<Tuple<RetarderType, string>>();
			PTOShaftGearWheelsList = new ObservableCollection<Tuple<string, string>>();
			PTOOtherElementsList = new ObservableCollection<Tuple<string, string>>();
			AuxFanTechnologyList = new ObservableCollection<Tuple<string, string>>();
			AuxSteeringPumpTechnologyList = new ObservableCollection<Tuple<string, string>>();
			AuxElectricSysteTechnologyList = new ObservableCollection<Tuple<string, string>>();
			AuxPneumatciSystemTechnologyList = new ObservableCollection<Tuple<string, string>>();
			AuxHVACTechnologyList = new ObservableCollection<Tuple<string, string>>();
			PCCList = new ObservableCollection<Tuple<PredictiveCruiseControlType, string>>();

			Axles = new ObservableCollection<AxleWheelsViewModel>();
			Axles.Add(new AxleWheelsViewModel(Axles.Count) { SimulationViewModel = this });
			Axles.Add(new AxleWheelsViewModel(Axles.Count) { SimulationViewModel = this });

			InitializeLists();

			BrowseViewModel = null;

			VectoWorker = new BackgroundWorker();
			VectoWorker.DoWork += RunSimulation;
			VectoWorker.ProgressChanged += ProgressChanged;
			VectoWorker.RunWorkerCompleted += SimulationCompleted;

			VectoWorker.WorkerReportsProgress = true;
			VectoWorker.WorkerSupportsCancellation = true;
		}


		private void InitializeLists()
		{
			foreach (var axleConfiguration in DeclarationData.Segments.GetAxleConfigurations()) {
				AxleconfigurationList.Add(Tuple.Create(axleConfiguration, axleConfiguration.GetName()));
			}

			foreach (var value in EnumHelper.GetValues<LegislativeClass>()) {
				LegislativeClassList.Add(Tuple.Create(value, value.GetLabel()));
			}

			foreach (var vehicleCategory in DeclarationData.Segments.GetVehicleCategories()) {
				VehicleCategoryList.Add(Tuple.Create(vehicleCategory, vehicleCategory.GetLabel()));
			}

			foreach (var angledriveType in EnumHelper.GetValues<AngledriveType>()) {
				AngledriveTypeList.Add(Tuple.Create(angledriveType, angledriveType.GetLabel()));
			}

			foreach (var retarderType in EnumHelper.GetValues<RetarderType>()) {
				RetarderTypeList.Add(Tuple.Create(retarderType, retarderType.ToXMLFormat()));
			}

			foreach (var technology in DeclarationData.Fan.GetTechnologies()) {
				AuxFanTechnologyList.Add(Tuple.Create(technology, technology));
			}

			foreach (var technology in DeclarationData.SteeringPump.GetTechnologies()) {
				AuxSteeringPumpTechnologyList.Add(Tuple.Create(technology, technology));
			}

			foreach (var technology in DeclarationData.ElectricSystem.GetTechnologies()) {
				AuxElectricSysteTechnologyList.Add(Tuple.Create(technology, technology));
			}

			foreach (var technology in DeclarationData.PneumaticSystem.GetTechnologies()) {
				AuxPneumatciSystemTechnologyList.Add(Tuple.Create(technology, technology));
			}

			foreach (var technology in DeclarationData.HeatingVentilationAirConditioning.GetTechnologies()) {
				AuxHVACTechnologyList.Add(Tuple.Create(technology, technology));
			}

			foreach (var technology in DeclarationData.PTOTransmission.GetTechnologies()) {
				var parts = technology.Split(new[] { '-' }, 2);
				if (parts.Length > 0 && !PTOShaftGearWheelsList.Contains(Tuple.Create(parts[0].Trim(), parts[0].Trim()))) {
					PTOShaftGearWheelsList.Add(Tuple.Create(parts[0].Trim(), parts[0].Trim()));
				}
				if (parts.Length > 1 && !PTOOtherElementsList.Contains(Tuple.Create(parts[1].Trim(), parts[1].Trim()))) {
					PTOOtherElementsList.Add(Tuple.Create(parts[1].Trim(), parts[1].Trim()));
				}
			}

			foreach (var pccType in EnumHelper.GetValues<PredictiveCruiseControlType>()) {
				PCCList.Add(Tuple.Create(pccType, pccType.ToXMLFormat()));
			}
		}


		public object BrowseViewModel
		{
			get { return _browseViewModel; }
			set { SetProperty(ref _browseViewModel, value); }
		}

		public string EngineComponent
		{
			get { return _engineComponent; }
			set { SetProperty(ref _engineComponent, value); }
		}

		public ICommand BrowseEngine
		{
			get { return _browseEngine ?? (_browseEngine = new DelegateCommand(p => ExecBrowseCommand(Components.Engine))); }
		}

		public string TransmissionComponent
		{
			get { return _transmissionComponent; }
			set { SetProperty(ref _transmissionComponent, value); }
		}

		public ICommand BrowseTransmission
		{
			get {
				return _browseTransmission ??
						(_browseTransmission = new DelegateCommand(p => ExecBrowseCommand(Components.Transmission)));
			}
		}

		public bool HasTorqueConverter
		{
			get { return _hasTorqueConverter; }
			set { SetProperty(ref _hasTorqueConverter, value); }
		}

		public string TorqueConverterComponent
		{
			get { return _torqueConverterComponent; }
			set { SetProperty(ref _torqueConverterComponent, value); }
		}

		public ICommand BrowseTorqueConverter
		{
			get {
				return _browseTorqueConverter ??
						(_browseTorqueConverter = new DelegateCommand(p => ExecBrowseCommand(Components.TorqueConverter)));
			}
		}

		public bool HasAngledrive
		{
			get { return _hasAngledrive; }
			set { SetProperty(ref _hasAngledrive, value); }
		}

		public string AngledriveComponent
		{
			get { return _angledriveComponent; }
			set { SetProperty(ref _angledriveComponent, value); }
		}

		public ICommand BrowseAngledrive
		{
			get {
				return _browseAngledrive ??
						(_browseAngledrive = new DelegateCommand(p => ExecBrowseCommand(Components.Angledrive)));
			}
		}

		public string AxlegearComponent
		{
			get { return _axlegearComponent; }
			set { SetProperty(ref _axlegearComponent, value); }
		}

		public ICommand BrowseAxlegear
		{
			get {
				return _browseAxlegear ?? (_browseAxlegear = new DelegateCommand(p => ExecBrowseCommand(Components.Axlegear)));
			}
		}


		public string AirdragComponent
		{
			get { return _airdragComponent; }
			set { SetProperty(ref _airdragComponent, value); }
		}

		public ICommand BrowseAirdrag
		{
			get { return _browseAirdrag ?? (_browseAirdrag = new DelegateCommand(p => ExecBrowseCommand(Components.AirDrag))); }
		}


		public bool HasRetarder
		{
			get { return _hasRetarder; }
			set { SetProperty(ref _hasRetarder, value); }
		}

		public string RetarderComponent
		{
			get { return _retarderComponent; }
			set { SetProperty(ref _retarderComponent, value); }
		}

		public ICommand BrowseRetarder
		{
			get {
				return _browseRetarder ?? (_browseRetarder = new DelegateCommand(p => ExecBrowseCommand(Components.Retarder)));
			}
		}


		public ICommand CancelSearchCommand
		{
			get { return _cancelSearchCommand ?? (_cancelSearchCommand = new DelegateCommand(p => ExecCancelBrowseCommand())); }
		}

		public string Manufacturer
		{
			get { return _manufacturer; }
			set { SetProperty(ref _manufacturer, value); }
		}

		public string ManufacturerAddress
		{
			get { return _manufacturerAddress; }
			set { SetProperty(ref _manufacturerAddress, value); }
		}

		public string Model
		{
			get { return _model; }
			set { SetProperty(ref _model, value); }
		}

		public string VIN
		{
			get { return _vin; }
			set { SetProperty(ref _vin, value); }
		}

		public ObservableCollection<Tuple<LegislativeClass, string>> LegislativeClassList { get; }

		public VehicleCategory VehicleCategory
		{
			get { return _vehicleCategory; }
			set { SetProperty(ref _vehicleCategory, value); }
		}

		public ObservableCollection<Tuple<AxleConfiguration, string>> AxleconfigurationList { get; }

		public double CurbMassChassis
		{
			get { return _curbMassChassis; }
			set { SetProperty(ref _curbMassChassis, value); }
		}

		public double TecnicalPermissibleMaximumLadenMass
		{
			get { return _tecnicalPermissibleMaximumLadenMass; }
			set { SetProperty(ref _tecnicalPermissibleMaximumLadenMass, value); }
		}

		public double IdlingSpeed
		{
			get { return _idlingSpeed; }
			set { SetProperty(ref _idlingSpeed, value); }
		}

		public LegislativeClass LegislativeClass
		{
			get { return _legislativeClass; }
			set { SetProperty(ref _legislativeClass, value); }
		}

		public ObservableCollection<Tuple<VehicleCategory, string>> VehicleCategoryList { get; }

		public AxleConfiguration AxleConfiguration
		{
			get { return _axleConfiguration; }
			set {
				if (!SetProperty(ref _axleConfiguration, value)) {
					return;
				}

				while (Axles.Count < value.NumAxles()) {
					Axles.Add(new AxleWheelsViewModel(Axles.Count) { SimulationViewModel = this });
				}
				while (Axles.Count > value.NumAxles()) {
					Axles.RemoveAt(Axles.Count - 1);
				}
			}
		}

		public ObservableCollection<Tuple<RetarderType, string>> RetarderTypeList { get; set; }

		public RetarderType RetarderType
		{
			get { return _retarderType; }
			set {
				SetProperty(ref _retarderType, value);
				HasRetarder = value.IsDedicatedComponent();
			}
		}

		public ObservableCollection<Tuple<AngledriveType, string>> AngledriveTypeList { get; }

		public AngledriveType AngledriveType
		{
			get { return _angledriveType; }
			set { SetProperty(ref _angledriveType, value); }
		}

		public ObservableCollection<Tuple<string, string>> PTOShaftGearWheelsList { get; }

		public ObservableCollection<Tuple<string, string>> PTOOtherElementsList { get; }

		public string PTOShaftGearWheels
		{
			get { return _ptoShaftGearWheels; }
			set { SetProperty(ref _ptoShaftGearWheels, value); }
		}

		public string PTOOtherElements
		{
			get { return _ptoOtherElements; }
			set { SetProperty(ref _ptoOtherElements, value); }
		}

		public bool EngineStopStart
		{
			get { return _engineStopStart; }
			set { SetProperty(ref _engineStopStart, value); }
		}

		public bool EcoRollWithoutEngineStop
		{
			get { return _ecoRollWithoutEngineStop; }
			set { SetProperty(ref _ecoRollWithoutEngineStop, value); }
		}

		public bool EcoRollWithEngineStop
		{
			get { return _ecoRollWithEngineStop; }
			set { SetProperty(ref _ecoRollWithEngineStop, value); }
		}

		public ObservableCollection<Tuple<PredictiveCruiseControlType, string>> PCCList { get; }

		public PredictiveCruiseControlType PCCOption
		{
			get { return _pccOption; }
			set { SetProperty(ref _pccOption, value); }
		}

		public ObservableCollection<Tuple<string, string>> AuxFanTechnologyList { get; }

		public ObservableCollection<Tuple<string, string>> AuxSteeringPumpTechnologyList { get; }

		public ObservableCollection<Tuple<string, string>> AuxElectricSysteTechnologyList { get; }

		public ObservableCollection<Tuple<string, string>> AuxPneumatciSystemTechnologyList { get; }

		public ObservableCollection<Tuple<string, string>> AuxHVACTechnologyList { get; }

		public string AuxFanTechnology
		{
			get { return _auxFanTechnology; }
			set { SetProperty(ref _auxFanTechnology, value); }
		}

		public string AuxSteeringPumpTechnology
		{
			get { return _auxSteeringPumpTechnology; }
			set { SetProperty(ref _auxSteeringPumpTechnology, value); }
		}

		public string AuxElectricSysteTechnology
		{
			get { return _auxElectricSysteTechnology; }
			set { SetProperty(ref _auxElectricSysteTechnology, value); }
		}

		public string AuxPneumaticSystemTechnology
		{
			get { return _auxPneumaticSystemTechnology; }
			set { SetProperty(ref _auxPneumaticSystemTechnology, value); }
		}

		public string AuxHVACTechnology
		{
			get { return _auxHvacTechnology; }
			set { SetProperty(ref _auxHvacTechnology, value); }
		}


		public ICommand LoadGITJob
		{
			get { return _loadGitJob ?? (_loadGitJob = new DelegateCommand(p => ExecuteLoadJob())); }
		}

		private void ExecuteLoadJob()
		{
			var selected = GUIHelpers.ShowSelectFilesDialog(false);
			if (selected != null && selected.Length > 0) {
				var reader = new XMLDeclarationReader(selected[0]);
				reader.ReadVectoJob(this);
			}
		}

		public ICommand SaveGITJob
		{
			get { return _saveGitJob ?? (_saveGitJob = new DelegateCommand(p => ExecuteSaveJob())); }
		}

		public double RetarderRatio
		{
			get { return _retarderRatio; }
			set { SetProperty(ref _retarderRatio, value); }
		}

		public bool Vocational
		{
			get { return _vocational; }
			set { SetProperty(ref _vocational, value); }
		}

		public bool SleeperCab
		{
			get { return _sleeperCab; }
			set { SetProperty(ref _sleeperCab, value); }
		}

		public IList<IAuxiliaryDeclarationInputData> Auxiliaries
		{
			get {
				return new IAuxiliaryDeclarationInputData[] {
					new AuxiliaryDataInputData() {
						Type = AuxiliaryType.ElectricSystem,
						Technology = new[] { AuxElectricSysteTechnology }
					},
					new AuxiliaryDataInputData() { Type = AuxiliaryType.Fan, Technology = new[] { AuxFanTechnology } },
					new AuxiliaryDataInputData() {
						Type = AuxiliaryType.PneumaticSystem,
						Technology = new[] { AuxPneumaticSystemTechnology }
					},
					new AuxiliaryDataInputData() { Type = AuxiliaryType.HVAC, Technology = new[] { AuxHVACTechnology } },
					new AuxiliaryDataInputData() {
						Type = AuxiliaryType.SteeringPump,
						Technology = new[] { AuxSteeringPumpTechnology }
					},
				};
			}
		}

		public ObservableCollection<AxleWheelsViewModel> Axles { get; }


		private void ExecuteSaveJob()
		{
			var writer = new XMLDeclarationWriter();
			var xml = writer.GenerateVectoJob(this);

			GUIHelpers.SaveFileDialog(xml, "XML-File (*.xml)|*.xml");
		}


		private void ExecCancelBrowseCommand()
		{
			BrowseViewModel = null;
		}

		public void ExecBrowseCommand(Components component, int? idx = null)
		{
			if (BrowseViewModel != null) {
				BrowseViewModel = null;
			}
			BrowseViewModel = _ninjectFactory.SearchSimulationComponentViewModel(component, idx);
			var tmp = BrowseViewModel as SearchSimulationComponentViewModel;
			if (tmp != null) {
				tmp.SimulationViewModel = this;
			}
		}

		public void SetSelected(Components component, string gitId, int? idx)
		{
			switch (component) {
				case Components.Engine:
					EngineComponent = gitId;
					break;
				case Components.Transmission:
					TransmissionComponent = gitId;
					break;
				case Components.TorqueConverter:
					TorqueConverterComponent = gitId;
					break;
				case Components.Axlegear:
					AxlegearComponent = gitId;
					break;
				case Components.Angledrive:
					AngledriveComponent = gitId;
					break;
				case Components.Retarder:
					RetarderComponent = gitId;
					break;
				case Components.Wheels:
					if (idx.HasValue && idx < Axles.Count) {
						Axles[idx.Value].TyreComponent = gitId;
					}
					break;
				case Components.AirDrag:
					AirdragComponent = gitId;
					break;
				default: throw new ArgumentOutOfRangeException(nameof(component), component, null);
			}

			BrowseViewModel = null;
		}


		public ICommand RunVECTOJob
		{
			get {
				return _runVectoJob ?? (_runVectoJob = new DelegateCommand(x => !VectoWorker.IsBusy, p => ExecRunVECTOJob()));
			}
		}

		private void ExecRunVECTOJob()
		{
			if (!VectoWorker.IsBusy) {
				var tmp = _ninjectFactory.VectoSimulationProgressViewModel();
				tmp.VectoSimulationViewModel = this;
				tmp.SimulationActive = () => VectoWorker.IsBusy;
				BrowseViewModel = tmp;
				VectoWorker.RunWorkerAsync();
			}
		}


		private void RunSimulation(object theSender, DoWorkEventArgs args)
		{
			var sender = theSender as BackgroundWorker;
			if (sender == null) {
				return;
			}

			try {
				sender.ReportProgress(0, new {Type = "Message", Message = "Starting VECTO Simulation"});
				var finishedRuns = new List<int>();
				var writer = new FileOutputWriter(VIN);
				var inputdData = _ninjectFactory.VectoInputData(this);
				var factory = new SimulatorFactory(ExecutionMode.Declaration, inputdData, writer) {
					WriteModalResults = true,
					Validate = false
				};
				var sumcontainer = new SummaryDataContainer(writer);
				var jobContainer = new JobContainer(sumcontainer);
				jobContainer.AddRuns(factory);

				sender.ReportProgress(
					0, new { Type = "Message", Message = string.Format("Finished reading data for job: {0}", VIN) });

				foreach (var cycle in jobContainer.GetCycleTypes()) {
					sender.ReportProgress(
						0, new { Type = "Message", Message = string.Format("Detected cycle: {0}: {1}", cycle.Name, cycle.CycleType) });
				}

				sender.ReportProgress(
					0,
					new {
						Type = "Progress",
						Message = string.Format("Starting Simulation - {0} Runs", jobContainer.GetProgress().Count)
					});

				jobContainer.Execute(true);

				var stopWatch = Stopwatch.StartNew();
				while (!jobContainer.AllCompleted) {
					if (sender.CancellationPending) {
						jobContainer.Cancel();
						return;
					}

					var progess = jobContainer.GetProgress();
					var sumProgress = progess.Sum(x => x.Value.Progress);
					sender.ReportProgress(
						Convert.ToInt32((sumProgress * 100) / progess.Count),
						new {
							Type = "Progress",
							Message = string.Format(
								"Duration: {0:N1}s, Current Progress: {1:P} ({2})", stopWatch.ElapsedMilliseconds / 1000,
								sumProgress / progess.Count,
								string.Join(", ", progess.Select(x => string.Format("{0,4:P}", x.Value.Progress)))
							)
						}
					);

					var justFinished = progess.Where(x => x.Value.Done && !finishedRuns.Contains(x.Key))
											.ToDictionary(x => x.Key, x => x.Value);
					PrintRuns(sender, justFinished, writer);
					finishedRuns.AddRange(justFinished.Select(x => x.Key));
					Thread.Sleep(100);
				}

				var remainingRuns = jobContainer.GetProgress().Where(x => x.Value.Done && !finishedRuns.Contains(x.Key))
												.ToDictionary(x => x.Key, x => x.Value);
				PrintRuns(sender, remainingRuns, writer);

				foreach (var progressEntry in jobContainer.GetProgress()) {
					sender.ReportProgress(
						100, new {
							Type = "Message",
							Message = string.Format(
								"{0,-60} {1,8:P} {2,10:F2}s - {3}",
								string.Format(
									"{0} {1} {2}", progressEntry.Value.RunName,
									progressEntry.Value.CycleName,
									progressEntry.Value.RunSuffix),
								progressEntry.Value.Progress,
								progressEntry.Value.ExecTime / 1000.0,
								progressEntry.Value.Success ? "Success" : "Aborted")
						});
					if (!progressEntry.Value.Success) {
						sender.ReportProgress(
							100,
							new {
								Type = "Error",
								Message = progressEntry.Value.Error.Message
							});
					}
				}

				var w = writer;
				foreach (var entry in new Dictionary<string, string> {
					{ w.XMLFullReportName, "XML Manufacturer Report" },
					{ w.XMLCustomerReportName, "XML Customer Report" },
					{ w.XMLVTPReportName, "VTP Report" },
					{ w.XMLMonitoringReportName, "XML Monitoring Report" }
				}) {
					if (File.Exists(entry.Key)) {
						sender.ReportProgress(
							100, new {
								Type = "Report",
								Message = string.Format("{2} for '{0}' written to {1}", VIN, entry.Key, entry.Value),
								Link = string.Format("<XML>{0}", entry.Key)
							});
					}
				}
			} catch (Exception e) {
				sender.ReportProgress(
					100, new { Type = "Error", Message = string.Format("Failed to run simulation: {0}", e.Message) });
			}
		}

		private void PrintRuns(
			BackgroundWorker sender, Dictionary<int, JobContainer.ProgressEntry> progress, FileOutputWriter writer)
		{
		//throw new NotImplementedException();
		}

		private void SimulationCompleted(object sender, RunWorkerCompletedEventArgs e) { }

		private void ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			dynamic msg = e.UserState;
			var progress = BrowseViewModel as VectoSimulationProgressViewModel;
			if (progress == null) {
				return;
			}

			switch ((string)msg.Type) {
				case "Report":
					progress.Report(msg.Message, msg.Link);
					break;
				case "Message":
					progress.Message(msg.Message);
					break;
				case "Progress": progress.ProgressMsg = msg.Message;
					break;
				case "Error":
					progress.Error(msg.Message);
					break;
				default: progress.Error(string.Format("unkown target: {0}, message: {1}", msg.Type, msg.Message));
					break;
			}

			progress.Progress = e.ProgressPercentage;
		}
	}
}
