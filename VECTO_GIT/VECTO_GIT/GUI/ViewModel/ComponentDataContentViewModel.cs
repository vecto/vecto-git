﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.View;

namespace VECTO_GIT.GUI.ViewModel
{
  public class ComponentDataContentViewModel : ViewModelBase
  {
    #region Members

    private readonly INinjectFactory ninjectFactory;
    private readonly SearchResultModel selectedComponent;

    private string manufacturerName;
    private string modelName;
    private string certificationNumber;
    private DateTime certificationDate;
    private VectoComponent locatedVectoComponent;
    private KeyValuePair<string, Blob> selectedPublicDataItem;
    private FileItem selectedPrivateDataItem;

    private ICommand showPublicDataCommand;
    private ICommand showPrivateDataCommand;
	  private string gitIdentifier;

	  #endregion

    #region Properties

    public string ManufacturerName
    {
      get { return manufacturerName; }
      set { SetProperty(ref manufacturerName, value); }
    }

    public string ModelName
    {
      get { return modelName; }
      set { SetProperty(ref modelName, value); }
    }

    public string CertificationNumber
    {
      get { return certificationNumber; }
      set { SetProperty(ref certificationNumber, value); }
    }

	public string GITIdentifier
	{
		get { return gitIdentifier; }
		set { SetProperty(ref gitIdentifier, value); }
	}


	public DateTime CertificationDate
    {
      get { return certificationDate; }
      set { SetProperty(ref certificationDate, value); }
    }

    public VectoComponent LocatedVectoComponent
    {
      get { return locatedVectoComponent; }
      set { SetProperty(ref locatedVectoComponent, value); }
    }

    public KeyValuePair<string, Blob> SelectedPublicDataItem
    {
      get { return selectedPublicDataItem; }
      set { SetProperty(ref selectedPublicDataItem, value); }
    }



    #endregion

    #region Commands

    public ICommand ShowPublicDataCommand
    {
      get
      {
        if (showPublicDataCommand == null)
        {
          showPublicDataCommand = new DelegateCommand(
            param => ExecShowPublicDataCommand(param));
        }
        return showPublicDataCommand;
      }
    }


    public ICommand ShowPrivateDataCommand
    {
      get
      {
        if (showPrivateDataCommand == null)
        {
          showPrivateDataCommand = new DelegateCommand(
            param => CanShowPrivateDataCommand(),
            param => ExecShowPrivateDataCommand());
        }
        return showPrivateDataCommand;
      }
    }


    #endregion

    public ComponentDataContentViewModel(INinjectFactory ninjectFactory,
      SearchResultModel selectedComponent)
    {
      this.ninjectFactory = ninjectFactory;
      this.selectedComponent = selectedComponent;

      LocatedVectoComponent = selectedComponent.VectoComponent;

      Item.OnItemSelected += SelectedTreeViewItem;

      SetSelectedComponent();
    }


    private void SetSelectedComponent()
    {
      ModelName = selectedComponent.Model;
      ManufacturerName = selectedComponent.Manufacturer;
      CertificationNumber = selectedComponent.CertificateNumber;
      CertificationDate = selectedComponent.CertificateDate;
	    GITIdentifier = selectedComponent.GitId;
    }

    #region Command Methods

    private void ExecShowPublicDataCommand(object param)
    {
      if (param is KeyValuePair<string, Blob>)
      {
        var convert = (KeyValuePair<string, Blob>)param;
        var repository = ninjectFactory.ComponentDataManagement().CurrentRepository;
        var blob = convert.Value;

        var blobSize = repository.ObjectDatabase.RetrieveObjectMetadata(blob.Id).Size;
        ShowContentWindow(blob, convert.Key, blobSize);
      }
    }

    private bool CanShowPrivateDataCommand()
    {
      return selectedPrivateDataItem != null;
    }

    private void ExecShowPrivateDataCommand()
    {
      var repository = ninjectFactory.ComponentDataManagement().CurrentRepository;
      var blob = repository.Lookup<Blob>(selectedPrivateDataItem.FilePath);

      var blobSize = repository.ObjectDatabase.RetrieveObjectMetadata(blob.Id).Size;
      ShowContentWindow(blob, selectedPrivateDataItem.Filename, blobSize);
    }



    #endregion


    private void ShowContentWindow(Blob blob, string blobName, long blobSize)
    {
      ContentWindow window = null;
      if (blobSize > int.MaxValue)
      {
        window = ninjectFactory.ContentWindow(blob, blobName, int.MaxValue);
      }
      else
      {
        var currentSize = Convert.ToInt32(blobSize);
        window = ninjectFactory.ContentWindow(blob, blobName, currentSize);
      }

      window.ShowDialog();
    }

    private void SelectedTreeViewItem(Item item)
    {
      if (item is FileItem)
      {
        selectedPrivateDataItem = (FileItem)item;
      }
      else
      {
        selectedPrivateDataItem = null;
      }
    }

  }
}
