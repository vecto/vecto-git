﻿using Ninject;
using System.Windows.Input;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel
{
  public class NavigationViewModel : ViewModelBase
  {
    [Inject]
    public IKernel Kernel { private get; set; }

    #region Members
    private ICommand appendDataCommand;
    private ICommand exportPacketCommand;
    private ICommand importPacketCommand;
    private ICommand saveComponentCommand;
    private ICommand searchComponentCommand;
    private ICommand backToStartViewCommand;

		private ICommand runVectoViewCommand;


		private string selectedViewName;
    private object selectedViewModel;

    #endregion

    #region Commands
    public ICommand SaveComponentCommand
    {
      get
      {
        if (saveComponentCommand == null)
        {
          saveComponentCommand = new DelegateCommand
            (p => ExecSaveComponentCommand());
        }
        return saveComponentCommand;
      }
    }

    public ICommand AppendDataCommand
    {
      get
      {
        if (appendDataCommand == null)
        {
          appendDataCommand = new DelegateCommand
            (p => ExecAppendDataCommand());
        }
        return appendDataCommand;
      }
    }

    public ICommand ExportPacketCommand
    {
      get
      {
        if (exportPacketCommand == null)
        {
          exportPacketCommand = new DelegateCommand
            (p => ExecExportPacketCommand());
        }
        return exportPacketCommand;
      }
    }

    public ICommand ImportPacketCommand
    {
      get
      {
        if (importPacketCommand == null)
        {
          importPacketCommand = new DelegateCommand
           (p => ExecImportPacketCommand());
        }
        return importPacketCommand;
      }
    }

    public ICommand SearchComponentCommand
    {
      get
      {
        if (searchComponentCommand == null)
        {
          searchComponentCommand = new DelegateCommand
          (param => ExecSearchComponentCommand());
        }
        return searchComponentCommand;
      }
    }

    public ICommand BackToStartViewCommand
    {
      get {
	      return backToStartViewCommand ?? (backToStartViewCommand = new DelegateCommand
		             (p => ExecBackToStartViewCommand()));
      }
    }


	  public ICommand NewVectoSimulation
	  {
		  get { return runVectoViewCommand ?? (runVectoViewCommand = new DelegateCommand(p => ExecRunVectoViewCommand())); }
	  }

	 #endregion

    #region Properties
    public object CurrentViewModel
    {
      get { return selectedViewModel; }
      set { SetProperty(ref selectedViewModel, value); }
    }

    public string SelectedViewName
    {
      get { return selectedViewName; }
      set { SetProperty(ref selectedViewName, value); }
    }

    private readonly INinjectFactory ninjectFactory;

	  #endregion

    public NavigationViewModel(INinjectFactory ninjectFactory)
    {
      this.ninjectFactory = ninjectFactory;
    }



    private void ExecAppendDataCommand()
    {
      ninjectFactory.MainWindowViewModel().CurrentViewName = "Append";
      CurrentViewModel = ninjectFactory.SearchComponentViewModel(true);
    }

    private void ExecExportPacketCommand()
    {
      var dataManagement = ninjectFactory.ComponentDataManagement();
      CurrentViewModel =
        ninjectFactory.ExportTransmissionPacketViewModel(dataManagement.VirtualDrive.VirtualPath);
    }

    private void ExecImportPacketCommand()
    {
      ninjectFactory.MainWindowViewModel().CurrentViewName = "Import Component";
      var dataManagement = ninjectFactory.ComponentDataManagement();
      CurrentViewModel = ninjectFactory.ImportTransmissionPacketViewModel(dataManagement);
    }

    private void ExecSaveComponentCommand()
    {
      SelectedViewName = "Save Component";
      CurrentViewModel = ninjectFactory.SaveComponentViewModel();
    }

    private void ExecSearchComponentCommand()
    {
      CurrentViewModel = ninjectFactory.SearchComponentViewModel(false);
    }

    public void ExecBackToStartViewCommand()
    {
      SelectedViewName = "Start";
      CurrentViewModel = null;
    }


	  private void ExecRunVectoViewCommand()
	  {
		  SelectedViewName = "Run VECTO Simulation";
		  CurrentViewModel = ninjectFactory.VectoSimulationViewModel();
	  }

	}
}
