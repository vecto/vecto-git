﻿using LibGit2Sharp;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.View;

namespace VECTO_GIT.GUI.ViewModel
{

  public class MergeDataContent : ModelBase
  {
    private MergeActionType mergeAction;

    public string BranchName { get; set; }
    public List<Item> LocalConflict { get; set; }
    public List<Item> RemoteConflict { get; set; }

    public List<string> UsedMeasurementNames { get; set; }
    public List<string> UsedUserDataNames { get; set; }

    public MergeActionType MergeAction
    {
      get { return mergeAction; }
      set { SetProperty(ref mergeAction, value); }
    }
  }

  public class MergeDataWindowViewModel : ViewModelBase
  {
    #region Constants

    private string DEFAULT_CLOSE_MESSAGE = "Do you really want to close, all settings will be lost!";
    private string NEGATIVE_SAVE_MESSAGE = "Not all merge conflicts are solved," +
                                           " on close all settings will be lost!";

    #endregion


    #region Members

    private readonly INinjectFactory ninjectFactory;
    private readonly List<MergePrivateBranchModel> conflictEntities;

    private ICommand saveMergeCommand;
    private ICommand cancelCommand;
    private ICommand selectedMergeOptionCommand;
    private ICommand closedMergeWindowCommand;
    private ICommand showLocalDataCommand;

    private FileItem selectedRemoteDataItem;
    private FileItem selectedLocalDataItem;

    private bool buttonEvent;
    #endregion

    #region Properties

    public IEnumerable<MergeActionType> MergeOptions { get; set; }
    public ObservableCollection<MergeDataContent> AllConflictItems { get; set; }

    #endregion

    #region Commands

    public ICommand SaveMergeCommand
    {
      get
      {
        if (saveMergeCommand == null)
        {
          saveMergeCommand = new DelegateCommand(
            param => ExecSaveMergeCommand(param));
        }

        return saveMergeCommand;
      }
    }


    public ICommand CancelCommand
    {
      get
      {
        if (cancelCommand == null)
        {
          cancelCommand = new DelegateCommand(
            param => ExecCancelCommand(param));
        }

        return cancelCommand;
      }
    }


    public ICommand ClosedMergeWindowCommand
    {
      get
      {
        if (closedMergeWindowCommand == null)
        {
          closedMergeWindowCommand = new DelegateCommand(
            p => ExecClosedMergeWindowCommand(p));
        }
        return closedMergeWindowCommand;
      }
    }


    public ICommand SelectedMergeOptionCommand
    {
      get
      {
        if (selectedMergeOptionCommand == null)
        {
          selectedMergeOptionCommand = new DelegateCommand(
            param => ExecSelectedMergeOptionCommand(param));
        }

        return selectedMergeOptionCommand;
      }
    }

    public ICommand ShowDataCommand
    {
      get
      {
        if (showLocalDataCommand == null)
        {
          showLocalDataCommand = new DelegateCommand(
            param => CanShowDataCommand(),
            param => ExecShowDataCommand());
        }

        return showLocalDataCommand;
      }
    }

    #endregion


    public MergeDataWindowViewModel(INinjectFactory ninjectFactory, List<MergePrivateBranchModel> conflictEntities)
    {
      this.ninjectFactory = ninjectFactory;
      this.conflictEntities = conflictEntities;
      Init();
      BuildListing();
    }

    private void Init()
    {
      Item.OnItemSelected += SelectedTreeViewItem;
      SetMergeOptions();
    }

    private void SetMergeOptions()
    {
      var fileDataTypes = new List<MergeActionType>();

      foreach (var type in Enum.GetValues(typeof(MergeActionType)).Cast<MergeActionType>())
      {
        if (type == MergeActionType.NothingSelected)
        {
          continue;
        }
        fileDataTypes.Add(type);
      }

      MergeOptions = fileDataTypes;
    }


    #region Commands


    private void ExecSaveMergeCommand(object param)
    {
      var convert = param as MergeDataWindow;
      if (convert != null)
      {
        if (AllMergedConflictsSolved())
        {
          buttonEvent = true;
          convert.DialogResult = true;
          UpdateConflictEntities();
          convert.Close();
        }
        else
        {
          if (CloseDialog(NEGATIVE_SAVE_MESSAGE) == MessageDialogResult.Affirmative)
          {
            buttonEvent = true;
            convert.DialogResult = false;
            convert.Close();
          }
        }
      }
    }

    private void ExecCancelCommand(object param)
    {
      var convert = param as MergeDataWindow;
      if (convert != null)
      {
        if (CloseDialog(DEFAULT_CLOSE_MESSAGE) == MessageDialogResult.Affirmative)
        {
          buttonEvent = true;
          convert.DialogResult = false;
          convert.Close();
        }
      }
    }

    private void ExecClosedMergeWindowCommand(object closeEvent)
    {
      if (closeEvent is CancelEventArgs)
      {
        if (!buttonEvent && CloseDialog(DEFAULT_CLOSE_MESSAGE) == MessageDialogResult.Negative)
        {
          ((CancelEventArgs)closeEvent).Cancel = true;
        }
      }
    }


    private MessageDialogResult CloseDialog(string dialogMessage)
    {
      var dialogSettings = new MetroDialogSettings()
      {
        AffirmativeButtonText = "Ok",
        NegativeButtonText = "Cancel",
        AnimateShow = true,
        AnimateHide = true,
      };
      var coordinator = DialogCoordinator.Instance;
      var result = coordinator.ShowModalMessageExternal(this, "", dialogMessage,
        MessageDialogStyle.AffirmativeAndNegative, dialogSettings);

      return result;
    }


    private void ExecSelectedMergeOptionCommand(object param)
    {
      var convert = param as MergeDataContent;
      if (convert != null && convert.MergeAction == MergeActionType.Rename)
      {
        var fileName = DialogCoordinator.Instance.ShowModalInputExternal
          (this, "Rename Imported Entry", "Name");

        if (fileName.IsNullOrEmptyOrWhitespace() || !TestFilenameUniqueness(convert.BranchName,
              fileName, convert.RemoteConflict.First().FileType))
        {
          convert.MergeAction = MergeActionType.NothingSelected;
        }
        else
        {
          var root = convert.RemoteConflict.First();
          root.Filename = fileName;
        }
      }
      else if (convert != null)
      {
        var root = convert.RemoteConflict.First();
        root.Filename = root.OriginFilename;
      }
    }

    private bool CanShowDataCommand()
    {
      return selectedLocalDataItem != null || selectedRemoteDataItem != null;
    }

    private void ExecShowDataCommand()
    {
      if(selectedLocalDataItem != null)
        ShowDataItem(selectedLocalDataItem);
      else 
        ShowDataItem(selectedRemoteDataItem);
    }


    private void ShowDataItem(FileItem selectedFileItem)
    {
      var repository = ninjectFactory.ComponentDataManagement().CurrentRepository;
      var blob = repository.Lookup<Blob>(selectedFileItem.FilePath);

      var blobSize = repository.ObjectDatabase.RetrieveObjectMetadata(blob.Id).Size;
      var filetype = selectedFileItem.IsLocalFileItem ? "Local Data:" : "Remote Data:";

      ShowContentWindow(blob, $"{filetype} {selectedFileItem.Filename}", blobSize);
    }
    #endregion
    
    private void ShowContentWindow(Blob blob, string blobName, long blobSize)
    {
      ContentWindow window = null;
      if (blobSize > int.MaxValue)
      {
        window = ninjectFactory.ContentWindow(blob, blobName, int.MaxValue);
      }
      else
      {
        var currentSize = Convert.ToInt32(blobSize);
        window = ninjectFactory.ContentWindow(blob, blobName, currentSize);
      }

      window.ShowDialog();
    }


    private bool AllMergedConflictsSolved()
    {
      for (int i = 0; i < AllConflictItems.Count; i++)
      {
        if (AllConflictItems[i].MergeAction == MergeActionType.NothingSelected)
          return false;
      }

      return true;
    }

    private void SelectedTreeViewItem(Item item)
    {
      var convert = item as FileItem;
      if (convert != null)
      {
        if (convert.IsLocalFileItem)
        {
          selectedLocalDataItem = convert;
          selectedRemoteDataItem = null;
        }
        else
        {
          selectedLocalDataItem = null;
          selectedRemoteDataItem = convert;
        }
      }
      else
      {
        selectedRemoteDataItem = null;
        selectedLocalDataItem = null;
      }
    }





    private bool TestFilenameUniqueness(string branchName, string filename, FileType fileType)
    {
      var occure = 0;
      foreach (var entry in AllConflictItems)
      {
        if (occure > 1)
          return false;

        var currentFileType = entry.RemoteConflict.First().FileType;

        if (entry.BranchName == branchName && fileType == currentFileType)
        {
          if (fileType == FileType.MeasurementData)
          {
            if (NameInFilenameList(entry.UsedMeasurementNames, filename))
              return false;
          }
          else if (fileType == FileType.UserData)
          {
            if (NameInFilenameList(entry.UsedUserDataNames, filename))
              return false;
          }

          if (entry.RemoteConflict.First().Filename == filename)
          {
            occure++;
          }
        }
      }

      return true;
    }

    private bool NameInFilenameList(List<string> filenames, string fileName)
    {
      if (filenames.IsNullOrEmpty())
        return false;

      for (int i = 0; i < filenames.Count; i++)
      {
        if (filenames[i] == fileName)
          return true;
      }

      return false;
    }


    private void BuildListing()
    {
      var items = new ObservableCollection<MergeDataContent>();

      foreach (var entity in conflictEntities)
      {

        for (int i = 0; i < entity.EntityConflicts.Count; i++)
        {
          var content = new MergeDataContent();

          var currentEntity = entity.EntityConflicts[i];
          var localItem = GetGUIItems(currentEntity.LocalTreeEntry, entity.EntityConflicts[i].DataType);
          var remoteItem = GetGUIItems(currentEntity.RemoteTreeEntry, entity.EntityConflicts[i].DataType);

          content.BranchName = entity.LocalPrivateBranch.FriendlyName;
          content.UsedMeasurementNames = entity.UsedMeasurementNames;
          content.UsedUserDataNames = entity.UsedUserDataNames;
          content.LocalConflict = localItem;
          content.RemoteConflict = remoteItem;

          items.Add(content);

        }
      }

      AllConflictItems = items;
    }

    private void UpdateConflictEntities()
    {
      var entriesCount = 0;
      for (int i = 0; i < conflictEntities.Count; i++)
      {
        for (int j = 0; j < conflictEntities[i].EntityConflicts.Count; j++)
        {
          var item = AllConflictItems[entriesCount + j];
          if (item.MergeAction == MergeActionType.Rename)
          {
            conflictEntities[i].EntityConflicts[j].MergeAction = MergeActionType.Rename;
            conflictEntities[i].EntityConflicts[j].FileName = item.RemoteConflict.First().Filename;
          }
          else
          {
            conflictEntities[i].EntityConflicts[j].MergeAction = item.MergeAction;
          }
        }

        entriesCount += conflictEntities[i].EntityConflicts.Count;
      }
    }



    private List<Item> GetGUIItems(TreeEntry treeEntry, FileType fileType)
    {
      if (treeEntry.TargetType == TreeEntryTargetType.Tree)
      {
        var result = CreateTreeItem(treeEntry.Target as Tree, fileType);

        var dirItem = new List<Item>
        {
          new DirectoryItem
          {
            FileType = fileType,
            OriginFilename = treeEntry.Name,
            FilePath = treeEntry.Target.Sha,
            Items = result
          }
        };

        return dirItem;
      }

      if (treeEntry.TargetType == TreeEntryTargetType.Blob)
      {
        return CreateSingleTreeItem(treeEntry, fileType);
      }

      return null;
    }

    private List<Item> CreateSingleTreeItem(TreeEntry treeEntry, FileType fileType)
    {
      if (treeEntry.TargetType != TreeEntryTargetType.Blob)
        return null;


      var items = new List<Item>();

      var item = new FileItem
      {
        FileType = fileType,
        OriginFilename = treeEntry.Name,
        FilePath = treeEntry.Target.Sha
      };
      items.Add(item);

      return items;
    }


    private List<Item> CreateTreeItem(Tree tree, FileType fileType)
    {
      if (tree == null)
        return null;

      var items = new List<Item>();
      foreach (var entry in tree)
      {
        if (entry.TargetType == TreeEntryTargetType.Tree)
        {
          var dir = new DirectoryItem
          {
            FileType = fileType,
            OriginFilename = entry.Name,
            FilePath = entry.Target.Sha,
            Items = CreateTreeItem((Tree)entry.Target, fileType)
          };
          items.Add(dir);
        }

        if (entry.TargetType == TreeEntryTargetType.Blob)
        {
          var item = new FileItem
          {
            FileType = fileType,
            OriginFilename = entry.Name,
            FilePath = entry.Target.Sha
          };
          items.Add(item);
        }
      }
      return items;
    }

  }
}
