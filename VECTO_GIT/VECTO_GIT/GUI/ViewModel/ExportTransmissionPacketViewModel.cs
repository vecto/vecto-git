﻿using LibGit2Sharp;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.ComponentTransfer;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.ViewModel
{
  public class ExportTransmissionPacketViewModel : ViewModelBase
  {
    #region Members
    private readonly INinjectFactory ninjectFactory;
    private string currentRepositoryPath;

    private ComponentForTransfer selectedComponent;

    private ICommand showDetailsCommand;
    private ICommand saveTransferPacketCommand;
    private ICommand cancelCommand;
    private ICommand removeSelectedCommand;
    private ICommand removeAllCommand;

    #endregion

    #region Properties

    public ObservableCollection<ComponentForTransfer> BranchesForTransfer { get; set; }

    public ComponentForTransfer SelectedComponent
    {
      get { return selectedComponent; }
      set { SetProperty(ref selectedComponent, value); }
    }

    #endregion

    #region Commands
    public ICommand ShowDetailsCommand
    {
      get
      {
        if (showDetailsCommand == null)
        {
          showDetailsCommand = new DelegateCommand(
            param => ExecShowDetailsCommand(param));
        }
        return showDetailsCommand;
      }
    }

    public ICommand SaveTransferPacketCommand
    {
      get
      {
        if (saveTransferPacketCommand == null)
        {
          saveTransferPacketCommand = new DelegateCommand(param => CanSaveTransferPacketCommand(),
            param => ExecSaveTransferPacketCommand());
        }
        return saveTransferPacketCommand;
      }
    }

    public ICommand CancelCommand
    {
      get
      {
        if (cancelCommand == null)
        {
          cancelCommand =
            ninjectFactory.NavigationViewModel().BackToStartViewCommand;
        }
        return cancelCommand;
      }
    }

    public ICommand RemoveAllCommand
    {
      get
      {
        if (removeAllCommand == null)
        {
          removeAllCommand = new DelegateCommand(
            param => CanRemoveAllCommand(),
            param => ExecRemoveAllCommand());
        }
        return removeAllCommand;
      }
    }

    public ICommand RemoveSelectedCommand
    {
      get
      {
        if (removeSelectedCommand == null)
        {
          removeSelectedCommand = new DelegateCommand(
              param => CanRemoveSelectedCommand(),
              param => ExecRemoveSelectedCommand()
            );
        }

        return removeSelectedCommand;
      }
    }

    #endregion

    public ExportTransmissionPacketViewModel(INinjectFactory ninjectFactory, string repositoryPath)
    {
      this.ninjectFactory = ninjectFactory;
      FetchAllTransferFiles(repositoryPath);
    }

    public void UpdateTransferFiles()
    {
      var repositoryPath = ninjectFactory.ComponentDataManagement().VirtualDrive.VirtualPath;
      FetchAllTransferFiles(repositoryPath);
    }


    private void FetchAllTransferFiles(string repositoryPath)
    {
      var xmlHandler = ninjectFactory.TransferXmlHandler();
      xmlHandler.UpdateTransferList(repositoryPath);

      BranchesForTransfer = new ObservableCollection<ComponentForTransfer>(xmlHandler.TransferList);
      currentRepositoryPath = repositoryPath;
    }

    private void ExecShowDetailsCommand(object parameter)
    {
      var component = parameter as ComponentForTransfer;
      if (component != null)
      {
        var vectoComponent = FetchRelatedVectoComponent(component);
        if (vectoComponent == null) return;

        var searchResult = GUIHelpers.SummarizeComponentToSearchResult(vectoComponent);
        SetTransferBranches(component, vectoComponent);

        ninjectFactory.NavigationViewModel().CurrentViewModel
          = ninjectFactory.ComponentDetailsViewModel(searchResult, this);
      }
    }

    private void SetTransferBranches(ComponentForTransfer transferComponent, VectoComponent component)
    {
      var repositoryPath = ninjectFactory.ComponentDataManagement().VirtualDrive.VirtualPath;
      var xmlHandler = ninjectFactory.TransferXmlHandler();
      xmlHandler.UpdateTransferList(repositoryPath);


      if (transferComponent.PublicBranch != null && transferComponent.PublicBranch.TransferBranch)
      {
        component.TransferPublicBranch = true;
      }

      if (transferComponent.StandardBranch != null && transferComponent.StandardBranch.TransferBranch)
      {
        component.TransferStandardValuesBranch = true;
      }

      if (transferComponent.PrivatBranch != null && transferComponent.PrivatBranch.TransferBranch)
      {
        component.TransferPrivateBranch = true;
      }
    }


    private VectoComponent FetchRelatedVectoComponent(ComponentForTransfer component)
    {
      var repository = ninjectFactory.ComponentDataManagement().CurrentRepository;
      Branch publicBranch = null;
      Branch privateBranch = null;

      if (component.PublicBranch != null && !component.PublicBranch.BranchName.IsNullOrEmptyOrWhitespace())
      {
        publicBranch = repository.Branches[component.PublicBranch.BranchName];
      }

      if (component.PrivatBranch != null && !component.PrivatBranch.BranchName.IsNullOrEmptyOrWhitespace())
      {
        privateBranch = repository.Branches[component.PrivatBranch.BranchName];
      }

      return new VectoComponent(repository, publicBranch, privateBranch);
    }


    private bool CanSaveTransferPacketCommand()
    {
      return BranchesForTransfer != null && BranchesForTransfer.Count > 0;
    }

    private void ExecSaveTransferPacketCommand()
    {
      var saveFileDialog = new SaveFileDialog()
      {
        Filter = "Repository package (*.zip)|*.zip",
        InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
      };
      if (saveFileDialog.ShowDialog() == true)
      {
        var dataManagement = ninjectFactory.ComponentDataManagement();

        var transferHandler = new TransferHandler();
        transferHandler.DownloadArchiveForTransfer(dataManagement.CurrentRepository, BranchesForTransfer,
                                                   saveFileDialog.FileName);


        //var branchNames = ExtractAvailableBranchNames();
        //transferHandler.DownloadArchiveForTransfer(dataManagement.CurrentRepository, branchNames,
        //  saveFileDialog.FileName);
      }
    }

    private bool CanRemoveSelectedCommand()
    {
      return SelectedComponent != null && BranchesForTransfer.Count > 0;
    }

    private void ExecRemoveSelectedCommand()
    {
      var xmlHandler = ninjectFactory.TransferXmlHandler();
      if (xmlHandler.DeletTransferComponent(SelectedComponent, currentRepositoryPath))
      {
        BranchesForTransfer.Remove(SelectedComponent);
      }
    }

    private bool CanRemoveAllCommand()
    {
      return BranchesForTransfer != null && BranchesForTransfer.Count > 0;
    }

    private void ExecRemoveAllCommand()
    {
      var xmlHandler = ninjectFactory.TransferXmlHandler();
      for (int i = 0; i < BranchesForTransfer.Count; i++)
      {
        xmlHandler.DeletTransferComponent(BranchesForTransfer[i], currentRepositoryPath);
      }
      BranchesForTransfer.Clear();
    }


    //private List<string> ExtractAvailableBranchNames()
    //{
    //  var result = new List<string>();

    //  for (int i = 0; i < BranchesForTransfer.Count; i++)
    //  {
    //    var file = BranchesForTransfer[i];
    //    if (file.StandardBranch != null)
    //    {
    //      result.Add(file.StandardBranch.BranchName);
    //    }

    //    if (file.PublicBranch != null)
    //    {
    //      result.Add(file.PublicBranch.BranchName);
    //    }

    //    if (file.PrivatBranch != null)
    //    {
    //      result.Add(file.PrivatBranch.BranchName);
    //    }
    //  }

    //  return result;
    //}

  }
}
