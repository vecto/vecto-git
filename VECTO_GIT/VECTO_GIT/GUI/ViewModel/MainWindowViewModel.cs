﻿using System;
using VECTO_GIT.GUI.Helper;
using System.Windows.Input;
using MahApps.Metro.Controls.Dialogs;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Model;

using System.ComponentModel;
using Ninject;
using VECTO_GIT.View;

namespace VECTO_GIT.GUI.ViewModel
{
  public class MainWindowViewModel : ViewModelBase
  {


    #region Members

    [Inject]
    public IKernel Kernel { private get; set; }
    private readonly INinjectFactory ninjectFactory;

    private string currentViewName;


    private ICommand closeMainWindowCommand;
    private ICommand loadedWindowCommand;
    private ICommand windowRenderedCommand;
    private ICommand openSettingsCommand;

    private SettingsModel settingsModel;

    #endregion

    #region Commands
    public ICommand CloseMainWindowCommand
    {
      get
      {
        if (closeMainWindowCommand == null)
        {
          closeMainWindowCommand = new DelegateCommand
          (
            p => ExecCloseMainWindowCommand(p)
          );
        }
        return closeMainWindowCommand;
      }
    }
    public ICommand WindowRenderedCommand
    {
      get
      {
        if (windowRenderedCommand == null)
        {
          windowRenderedCommand = new DelegateCommand(
            p => CanWindowRenderedCommand(),
            p => ExecWindowRenderedCommand(p));
        }
        return windowRenderedCommand;
      }
    }
    public ICommand OpenSettingsCommand
    {
      get
      {
        if (openSettingsCommand == null)
        {
          openSettingsCommand = new DelegateCommand
          (
            p => ExecOpenSettingsCommand(p)
          );
        }
        return openSettingsCommand;
      }
    }
    public ICommand LoadWindowCommand
    {
      get
      {
        if (loadedWindowCommand == null)
        {
          loadedWindowCommand = new DelegateCommand(
            p => ExecWindowLoadedCommand());
        }
        return loadedWindowCommand;
      }
    }


    #endregion


    #region Properties
    public NavigationViewModel NavigationViewModel { get; private set; }

    public string CurrentViewName
    {
      get { return currentViewName; }
      set { SetProperty(ref currentViewName, value); }
    }

    #endregion

    public MainWindowViewModel(INinjectFactory ninjectFactory)
    {
      this.ninjectFactory = ninjectFactory;
      NavigationViewModel = ninjectFactory.NavigationViewModel();
    }

    private void ExecCloseMainWindowCommand(object cancelEvent)
    {
      if (ValidRepositorySettings())
      {
        CloseDialogOnError((CancelEventArgs)cancelEvent);
      }
    }


    private void CloseDialogOnError(CancelEventArgs cancelEvent)
    {
      var mySettings = new MetroDialogSettings()
      {
        AffirmativeButtonText = "Ok",
        NegativeButtonText = "Cancel",
        AnimateShow = true,
        AnimateHide = true,
      };

      var coordinator = DialogCoordinator.Instance;
      var result = coordinator.ShowModalMessageExternal(this, "", "Do you really want to close?",
        MessageDialogStyle.AffirmativeAndNegative, mySettings);

      if (result == MessageDialogResult.Negative)
      {
        cancelEvent.Cancel = true;
      }
      else
      {
        Kernel.Dispose();
      }
    }

    private void ExecOpenSettingsCommand(object commandParameter)
    {
      var mainWindow = (MainWindow)commandParameter;
      var settingsWindow = ninjectFactory.SettingsWindow();

      settingsWindow.CenterToSrcWindow(mainWindow);

      if (!(bool)settingsWindow.ShowDialog() && !ValidRepositorySettings())
      {
        mainWindow.Close();
      }
    }

    private bool CanWindowRenderedCommand()
    {
      return !ValidRepositorySettings();
    }

    private void ExecWindowRenderedCommand(object commandParameter)
    {
      ExecOpenSettingsCommand(commandParameter);
    }

    private void ExecWindowLoadedCommand()
    {
      if (ValidRepositorySettings())
      {
        //ToDo Comment out virtual drive for the moment
        var componentDataManagement = ninjectFactory.ComponentDataManagement();
        var settingsModel = ninjectFactory.SettingsModel();
        componentDataManagement.SetCurrentRepository(settingsModel.RepositoryPath);
      }
    }

    private bool ValidRepositorySettings()
    {
      if (settingsModel == null)
        settingsModel = ninjectFactory.SettingsModel();

      if (!GUIHelpers.IsValidMailAddress(settingsModel.CommitterEMail))
      {
        return false;
      }

      if (string.IsNullOrEmpty(settingsModel.CommitterName))
      {
        return false;
      }

      if (!settingsModel.RepositoryPath.IsExistingFolderPath())
      {
        return false;
      }

      return true;
    }

  }



}

