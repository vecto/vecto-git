﻿using System.Collections.Generic;
using LibGit2Sharp;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel
{
  public class ImageContentViewModel : ViewModelBase
  {
    #region Members
    private double currentScale;
    private readonly double minScale;
    private readonly double maxScale;
    private readonly double zoomInterval;

    private bool zoomInEnabled;
    private bool zoomOutEnabled;

    private BitmapImage loadedImage;

    private BitmapSource transformedImage;
    private Blob blobContent;
    private string fileName;
    private ICommand saveImageCommand;
    private ICommand zoomInCommand;
    private ICommand zoomOutCommand;
    private ICommand mouseWheelCommand;

    #endregion


    #region Properties
    public bool ZoomInEnabled
    {
      get { return zoomInEnabled; }
      set { SetProperty(ref zoomInEnabled, value); }
    }

    public bool ZoomOutEnabled
    {
      get { return zoomOutEnabled; }
      set { SetProperty(ref zoomOutEnabled, value); }
    }


    public BitmapSource TransformedImage
    {
      get { return transformedImage; }
      set { SetProperty(ref transformedImage, value); }
    }

    #endregion

    #region Commands

    public ICommand ZoomInCommand
    {
      get
      {
        if (zoomInCommand == null)
        {
          zoomInCommand = new DelegateCommand(
            param => CanZoomInCommand(),
            param => ExecZoomInCommand());
        }
        return zoomInCommand;
      }
    }

    public ICommand ZoomOutCommand
    {
      get
      {
        if (zoomOutCommand == null)
        {
          zoomOutCommand = new DelegateCommand(
            param => CanZoomOutCommand(),
            param => ExecZoomOutCommand());
        }
        return zoomOutCommand;
      }
    }


    public ICommand SaveImageCommand
    {
      get
      {
        if (saveImageCommand == null)
        {
          saveImageCommand = new DelegateCommand(param => ExecSaveImageCommand());
        }
        return saveImageCommand;
      }
    }


    public ICommand MouseWheelCommand
    {
      get
      {
        if (mouseWheelCommand == null)
        {
          mouseWheelCommand = new DelegateCommand(ExecMouseWheelCommand);
        }

        return mouseWheelCommand;
      }
    }

    #endregion

    public ImageContentViewModel()
    {
      currentScale = 1.0;
      minScale = 0.1;
      maxScale = 2.0;
      zoomInterval = 0.1;
      ZoomInEnabled = true;
      ZoomOutEnabled = true;
    }


    private void ExecMouseWheelCommand(object param)
    {
      var mouseWheelEvent = param as MouseWheelEventArgs;
      if (mouseWheelEvent != null)
      {
        if (mouseWheelEvent.Delta < 0)
        {
          if (MinScale())
            ExecZoomOutCommand();
        }

        if (mouseWheelEvent.Delta >= 0)
        {
          if (MaxScale())
            ExecZoomInCommand();
        }
      }
    }

    private bool CanZoomInCommand()
    {
      return true;
    }

    private void ExecZoomInCommand()
    {
      if (MaxScale())
        TransformedImage = ZoomImage(currentScale + zoomInterval, loadedImage);
    }

    private bool MinScale()
    {
      var nextScale = currentScale - zoomInterval;
      return nextScale >= minScale;
    }

    private bool MaxScale()
    {
      var nextScale = currentScale + zoomInterval;
      return nextScale <= maxScale;
    }


    private bool CanZoomOutCommand()
    {
      return true;
    }

    private void ExecZoomOutCommand()
    {
      if (MinScale())
        TransformedImage = ZoomImage(currentScale - 0.1, loadedImage);
    }

    private BitmapSource ZoomImage(double selectedScale, BitmapSource image)
    {
      currentScale = selectedScale;

      var transformedBitmap
        = new TransformedBitmap(image,
          new ScaleTransform(selectedScale, selectedScale, image.Height / 2.0, image.Width / 2.0));

      return transformedBitmap;
    }

    public void LoadImageContent(KeyValuePair<string,Blob> currentBlob)
    {
      blobContent = currentBlob.Value;
      fileName = currentBlob.Key;

      var image = new BitmapImage();

      using (var stream = blobContent.GetContentStream())
      {
        image.BeginInit();
        image.StreamSource = stream;
        image.CacheOption = BitmapCacheOption.OnLoad;
        image.EndInit();
        image.Freeze();
      }

      loadedImage = image;
      TransformedImage = GetBitmapSource(image);
    }

    private void ExecSaveImageCommand()
    {
      GUIHelpers.SaveFileDialog(blobContent, "PDF-File (*.pdf)|*.pdf");
    }

    private BitmapSource GetBitmapSource(BitmapImage image)
    {
      return new TransformedBitmap(image, new ScaleTransform(1.0, 1.0, image.Height / 2.0, image.Width / 2.0));
    }


  }
}