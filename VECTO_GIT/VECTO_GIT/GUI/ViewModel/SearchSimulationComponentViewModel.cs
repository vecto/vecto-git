﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.ViewModel
{
	public class SearchSimulationComponentViewModel : ViewModelBase
	{
		private INinjectFactory _ninjectFactory;
		private VectoSimulationViewModel.Components _component;
		private string _componentName;
		private string _certCountry;
		private string _certCertificationAct;
		private string _certAmendingAct;
		private string _certComponentType;
		private string _certBaseNumber;
		private string _certExtensionNumber;
		private string _manufacturerName;
		private string _modelName;
		private string _gitIdentifier;
		private ICommand _searchCommand;
		private ICommand _cancelCommand;

		private readonly CertificationNumberModel _certificationNumber = new CertificationNumberModel();

		private VectoSearchTerms _vectoSearchTerms = new VectoSearchTerms();
		private ICommand _selectedActionCommand;
		private string _actionButtonText = "Select";
		private int? _idx;

		public VectoSimulationViewModel SimulationViewModel { get; set; }

		public SearchSimulationComponentViewModel(
			INinjectFactory ninjectFactory, VectoSimulationViewModel.Components component, int? idx = null)
		{
			_ninjectFactory = ninjectFactory;
			_component = component;
			ComponentName = _component.Name();

			CertComponentType = _component.ComponentTypeNumber();
			_certificationNumber.ComponentType = CertComponentType;
			_idx = idx;
		}

		public string ComponentName
		{
			get { return _componentName; }
			set { SetProperty(ref _componentName, value); }
		}

		public string CertCountry
		{
			get { return _certCountry; }
			set {
				if (SetProperty(ref _certCountry, value)) {
					_certificationNumber.Country = value;
				}
			}
		}

		public string CertCertificationAct
		{
			get { return _certCertificationAct; }
			set {
				if (SetProperty(ref _certCertificationAct, value)) {
					_certificationNumber.CertificationAct = value;
				}
			}
		}

		public string CertAmendingAct
		{
			get { return _certAmendingAct; }
			set {
				if (SetProperty(ref _certAmendingAct, value)) {
					_certificationNumber.AmendingAct = value;
				}
			}
		}

		public string CertComponentType
		{
			get { return _certComponentType; }
			private set { SetProperty(ref _certComponentType, value); }
		}

		public string CertBaseNumber
		{
			get { return _certBaseNumber; }
			set {
				if (SetProperty(ref _certBaseNumber, value)) {
					_certificationNumber.BaseNumber = value;
				}
			}
		}

		public string CertExtensionNumber
		{
			get { return _certExtensionNumber; }
			set {
				if (SetProperty(ref _certExtensionNumber, value)) {
					_certificationNumber.ExtensionNumber = value;
				}
			}
		}

		public string ManufacturerName
		{
			get { return _manufacturerName; }
			set {
				if (SetProperty(ref _manufacturerName, value)) {
					_vectoSearchTerms.Manufacturer = value;
				}
			}
		}

		public string ModelName
		{
			get { return _modelName; }
			set {
				if (SetProperty(ref _modelName, value)) {
					_vectoSearchTerms.Model = value;
				}
			}
		}

		public string GitIdentifier
		{
			get { return _gitIdentifier; }
			set {
				if (SetProperty(ref _gitIdentifier, value)) {
					_vectoSearchTerms.GitID = value;
				}
			}
		}

		public ICommand SearchCommand
		{
			get { return _searchCommand ?? (_searchCommand = new DelegateCommand(ExecSearchCommand)); }
		}

		public ICommand CancelCommand
		{
			get { return _cancelCommand ?? (_cancelCommand = SimulationViewModel.CancelSearchCommand); }
		}


		private void ExecSearchCommand(object param)
		{
			SetSearchResults();
		}

		private void SetSearchResults()
		{
			var compDataManagement = _ninjectFactory.ComponentDataManagement();

			//SetCertificationSearchDate();

			_vectoSearchTerms.CertificationNumber = _certificationNumber;
			var results = compDataManagement.SearchVectoComponentsByGivenTerms(_vectoSearchTerms);
			SearchResults = GUIHelpers.SummarizeComponentSearchResult(results);
			OnPropertyChanged("SearchResults");
		}

		public ObservableCollection<SearchResultModel> SearchResults { get; private set; }

		public ICommand SelectedActionCommand
		{
			get {
				return _selectedActionCommand ??
				       (_selectedActionCommand = new DelegateCommand(param => ExecSelectedActionCommand(param)));
			}
		}

		private void ExecSelectedActionCommand(object param)
		{
			var component = param as SearchResultModel;
			if (component == null) {
				return;
			}

			SimulationViewModel.SetSelected(_component, component.GitId, _idx);
		}

		public string ActionButtonText
		{
			get { return _actionButtonText; }
			set { SetProperty(ref _actionButtonText, value); }
		}
	}
}
