﻿using LibGit2Sharp;
using PdfiumViewer;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using Image = System.Drawing.Image;

namespace VECTO_GIT.GUI.ViewModel
{
  public class ContentWindowViewModel : ViewModelBase
  {
    #region Members
    private static string XML_FILE_TYPE = "text/xml";
    private static string PDF_FILE_TYPE = "application/pdf";
    private static string PLAIN_FILE_TYPE = "text/plain";
    private readonly INinjectFactory ninjectFactory;
    private readonly int blobSize;
    private readonly Blob currentBlob;

    private KeyValuePair<string, Blob> selectedXMLBlob;
    private KeyValuePair<string, Blob> selectedPDFBlob;
    private KeyValuePair<string, Blob> selectedImageBlob;
    private string blobName;
    private string xmlFileContent;
    private string windowTitle;

    private ICommand windowRenderedCommand;

    #endregion

    #region Properties

    public KeyValuePair<string, Blob> SelectedPDFBlob
    {
      get { return selectedPDFBlob; }
      set { SetProperty(ref selectedPDFBlob, value); }
    }

    public KeyValuePair<string, Blob> SelectedXMLBlob
    {
      get { return selectedXMLBlob; }
      set { SetProperty(ref selectedXMLBlob, value); }
    }

    public KeyValuePair<string, Blob> SelectedImageBlob
    {
      get { return selectedImageBlob; }
      set { SetProperty(ref selectedImageBlob, value); }
    }


    public string WindowTitle
    {
      get { return windowTitle; }
      set { SetProperty(ref windowTitle, value); }
    }

    #endregion

    #region Commands

    public ICommand WindowRenderedCommand
    {
      get
      {
        if (windowRenderedCommand == null)
        {
          windowRenderedCommand = new DelegateCommand(param => ExecWindowRenderedCommand());
        }

        return windowRenderedCommand;
      }
    }

    #endregion

    public ContentWindowViewModel(INinjectFactory ninjectFactory, Blob currentBlob, string blobName, int blobSize)
    {
      this.ninjectFactory = ninjectFactory;
      this.blobSize = blobSize;
      this.currentBlob = currentBlob;
      this.blobName = blobName;
      WindowTitle = blobName;
    }

    private void ExecWindowRenderedCommand()
    {
      var fileType = GetFileType();
      if (fileType == XML_FILE_TYPE || fileType == PLAIN_FILE_TYPE)
      {
        SelectedXMLBlob = new KeyValuePair<string, Blob>(blobName,currentBlob);
      }
      else if (fileType == PDF_FILE_TYPE)
      {
        SelectedPDFBlob = new KeyValuePair<string, Blob>(blobName, currentBlob);
      }
      else if (fileType.StartsWith("image/"))
      {
        SelectedImageBlob = new KeyValuePair<string, Blob>(blobName, currentBlob);
      }
    }
    
    private string GetFileType()
    {
      if (currentBlob == null)
        return null;

      return MIMEHelper.GetFileTypeOfBlob(currentBlob, blobSize);
    }
  }
}
