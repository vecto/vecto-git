﻿using LibGit2Sharp;
using PdfiumViewer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using Microsoft.Win32;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.ViewModel
{
  public class PDFContentViewModel : ViewModelBase, IDisposable
  {

    #region Members

//    private KeyValuePair<string ,Blob> currentBlobContent;
    private string fileName;
    private Blob blobContent;
    private bool disposed;
    private PdfViewer pdfViewer;
    private Stream pdfStream;
    private WindowsFormsHost formHost;

    private ICommand zoomInCommand;
    private ICommand zoomOutCommand;
    private ICommand saveCommand;

    #endregion

    #region Properties



    #endregion

    #region Commands

    public ICommand ZoomInCommand
    {
      get
      {
        if (zoomInCommand == null)
        {
          zoomInCommand = new DelegateCommand(param => ExecZoomInCommand());
        }

        return zoomInCommand;
      }
    }


    public ICommand ZoomOutCommand
    {
      get
      {
        if (zoomOutCommand == null)
        {
          zoomOutCommand = new DelegateCommand(param => ExecZoomOutCommand());
        }

        return zoomOutCommand;
      }
    }

    public ICommand SaveCommand
    {
      get
      {
        if (saveCommand == null)
        {
          saveCommand = new DelegateCommand(param => ExecSaveCommand());
        }

        return saveCommand;
      }
    }
    
    #endregion

    public void LoadPdfContent(Grid contentGrid, KeyValuePair<string, Blob> currentBlob)
    {
      blobContent = currentBlob.Value;
      fileName = currentBlob.Key;
      
      formHost = new WindowsFormsHost();

      pdfStream = blobContent.GetContentStream();
      pdfViewer = new PdfViewer
      {
        ShowToolbar = false,
        Document = PdfDocument.Load(pdfStream)
      };
      
      formHost.Child = pdfViewer;
      contentGrid.Children.Add(formHost);
    }
    

    private void ExecZoomInCommand()
    {
      pdfViewer.Renderer.ZoomIn();
    }
    
    private void ExecZoomOutCommand()
    {
      pdfViewer.Renderer.ZoomOut();
    }

    private void ExecSaveCommand()
    {
      GUIHelpers.SaveFileDialog(blobContent, "PDF-File (*.pdf)|*.pdf");
    }


    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        if (pdfStream != null)
        {
          pdfStream.Dispose();
        }

        if (pdfViewer != null)
        {
          pdfViewer.Dispose();
        }

        if (formHost != null)
        {
          formHost.Dispose();
        }
      }
      disposed = true;
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }


  }
}
