﻿using Ninject.Modules;
using VECTO_GIT.GUI.ViewModel;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.ComponentCommit;
using Ninject.Extensions.Factory;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.View;
using VECTO_GIT.View;

namespace VECTO_GIT.GUI.DI
{
  class InjectModule : NinjectModule
  {
    public override void Load()
    {
      BindModels();
      BindViewModels();
      BindWindows();

      BindBusinessLogic();
      BindFactory();

    }

    private void BindFactory()
    {
      Bind<INinjectFactory>().ToFactory();
    }

    private void BindBusinessLogic()
    {
      Bind<ComponentDataManagement>().ToSelf().InSingletonScope();
      Bind<TransferXmlHandler>().ToSelf().InSingletonScope();
      Bind<XmlValidator>().ToSelf().InThreadScope();
    }

   

    private void BindModels()
    {
      Bind<SettingsModel>().ToSelf().InSingletonScope();
    }
    
    private void BindViewModels()
    {
      Bind<MainWindowViewModel>().ToSelf().InThreadScope();
      Bind<NavigationViewModel>().ToSelf().InThreadScope();

      Bind<ContentWindowViewModel>().ToSelf().InTransientScope();

      Bind<ComponentDetailsViewModel>().ToSelf().InTransientScope();
      Bind<AppendDataToComponentViewModel>().ToSelf().InTransientScope();
      Bind<ExportTransmissionPacketViewModel>().ToSelf().InTransientScope();
      Bind<ImportTransmissionPacketViewModel>().ToSelf().InTransientScope();
      Bind<SaveComponentViewModel>().ToSelf().InTransientScope();
      Bind<SearchComponentViewModel>().ToSelf().InTransientScope();
      Bind<SettingsViewModel>().ToSelf().InTransientScope();
      Bind<MergeDataWindowViewModel>().ToSelf().InTransientScope();

		Bind<VectoSimulationViewModel>().ToSelf().InTransientScope();
	    Bind<SearchSimulationComponentViewModel>().ToSelf().InTransientScope();
		Bind<VectoSimulationProgressViewModel>().ToSelf().InTransientScope();
		Bind<VectoGitInputData>().ToSelf().InTransientScope();
	}

    private void BindWindows()
    {
      Bind<SettingsWindow>().ToSelf().InTransientScope();
      Bind<ContentWindow>().ToSelf().InTransientScope();
      //Bind<MainWindow>().ToSelf().InSingletonScope();
    }

  }
}
