﻿using System.Collections.Generic;
using LibGit2Sharp;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.View;
using VECTO_GIT.GUI.ViewModel;
using VECTO_GIT.View;

namespace VECTO_GIT.GUI.DI
{
	public interface INinjectFactory
	{
		//Models
		SettingsModel SettingsModel();

		//ViewModels
		MainWindowViewModel MainWindowViewModel();

		NavigationViewModel NavigationViewModel();

		AppendDataToComponentViewModel AppendDataToComponentViewModel(
			SearchResultModel selectedComponent,
			SearchComponentViewModel searchViewModel);

		ExportTransmissionPacketViewModel ExportTransmissionPacketViewModel(string repositoryPath);
		ImportTransmissionPacketViewModel ImportTransmissionPacketViewModel(ComponentDataManagement dataManagement);
		SaveComponentViewModel SaveComponentViewModel();
		SearchComponentViewModel SearchComponentViewModel(bool isAppendSearch);

		ComponentDetailsViewModel ComponentDetailsViewModel(
			SearchResultModel selectedComponent,
			object previousViewModel);

		SettingsViewModel SettingsViewModel();

		//    ActionResultWindowViewModel ActionResultWindowViewModel();
		//    SaveComponentResultViewModel SaveComponentResultViewModel(IEnumerable<object> errors);

		//    ItemProvider ItemProvider();

		//    ContentWindowViewModel ContentWindowViewModel(Blob blob);

		ComponentDataContentViewModel ComponentDataContentViewModel(SearchResultModel selectedComponent);

		//Windows
		SettingsWindow SettingsWindow();

		ContentWindow ContentWindow(Blob blob, string blobName, int blobSize);


		//Business Logic
		ComponentDataManagement ComponentDataManagement();

		XmlValidator XmlValidator(string xsdFilePath, string xsdNameSpace);
		TransferXmlHandler TransferXmlHandler();

    MergeDataWindow MergeDataWindow(List<MergePrivateBranchModel> mergeConflicts);
    MergeDataWindowViewModel MergeDataWindowViewModel(INinjectFactory ninjectFactory, List<MergePrivateBranchModel> mergeConflicts);
		// VECTO
		VectoSimulationViewModel VectoSimulationViewModel();

		SearchSimulationComponentViewModel SearchSimulationComponentViewModel(
			VectoSimulationViewModel.Components component, int? idx);

		VectoSimulationProgressViewModel VectoSimulationProgressViewModel();

		VectoGitInputData VectoInputData(VectoSimulationViewModel vectoSimulationViewModel);
	}
}
