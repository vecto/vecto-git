﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;

namespace VECTO_GIT.GUI.Helper
{
  public class ViewModelBase : ModelBase, INotifyDataErrorInfo
  {
    private Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();
    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

    public IEnumerable GetErrors(string propertyName)
    {
      if(propertyName == null || propertyName == string.Empty)
      {
        return null;
      }

      if (errors.ContainsKey(propertyName))
      {
        return errors[propertyName];
      }
      return null;
    }

    public bool HasErrors
    {
      get
      {
        return errors.Count > 0;
      }
    }

    public bool IsValid
    {
      get
      {
        return !HasErrors;
      }
    }

    public void AddPropertyError(string propertyName, string error)
    {
      if (errors.ContainsKey(propertyName))
      {
        errors[propertyName].Add(error);
      }
      else
      {
        errors[propertyName] = new List<string>() { error };
      }
      NotifyErrorsChanged(propertyName);
    }

    public void RemovePropertyError(string propertyName)
    {
      if (errors.ContainsKey(propertyName))
      {
        errors.Remove(propertyName);
      }
      NotifyErrorsChanged(propertyName);
    }

    public void NotifyErrorsChanged(string propertyName)
    {
      ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
    }
  }
}
