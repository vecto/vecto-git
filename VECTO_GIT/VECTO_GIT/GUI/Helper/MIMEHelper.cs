﻿using LibGit2Sharp;
using System;
using System.Runtime.InteropServices;
using System.Text;


namespace VECTO_GIT.GUI.Helper
{
  public static class MIMEHelper
  {
    private static int MAX_SEARCH_LENGTH = 256;

    [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
    private extern static UInt32 FindMimeFromData(
      UInt32 pBC,
      [MarshalAs(UnmanagedType.LPStr)] String pwzUrl,
      [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
      UInt32 cbSize,
      [MarshalAs(UnmanagedType.LPStr)] String pwzMimeProposed,
      UInt32 dwMimeFlags,
      out UInt32 ppwzMimeOut,
      UInt32 dwReserverd
    );

    //https://stackoverflow.com/questions/2826808/how-to-identify-the-extension-type-of-the-file-using-c/2826884#2826884
    //https://stackoverflow.com/questions/2159150/how-to-free-intptr-in-c


    public static string GetFileTypeOfBlob(Blob blob, int blobSize)
    {
      if (blob == null)
        return null;

      var selectedLength = MAX_SEARCH_LENGTH;
      if (blobSize < MAX_SEARCH_LENGTH)
        selectedLength = blobSize;

      var buffer = new byte[selectedLength];


      using (var stream = blob.GetContentStream())
      {
        stream.Read(buffer, 0, selectedLength);
      }


      try
      {
        System.UInt32 mimetype;
        FindMimeFromData(0, null, buffer, Convert.ToUInt32(selectedLength), null, 0, out mimetype, 0);
        System.IntPtr mimeTypePtr = new IntPtr(mimetype);
        string mime = Marshal.PtrToStringUni(mimeTypePtr);
        Marshal.FreeCoTaskMem(mimeTypePtr);
        return mime;
      }
      catch (Exception e)
      {
        return null;
      }

    }


    public static string GetMimeFormFileContent(string fileContent)
    {
      if (string.IsNullOrEmpty(fileContent))
        return null;

      var selectedLength = MAX_SEARCH_LENGTH;
      if (fileContent.Length < MAX_SEARCH_LENGTH)
        selectedLength = fileContent.Length;

      var selectedContent = fileContent.ToCharArray(0, selectedLength);
      byte[] buffer = Encoding.UTF8.GetBytes(selectedContent, 0, selectedLength);

      try
      {
        System.UInt32 mimetype;
        FindMimeFromData(0, null, buffer, Convert.ToUInt32(selectedLength), null, 0, out mimetype, 0);
        System.IntPtr mimeTypePtr = new IntPtr(mimetype);
        string mime = Marshal.PtrToStringUni(mimeTypePtr);
        Marshal.FreeCoTaskMem(mimeTypePtr);
        return mime;
      }
      catch (Exception e)
      {
        return null;
      }

    }



  }
}
