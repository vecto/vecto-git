﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VECTO_GIT.GUI.Helper
{
  public class DelegateCommand : ICommand
  {
    private Action<object> execute;
    private Func<object, bool> canExecute;

    public DelegateCommand(Action<object> execute) : this(null, execute)
    {
    }

    public DelegateCommand(Func<object, bool> canExecute, Action<object> execute)
    {
      if (execute == null)
      {
        throw new ArgumentNullException("execute");
      }
      this.execute = execute;
      this.canExecute = canExecute;
    }

    public event EventHandler CanExecuteChanged
    {
      add { CommandManager.RequerySuggested += value; }
      remove { CommandManager.RequerySuggested -= value; }
    }

    public bool CanExecute(object parameter)
    {
      return (canExecute == null) ? true : canExecute(parameter);
    }

    public void Execute(object parameter)
    {
      try
      {
        execute(parameter);
      }
      catch (Exception e)
      {
        // TODO: Do something! message box?
      }
    }

  }
}
