﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;


namespace VECTO_GIT.GUI.Helper
{
  public class ObservableCollectionEx<T> : ObservableCollection<T>
  where T : INotifyPropertyChanged
  {

    public event PropertyChangedEventHandler CollectionItemChanged;

    public ObservableCollectionEx()
    {
      CollectionChanged += FullObservableCollectionCollectionChanged;
    }
    
    private void FullObservableCollectionCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.NewItems != null)
      {
        foreach (Object item in e.NewItems)
        {
          ((INotifyPropertyChanged)item).PropertyChanged += ItemPropertyChanged;
        }
      }
      if (e.OldItems != null)
      {
        foreach (Object item in e.OldItems)
        {
          ((INotifyPropertyChanged)item).PropertyChanged -= ItemPropertyChanged;
        }
      }
    }

    private void ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      this.CollectionItemChanged?.Invoke(sender, e);

      var args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, sender, sender, IndexOf((T)sender));
      OnCollectionChanged(args);         
   }
  }

}
