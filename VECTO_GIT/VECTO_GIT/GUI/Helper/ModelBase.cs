﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace VECTO_GIT.GUI.Helper
{
  public class ModelBase : INotifyPropertyChanged
  {
    #region INotifyPropertyChanged 
    public event PropertyChangedEventHandler PropertyChanged;

    protected bool SetProperty<T>(ref T field, T value, [CallerMemberName]string proptertyName = null)
    {
      bool propertyChanged = false;

      if (!EqualityComparer<T>.Default.Equals(field, value))
      {
        field = value;
        OnPropertyChanged(proptertyName);
        propertyChanged = true;
      }
      return propertyChanged;
    }

    protected void OnPropertyChanged([CallerMemberName]string propterty = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propterty));
    }
    #endregion
    
  }
}
