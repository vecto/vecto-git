﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VECTO_GIT.GUI.Helper
{
  public class CommandAsync : ICommand
  {
    private readonly Func<object, Task> executedMethod;
    private readonly Func<object, bool> canExecuteMethod;

    public event EventHandler CanExecuteChanged;
    public CommandAsync(Func<object, Task> execute) : this(execute, null) { }

    public CommandAsync(Func<object, Task> execute, Func<object, bool> canExecute)
    {
			if (execute == null) {
				throw new ArgumentNullException("execute");
			}
      this.executedMethod = execute;
      this.canExecuteMethod = canExecute;
    }

    public bool CanExecute(object parameter) => this.canExecuteMethod == null || this.canExecuteMethod((object)parameter);
    public async void Execute(object parameter) => await this.executedMethod((object)parameter);
    public void OnCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);
  }
}
