﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Helper
{
    public static class MimeTypeHelper
    {
    private static readonly Lazy<IDictionary<string, string>> mimeTypeMappings = new Lazy<IDictionary<string, string>>(BuildMappings);

      private static IDictionary<string, string> BuildMappings()
      {
        var mapping = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
          {"text/comma-separated-values",".csv"},
          {"text/css",".css"},
          {"text/html",".html"},
          {"text/javascript",".js"},
          {"text/plain",".txt"},
          {"text/xml",".xml"},
          {"application/pdf",".pdf"},
          {"image/svg+xml",".svg"},
          {"image/bmp", ".bmp"},
          {"image/jpeg", ".jpg"},
          {"image/pict", ".pic"},
          {"image/png", ".png"}, 
          {"image/x-png", ".png"}, 
          {"image/tiff", ".tiff"},
          {"image/gif",".gif"},
          {"image/x-macpaint", ".mac"},
          {"image/x-quicktime", ".qti"},
          {"image/x-icon",".ico"},
        };

        return mapping;
      }

      public static string GetFileExtension(string mimeType)
      {
        if (mimeType.IsNullOrEmptyOrWhitespace())
          return string.Empty;

        var fileExtension = string.Empty;

        if (mimeTypeMappings.Value.TryGetValue(mimeType, out fileExtension))
        {
          return fileExtension;
        }

        return fileExtension;
      }
    }
}
