﻿using LibGit2Sharp;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.Model;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace VECTO_GIT.GUI.Helper
{
  public static class GUIHelpers
  {

    public static bool IsValidMailAddress(string eMail)
    {
      try
      {
        var mailaddress = new MailAddress(eMail);
        return true;
      }
      catch
      {
        return false;
      }
    }


    public static string DisplayName(this Enum enumValue)
    {
      var enumType = enumValue.GetType();
      var memberInfo = enumType.GetMember(enumValue.ToString()).First();

      if (memberInfo == null || !memberInfo.CustomAttributes.Any()) return enumValue.ToString();

      var displayAttribute = memberInfo.GetCustomAttribute<DisplayAttribute>();

      if (displayAttribute == null) return enumValue.ToString();

      if (displayAttribute.ResourceType != null && displayAttribute.Name != null)
      {
        var manager = new ResourceManager(displayAttribute.ResourceType);
        return manager.GetString(displayAttribute.Name);
      }

      return displayAttribute.Name ?? enumValue.ToString();
    }

    public static string[] DisplayNamesFromEnum(IEnumerable<MergeActionType> enumValues)
    {
      var counter = 0;
      var result = new string[enumValues.Count()];
      foreach (var value in enumValues)
      {
        result[counter] = DisplayName(value);
        counter++;
      }
      return result;
    }

    public static string[] DisplayNamesFromEnum(IEnumerable<FileType> enumValues)
    {
      var counter = 0;
      var result = new string[enumValues.Count()];
      foreach (var value in enumValues)
      {
        result[counter] = DisplayName(value);
        counter++;
      }
      return result;
    }

    public static T GetValueFromName<T>(string name)
    {
      var type = typeof(T);
      if (!type.IsEnum) throw new InvalidOperationException();

      foreach (var field in type.GetFields())
      {
        var attribute = Attribute.GetCustomAttribute(field,
            typeof(DisplayAttribute)) as DisplayAttribute;
        if (attribute != null)
        {
          if (attribute.Name == name)
          {
            return (T)field.GetValue(null);
          }
        }
        else
        {
          if (field.Name == name)
            return (T)field.GetValue(null);
        }
      }

      throw new ArgumentOutOfRangeException("name");
    }


    public static ObservableCollection<SearchResultModel> SummarizeComponentSearchResult(IEnumerable<VectoComponent> searchResults)
    {
      var summerizeResult = new ObservableCollection<SearchResultModel>();

      foreach (var result in searchResults)
      {
        var searchResult = SummarizeComponentToSearchResult(result);
        summerizeResult.Add(searchResult);
      }
      return summerizeResult;
    }

    public static SearchResultModel SummarizeComponentToSearchResult(VectoComponent vectoComponent)
    {
      var searchResult = new SearchResultModel();

      IBranchName branchNaming;
      if (vectoComponent.PublicBranch != null)
      {
        branchNaming = vectoComponent.PublicBranchNaming;
      }
      else
      {
        branchNaming = vectoComponent.PrivateBranchNaming;
      }

      searchResult.GitId = vectoComponent.GetPublicGitId;
      searchResult.CertificateDate = vectoComponent.CertificateDate.Date;
      searchResult.CertificateNumber = vectoComponent.CertificateNumber;
      if (branchNaming != null)
      {
        var stringConverter = StringCharacterLookup.Instance;
        searchResult.Manufacturer = stringConverter.ConvertToOrigin(branchNaming.GetManufacturerName());
        searchResult.Model = stringConverter.ConvertToOrigin(branchNaming.GetModelName());
      }
      else
      {
        searchResult.Manufacturer = string.Empty;
        searchResult.Model = string.Empty;
      }

      searchResult.VectoComponent = vectoComponent;

      return searchResult;
    }

    public static void SaveFileDialog(Blob blob, string saveFilter, string fileName = "")
    {
      var saveFileDialog = new SaveFileDialog()
      {
        Filter = saveFilter,
        FileName = fileName
      };

      if (saveFileDialog.ShowDialog() == true)
      {
        using (var stream = blob.GetContentStream())
        {
          SaveFileByStream(stream, saveFileDialog.FileName);
        }
      }
    }

	public static void SaveFileDialog(XDocument doc, string saveFilter, string fileName = "")
	{
		var saveFileDialog = new SaveFileDialog() {
			Filter = saveFilter,
			FileName = fileName
		};

		if (saveFileDialog.ShowDialog() == true) {
			var writer = new XmlTextWriter( saveFileDialog.FileName, Encoding.UTF8);
			writer.Formatting = Formatting.Indented;
			doc.WriteTo(writer);
			writer.Flush();
			writer.Close();
		}
	}

		private static void SaveFileByStream(Stream stream, string destPath)
    {
      using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
      {
        stream.CopyTo(fileStream);
      }
    }

    public static string[] ShowSelectFilesDialog(bool multiselect, string initialDirectory = null)
    {
      using (var openFileDialog = new OpenFileDialog())
      {
        openFileDialog.InitialDirectory = initialDirectory;
        openFileDialog.Multiselect = multiselect;
        var result = openFileDialog.ShowDialog();

        if (result == DialogResult.OK)
        {
          return openFileDialog.FileNames;
        }
      }

      return null;
    }

    public static string ShowSelectDirectoryDialog(string initialDirectory = null)
    {
      using (var dialog = new CommonOpenFileDialog())
      {
        dialog.InitialDirectory = initialDirectory;
        dialog.IsFolderPicker = true;

        var result = dialog.ShowDialog();
        if (result == CommonFileDialogResult.Ok)
        {
          return dialog.FileName;
        }
      }

      return null;
    }


    public static void InterchangeDirectoryItem(string newDirPath, string oldDirPath, IList<List<Item>> itemList)
    {
      if (!newDirPath.IsNullOrEmptyOrWhitespace())
      {
        if (newDirPath != oldDirPath)
        {
          var itemPosition = -1;
          for (int i = 0; i < itemList.Count; i++)
          {
            var currentItem = itemList[i][0];
            if (currentItem is DirectoryItem)
            {
              if (currentItem.FilePath == newDirPath)
              {
                return;
              }

              if (currentItem.FilePath == oldDirPath)
              {
                itemPosition = i;
              }
            }
          }

          itemList[itemPosition] = GetFolderTreeViewItemFromPath(newDirPath);
        }
      }
    }

    public static void InterchangeFileItem(string newFilePath, string oldFilePath, IList<List<Item>> itemList)
    {
      if (!newFilePath.IsNullOrEmptyOrWhitespace())
      {
        if (newFilePath != oldFilePath)
        {
          var itemPosition = -1;
          for (int i = 0; i < itemList.Count; i++)
          {
            var currentItem = itemList[i][0];
            if (currentItem is FileItem)
            {
              if (currentItem.FilePath == newFilePath)
              {
                return;
              }

              if (currentItem.FilePath == oldFilePath)
              {
                itemPosition = i;
              }
            }
          }

          itemList[itemPosition] = GetGUIFileItem(newFilePath);
        }
      }
    }

    public static List<Item> GetFolderTreeViewItemFromPath(string path)
    {
      var tree = ItemProvider.GetItems(path);

      var rootItem = new DirectoryItem
      {
        Items = tree,
        Filename = new DirectoryInfo(path).Name,
        FilePath = path
      };

      return new List<Item> { rootItem };
    }


    public static List<Item> GetGUIFileItem(string filePath)
    {
      return new List<Item>
      {
        new FileItem()
        {
          Filename = Path.GetFileName(filePath),
          FilePath = filePath
        }
      };
    }
  }
}
