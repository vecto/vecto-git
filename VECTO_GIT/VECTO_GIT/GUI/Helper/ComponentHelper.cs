﻿using System;
using VECTO_GIT.GUI.View;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.Helper
{
	public static class ComponentHelper
	{
		public static string Name(this VectoSimulationViewModel.Components component)
		{
			switch (component) {
				case VectoSimulationViewModel.Components.Engine: return "Engine";
				case VectoSimulationViewModel.Components.Transmission:
				case VectoSimulationViewModel.Components.Axlegear:
				case VectoSimulationViewModel.Components.Angledrive:
				case VectoSimulationViewModel.Components.Retarder:
				case VectoSimulationViewModel.Components.Wheels:
				case VectoSimulationViewModel.Components.AirDrag:
				default: return component.DisplayName();
			}
		}

		public static string ComponentTypeNumber(this VectoSimulationViewModel.Components component)
		{
			switch (component) {
				case VectoSimulationViewModel.Components.Engine: return "E";
				case VectoSimulationViewModel.Components.Transmission: return "G";
				case VectoSimulationViewModel.Components.TorqueConverter: return "C";
				case VectoSimulationViewModel.Components.Axlegear: return "L";
				case VectoSimulationViewModel.Components.Angledrive: return "D";
				case VectoSimulationViewModel.Components.Retarder: return "O";
				case VectoSimulationViewModel.Components.Wheels: return "T";
				case VectoSimulationViewModel.Components.AirDrag: return "P";
				default: throw new ArgumentOutOfRangeException(nameof(component), component, null);
			}
		}
	}
}
