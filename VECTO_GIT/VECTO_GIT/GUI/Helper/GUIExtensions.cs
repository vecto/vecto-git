﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace VECTO_GIT.GUI.Helper
{
  public static class GUIExtensions
  {
    public static void CenterToSrcWindow(this Window destWindow, Window srcWindow)
    {
      if(srcWindow != null)
      {
        var srcWidth = destWindow.ActualWidth == 0 ? destWindow.Width : destWindow.ActualWidth;
        var srcHeight = destWindow.ActualHeight == 0 ? destWindow.Height : destWindow.ActualHeight;

        destWindow.Left = srcWindow.Left + (srcWindow.Width - srcWidth) / 2;
        destWindow.Top = srcWindow.Top + (srcWindow.Height - srcHeight) / 2;
      }
    }


  }
}
