﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using LibGit2Sharp;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class ContentViewCollapseConverter : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is KeyValuePair<string, Blob>)
      {
        var convert = (KeyValuePair<string, Blob>) value;
        if (convert.Value == null)
        {
          return Visibility.Collapsed;
        }
      }
      return Visibility.Visible;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
