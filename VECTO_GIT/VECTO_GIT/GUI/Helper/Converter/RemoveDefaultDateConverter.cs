﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class RemoveDefaultDateConverter : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is DateTime)
      {
        var convert = (DateTime)value;
        return convert == default(DateTime) ? string.Empty : value;
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
