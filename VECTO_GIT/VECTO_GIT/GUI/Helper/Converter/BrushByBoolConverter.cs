﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class BrushByBoolConverter : BaseConverter, IValueConverter 
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is KeyValuePair<string, bool>)
      {
        var convert = (KeyValuePair<string, bool>) value;
        return convert.Value ?  Brushes.Red : Brushes.Green;
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
