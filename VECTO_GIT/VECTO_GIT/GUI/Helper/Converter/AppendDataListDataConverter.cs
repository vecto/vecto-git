﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class AppendDataListDataConverter : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var convert = value as VectoComponent;
      if (convert != null)
      {
        if (convert.PrivateBranch != null)
        {
          var privateDataListing = new List<List<Item>>();
          if (convert.MeasurementData != null)
          {
            var rootDirectory = GetWithRootDirectory(
              ConfigValue.MEASUREMENT_DATA_TREE_NAME, convert.MeasurementData);
            privateDataListing.Add(rootDirectory);
          }

          if (convert.UserData != null)
          {
            var rootDirectory = GetWithRootDirectory(
              ConfigValue.USER_DATA_TREE_NAME, convert.UserData);
            privateDataListing.Add(rootDirectory);
          }
          return privateDataListing;
        }
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }

    private List<Item> GetWithRootDirectory(string rootName, List<Item> containingData)
    {
      var rootDirectory = new DirectoryItem()
      {
        Items = containingData,
        Filename = rootName
      };
      return new List<Item> { rootDirectory };
    }

  }
}
