﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using VECTO_GIT.ComponentNaming;

namespace VECTO_GIT.GUI.Helper.Converter
{
  class SplitPrivatBranchNameConverter :BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var convert = value as string;
      if (convert != null &&  convert.StartsWith(PrivateBranchNaming.TREE_NAME))
      {
        var stringConverter = StringCharacterLookup.Instance;
        var naming =  new PrivateBranchNaming(convert);
        return $"Manufacturer: {stringConverter.ConvertToOrigin(naming.Manufacturer)}"
               + Environment.NewLine
               + $"Model: {stringConverter.ConvertToOrigin(naming.Model)}";
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
