﻿using System;
using System.Windows.Markup;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public abstract class BaseConverter : MarkupExtension
  {
    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      return this;
    }
  }
}
