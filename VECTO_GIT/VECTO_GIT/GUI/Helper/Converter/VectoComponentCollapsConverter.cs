﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class VectoComponentCollapsConverter : BaseConverter, IValueConverter
  {
    private const string PRIVATE_DATA = "private";
    private const string PUBLIC_DATA = "public";

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var convert = parameter as string;
      var valueConvert = value as VectoComponent;
      if (convert != null && valueConvert != null)
      {
        if (convert.Equals(PRIVATE_DATA))
        {
          return valueConvert.PrivateBranch != null ? Visibility.Visible : Visibility.Collapsed;
        }

        if (convert.Equals(PUBLIC_DATA))
        {
          return (valueConvert.PublicBranch != null ) ?
            Visibility.Visible : Visibility.Collapsed;
        }

        return Visibility.Collapsed;
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
