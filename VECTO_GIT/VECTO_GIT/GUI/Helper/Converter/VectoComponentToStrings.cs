﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class VectoComponentToStrings : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var convert = value as VectoComponent;
      if (convert != null)
      {
        return FetchPublicData(convert);
      }
      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }

    private Dictionary<string, Blob> FetchPublicData(VectoComponent vectoComponent)
    {
      var files = new Dictionary<string, Blob>();

      if (vectoComponent.StandardValues != null)
      {
        files[ConfigValue.STANDARD_VALUES_FILE_NAME] = vectoComponent.StandardValues;
      }
      if (vectoComponent.ComponentData != null)
      {
        files[ConfigValue.COMPONENT_DATA_FILE_NAME] = vectoComponent.ComponentData;
      }
      if (vectoComponent.CertificateFile != null)
      {
        files[ConfigValue.CERTIFICATE_DATA_FILE_NAME] = vectoComponent.CertificateFile;
      }
      return files;
    }
  }
}
