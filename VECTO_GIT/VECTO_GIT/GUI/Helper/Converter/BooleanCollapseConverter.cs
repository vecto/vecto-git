﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class BooleanCollapseConverter : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is bool)
      {
        var convert = (bool)value;
        return convert ? Visibility.Visible : Visibility.Collapsed;
      }
      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
