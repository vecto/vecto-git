﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class OriginStringConvert : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var convert = value as string;
      if (convert != null)
      {
        return StringCharacterLookup.Instance.ConvertToOrigin(convert);
      }
      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
