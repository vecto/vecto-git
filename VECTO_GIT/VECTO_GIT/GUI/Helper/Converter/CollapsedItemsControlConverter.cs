﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class CollapsedItemsControlConverter : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is string)
      {
        return Visibility.Visible;
      }

      if(value is int)
      {
        var count = (int)value;
        return (count > 0) ? Visibility.Visible : Visibility.Collapsed;
      }

      if(value == null)
      {
        return Visibility.Collapsed;
      }

      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
