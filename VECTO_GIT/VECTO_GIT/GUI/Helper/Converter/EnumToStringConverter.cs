﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Controls;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.Helper.Converter
{
  public class EnumToStringConverter : BaseConverter, IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is IEnumerable<FileType>)
      {
        var enumFormat = value as IEnumerable<FileType>;
        if (enumFormat != null && enumFormat.Any())
        {
          return GUIHelpers.DisplayNamesFromEnum(enumFormat);
        }
      }
      else if(value is IEnumerable<MergeActionType>)
      {
        var enumFormat = value as IEnumerable<MergeActionType>;
        if (enumFormat != null && enumFormat.Any())
        {
          return GUIHelpers.DisplayNamesFromEnum(enumFormat);
        }
      }
      else if (value is List <Item>)
      {
        var fileType = value as List<Item>;
        return fileType[0].FileType.DisplayName();
      }


      return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var format = value as string;
      if (value != null && typeof(FileType) == targetType)
      {
        return GUIHelpers.GetValueFromName<FileType>(format);
      }

      if (value != null && typeof(MergeActionType) == targetType)
      {
        return GUIHelpers.GetValueFromName<MergeActionType>(format);
      }

      return null;
    }
  }
}
