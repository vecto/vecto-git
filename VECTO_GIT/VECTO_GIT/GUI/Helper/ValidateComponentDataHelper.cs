﻿using System.Collections.Generic;
using System.IO;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.Helper
{
  public class ValidateComponentDataHelper
  {
    #region Members
    private const string XML_EXTENSION = ".xml";
    private const int STD_VALUES_COUNT = 1;
    private const int CERT_DATA_COUNT = 1;
    private const int CMP_DATA_COUNT = 1;

    private IList<List<Item>> selectedFiles;

    private int allowedCompDataCount;
    private int allowedCertDataCount;
    private int allowedStdValuesCount;
    private int measurementDataCount;
    private int userDataCount;

    private bool compDataMessagePosted;
    private bool compDataAlreadySaved;
    private bool certDataAlreadySaved;
    private bool stdDataAlreadySaved;
    
    #endregion

    #region Properties

    public List<string> ErrorMessages { get; set; }

    #endregion

    public ValidateComponentDataHelper() { }

    public ValidateComponentDataHelper(IEnumerable<FileType> fileDataTypes)
    {
      SetupExistingSavedData(fileDataTypes);
    }

    private void SetupExistingSavedData(IEnumerable<FileType> fileDataTypes)
    {

      foreach (var dataType in fileDataTypes)
      {
        switch (dataType)
        {
          case FileType.ComponentData:
            compDataAlreadySaved = true;
            break;
          case FileType.CertificateData:
            certDataAlreadySaved = true;
            break;
          case FileType.StandardValues:
            stdDataAlreadySaved = true;
            break;
        }
      }
    }


    private void SetupInitValues(IList<List<Item>> files)
    {
      ErrorMessages = new List<string>();
      selectedFiles = files;

      allowedCompDataCount = CMP_DATA_COUNT;
      allowedCertDataCount = CERT_DATA_COUNT;
      allowedStdValuesCount = STD_VALUES_COUNT;

      measurementDataCount = 0;
      userDataCount = 0;
      compDataMessagePosted = false;
    }

    public bool Validate(IList<List<Item>> files)
    {
      SetupInitValues(files);
      ValidateSelectedFiles(files);

      return ErrorMessages.Count <= 0;
    }


    private void ValidateFile(List<Item> file)
    {
      if (file == null || file?.Count == 0)
        return;

      string selectionMessage = null;
      var currentItem = file[0];

      if(currentItem == null)
        return;

      switch (currentItem.FileType)
      {
        case FileType.NothingSelected:
          IsNoDataTypeSelected(file);
          break;
        case FileType.ComponentData:
          if (!compDataAlreadySaved && IsSingleFile(file))
          {
            selectionMessage = ValidateComponentData();
            IsXml(currentItem);
          }
          break;
        case FileType.StandardValues:
          if (!stdDataAlreadySaved && IsSingleFile(file))
          {
            selectionMessage = ValidateStandardValues();
            IsXml(currentItem);
          }
          break;
        case FileType.CertificateData:
          if (!certDataAlreadySaved && IsSingleFile(file))
          {
            selectionMessage = ValidateCertificateData();
          }
          break;
        case FileType.MeasurementData:
          CountMeasurementData();
          break;
        case FileType.UserData:
          CountUserData();
          break;
      }

      if (selectionMessage != null)
      {
        ErrorMessages.Add(selectionMessage);
      }
    }


    private void ValidateSelectedFiles(IList<List<Item>> selectedData)
    {
      if (selectedData.IsNullOrEmpty())
        return;

      for (int i = 0; i < selectedData.Count; i++)
      {
        ValidateFile(selectedData[i]);
      }

      ValidateStandardValuesSelections(selectedFiles);
    }

    private bool IsSingleFile(List<Item> selectedFile)
    {
      if (selectedFile[0] is DirectoryItem)
      {
        var errorMessage = string.Format("Only a single File as \"{0}\" is allowed!",
            GUIHelpers.DisplayName(selectedFile[0].FileType));

        ErrorMessages.Add(errorMessage);
        return false;
      }
      return true;
    }

    private void IsNoDataTypeSelected(List<Item> selectedFile)
    {
      var filetype = selectedFile[0] is DirectoryItem ? "Folder" : "File";
      ErrorMessages.Add($"No data type selected for selected {filetype} \"{selectedFile[0].Filename}\"!");
    }

    private void ValidateStandardValuesSelections(IList<List<Item>> selectedData)
    {
      if (allowedStdValuesCount == 0)
      {
        if (allowedCompDataCount == CMP_DATA_COUNT)
        {

          if (selectedData.Count > STD_VALUES_COUNT)
          {
            ErrorMessages.Add("A \"Standard Values\" File can only be saved alone or in combination with an \"Component Data\" File.");
          }
        }
      }
      else
      {
        if (!compDataAlreadySaved && !compDataMessagePosted)
          IsComponentDataSelected();
      }
    }


    private void IsComponentDataSelected()
    {

      if (allowedCompDataCount == 1)
      {
        ErrorMessages.Insert(0, "A \"Component Data\" File must be selected!");
        compDataMessagePosted = true;
      }
    }

    private string ValidateComponentData()
    {
      string result = null;
      if (allowedCompDataCount == 0)
      {
        result = "Only one \"Component Data\" File is per save allowed!";
      }
      allowedCompDataCount--;
      return result;
    }

    private string ValidateStandardValues()
    {
      string result = null;
      if (allowedStdValuesCount == 0)
      {
        result = "Only one \"Standard Values\" File is per save allowed!";
      }
      allowedStdValuesCount--;
      return result;
    }

    private string ValidateCertificateData()
    {
      string result = null;
      if (allowedCertDataCount == 0)
      {
        result = "Only one \"Certificate Data\" File is per save allowed!";
      }
      allowedCertDataCount--;
      return result;
    }

    private void CountUserData()
    {
      userDataCount++;
    }
    private void CountMeasurementData()
    {
      measurementDataCount++;
    }

    private void IsXml(Item selectedFile)
    {
      if (selectedFile?.FilePath != null && !Path.GetExtension(selectedFile.FilePath).ToLower().Equals(XML_EXTENSION))
      {
        ErrorMessages.Add(
          $"{GUIHelpers.DisplayName(selectedFile.FileType)} are only accepted as XML-Files, which the selected File" +
          $" \"{selectedFile.Filename}\" not fulfills!");
      }
    }
  }
}
