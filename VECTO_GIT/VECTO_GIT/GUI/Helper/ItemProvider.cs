﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.Helper
{
  public static class ItemProvider
  {
    public static List<Item> GetItems(string path)
    {
      var items = new List<Item>();
      var dirInfo = new DirectoryInfo(path);
      
      foreach (var directory in dirInfo.GetDirectories())
      {
        var item = new DirectoryItem
        {
          Filename = directory.Name,
          FilePath = directory.FullName,
          Items = GetItems(directory.FullName)
        };

        items.Add(item);
      }

      foreach (var file in dirInfo.GetFiles())
      {
        var item = new FileItem
        {
          Filename = file.Name,
          FilePath = file.FullName
        };

        items.Add(item);
      }

      return items;
    }

  }
}
