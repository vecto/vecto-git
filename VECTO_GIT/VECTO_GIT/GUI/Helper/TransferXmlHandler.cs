﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.Helper
{
  public class TransferXmlHandler
  {
    private readonly XmlSerializer xmlSerializer;

    public List<ComponentForTransfer> TransferList { get; set; }

    public TransferXmlHandler()
    {
      xmlSerializer = new XmlSerializer(typeof(ComponentForTransfer));
    }

    public void SerializeTransferComponent(ComponentForTransfer component, string repositoryPath)
    {
      CreateTransferFolder(repositoryPath);
      var filePath = Path.Combine(GetTransferFolderPath(repositoryPath), component.Filename);
      using (var writer = new StreamWriter(filePath))
      {
        xmlSerializer.Serialize(writer, component);
      }
    }

    public bool DeletTransferComponent(ComponentForTransfer component, string repositoryPath)
    {
      var folderPath = GetTransferFolderPath(repositoryPath);
      if (Directory.Exists(folderPath))
      {
        var filePath = Path.Combine(GetTransferFolderPath(repositoryPath), component.Filename);
        if (File.Exists(filePath))
        {
          try
          {
            File.Delete(filePath);
            return true;
          }
          catch (Exception ex)
          {
            return false;
          }
        }
      }
      return false;
    }


    public ComponentForTransfer GetRelatedComponentForTransfer(string repositoryPath, string manufacturerName, string modelName)
    {
      SetTransferList(repositoryPath);

      if (!TransferList.IsNullOrEmpty())
      {
        var convertManufacturer = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(manufacturerName);
        var convertModel = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(modelName);

        for (int i = 0; i < TransferList.Count; i++)
        {
          var component = TransferList[i];
          if (component.ManufacturerName == convertManufacturer &&
              component.ModelName == convertModel)
          {
            return component;
          }
        }

      }

      return null;
    }




    private void SetTransferList(string repositoryPath)
    {
      var existingFilePaths = FetchAllFilesFromDirectory(repositoryPath);
      TransferList = new List<ComponentForTransfer>();


      if (existingFilePaths != null)
      {
        for (var i = 0; i < existingFilePaths.Length; i++)
        {
          var filePath = existingFilePaths[i];

          using (var reader = new StreamReader(filePath))
          {
            var component = xmlSerializer.Deserialize(reader) as ComponentForTransfer;
            TransferList.AddIfNotNull(component);
          }
        }
      }

    }


    public void UpdateTransferList(string repositoryPath)
    {
      CreateTransferFolder(repositoryPath);
      SetTransferList(repositoryPath);
    }


    private string GetTransferFolderPath(string repositoryPath)
    {
      return Path.Combine(repositoryPath, ConfigValue.TRANSFER_FOLDER_SETTINGS);
    }

    private void CreateTransferFolder(string repositoryPath)
    {
      if (!Directory.Exists(GetTransferFolderPath(repositoryPath)))
      {
        Directory.CreateDirectory(GetTransferFolderPath(repositoryPath));
      }
    }


    private string[] FetchAllFilesFromDirectory(string repositoryPath)
    {
      if (Directory.Exists(GetTransferFolderPath(repositoryPath)))
      {
        return Directory.GetFiles(GetTransferFolderPath(repositoryPath), "*.xml", SearchOption.TopDirectoryOnly);
      }
      return null;
    }
  }
}
