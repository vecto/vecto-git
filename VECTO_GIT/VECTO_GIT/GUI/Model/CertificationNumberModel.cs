﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Model
{
  public class CertificationNumberModel
  {
    private const string CONNECTION_SYMBOL = "*";

    private string country;
    private string certificationAct;
    private string amendingAct;
    private string componentType;
    private string baseNumber;
    private string extensionNumber;

    public string ConnectionSymbol
    {
      get
      {
        return StringCharacterLookup.Instance.ConvertToFileSystemCompatible(CONNECTION_SYMBOL);
      }
    }
    public string Country
    {
      get
      {
        return country;
      }
      set
      {
        country = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }

    public string CertificationAct
    {
      get
      {
        return certificationAct;
      }
      set
      {
        certificationAct = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }

    public string AmendingAct
    {
      get
      {
        return amendingAct;
      }
      set
      {
        amendingAct = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }

    public string ComponentType
    {
      get
      {
        return componentType;
      }
      set
      {
        componentType = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }

    public string BaseNumber
    {
      get
      {
        return baseNumber;
      }
      set
      {
        baseNumber = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }

    public string ExtensionNumber
    {
      get
      {
        return extensionNumber;
      }
      set
      {
        extensionNumber = StringCharacterLookup.Instance.ConvertToFileSystemCompatible(value).ToLower();
      }
    }


    public bool AnyCertificateSearchParts()
    {
      return !string.IsNullOrEmpty(country) || !string.IsNullOrWhiteSpace(country) ||
        !string.IsNullOrEmpty(certificationAct) || !string.IsNullOrWhiteSpace(certificationAct) ||
        !string.IsNullOrEmpty(amendingAct) || !string.IsNullOrWhiteSpace(amendingAct) ||
        !string.IsNullOrEmpty(componentType) || !string.IsNullOrWhiteSpace(componentType) ||
        !string.IsNullOrEmpty(baseNumber) || !string.IsNullOrWhiteSpace(baseNumber) ||
        !string.IsNullOrEmpty(extensionNumber) || !string.IsNullOrWhiteSpace(extensionNumber);
    }

    public int GetCertificateSearchPartsCount()
    {
      var counter = 0;

      if (!string.IsNullOrWhiteSpace(Country))
        counter++;

      if (!string.IsNullOrWhiteSpace(CertificationAct))
        counter++;

      if (!string.IsNullOrWhiteSpace(AmendingAct))
        counter++;

      if (!string.IsNullOrWhiteSpace(ComponentType))
        counter++;

      if (!string.IsNullOrWhiteSpace(BaseNumber))
        counter++;

      if (!string.IsNullOrWhiteSpace(ExtensionNumber))
        counter++;

      return counter;
    }
  }
}
