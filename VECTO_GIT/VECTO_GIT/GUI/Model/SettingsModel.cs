﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Model
{
  public class SettingsModel
  {
    #region Properties

    public string RepositoryPath
    {
      get
      {
        return Properties.Settings.Default.RepositoryPath;        
      }
      set
      {
        Properties.Settings.Default.RepositoryPath = value;
        Properties.Settings.Default.Save();
      }
    }

    public string CommitterName
    {
      get
      {
        return Properties.Settings.Default.CommitterName;
      }
      set
      {
        Properties.Settings.Default.CommitterName = value;
        Properties.Settings.Default.Save();
      }
    }
    
    public string CommitterEMail
    {
      get
      {
        return Properties.Settings.Default.CommitterEMail;
      }
      set
      {
        Properties.Settings.Default.CommitterEMail = value;
        Properties.Settings.Default.Save();
      }
    }
    
    #endregion
  }
}
