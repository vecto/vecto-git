﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Model
{
  public class ComponentData
  {
    public string Name { get; set; }
    public string SHA1 { get; set; }
  }
}
