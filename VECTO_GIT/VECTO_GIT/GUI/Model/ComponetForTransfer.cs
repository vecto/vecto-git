﻿using LibGit2Sharp;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.GUI.Model
{
  public class ComponentForTransfer
  {
    //https://stackoverflow.com/questions/5818513/xml-serialization-hide-null-values
    //https://docs.microsoft.com/en-us/dotnet/framework/winforms/controls/defining-default-values-with-the-shouldserialize-and-reset-methods

    public string Filename
    {
      get { return string.Format("{0}_{1}.xml", ManufacturerName, ModelName); }
    }

    public string ManufacturerName { get; set; }
    public string ModelName { get; set; }


    public ComponentStandardBranch StandardBranch { get; set; }
    public ComponentPublicBranch PublicBranch { get; set; }
    public ComponentPrivateBranch PrivatBranch { get; set; }


    public bool ShouldSerializeStandardBranch()
    {
      return StandardBranch != null;
    }

    public bool ShouldSerializePublicBranch()
    {
      return PublicBranch != null;
    }

    public bool ShouldSerializePrivateBranch()
    {
      return PrivatBranch != null;
    }

    [XmlIgnore]
    public bool TransferStandardBranch
    {
      get { return StandardBranch != null && StandardBranch.TransferBranch; }
    }

    [XmlIgnore]
    public bool TransferPublicBranch
    {
      get { return PublicBranch != null && PublicBranch.TransferBranch; }
    }

    [XmlIgnore]
    public bool TransferPrivateBranch
    {
      get { return PrivatBranch != null && PrivatBranch.TransferBranch; }
    }

    public void SetComponentForTransferContent(VectoComponent vectoComponent,
      bool transferPubData, bool transferPrvData, bool transferStdData)
    {
      if (vectoComponent.PublicBranch != null)
      {
        PublicBranch = GetComponentPublicBranch(vectoComponent, transferPubData);
      }

      if (vectoComponent.PrivateBranch != null)
      {
        PrivatBranch = GetComponentPrivateBranch(vectoComponent, transferPrvData);
      }

      if (vectoComponent.StandardValues != null)
      {
        StandardBranch = GetComponentStandardBranch(vectoComponent, transferStdData);
      }

      SetTransferFileName(vectoComponent);
    }


    private ComponentPublicBranch GetComponentPublicBranch(VectoComponent vectoComponent, bool transferPubData)
    {
      return new ComponentPublicBranch()
      {
        BranchName = vectoComponent.PublicBranch.FriendlyName,
        TransferBranch = transferPubData
      };
    }


    private ComponentPrivateBranch GetComponentPrivateBranch(VectoComponent vectoComponent, bool transferPrvData)
    {
      return new ComponentPrivateBranch
      {
        BranchName = vectoComponent.PrivateBranch.FriendlyName,
        TransferBranch = transferPrvData
      };
    }


    private ComponentStandardBranch GetComponentStandardBranch(VectoComponent vectoComponent, bool transferStdData)
    {
      string branchName;
      string stdCommitId = string.Empty;
      if (StandardDataCommit.IsStandardValuesBranch(vectoComponent.PublicBranch))
      {
        branchName = vectoComponent.PublicBranch.FriendlyName;
      }
      else
      {
        var stdCommit = StandardDataCommit.GetStandardDataCommit(vectoComponent.PublicBranch);
        var stdBlob = StandardDataCommit.GetStandardDataBlob(stdCommit);
        var pubBranchNaming = ReadPublicBranchNaming(stdBlob);
        branchName = pubBranchNaming.GetGitPathBranchName();
        stdCommitId = stdCommit.Sha;
      }

      return new ComponentStandardBranch
      {
        StandardCommitId = stdCommitId,
        BranchName = branchName,
        TransferBranch = transferStdData
      };
    }

    private PublicBranchNaming ReadPublicBranchNaming(Blob componentDataBlob)
    {
      using (var streamReader = new StreamReader(componentDataBlob.GetContentStream(), Encoding.Default))
      {
        var xmlReader = new XmlContentReader();
        return xmlReader.ReadOutBranchNameData(streamReader);
      }
    }

    private void SetTransferFileName(VectoComponent vectoComponent)
    {
      if (vectoComponent.PublicBranchNaming != null)
      {
        ManufacturerName = vectoComponent.PublicBranchNaming.GetManufacturerName();
        ModelName = vectoComponent.PublicBranchNaming.GetModelName();
      }
      else if (vectoComponent.PrivateBranchNaming != null)
      {
        ManufacturerName = vectoComponent.PrivateBranchNaming.GetManufacturerName();
        ModelName = vectoComponent.PrivateBranchNaming.GetModelName();
      }
    }
  }
}
