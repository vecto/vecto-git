﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Model
{
  public class ComponentBranch
  {
    public string BranchName { get; set; }
    public bool TransferBranch { get; set; }
  }
}
