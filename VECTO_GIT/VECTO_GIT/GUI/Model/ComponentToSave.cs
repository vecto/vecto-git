﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Linq;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.GUI.Model
{
  public class ComponentToSave
  {
    public Branch StandardValuesBranch { get; set; }
    public Branch ComponentDataBranch { get; set; }
    public Branch PrivateDataBranch { get; set; }

    public string ComponentDataFilePath { get; set; }
    public string CertificatetDataFilePath { get; set; }
    public string StandardValuesFilePath { get; set; }

    public List<Item> SelectedUserData { get; set; }
    public List<Item> SelectedMeasurementData { get; set; }

    public ComponentToSave(IList<List<Item>> selectedData, VectoComponent vectoComponent = null)
    {
      SelectedUserData = new List<Item>();
      SelectedMeasurementData = new List<Item>();
      SetRelatedBranches(vectoComponent);
      SplitUpSelectedData(selectedData);
    }

    private void SplitUpSelectedData(IList<List<Item>> selectedData)
    {
      if (selectedData == null)
        return;

      for (int i = 0; i < selectedData.Count; i++)
      {
        var data = selectedData[i][0];

        switch (data.FileType)
        {
          case FileType.ComponentData:
            ComponentDataFilePath = data.FilePath;
            break;
          case FileType.StandardValues:
            StandardValuesFilePath = data.FilePath;
            break;
          case FileType.CertificateData:
            CertificatetDataFilePath = data.FilePath;
            break;
          case FileType.MeasurementData:
            SelectedMeasurementData.Add(data);
            break;
          case FileType.UserData:
            SelectedUserData.Add(data);
            break;
        }
      }
    }

    private void SetRelatedBranches(VectoComponent vectoComponent)
    {
      if (vectoComponent == null)
        return;

      PrivateDataBranch = vectoComponent.PrivateBranch;

      if (StandardDataCommit.IsStandardValuesBranch(vectoComponent.PublicBranch))
        StandardValuesBranch = vectoComponent.PublicBranch;
      else
        ComponentDataBranch = vectoComponent.PublicBranch;
    }
  }
}
