﻿using System;
using VECTO_GIT.ComponentSearch;

namespace VECTO_GIT.GUI.Model
{
  public class SearchResultModel
  {
    public string Manufacturer { get; set; }
    public string Model { get; set; }
    public DateTime CertificateDate { get; set; }
    public string CertificateNumber { get; set; }
    public string GitId { get; set; }
    
    public VectoComponent VectoComponent { get; set; }
  }
}
