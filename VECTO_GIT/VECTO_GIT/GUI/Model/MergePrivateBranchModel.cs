﻿using System.Collections.Generic;
using LibGit2Sharp;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.Model
{
  public class MergePrivateBranchModel : ModelBase
  {
    private Branch localPrivateBranch;
    private Branch remotePrivateBranch;
    private List<MergeEntityModel> entityConflicts;
    private List<string> usedMeasurementNames;
    private List<string> usedUserDataNames;

    public Branch LocalPrivateBranch
    {
      get { return localPrivateBranch; }
      set { SetProperty(ref localPrivateBranch, value); }
    }
    public Branch RemotePrivateBranch
    {
      get { return remotePrivateBranch; }
      set { SetProperty(ref remotePrivateBranch, value); }
    }

    public List<MergeEntityModel> EntityConflicts
    {
      get { return entityConflicts; }
      set { SetProperty(ref entityConflicts, value); }
    }

    public List<TreeEntry> NewUserDataEntries { get; set; }

    public List<TreeEntry> NewMeasurementEntries { get; set; }


    public List<string> UsedMeasurementNames
    {
      get { return usedMeasurementNames; }
      set { SetProperty(ref usedMeasurementNames, value); }
    }

    public List<string> UsedUserDataNames
    {
      get { return usedUserDataNames; }
      set { SetProperty(ref usedUserDataNames, value); }
    }

    public MergePrivateBranchModel(Branch localPrivateBranch, Branch remotePrivateBranch)
    {
      this.localPrivateBranch = localPrivateBranch;
      this.remotePrivateBranch = remotePrivateBranch;
      entityConflicts = new List<MergeEntityModel>();
      usedMeasurementNames = new List<string>();
      usedUserDataNames = new List<string>();
    }

    public void SetEntityConflicts(List<MergeEntityModel> conflicts)
    {
      if (!conflicts.IsNullOrEmpty())
      {
        entityConflicts.AddRange(conflicts);
      }
    }

    public void SetAlreadyUsedFileNames(List<TreeEntry> entries, FileType fileType)
    {
      if (fileType == FileType.MeasurementData)
      {
        var filenames = GetFilenames(entries);
        if (filenames != null)
          usedMeasurementNames.AddRange(filenames);
      }
      else if (fileType == FileType.UserData)
      {
        var filenames = GetFilenames(entries);
        if (filenames != null)
          usedUserDataNames.AddRange(GetFilenames(entries));
      }
    }

    private List<string> GetFilenames(List<TreeEntry> entries)
    {
      if (entries.IsNullOrEmpty())
        return null;

      var filenames = new List<string>();
      for (int i = 0; i < entries.Count; i++)
      {
        filenames.Add(entries[i].Name);
      }
      return filenames;
    }
  }
}
