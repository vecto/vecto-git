﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.Model
{

  public enum MergeActionType
  {
    NothingSelected,
    [Display(Name = "Replace with imported Data")]
    Replace,
    [Display(Name = "Rename Data")]
    Rename,
    [Display(Name = "Ignore Data")]
    Ignore
  }

  public class MergeEntityModel : ModelBase
  {
    private MergeActionType mergeAction;

    public MergeEntityModel(TreeEntry localTreeEntry, TreeEntry remoteTreeEntry, FileType dataType)
    {
      LocalTreeEntry = localTreeEntry;
      RemoteTreeEntry = remoteTreeEntry;
      DataType = dataType;
    }

    public FileType DataType { get; }
    public TreeEntry LocalTreeEntry { get; }
    public TreeEntry RemoteTreeEntry { get; }
    public string FileName { get; set; }

    public MergeActionType MergeAction
    {
      get { return mergeAction; }
      set { SetProperty(ref mergeAction, value); }
    }
  }
}
