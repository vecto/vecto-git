﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using LibGit2Sharp;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.Model
{

  public delegate void TreeviewItemSelectedHandler(Item item);

  public class Item : ModelBase
  {
    private string originFilename;
    private string filename;
    private string filePath;
    private FileType fileType;
    private GitObject existingBlob;
    private TreeEntryTargetType existingBlobEntryType;
    private bool isSelected;

    public static event TreeviewItemSelectedHandler OnItemSelected = delegate { };

    public bool IsSelected
    {
      get { return isSelected; }
      set
      {
        isSelected = value;
        if (value)
          OnItemSelected(this);
      }
    }

    public string OriginFilename
    {
      get { return originFilename; }
      set
      {
        originFilename = value;
        Filename = originFilename;
      }
    }


    public string Filename
    {
      get { return filename; }
      set { SetProperty(ref filename, value); }
    }

    public string FilePath
    {
      get { return filePath; }
      set { SetProperty(ref filePath, value); }
    }

    public FileType FileType
    {
      get { return fileType; }
      set { SetProperty(ref fileType, value); }
    }

    public GitObject ExistingBlob
    {
      get { return existingBlob; }
      set { SetProperty(ref existingBlob, value); }
    }

    public TreeEntryTargetType ExistingBlobEntryType
    {
      get { return existingBlobEntryType; }
      set { SetProperty(ref existingBlobEntryType, value); }
    }

  }
}
