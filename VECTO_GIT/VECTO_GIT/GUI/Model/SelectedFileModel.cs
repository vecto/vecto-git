﻿using System.ComponentModel.DataAnnotations;
using VECTO_GIT.GUI.Helper;

namespace VECTO_GIT.GUI.Model
{

  public enum FileType
  {
    NothingSelected,
    [Display(Name = "Component Data")]
    ComponentData,
    [Display(Name = "Standard Values")]
    StandardValues,
    [Display(Name = "Certificate File")]
    CertificateData,
    [Display(Name = "Measurement Data")]
    MeasurementData,
    [Display(Name = "User Data")]
    UserData
  }

  public class SelectedFileModel : ModelBase
  {

    private FileType relatedFileType;
    public FileType RelatedFileType
    {
      get
      {
        return relatedFileType;
      }
      set
      {
        SetProperty(ref relatedFileType, value);
      }
    }
    private string fileName;
    public string FileName
    {
      get
      {
        return fileName;
      }
      set
      {
        SetProperty(ref fileName, value);
      }
    }
    private string filePath;
    public string FilePath {
      get
      {
        return filePath;
      }
      set
      {
        SetProperty(ref filePath, value);
      }
    }
  }
}
