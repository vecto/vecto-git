﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Model
{
  public class ComponentPublicBranch : ComponentBranch
  {
    public List<ComponentData> ComponentData { get; set; }
  }
}
