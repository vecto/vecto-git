﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.GUI.Model
{
  public class ComponentStandardBranch : ComponentBranch
  {
    public string StandardCommitId { get; set; }
  }
}
