﻿using LibGit2Sharp;
using MahApps.Metro.Controls;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for ContentWindow.xaml
  /// </summary>
  public partial class ContentWindow : MetroWindow
  {
    public ContentWindow(INinjectFactory ninjectFactory, Blob blob, string blobName, int blobSize)
    {
      InitializeComponent();
      DataContext = new ContentWindowViewModel(ninjectFactory, blob, blobName, blobSize);
    }
  }
}
