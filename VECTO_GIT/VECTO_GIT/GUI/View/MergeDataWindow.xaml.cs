﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using VECTO_GIT.GUI.DI;
using VECTO_GIT.GUI.Helper;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.ViewModel;
using MenuItem = System.Windows.Controls.MenuItem;

namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for MergeDataWindow.xaml
  /// </summary>
  public partial class MergeDataWindow : MetroWindow
  {

    private readonly MergeDataWindowViewModel viewModel;

    public MergeDataWindow(INinjectFactory ninjectFactory, List<MergePrivateBranchModel> mergeConflicts)
    {
      InitializeComponent();
      viewModel = new MergeDataWindowViewModel(ninjectFactory, mergeConflicts);
      DataContext = viewModel;
      SetGrouping();

      ShowDataMenu.Command = viewModel.ShowDataCommand;
    }

    private void SetGrouping()
    {
      ICollectionView mergeGrouping = CollectionViewSource.GetDefaultView(viewModel.AllConflictItems);
      mergeGrouping.GroupDescriptions.Add(new PropertyGroupDescription("BranchName"));
      MergeDataGrid.DataContext = mergeGrouping;
    }

    private void MarkSelectedLocalTreeItem(object sender, MouseButtonEventArgs e)
    {
      var convert = sender as TreeViewItem;
      if (convert != null && convert.DataContext is FileItem)
      {
        var fileItem = convert.DataContext as FileItem;
        fileItem.IsLocalFileItem = true;
        convert.IsSelected = true;
      }
    }
    
    private void MarkSelectedRemoteTreeItem(object sender, MouseButtonEventArgs e)
    {
      var convert = sender as TreeViewItem;
      if (convert != null && convert.DataContext is FileItem)
      {
        convert.IsSelected = true;
      }
    }

    private void MergeDataGrid_OnLoadingRow(object sender, DataGridRowEventArgs e)
    {
      var convert = sender as DataGrid;
      var index = e.Row.GetIndex();
      if (index <= viewModel?.AllConflictItems?.Count)
      {
        var fileType = viewModel?.AllConflictItems[index]?.LocalConflict?.First()?.FileType;
        if (convert != null && fileType == FileType.MeasurementData)
        {
          convert.RowHeaderWidth = 115;
        }
        else if (convert != null)
        {
          convert.RowHeaderWidth = 65;
        }
        e.Row.Header = "  " + fileType.DisplayName();
      }

    }
  }
}
