﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.View
{
  public partial class PDFContentView : UserControl
  {
    private readonly PDFContentViewModel viewModel;

    #region Properties

    public static readonly DependencyProperty SetPdfContentProperty =
      DependencyProperty.Register("PdfBlobContent", typeof(KeyValuePair<string, Blob>), typeof(PDFContentView),
        new PropertyMetadata(default(KeyValuePair<string, Blob>), OnPdfContentChanged));
    
    public KeyValuePair<string, Blob> PdfBlobContent
    {
      get { return (KeyValuePair<string, Blob>)GetValue(SetPdfContentProperty); }
      set { SetValue(SetPdfContentProperty, value); }
    }

    #endregion


    public PDFContentView()
    {
      InitializeComponent();
      Loaded += UserControlLoaded;
      viewModel = new PDFContentViewModel();
      DataContext = viewModel;
    }



    private static void OnPdfContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var pdfView = d as PDFContentView;
      if (pdfView != null)
      {
        pdfView.OnPdfContentChanged(e);
      }
    }

    private void OnPdfContentChanged(DependencyPropertyChangedEventArgs e)
    {
      if (viewModel != null && e.NewValue is KeyValuePair<string, Blob>)
      {
        var selectedBlob = (KeyValuePair<string, Blob>)e.NewValue;
        viewModel.LoadPdfContent(PDFContentGrid, selectedBlob);
      }
    }


    void UserControlLoaded(object sender, RoutedEventArgs e)
    {
      var parentWindow = Window.GetWindow(this);
      if (parentWindow != null)
      {
        parentWindow.Closing += ParentWindowClosing;
      }
    }
    void ParentWindowClosing(object sender, CancelEventArgs e)
    {
      viewModel.Dispose();
    }
  }
}
