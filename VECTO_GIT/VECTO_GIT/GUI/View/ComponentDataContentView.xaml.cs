﻿using System.Windows.Controls;
using System.Windows.Input;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for ComponentDataContentView.xaml
  /// </summary>
  public partial class ComponentDataContentView : UserControl
  {
    public ComponentDataContentView()
    {
      InitializeComponent();
    }
    private void MarkSelectedTreeItem(object sender, MouseButtonEventArgs e)
    {
      var convert = sender as TreeViewItem;
      if (convert != null && convert.DataContext is FileItem)
      {
        convert.IsSelected = true;
      }
    }
  }
}
