﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.ViewModel;
namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for AppendDataToComponentView.xaml
  /// </summary>
  public partial class AppendDataToComponentView : UserControl
  {
    public AppendDataToComponentView()
    {
      InitializeComponent();
    }

    private void RightClickHandler(object sender, MouseButtonEventArgs e)
    {
      var dataContext = DataContext as AppendDataToComponentViewModel;
      if (dataContext != null)
      {
        var datagridCell = sender as DataGridCell;
        if (datagridCell != null)
        {
          var item = datagridCell.DataContext as List<Item>;
          if (item != null)
          {
            dataContext.RightClickSelectedItem = item;
          }
        }
      }
    }



  }
}
