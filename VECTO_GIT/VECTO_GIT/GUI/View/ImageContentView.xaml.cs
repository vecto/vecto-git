﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.View
{

  public partial class ImageContentView : UserControl
  {
    private readonly ImageContentViewModel viewModel;

    #region Properties

    public static readonly DependencyProperty SetImageContentProperty =
      DependencyProperty.Register("ImageBlobContent", typeof(KeyValuePair<string, Blob>), typeof(ImageContentView),
        new PropertyMetadata(default(KeyValuePair<string, Blob>), OnImageContentChanged));

    public KeyValuePair<string, Blob> ImageBlobContent
    {
      get { return (KeyValuePair<string, Blob>)GetValue(SetImageContentProperty); }
      set { SetValue(SetImageContentProperty, value); }
    }

    #endregion


    public ImageContentView()
    {
      InitializeComponent();
      viewModel = new ImageContentViewModel();
      DataContext = viewModel;
    }

    private static void OnImageContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var pdfView = d as ImageContentView;
      if (pdfView != null)
      {
        pdfView.OnImageContentChanged(e);
      }
    }

    private void OnImageContentChanged(DependencyPropertyChangedEventArgs e)
    {
      if (viewModel != null && e.NewValue is KeyValuePair<string, Blob>)
      {
        var selectedBlobContent = (KeyValuePair<string, Blob>)e.NewValue;
        viewModel.LoadImageContent(selectedBlobContent);
      }
    }
  }
}
