﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using VECTO_GIT.GUI.Model;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for SaveComponentView.xaml
  /// </summary>
  public partial class SaveComponentView : UserControl
  {
    public SaveComponentView()
    {
      InitializeComponent();      
    }

    private void RightClickHandler(object sender, MouseButtonEventArgs e)
    {
      var dataContext = DataContext as SaveComponentViewModel;
      if (dataContext != null)
      {
        var datagridCell = sender as DataGridCell;
        if (datagridCell != null)
        {
          var item = datagridCell.DataContext as List<Item>;
          if (item != null)
          {
            dataContext.RightClickSelectedItem = item;
          }
        }
      }
    }
  }
}
