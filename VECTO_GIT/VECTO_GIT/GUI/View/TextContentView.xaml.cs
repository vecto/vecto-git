﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.View
{

  public partial class TextContentView : UserControl
  {
    #region Properties

    public static readonly DependencyProperty SetTextBlobProperty =
      DependencyProperty.Register("TextBlobContent", typeof(KeyValuePair<string, Blob>), typeof(TextContentView),
        new PropertyMetadata(default(KeyValuePair<string, Blob>), OnSetTextContentChanged));

    public KeyValuePair<string, Blob> TextBlobContent
    {
      get { return (KeyValuePair<string, Blob>)GetValue(SetTextBlobProperty); }
      set { SetValue(SetTextBlobProperty, value); }
    }

    #endregion

    public TextContentView()
    {
      InitializeComponent();
      DataContext = new TextContentViewModel();
    }

    private static void OnSetTextContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var textView = d as TextContentView;
      if (textView != null)
      {
        textView.OnSetTextContentChanged(e);
      }
    }

    private void OnSetTextContentChanged(DependencyPropertyChangedEventArgs e)
    {
      var viewModel = DataContext as TextContentViewModel;
      if (viewModel != null && e.NewValue is KeyValuePair<string, Blob>)
      {
        var selectedBlob = (KeyValuePair<string, Blob>)e.NewValue;
        viewModel.LoadBlobContent(selectedBlob);

      }
    }

  }
}
