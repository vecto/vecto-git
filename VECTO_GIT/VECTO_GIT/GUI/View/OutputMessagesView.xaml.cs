﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using VECTO_GIT.GUI.ViewModel;

namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for ErrorMessageView.xaml
  /// </summary>
  public partial class OutputMessagesView : UserControl
  {
    #region Members

    private readonly OutputMessagesViewModel viewModel;

    public static readonly DependencyProperty SetMessagesProperty =
      DependencyProperty.Register("Messages", typeof(List<KeyValuePair<string, bool>>), typeof(OutputMessagesView),
        new PropertyMetadata(default(List<KeyValuePair<string, bool>>), OnMessagesChanged));

    #endregion


    #region Properties

    public List<KeyValuePair<string, bool>> Messages
    {
      get { return (List<KeyValuePair<string, bool>>)GetValue(SetMessagesProperty); }
      set { SetValue(SetMessagesProperty, value); }
    }

    #endregion


    public OutputMessagesView()
    {
      InitializeComponent();
      viewModel = new OutputMessagesViewModel();
      DataContext = viewModel;
    }

    private static void OnMessagesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var outputMessageView = d as OutputMessagesView;
      outputMessageView?.OnMessagesChanged(e);
    }

    private void OnMessagesChanged(DependencyPropertyChangedEventArgs e)
    {
      viewModel?.SetMessages(e.NewValue as List<KeyValuePair<string, bool>>);
    }
  }
}
