﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace VECTO_GIT.GUI.View
{
  /// <summary>
  /// Interaction logic for SearchComponentView.xaml
  /// </summary>
  public partial class SearchComponentView : UserControl
  {
    public SearchComponentView()
    {
      InitializeComponent();
    }

    private void Test_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      var convert = sender as DataGrid;
      if (convert == null)
        return;

      var rows = GetDataGridRows(convert);

      foreach (var row in rows)
      {
        if (row == null)
          return;

        if (row.IsSelected)
        {
          var manufacturer = FindChild<TextBox>(row, "ManufactureTextBox");
          if (manufacturer != null) manufacturer.Foreground = Brushes.White;

          var model = FindChild<TextBox>(row, "ModelTextBox");
          if (manufacturer != null) model.Foreground = Brushes.White;

          var certificateDate = FindChild<TextBox>(row, "CertificateDateTextBox");
          if (certificateDate != null) certificateDate.Foreground = Brushes.White;

          var certificateNumber = FindChild<TextBox>(row, "CertificateNumberTextBox");
          if (certificateNumber != null) certificateNumber.Foreground = Brushes.White;

          var gitId = FindChild<TextBox>(row, "GitIdTextBox");
          if (gitId != null) gitId.Foreground = Brushes.White;
        }
        else
        {
          var manufacturer = FindChild<TextBox>(row, "ManufactureTextBox");
          if (manufacturer != null) manufacturer.Foreground = Brushes.Black;

          var model = FindChild<TextBox>(row, "ModelTextBox");
          if (manufacturer != null) model.Foreground = Brushes.Black;

          var certificateDate = FindChild<TextBox>(row, "CertificateDateTextBox");
          if (certificateDate != null) certificateDate.Foreground = Brushes.Black;

          var certificateNumber = FindChild<TextBox>(row, "CertificateNumberTextBox");
          if (certificateNumber != null) certificateNumber.Foreground = Brushes.Black;

          var gitId = FindChild<TextBox>(row, "GitIdTextBox");
          if (gitId != null) gitId.Foreground = Brushes.Black;
        }
      }
    }

    private IEnumerable<DataGridRow> GetDataGridRows(DataGrid grid)
    {
      var itemsSource = grid.ItemsSource;

      if (null == itemsSource)
      {
        yield return null;
      }

      foreach (var item in itemsSource)
      {
        var row = grid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;

        if (null != row)
        {
          yield return row;
        }
      }
    }


    private T FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
    {
      if (parent == null)
      {
        return null;
      }

      T foundChild = null;

      int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

      for (int i = 0; i < childrenCount; i++)
      {
        var child = VisualTreeHelper.GetChild(parent, i);
        T childType = child as T;

        if (childType == null)
        {
          foundChild = FindChild<T>(child, childName);

          if (foundChild != null) break;
        }
        else
        if (!string.IsNullOrEmpty(childName))
        {
          var frameworkElement = child as FrameworkElement;

          if (frameworkElement != null && frameworkElement.Name == childName)
          {
            foundChild = (T)child;
            break;
          }
          else
          {
            foundChild = FindChild<T>(child, childName);

            if (foundChild != null)
            {
              break;
            }
          }
        }
        else
        {
          foundChild = (T)child;
          break;
        }
      }

      return foundChild;
    }
  }
}
