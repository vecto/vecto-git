﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;
using System.ComponentModel;


namespace VECTO_GIT
{
  public class VirtualDrive
  {
    public static string PREFERRED_DRIVE_LETTER = "A";
    public string DriveLetter { get; private set; }
    public string OriginDirectoryPath { get; private set; }
    public string VirtualPath { get; private set; }

    private const uint DDD_REMOVE_DEFINITION = 0x00000002;

    public VirtualDrive()
    {
    }


    public string MapDrive(string dirPath)
    {
      if (dirPath != null && !Directory.Exists(dirPath))
      {
        throw new ArgumentException("Given directory path is invalid!");
      }

      if (!(Properties.Settings.Default.LastDriveLetter.IsNullOrEmptyOrWhitespace()))
      {
        TryToUnmapDrive(Properties.Settings.Default.LastDriveLetter,
                        Properties.Settings.Default.LastDirPath);
      }

      DriveLetter = SelectDriveLetter();

      if (!DefineDosDevice(0, DriveLetter, dirPath))
      {
        return null;
      }

      OriginDirectoryPath = dirPath;
      VirtualPath = GetVirtualPath();
      SaveCurrentMount();

      return VirtualPath;
    }

    private void SaveCurrentMount()
    {
      Properties.Settings.Default.LastDirPath = OriginDirectoryPath;
      Properties.Settings.Default.LastDriveLetter = DriveLetter;
      Properties.Settings.Default.Save();
    }


    private void TryToUnmapDrive(string driveLetter, string dirPath)
    {
      if (driveLetter.IsNullOrEmptyOrWhitespace())
        return;

      var currentDrivers = DriveInfo.GetDrives();
      for (int i = 0; i < currentDrivers.Length; i++)
      {
        var availableDrive = currentDrivers[i].Name;
        if (driveLetter.First() == availableDrive.First())
        {
          Unmap(driveLetter, dirPath);
          return;
        }
      }
    }


    public bool UnmapDrive()
    {
      if (DriveLetter.IsNullOrEmptyOrWhitespace())
      {
        return false;
      }

      if (!Unmap(DriveLetter, null))
        return false;

      DriveLetter = null;
      OriginDirectoryPath = null;
      VirtualPath = null;
      ClearLastDriveProperties();

      return true;
    }

    private bool Unmap(string driveLetter, string dirPath)
    {
      return DefineDosDevice(DDD_REMOVE_DEFINITION, driveLetter, dirPath);
    }

    private void ClearLastDriveProperties()
    {
      Properties.Settings.Default.LastDirPath = string.Empty;
      Properties.Settings.Default.LastDriveLetter = string.Empty;
      Properties.Settings.Default.Save();
    }

    public bool AnyExistingVirtualDrive()
    {
      return Directory.Exists(VirtualPath);
    }

    public string ChangeVirtualDrive(string dirPath)
    {
      if (!UnmapDrive())
      {
        throw new Exception("error in virtual drive unmap");
      }
      return MapDrive(dirPath);
    }


    private string GetVirtualPath()
    {
      return string.Format(@"{0}{1}", DriveLetter, Path.DirectorySeparatorChar);
    }

    private string SelectDriveLetter()
    {
      var availableLetters = GetAllFreeDriveLetters();
      string selectedLetter = string.Empty;

      if (availableLetters.Contains(PREFERRED_DRIVE_LETTER))
      {
        selectedLetter = PREFERRED_DRIVE_LETTER;
      }
      else
      {
        selectedLetter = availableLetters.First();
      }

      return string.Format("{0}:", selectedLetter);
    }

    private List<string> GetAllFreeDriveLetters()
    {
      var availableLetters = GetAlphabet();

      var currentDrives = DriveInfo.GetDrives();
      for (int i = 0; i < currentDrives.Length; i++)
      {
        var drive = currentDrives[i];
        availableLetters.Remove(drive.Name[0].ToString());
      }

      return availableLetters;
    }

    private List<string> GetAlphabet()
    {
      var alphabet = Enumerable.Range('A', 26).Select(x => (char)x);
      return alphabet.Select(c => c.ToString()).ToList();
    }

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    private static extern bool DefineDosDevice(uint dwFlags, string lpDeviceName, string lpTargetPath);
  }
}
