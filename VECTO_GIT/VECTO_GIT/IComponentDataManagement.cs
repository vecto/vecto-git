﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.Model;
using System.IO;

namespace VECTO_GIT
{
  public interface IComponentDataManagement
  {
    ComponentDataCommit VerifySaveComponentData(string compDataFilePath, Branch stdValuesBranch,
      string stdValuesFilePath, bool validateXml);

    StandardDataCommit VerifySaveStandardValueFile(string filePath, bool validateXml);

    CertificateDataCommit VerifySaveCertificateByComponentFile(string certFilePath, string compFilePath, Branch publicBranch);
    
    MeasurementDataCommit VerifySaveMeasurementData(ComponentToSave componentToSave);

    UserDataCommit VerifySaveUserData(ComponentToSave componentToSave);
    
    IEnumerable<VectoComponent> SearchVectoComponentsByGivenTerms(VectoSearchTerms searchTerms);
    IEnumerable<VectoComponent> SearchAllVectoComponents();
  }

}
