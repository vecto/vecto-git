﻿using LibGit2Sharp;
using System;
using System.IO;

namespace VECTO_GIT
{
  public static class Helpers
  {

    private static string PROJECT_PATH = AppDomain.CurrentDomain.BaseDirectory;
    private static string PROJECT_FOLDER_NAME = "VECTO_GIT";
    private static string TRANSFER_FOLDER_NAME = "Git_Transfer";
    private static string XSD_FOLDER_NAME = "XSDFiles";
    private static string COMMON_PATH = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

    public static string AppFolderPath
    {
      get
      {
        return Path.Combine(COMMON_PATH, PROJECT_FOLDER_NAME);
      }
    }

    public static string XSDFolderPath
    {
      get
      {
        return Path.GetFullPath(Path.Combine(PROJECT_PATH, @"..\..\..\",
               PROJECT_FOLDER_NAME,
               XSD_FOLDER_NAME + @"\"));
      }
    }

    public static Repository LoadRepository(string repositoryPath, bool isBare)
    {
      if (repositoryPath != null)
      {
        if (Repository.IsValid(repositoryPath))
        {
          return new Repository(repositoryPath);
        }
      }

      return null;
    }

    public static Repository CreateOrLoadRepository(string repositoryPath, bool isBare)
    {
      if (repositoryPath != null)
      {
        if (!Repository.IsValid(repositoryPath))
        {
          repositoryPath = Repository.Init(repositoryPath, isBare);
        }
        return new Repository(repositoryPath);
      }

      return null;

    }
    
    public static string ExceptionMessage(string variableName)
    {
      return string.Format("Invalid {0} was given!", variableName);
    }
    
    public static void NormalizeDirectoryAttributes(string dirPath)
    {
      var filePaths = Directory.GetFiles(dirPath);
      var subdirectoryPaths = Directory.GetDirectories(dirPath);

      foreach (string filePath in filePaths)
      {
        File.SetAttributes(filePath, FileAttributes.Normal);
      }
      foreach (string subdirectoryPath in subdirectoryPaths)
      {
        NormalizeDirectoryAttributes(subdirectoryPath);
      }
      File.SetAttributes(dirPath, FileAttributes.Normal);
    }


    public static void DeleteDirectory(string dirPath)
    {
      if (Directory.Exists(dirPath))
      {
        NormalizeDirectoryAttributes(dirPath);
        Directory.Delete(dirPath, true);
      }
    }

    public static void CreateDirectory(string dirPath)
    {
      DeleteDirectory(dirPath);
      Directory.CreateDirectory(dirPath);
    }
  }
}
