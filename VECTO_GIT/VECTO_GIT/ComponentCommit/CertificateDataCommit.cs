﻿using LibGit2Sharp;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Navigation;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentNaming;

namespace VECTO_GIT.ComponentCommit
{
  public class CertificateDataCommit : ComponentCoreCommit
  {
    private readonly CertificateNumberHandler certNumberHandler;
    private readonly PublicBranchNaming publicBranchNaming;
    private readonly string certFilePath;
    private readonly Blob certFileBlob;

    private Branch relatedBranch;
    private FileToCommit fileToCommit;

    public CertificateDataCommit(Repository repository, string certFilePath, PublicBranchNaming publicBranchNaming,
      Branch relatedBranch, CertificateNumberHandler certNumberHandler) : base(repository)
    {
      this.certFilePath = certFilePath.CheckFilePath();
      this.publicBranchNaming = publicBranchNaming.CheckNull(nameof(publicBranchNaming));
      this.certNumberHandler = certNumberHandler.CheckNull(nameof(certNumberHandler));
      this.relatedBranch = relatedBranch;
    }

    public CertificateDataCommit(Repository repository, Branch compBranch, Blob certFileBlob, Blob compBlob)
    : base(repository)
    {
      relatedBranch = compBranch.CheckNull(nameof(compBranch));
      this.certFileBlob = certFileBlob.CheckNull(nameof(certFileBlob));
      compBlob.CheckNull(nameof(compBlob));

      certNumberHandler = GetCertificateNumberHandler(compBlob);
    }

    public override bool VerificationCheckBeforeCommit()
    {
      verificationCheckResult = VerificationChecks();
      return verificationCheckResult;
    }

    public override Branch ExecuteCommit()
    {
      if (!verificationCheckResult)
        return null;

      relatedBranch = SetRelatedBranch(relatedBranch, publicBranchNaming);


      if (relatedBranch == null) //ToDo error message if no related branch couldn't found for certificate
        return null;

      ExecuteCommitSteps(relatedBranch);
      AddCertificateRelatedBranches();

      return RefreshBranchByName(relatedBranch.FriendlyName);
    }


    private void ExecuteCommitSteps(Branch branch)
    {
      var blob = CreateCertificateBlob();
      var parentCommit = ComponentDataCommit.GetComponentDataCommit(branch);
      var tree = CreateTree(parentCommit, blob, ConfigValue.CERTIFICATE_DATA_FILE_NAME);
      CreateCommit(branch.Reference, parentCommit, tree, string.Empty);
    }


    //ToDo FIX ME!! add additional checks for: e.g. Certificate with different content?!?
    // selection per gui not possible
    protected override bool VerificationChecks()
    {
      if (certFileBlob != null && relatedBranch != null)
        return true;

      fileToCommit = CreateFileToCommit();
      var verification = new CommitVerification(repository);

      relatedBranch = SetRelatedBranch(relatedBranch, publicBranchNaming);
      
      if (relatedBranch != null)
      {
        if (verification.FileOrFileNameSaved(fileToCommit, relatedBranch))
        {
          verificationErrors = verification.Errors;
          return false;
        }
      }
      return true;
    }

    private Blob CreateCertificateBlob()
    {
      if (certFileBlob != null)
        return certFileBlob;

      return CreateBlob(certFilePath);
    }

    private Branch SetRelatedBranch(Branch pubBranch, PublicBranchNaming branchNaming)
    {
      return pubBranch != null ? pubBranch : FetchBranchByName(branchNaming.GetGitPathBranchName());
    }


    private void AddCertificateRelatedBranches()
    {
      var certTags = GetTagsByTagName(certNumberHandler.GetGitPathNameWithoutXmlHash(), TagNameType.SearchTagName);
      if (!certTags.IsNullOrEmpty())
      {
        var branches = GetBranchesWithoutCertificate(certTags);
        foreach (var branch in branches)
        {
          ExecuteCommitSteps(branch);
        }
      }
    }


    private List<Branch> GetBranchesWithoutCertificate(Tag[] tags)
    {
      var foundedBranches = new List<Branch>();

      foreach (var tag in tags)
      {
        var branchNameHandler = PublicBranchNaming.DeserializeBranchName(tag.Annotation.Message);
        var branch = repository.Branches[branchNameHandler.GetGitPathBranchName()];
        if (branch != null)
        {
          var certCommit = GetCertificateDataCommit(branch);
          if (certCommit == null)
          {
            foundedBranches.Add(branch);
          }

//          var verification = new CommitVerification(repository);
//          if (!verification.FileOrFileNameSaved(fileToCommit, branch))
//          {
//            foundedBranches.Add(branch);
//          }
        }
      }
      return foundedBranches;
    }

    private CertificateNumberHandler GetCertificateNumberHandler(Blob compDataBlob)
    {
      var xmlContentReader = new XmlContentReader();
      using (var streamReader = new StreamReader(compDataBlob.GetContentStream(), Encoding.Default))
      {
        return xmlContentReader.ReadOutCertificateId(streamReader);
      }
    }


    private FileToCommit CreateFileToCommit()
    {
      return new FileToCommit(ConfigValue.CERTIFICATE_DATA_FILE_NAME, certFilePath, CommitType.PublicData);
    }

    public static Commit GetCertificateDataCommit(Branch branch)
    {
      branch.CheckNull(nameof(branch));

      if (!branch.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME) &&
          !branch.FriendlyName.StartsWith(ConfigValue.REMOTE_NAME + "/" + PublicBranchNaming.TREE_NAME))
      {
        return null;
      }

      if (!branch.Commits.Any() && !(branch.Commits.Count() >= 2))
      {
        return null;
      }

      foreach (var commit in branch.Commits)
      {
        var certBlob = GetCertificateBlob(commit);
        if (certBlob != null)
        {
          return commit;
        }
      }

      return null;
    }


    public static Blob GetCertificateBlob(Commit commit)
    {
      if (commit == null)
      {
        return null;
      }

      foreach (var entry in commit.Tree)
      {
        if (entry.Name == ConfigValue.CERTIFICATE_DATA_FILE_NAME)
        {
          return entry.Target as Blob;
        }
      }
      return null;
    }

    public static Blob GetCertificateBlob(Branch branch)
    {
      var certCommit = GetCertificateDataCommit(branch);
      return (certCommit == null) ?
        null : GetCertificateBlob(certCommit);
    }

  }
}
