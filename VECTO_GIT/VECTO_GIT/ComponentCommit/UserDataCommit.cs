﻿using LibGit2Sharp;
using System.Collections.Generic;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.ComponentCommit
{
  public class UserDataCommit : PrivateDataCommit
  {
    private readonly Tree userDataTree;
    private readonly bool isMergeCommit;

    public UserDataCommit(Repository repository, List<Item> selectedPaths, string privateBranchName)
      : base(repository, selectedPaths)
    {
      this.privateBranchName = privateBranchName.CheckNull(nameof(privateBranchName));
    }


    public UserDataCommit(Repository repository, Tree userDataTree, Branch privateBranch)
      : base(repository, privateBranch)
    {
      this.userDataTree = userDataTree.CheckNull(nameof(userDataTree));
      isMergeCommit = true;
    }


    public override bool VerificationCheckBeforeCommit()
    {
      verificationCheckResult = VerificationChecks();
      return verificationCheckResult;
    }

    public override Branch ExecuteCommit()
    {
      if (isMergeCommit)
      {
        var mergedTree = MergeTree(privateBranch.Tip.Tree, ConfigValue.USER_DATA_TREE_NAME, userDataTree);
        CreateCommit(privateBranch.Reference, privateBranch.Tip, mergedTree, string.Empty);
        return RefreshBranchByName(privateBranch.FriendlyName);
      }


      if (!verificationCheckResult)
        return null;

      privateBranch = RefreshBranchByName(privateBranchName);

      verificationCheckResult = false;

      return privateBranch == null ? CreateNewPrivateBranch() : AppendDataToExistingBranch();
    }
    

    private Branch CreateNewPrivateBranch()
    {
      var tree = CreateTreeFromDirectory(selectedPaths);
      var resultTree = SetTreeRoot(ConfigValue.USER_DATA_TREE_NAME, tree);
      var commit = CreateCommit(resultTree, string.Empty);
      return CreateOrphanBranch(commit, privateBranchName);
    }


    private Branch AppendDataToExistingBranch()
    {
      var existingFiles = FetchSavedFiles(ConfigValue.USER_DATA_TREE_NAME);
      if (existingFiles != null)
        selectedPaths.AddRange(existingFiles);
      var newTree = CreateTreeFromDirectory(selectedPaths);
      var mergedTree = MergeTree(privateBranch.Tip.Tree, ConfigValue.USER_DATA_TREE_NAME, newTree);
      var commitResult = CreateCommit(privateBranch.Reference, privateBranch.Tip, mergedTree, string.Empty);

      return commitResult != null ? RefreshBranchByName(privateBranchName) : null;
    }

    
    protected override bool VerificationChecks()
    {
      if (isMergeCommit)
        return true;

      if (privateBranchName == null)
        return false;

      privateBranch = RefreshBranchByName(privateBranchName);

      if (privateBranch != null)
      {
        if (!VerifyGivenFilePaths(privateBranch, CommitType.UserDefinedData)
          || selectedPaths.IsEmpty())
        {
          return false;
        }
      }

      return true;
    }
  }
}
