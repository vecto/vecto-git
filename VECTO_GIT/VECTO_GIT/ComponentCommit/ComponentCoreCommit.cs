﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LibGit2Sharp;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentNaming;




namespace VECTO_GIT.ComponentCommit
{

  public enum BranchTyp
  {
    Private, 
    Public
  }

  public abstract class ComponentCoreCommit
  {
    protected Repository repository;
    protected List<CommitVerificationError> verificationErrors;
    protected bool verificationCheckResult;

    protected ComponentCoreCommit(Repository repository)
    {
      this.repository = repository.CheckValidRepository();
      verificationErrors = new List<CommitVerificationError>();
    }

    public abstract Branch ExecuteCommit();
    public abstract bool VerificationCheckBeforeCommit();

    protected abstract bool VerificationChecks();

    public List<CommitVerificationError> GetVerificationErrors()
    {
      return verificationErrors;
    }
    
    protected Branch FetchBranchByName(string branchName)
    {
      if (!string.IsNullOrEmpty(branchName))
      {
        return repository.Branches[branchName];
      }
      return null;
    }

//    protected Branch FetchBranchByTag(INamingHandler tagName, BranchTyp branchTyp)
//    {
//      var tagType = (tagName is GitIdHandler ) ? TagNameType.GitId : TagNameType.CertificateNumber;
//
//      var tags = GetTagsByTagName(tagName, tagType);
//      if (!tags.IsNullOrEmpty())
//      {
//        var branchName = GetBranchName(branchTyp, tags);
//        return repository.Branches[branchName];
//      }
//      return null;
//    }

    protected Branch RefreshBranchByName(string branchName)
    {
      return repository.Branches[branchName];
    }


    private string GetBranchName(BranchTyp branchTyp, IEnumerable<Tag> tags)
    {
      var tagMessage = tags.First().Annotation.Message;
      var branchNaming = PublicBranchNaming.DeserializeBranchName(tagMessage);

      if (branchTyp.Equals(BranchTyp.Private))
      {
        return branchNaming.GetGitPrivateBranchName();
      }
      else
      {
        return branchNaming.GetGitPathBranchName();
      }
    }




    #region Default GIT command calls


    protected Blob CreateBlob(string filePath)
    {
      var command = new CreateBlobCommand(repository, filePath);
      return command.Result();
    }

    protected Blob CreateBlob(Stream blobContentStream)
    {
      var command = new CreateBlobCommand(repository, blobContentStream);
      return command.Result();
    }


    protected Tree CreateTree(Commit parentCommit, Blob blob, string blobName)
    {
      var blobsToCommit = new Dictionary<string, Blob>
      {
        { blobName, blob }
      };

      return CreateTree(parentCommit, blobsToCommit);
    }

    protected Tree CreateTree(Commit parentCommit, Dictionary<string, Blob> blobsToCommit)
    {
      var command = new CreateTreeCommand(repository, parentCommit, blobsToCommit);
      return command.Result();
    }


    protected Tree CreateTree(Blob blob, string blobName)
    {
      var blobsToCommit = new Dictionary<string, Blob>
      {
        { blobName, blob }
      };

      return CreateTree(blobsToCommit);
    }

    protected Tree CreateTree(Dictionary<string, Blob> blobsToCommit)
    {
      var command = new CreateTreeCommand(repository, blobsToCommit);
      return command.Result();
    }

    

    protected Commit CreateCommit(Reference branchReference, Commit parentCommit,
      Tree commitTree, string commitMessage)
    {
      var command = new CreateTreeCommitCommand(repository, commitTree, commitMessage,
        branchReference, parentCommit);
      return command.Result();
    }


    protected Commit CreateCommit(Tree commitTree, string commitMessage)
    {
      var command = new CreateTreeCommitCommand(repository, commitTree, commitMessage);
      return command.Result();
    }


    protected Tag CreateTag(Commit commit, string tagMessage, string tagName)
    {
      var command = new CreateTagCommand(repository, tagName, tagMessage, commit.Id.Sha);
      return command.Result();
    }

    protected Branch CreateOrphanBranch(Commit commit, string branchName)
    {
      var command = new CreateOrphanBranchCommand(repository, branchName, commit);
      return command.Result();
    }


    protected Tag[] GetTagsByTagName(string tagName, TagNameType tagNameType)
    {
      var command = new SearchTagCommand(repository, tagName, tagNameType);
      return command.Result();
    }

    
//    protected Tag[] GetTagsByTagName(INamingHandler tagName, TagNameType tagNameType)
//    {
//     
//      var command = new SearchTagCommand(repository, tagName);
//      return command.Result();
//    }

    protected Branch[] SearchBranchByTerms(string [] searchTerms)
    {
      var command = new SearchBranchCommand(repository, searchTerms);
      return command.Result().ToArray();
    }

    protected Branch[] SearchBranchByTerm(string searchTerm)
    {
      var command = new SearchBranchCommand(repository, searchTerm);
      return command.Result().ToArray();
    }

    #endregion




  }




}
