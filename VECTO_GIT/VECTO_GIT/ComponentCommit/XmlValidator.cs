﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace VECTO_GIT.ComponentCommit
{

  public class XmlValidatorResult
  {
    public string XmlFilePath { get; set; }
    public List<string> Errors { get; set; }
    public List<string> Warnings { get; set; }

    public XmlValidatorResult()
    {
      Errors = new List<string>();
      Warnings = new List<string>();
    }
  }

  public class XmlValidator
  {
    public XmlValidatorResult XmlValidationResult { private set; get; }

    private readonly string xsdFilePath;
    private readonly string xsdNameSpace;

    private const string XSD_SIG_NAMESPACE = @"http://www.w3.org/2000/09/xmldsig#";
    private const string XSD_SIG_FILENAME = "xmldsig-core-schema.xsd";

    public XmlValidator(string xsdFilePath, string xsdNameSpace)
    {
      this.xsdFilePath = xsdFilePath.CheckFilePath();
      this.xsdNameSpace = xsdNameSpace.CheckNullOrEmtpy("xsdNameSpace");
    }

    public bool ValidateXml(string xmlFilePath)
    {
      XDocument xmlDoc = null;
      var settings = new XmlReaderSettings();
      settings.DtdProcessing = DtdProcessing.Ignore;

      try
      {
        using (var xmlReader = XmlReader.Create(xmlFilePath, settings))
        {
          xmlDoc = XDocument.Load(xmlReader);
          XmlValidationResult = new XmlValidatorResult
          {
            XmlFilePath = xmlFilePath
          };

          var schemaSet = GetXmlSchemaSet();
          var eventHandler = GetValidationEventHandler();
          xmlDoc.Validate(schemaSet, eventHandler);
        }
      }
      catch (Exception ex)
      {
        Console.Error.Write(ex.ToString());
        return false;
      }

      return XmlValidationResult != null &&
        (XmlValidationResult.Errors.Count > 0 || XmlValidationResult.Warnings.Count > 0) ? false : true;

      //return (Warnings.Count > 0 || Errors.Count > 0) ? false : true;
    }

    private XmlSchemaSet GetXmlSchemaSet()
    {
      var xsdFolder = Path.GetDirectoryName(xsdFilePath);
      var xsdSignaturePath = Path.Combine(xsdFolder, XSD_SIG_FILENAME);

      var schemas = new XmlSchemaSet();
      schemas.Add(xsdNameSpace, xsdFilePath);

      using (var fs = File.OpenRead(xsdSignaturePath))
      using (var reader = XmlReader.Create(fs, new XmlReaderSettings()
      {
        DtdProcessing = DtdProcessing.Ignore // important
      }))
      {
        schemas.Add(XSD_SIG_NAMESPACE, reader);
      }

      return schemas;
    }


    private ValidationEventHandler GetValidationEventHandler()
    {
      return ValidationCallBack;
    }

    private void ValidationCallBack(object sender, ValidationEventArgs args)
    {
      if (args.Severity == XmlSeverityType.Warning)
      {
        XmlValidationResult.Warnings.Add(args.Message);
      }
      else
      {
        XmlValidationResult.Errors.Add(args.Message);
      }
    }
  }
}
