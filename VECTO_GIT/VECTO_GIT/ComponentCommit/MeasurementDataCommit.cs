﻿using LibGit2Sharp;
using System.Collections.Generic;
using VECTO_GIT.GUI.Model;


namespace VECTO_GIT.ComponentCommit
{
  public class MeasurementDataCommit : PrivateDataCommit
  {
    private readonly Tree measurementTree;
    private readonly bool isMergeCommit;

    public MeasurementDataCommit(Repository repository, List<Item> selectedPaths, string privateBranchName)
      : base(repository, selectedPaths)
    {
      this.privateBranchName = privateBranchName.CheckNull(nameof(privateBranchName));
    }

    public MeasurementDataCommit(Repository repository, Tree measurementTree, Branch privateBranch)
      : base(repository, privateBranch)
    {
      this.measurementTree = measurementTree.CheckNull(nameof(measurementTree));
      isMergeCommit = true;
    }


    public override bool VerificationCheckBeforeCommit()
    {
      verificationCheckResult = VerificationChecks();
      return verificationCheckResult;
    }
    

    public override Branch ExecuteCommit()
    {
      if (isMergeCommit)
      {
        var mergedTree = MergeTree(privateBranch.Tip.Tree, ConfigValue.MEASUREMENT_DATA_TREE_NAME, measurementTree);
        CreateCommit(privateBranch.Reference, privateBranch.Tip, mergedTree, string.Empty);
        return RefreshBranchByName(privateBranch.FriendlyName);

      }

      if (!verificationCheckResult)
        return null;

      privateBranch = RefreshBranchByName(privateBranchName);

      verificationCheckResult = false;
      return privateBranch == null ? CreateNewPrivateBranch() : AppendDataToExistingBranch();
    }


    private Branch AppendDataToExistingBranch()
    {
      var existingFiles = FetchSavedFiles(ConfigValue.MEASUREMENT_DATA_TREE_NAME);
      if (existingFiles != null)
        selectedPaths.AddRange(existingFiles);
      var tree = CreateTreeFromDirectory(selectedPaths);
      var mergeTree = MergeTree(privateBranch.Tip.Tree, ConfigValue.MEASUREMENT_DATA_TREE_NAME, tree);

      CreateCommit(privateBranch.Reference, privateBranch.Tip, mergeTree, string.Empty);

      return RefreshBranchByName(privateBranchName);
    }

    private Branch CreateNewPrivateBranch()
    {
      var tree = CreateTreeFromDirectory(selectedPaths);
      var resultTree = SetTreeRoot(ConfigValue.MEASUREMENT_DATA_TREE_NAME, tree);
      var commit = CreateCommit(resultTree, string.Empty);
      return CreateOrphanBranch(commit, privateBranchName);
    }

    protected override bool VerificationChecks()
    {
      if (isMergeCommit)
        return true;

      if (privateBranchName == null)
        return false;

      privateBranch = RefreshBranchByName(privateBranchName);

      if (privateBranch != null)
      {
        if (!VerifyGivenFilePaths(privateBranch, CommitType.MeasurementData)
            || selectedPaths.IsEmpty())
        {
          return false;
        }
      }
      return true;
    }

  }
}
