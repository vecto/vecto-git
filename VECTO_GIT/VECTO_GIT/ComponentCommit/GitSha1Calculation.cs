﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
//using Ude;
using System.Security.Cryptography;

namespace VECTO_GIT.ComponentCommit
{
  public class GitSha1Calculation
  {
    private string textFileGitHash;
    private string binaryFileGitHash;

    public string TextFileGitHash
    {
      private set
      {
        textFileGitHash = value;
      }
      get
      {
        if (textFileGitHash == null)
        {
          textFileGitHash = ComputeTextFileHash();
        }
        return textFileGitHash;
      }
    }
    public string BinaryFileGitHash
    {
      private set
      {
        binaryFileGitHash = value;
      }

      get
      {
        if (binaryFileGitHash == null)
        {
          binaryFileGitHash = ComputeBinaryFileHash();
        }
        return binaryFileGitHash;
      }
    }
    public bool IsBinary
    {
      get
      {
        return CheckIsBinary();
      }
    }

    private string filePath;
    private const string OBJECT_TYPE = "blob ";


    public GitSha1Calculation(string filePath)
    {
      this.filePath = filePath.CheckFilePath();
    }


    private string ComputeTextFileHash()
    {
      var fileContent = ReadFileAsBinary(filePath);
      var sha1Head = Encoding.ASCII.GetBytes(OBJECT_TYPE + fileContent.Length.ToString() + "\0");
      var concatData = sha1Head.Concat(fileContent).ToArray();
      return ComputeHashWith(concatData);
    }

    private string ComputeBinaryFileHash()
    {
      var fileContent = ReadFileAsText(filePath);
      var sha1Head = OBJECT_TYPE + fileContent.Length.ToString() + "\0";
      return ComputeHashWith(Encoding.ASCII.GetBytes(sha1Head + fileContent));
    }


    private string ComputeHashWith(byte[] contentStream)
    {
      using (SHA1Managed sha1 = new SHA1Managed())
      {
        var hash = sha1.ComputeHash(contentStream);
        var result = new StringBuilder(hash.Length * 2);

        foreach (byte b in hash)
        {
          result.Append(b.ToString("x2"));
        }
        return result.ToString();
      }
    }


    private string ReadFileAsText(string filePath)
    {
      return File.ReadAllText(filePath).Replace("\r\n", "\n");
    }

    private byte[] ReadFileAsBinary(string filePath)
    {
      return File.ReadAllBytes(filePath);
    }



    //#define FIRST_FEW_BYTES 8000
    //int buffer_is_binary(const char* ptr, unsigned long size)
    //  {
    //	if (FIRST_FEW_BYTES<size)
    //    size = FIRST_FEW_BYTES;
    //	return !!memchr(ptr, 0, size);
    //  }
    private bool CheckIsBinary()
    {
      using (var objStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
      {
        var size = objStream.Length;

        var firstFewBytes = 8000;
        if (firstFewBytes < objStream.Length)
        {
          size = firstFewBytes;
        }

        for (int i = 0; i < size; i++)
        {
          var b = objStream.ReadByte();
          if (b == 0x0)
          {
            return true;
          }
        }
        return false;
      }
    }



  }
}
