﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT.ComponentCommit
{
  public class GitCommitSha1Calculation
  {
    public const string EXPECTED_SHA1_RESULT = "39aa60d33b5620daa658c0fd0e73e494f760a827";
    private const string HEADER_START = "commit ";
    private const string HEADER_END = "\0";


    public string GetHashOfString(string str)
    {
      var sha1Head = HEADER_START + str.Length.ToString() + HEADER_END;
      return ComputeHashWith(Encoding.ASCII.GetBytes(sha1Head + str));
    }

    public string GetHashOfFile(string path)
    {
      var fileContent = ReadFileAsText(path);
      var sha1Head = HEADER_START + fileContent.Length.ToString() + HEADER_END;
      return ComputeHashWith(Encoding.ASCII.GetBytes(sha1Head + fileContent));
    }
    

    private string ComputeHashWith(byte[] contentStream)
    {
      using (SHA1Managed sha1 = new SHA1Managed())
      {
        var hash = sha1.ComputeHash(contentStream);
        var result = new StringBuilder(hash.Length * 2);

        foreach (byte b in hash)
        {
          result.Append(b.ToString("x2"));
        }
        return result.ToString();
      }
    }
    
    private string ReadFileAsText(string filePath)
    {
      return File.ReadAllText(filePath).Replace("\r\n", "\n");
    }

    private byte[] ReadFileAsBinary(string filePath)
    {
      return File.ReadAllBytes(filePath);
    }
    
    private string TestString()
    {
      return "tree 678539e2cda7a47b56a93ba135720b8a7feca6c9" + "\n" +
             "author Vecto Gitba <vecto.git@tugraz.atss> 1547469987 +0100" + "\n" +
             "committer Vecto Gitba <vecto.git@tugraz.atss> 1547469987 +0100" + "\n\n" +
             "{" + "\n" +
             "  \"CertificationDate\": \"2017-02-17T11:00:00Z\"" + "\n" +
             "}" + "\n";
    }
  }
}
