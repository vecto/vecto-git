﻿using LibGit2Sharp;
using System;
using System.Linq;
using VECTO_GIT.ComponentNaming;

namespace VECTO_GIT.ComponentCommit
{
  public class StandardDataCommit : ComponentCoreCommit
  {
    private readonly string filePath;
    private readonly Commit standardDataCommit;
    private CommitDateHandler dateHandler;
    private PublicBranchNaming publicBranchName;

    public StandardDataCommit(Repository repository, string filePath)
      : base(repository)
    {
      this.filePath = filePath.CheckFilePath();
      ReadXmlContent();
    }

    public StandardDataCommit(Repository repository, Commit standardDataCommit,
      PublicBranchNaming publicBranchName) : base(repository)
    {
      this.standardDataCommit = standardDataCommit.CheckNull(nameof(standardDataCommit));
      this.publicBranchName = publicBranchName.CheckNull(nameof(publicBranchName));
    }


    public override bool VerificationCheckBeforeCommit()
    {
      verificationCheckResult = VerificationChecks();
      return verificationCheckResult;
    }

    public override Branch ExecuteCommit()
    {
      if (!verificationCheckResult)
        return null;

      Commit commit;
      if (standardDataCommit == null)
      {
        var blob = CreateBlob(filePath);
        var tree = CreateTree(blob, ConfigValue.STANDARD_VALUES_FILE_NAME);
        commit = CreateCommit(tree, dateHandler.GetJsonRepresentation());
      }
      else
      {
        commit = standardDataCommit;
      }

      verificationCheckResult = false;
      return CreateOrphanBranch(commit, publicBranchName.GetGitPathBranchName());
    }

    protected override bool VerificationChecks()
    {
      var verification = new CommitVerification(repository);
      if (verification.BranchNameInUse(publicBranchName.GetGitPathBranchName(), filePath))
      {
        verificationErrors = verification.Errors;
        return false;
      }

      return true;
    }

    private void ReadXmlContent()
    {
      var xmlReader = new XmlContentReader();
      publicBranchName = xmlReader.ReadOutBranchNameData(filePath, true);
      var date = xmlReader.ReadOutDateOfFile(filePath);
      dateHandler = new CommitDateHandler(date, CommitDateType.StandardData);
    }

    public static bool IsStandardValuesBranch(Branch branch)
    {
      if (branch == null)
        return false;

      if (!branch.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME) &&
          !branch.FriendlyName.StartsWith(ConfigValue.REMOTE_NAME + "/" + PublicBranchNaming.TREE_NAME))
      {
        return false;
      }

      if (branch.Commits == null || branch.Commits.Count() > 1)
      {
        return false;
      }

      return GetStandardDataBlob(branch.Commits.First()) != null;
    }

    public static Commit GetStandardDataCommit(Branch branch)
    {
      branch.CheckNull(nameof(branch));

      if (!branch.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME) &&
          !branch.FriendlyName.StartsWith(ConfigValue.REMOTE_NAME + "/" + PublicBranchNaming.TREE_NAME))
      {
        return null;
      }

      if (branch.Commits == null || !(branch.Commits.Any()))
      {
        return null;
      }


      Commit expectedStdCommit = null;
      foreach (var commit in branch.Commits)
      {
        if (commit.Message.Contains(CommitDateHandler.STD_VALUES_DATE_NAME))
        {
          expectedStdCommit = commit;
          break;
        }
      }

      if (GetStandardDataBlob(expectedStdCommit) == null)
      {
        return null;
      }
      return expectedStdCommit;
    }

    public static Blob GetStandardDataBlob(Commit commit)
    {
      if (commit == null)
      {
        return null;
      }

      var tree = commit.Tree;
      if (tree.Count != 1)
      {
        return null;
      }

      var stdBlob = tree.First();
      if (stdBlob.Name != ConfigValue.STANDARD_VALUES_FILE_NAME)
      {
        return null;
      }

      return stdBlob.Target as Blob;
    }

    public static Blob GetStandardDataBlob(Branch branch)
    {
      var stdCommit = GetStandardDataCommit(branch);
      return (stdCommit == null) ?
        null : GetStandardDataBlob(stdCommit);
    }


  }
}
