﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LibGit2Sharp;

namespace VECTO_GIT.ComponentCommit
{

  public abstract class CommitVerificationError
  {
    protected string errorMessage;

    public abstract string GetErrorMessage ();
  }

  public class BranchNameInUse : CommitVerificationError
  {
    private string branchName;
    private string fileName;
    
    public BranchNameInUse(string branchName, string fileName)
    {
      this.branchName = branchName;
      this.fileName = fileName;   
    }
    
    public override string GetErrorMessage()
    {
      return string.Format("The created Branch FileName: \"{0}\" of the File: \"{1}\" is already in use!",
        branchName, fileName);
    }

  }


  public class FileNameInUse : CommitVerificationError
  {
    private string fileName;
    private string filePath;

    public FileNameInUse(string fileName, string filePath)
    {
      this.fileName = fileName;
      this.filePath = filePath;
    }
    public override string GetErrorMessage()
    {
      return string.Format("The Filename \"{0}\" is already used within the selected Branch!"
        , fileName);
    }

  }


  public class FileContentSaved : CommitVerificationError
  {
    private string fileName;
    private string filePath;

    public FileContentSaved(string fileName, string filePath)
    {
      this.fileName = fileName;
      this.filePath = filePath;
    }

    public override string GetErrorMessage()
    {
      return string.Format("The Content of the selected File \"{0}\" is already used within the selected Branch!"
        , fileName);
    }

  }

  public class FileAlreadySaved : CommitVerificationError
  {
    private string fileName;
    private string filePath;


    public FileAlreadySaved(string fileName, string filePath)
    {
      this.fileName = fileName;
      this.filePath = filePath;
    }
    public override string GetErrorMessage()
    {
      return string.Format("The selectd File \"{0}\" is already within the Branch saved!", fileName);
    }

  }


  public class BranchNotAvailable : CommitVerificationError
  {
    private string branchName;

    public BranchNotAvailable(string branchName)
    {
      this.branchName = branchName;
    }

    public override string GetErrorMessage()
    {
      return "BranchNotAvailable";
    }

  }


  public class GitIdNotAvailable : CommitVerificationError
  {
    private string gitId;

    public GitIdNotAvailable(string gitId)
    {
      this.gitId = gitId;
    }

    public override string GetErrorMessage()
    {
      return "GitIdNotAvailable";
    }

  }

  public class CertificateNumberNotAvailable : CommitVerificationError
  {
    private string certNumber;
    
    public CertificateNumberNotAvailable(string certNumber)
    {
      this.certNumber = certNumber;
    }

    public override string GetErrorMessage()
    {
      return "CertificateNumberNotAvailable";
    }

  }

  public class FolderNameAlreadyInUse : CommitVerificationError
  {
    public string folderName;

    public FolderNameAlreadyInUse(string folderName)
    {
      this.folderName = folderName;
    }

    public override string GetErrorMessage()
    {
      return "FolderNameAlreadyInUse";
    }
  }
  
  public class PubManufacturerModelNaming : CommitVerificationError
  {
    public override string GetErrorMessage()
    {
      return "Model and Manufacturer name do not match between Component Data and Standard Value file!";
    }
  }

  public class PrvManufacturerModelNaming : CommitVerificationError
  {
    public override string GetErrorMessage()
    {
      return "Model and Manufacturer name do not match between public and private data!";
    }
  }

}
