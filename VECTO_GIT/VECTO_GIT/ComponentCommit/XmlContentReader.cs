﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Globalization;
using System.IO;
using System.Xml.Schema;
using LibGit2Sharp;
using VECTO_GIT.ComponentNaming;


namespace VECTO_GIT.ComponentCommit
{
  public class XmlContentReader
  {
    private const string MANUFACTURER_TAG = "Manufacturer";
    private const string MODEL_TAG = "Model";
    private const string DATE_TAG = "Date";
    private const string HASH_TAG = "DigestValue";
    private const string CERTIFICATION_TAG = "CertificationNumber";

    private XmlDocument LoadXmlFile(string xmlFilePath)
    {
      var xmlDocument = new XmlDocument();
      xmlDocument.Load(xmlFilePath);
      return xmlDocument;
    }

    private XmlDocument LoadXmlFile(StreamReader streamReader)
    {
      var xmlDocument = new XmlDocument();
      xmlDocument.Load(streamReader);
      return xmlDocument;
    }


    private XmlDocument LoadXmlFile(Stream stream)
    {
      var xmlDocument = new XmlDocument();
      xmlDocument.Load(stream);
      return xmlDocument;
    }

    public PublicBranchNaming ReadOutBranchNameData(Stream stream, bool standardValuesFile = false)
    {
      var xmlDocument = LoadXmlFile(stream);
      return ReadOutPublicBranch(xmlDocument, standardValuesFile);
    }

    public PublicBranchNaming ReadOutBranchNameData(StreamReader streamReader, bool standardValuesFile = false)
    {
      var xmlDocument = LoadXmlFile(streamReader);
      return ReadOutPublicBranch(xmlDocument, standardValuesFile);
    }

    public PublicBranchNaming ReadOutBranchNameData(string filePath, bool standardValuesFile = false)
    {
      var xmlDocument = LoadXmlFile(filePath);
      return ReadOutPublicBranch(xmlDocument, standardValuesFile);
    }

    private PublicBranchNaming ReadOutPublicBranch(XmlDocument xmlDocument, bool standardValuesFile)
    {
      var manufacturer = xmlDocument.GetElementsByTagName(MANUFACTURER_TAG)[0].InnerText;
      var model = xmlDocument.GetElementsByTagName(MODEL_TAG)[0].InnerText;
	    var xmlHash = xmlDocument.SelectSingleNode(string.Format("//*[local-name()='{0}']", HASH_TAG))?.InnerText ?? ""; //.GetElementsByTagName(HASH_TAG)[0].InnerText;
      var date = xmlDocument.GetElementsByTagName(DATE_TAG)[0].InnerText; 
//      if (standardValuesFile)
//      {
//        date = DateTime.UtcNow.ToString(CommitDateHandler.DATE_FORMAT);
//      }
//      else
//      {
//        date = xmlDocument.GetElementsByTagName(DATE_TAG)[0].InnerText;
//      }
      var year = GetYearFromDateString(date);

      var branchName = new PublicBranchNameParts(manufacturer, model, year, xmlHash);

      return new PublicBranchNaming(branchName);
    }

    public DateTime ReadOutDateOfFile(Blob fileBlob)
    {
      using (var fileStream = fileBlob.GetContentStream())
      {
        var xmlDocument = LoadXmlFile(fileStream);
        var date = xmlDocument.GetElementsByTagName(DATE_TAG)[0].InnerText;
	      var dateTime = XmlConvert.ToDateTime(date, XmlDateTimeSerializationMode.Utc); // DateTime.ParseExact(date, CommitDateHandler.DATE_FORMAT, CultureInfo.InvariantCulture); //ISO 8601 Format 
        return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
      }
    }


    public DateTime ReadOutDateOfFile(string filePath)
    {
      var xmlDocument = LoadXmlFile(filePath);
      var date = xmlDocument.GetElementsByTagName(DATE_TAG)[0].InnerText;
	    var dateTime = XmlConvert.ToDateTime(date, XmlDateTimeSerializationMode.Utc); // DateTime.ParseExact(date, CommitDateHandler.DATE_FORMAT, CultureInfo.InvariantCulture); //ISO 8601 Format 
      return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);
    }


    private string GetYearFromDateString(string date)
    {
	    var parsedDate = XmlConvert.ToDateTime(date, XmlDateTimeSerializationMode.Utc); // DateTime.ParseExact(date, CommitDateHandler.DATE_FORMAT, CultureInfo.InvariantCulture);
      return parsedDate.Year.ToString();
    }


    public CertificateNumberHandler ReadOutCertificateId(StreamReader reader)
    {
      var xmlDocument = LoadXmlFile(reader);
      return CreateCertificatNumberHandler(xmlDocument);
    }

    public CertificateNumberHandler ReadOutCertificateId(Stream reader)
    {
      var xmlDocument = LoadXmlFile(reader);
      return CreateCertificatNumberHandler(xmlDocument);
    }


    public CertificateNumberHandler ReadOutCertificateId(string filePath)
    {
      var xmlDocument = LoadXmlFile(filePath);
      return CreateCertificatNumberHandler(xmlDocument);
    }


    private CertificateNumberHandler CreateCertificatNumberHandler(XmlDocument xmlDocument)
    {
      var certNumber = xmlDocument.GetElementsByTagName(CERTIFICATION_TAG)[0].InnerText;
	    var xmlHash = xmlDocument.SelectSingleNode(string.Format("//*[local-name()='{0}']", HASH_TAG))?.InnerText ?? ""; // GetElementsByTagName(HASH_TAG)[0].InnerText;

      return new CertificateNumberHandler(certNumber, xmlHash);
    }

  }
}
