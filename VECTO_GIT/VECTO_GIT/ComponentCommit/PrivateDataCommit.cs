﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using VECTO_GIT.Commands;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.ComponentCommit
{
  public abstract class PrivateDataCommit : ComponentCoreCommit
  {
    protected List<Item> selectedPaths;
//    protected Branch publicBranch;
    protected Branch privateBranch;
    protected string privateBranchName;
    protected CommitVerification verification;
    
    protected PrivateDataCommit(Repository repository, List<Item> selectedPaths) : base(repository)
    {
      this.selectedPaths = selectedPaths.CheckForNullEntry();
      verification = new CommitVerification(repository);
    }

    protected PrivateDataCommit(Repository repository, Branch privateBranch) : base(repository)
    {
      this.privateBranch = privateBranch.CheckNull("privateBranch");
    }

    protected bool VerifyGivenFilePaths(Branch branchToVerify, CommitType commitType)
    {
      var directoryPaths = GetTierOneDirectoryPaths();
      var filePaths = GetTierOneFilePaths();
      bool errors = false;

      if (directoryPaths.Count > 0)
      {
        foreach (var dirPath in directoryPaths)
        {
          var dirName = Path.GetDirectoryName(dirPath);
          var fileToCommit = new FileToCommit(dirName, dirPath, commitType);
          if (verification.FolderNameAlreadyUsed(fileToCommit, branchToVerify))
          {
            errors = true;
          }
        }
      }

      if (filePaths.Count > 0)
      {
        foreach (var filePath in filePaths)
        {
          var fileName = Path.GetFileName(filePath);
          var fileToCommit = new FileToCommit(fileName, filePath, commitType);
          if (verification.FileOrFileNameSaved(fileToCommit, branchToVerify))
          {
            errors = true;
          }
        }
      }

      if (errors)
      {
        verificationErrors = verification.Errors;
        return false;
      }
      return true;
    }


//    protected string GetPrivateBranchName()
//    {
//      var branchName = (string)null;
//      try
//      {
//        var branchNaming = new PublicBranchNaming(publicBranch.FriendlyName);
//        branchName = branchNaming.GetGitPrivateBranchName();
//      }
//      catch (ArgumentException ex)
//      {
//        verificationErrors.Add(new BranchNotAvailable(publicBranch.FriendlyName));
//      }
//
//      return branchName;
//    }


    protected List<Item> FetchSavedFiles(string rootTreeName)
    {
      var commitTree = privateBranch.Tip.Tree;
      var existingFiles = new List<Item>();

      foreach (var entry in commitTree)
      {
        if (entry.Name == rootTreeName &&
          entry.TargetType == TreeEntryTargetType.Tree && entry.Mode == Mode.Directory)
        {
          var measurementTree = entry.Target as Tree;
          if (measurementTree != null)
          {
            foreach (var mEntry in measurementTree)
            {
              if (mEntry.TargetType == TreeEntryTargetType.Blob)
              {
                existingFiles.Add(new FileItem()
                {
                  ExistingBlob = mEntry.Target,
                  ExistingBlobEntryType = mEntry.TargetType,
                  Filename = mEntry.Name
                });
              }
              else if (mEntry.TargetType == TreeEntryTargetType.Tree)
              {
                existingFiles.Add(new DirectoryItem()
                {
                  ExistingBlob = mEntry.Target,
                  ExistingBlobEntryType = mEntry.TargetType,
                  Filename = mEntry.Name
                });
              }
            }
          }
        }
      }

      return existingFiles.Count > 0 ? existingFiles : null;
    }


//    protected Tree GetTreeByName(Tree rootTree, string treeName)
//    {
//      foreach (var entry in rootTree)
//      {
//        if (entry.TargetType == TreeEntryTargetType.Tree && entry.Name == treeName)
//          return entry.Target as Tree;
//      }
//
//      return null;
//    }
//
//    protected Tree MergeTreeDefinition(Tree srcTree, List<TreeEntry> treeEntries)
//    {
//      var treeDefinition = TreeDefinition.From(srcTree);
//      foreach (var entry in treeEntries)
//      {
//        treeDefinition.Add(entry.Name, entry);
//      }
//      return repository.ObjectDatabase.CreateTree(treeDefinition);
//    }

    protected Tree MergeTree(Tree srcTree, string treeName, Tree newTree)
    {
      var treeDefinition = TreeDefinition.From(srcTree);
      treeDefinition.Add(treeName, newTree);
      return repository.ObjectDatabase.CreateTree(treeDefinition);
    }


    protected Tree SetTreeRoot(string treeRootName, Tree treeContent)
    {
      var treeDefinition = new TreeDefinition();
      treeDefinition.Add(treeRootName, treeContent);
      return repository.ObjectDatabase.CreateTree(treeDefinition);
    }

    protected Tree CreateTreeFromDirectory(List<Item> selectedDirectory)
    {
      var newDirectoryDef = new TreeDefinition();

      for (var i = 0; i < selectedDirectory.Count; i++)
      {
        var currentItem = selectedDirectory[i];

        if (currentItem is DirectoryItem)
        {
          GitObject resultTree;
          if (currentItem.ExistingBlob != null && currentItem.ExistingBlobEntryType == TreeEntryTargetType.Tree)
          {
            resultTree = currentItem.ExistingBlob;
          }
          else
          {
            resultTree = CreateTreeFromDirectory(((DirectoryItem)selectedDirectory[i]).Items);
          }
          newDirectoryDef.Add(currentItem.Filename, (Tree)resultTree);
        }
        else
        {
          GitObject resultBlob;
          if (currentItem.ExistingBlob != null && currentItem.ExistingBlobEntryType == TreeEntryTargetType.Blob)
          {
            resultBlob = currentItem.ExistingBlob;
          }
          else
          {
            resultBlob = new CreateBlobCommand(repository, currentItem.FilePath).Result();
          }

          newDirectoryDef.Add(selectedDirectory[i].Filename, (Blob)resultBlob, Mode.NonExecutableFile);
        }
      }

      return repository.ObjectDatabase.CreateTree(newDirectoryDef);
    }



    private List<string> GetTierOneFilePaths()
    {
      var paths = new List<string>();
      for (int i = 0; i < selectedPaths.Count; i++)
      {

        if (selectedPaths[i].GetType() == typeof(FileItem))
        {
          paths.Add(selectedPaths[i].FilePath);
        }
      }
      return paths;
    }

    private List<string> GetTierOneDirectoryPaths()
    {
      var dirNames = new List<string>();
      for (int i = 0; i < selectedPaths.Count; i++)
      {
        if (selectedPaths[i].GetType() == typeof(DirectoryItem))
        {
          dirNames.Add(selectedPaths[i].FilePath);
        }
      }
      return dirNames;
    }


    public static Tree GetPrivateDataTree(string privateTreeName, Branch branch)
    {
      privateTreeName.CheckNullOrEmtpy("treeName");
      branch.CheckNull("branch");

      if (!branch.FriendlyName.StartsWith(PrivateBranchNaming.TREE_NAME) &&
          !branch.FriendlyName.StartsWith(ConfigValue.REMOTE_NAME + "/" + PrivateBranchNaming.TREE_NAME))
      {
        return null;
      }

      var topCommit = branch.Tip;
      if (topCommit == null)
      {
        return null;
      }

      foreach (var entry in topCommit.Tree)
      {
        if (entry.Name == privateTreeName &&
            entry.TargetType == TreeEntryTargetType.Tree)
        {
          return entry.Target as Tree;
        }
      }
      return null;
    }
    

//    protected void SetPrivateBranchName(PublicBranchNaming pubBranchNaming, Branch prvBranch)
//    {
//      if (pubBranchNaming != null)
//        privateBranchName = pubBranchNaming.GetGitPrivateBranchName();
//      else if (prvBranch != null)
//        privateBranchName = prvBranch.FriendlyName;
//    }


  }
}
