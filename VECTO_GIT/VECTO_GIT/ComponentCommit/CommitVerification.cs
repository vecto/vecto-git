﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VECTO_GIT.ComponentNaming;

namespace VECTO_GIT.ComponentCommit
{
  public enum CommitType
  {
    PublicData,
    MeasurementData,
    UserDefinedData
  }

  public class FileToCommit
  {
    public string FileName { get; }
    public string FilePath { get; }
    public CommitType CommitTreeType { get; }

    private GitSha1Calculation gitSha1Calculation;

    public string TextFileHash
    {
      get { return gitSha1Calculation.TextFileGitHash; }
    }

    public string BinaryFileHash
    {
      get { return gitSha1Calculation.BinaryFileGitHash; }
    }





    public FileToCommit(string fileName, string filePath, CommitType commitTreeType)
    {
      FilePath = filePath;
      FileName = fileName;
      CommitTreeType = commitTreeType;
      CalculateFileContentHash();
    }

    private void CalculateFileContentHash()
    {
      if (!FilePath.IsExistingFolderPath())
      {
        gitSha1Calculation = new GitSha1Calculation(FilePath);
      }
    }
  }

  public class CommitVerification
  {

    public List<CommitVerificationError> Errors { get; }
    private readonly Repository repository;

    public CommitVerification(Repository repository)
    {
      this.repository = repository;
      Errors = new List<CommitVerificationError>();
    }

    public bool BranchNameInUse(string branchName, string filePath)
    {
      if (string.IsNullOrEmpty(branchName))
        throw new ArgumentException("Not a valid branch name");

      if (!repository.Branches.Any())
      {
        return false;
      }

      var branch = repository.Branches[branchName];
      if (branch != null)
      {
        var fileName = string.Empty;
        if (File.Exists(filePath))
        {
          fileName = Path.GetFileName(filePath);
        }
        else
        {
          fileName = filePath;
        }
        Errors.Add(new BranchNameInUse(branchName, fileName));
        return true;
      }
      return false;
    }

    public bool FileOrFileNameSaved(FileToCommit fileToCommit, Branch branch)
    {
      string branchName;
      if (branch != null)
        branchName = branch.FriendlyName;
      else
        throw new ArgumentException("Not a valid branch");

      if (string.IsNullOrEmpty(fileToCommit.FilePath))
        throw new ArgumentException("Not a valid file path");
      if (!branchName.StartsWith(PublicBranchNaming.TREE_NAME) &&
        !branchName.StartsWith(PrivateBranchNaming.TREE_NAME))
      {
        throw new ArgumentException("Branch with wrong naming convention");
      }

      var selectedTree = SelectTreeOfLastCommit(branch, fileToCommit.CommitTreeType);

      if (selectedTree != null)
      {
        return CheckFileSimilarities(selectedTree, fileToCommit);
      }
      return false;
    }

    public bool FolderNameAlreadyUsed(FileToCommit fileToCommit, Branch branch)
    {
      string branchName;
      if (branch != null)
        branchName = branch.FriendlyName;
      else
        throw new ArgumentException("Not a valid branch");

      if (!branchName.StartsWith(PublicBranchNaming.TREE_NAME) &&
        !branchName.StartsWith(PrivateBranchNaming.TREE_NAME))
      {
        throw new ArgumentException("Branch with wrong naming convention");
      }

      var selectedTree = SelectTreeOfLastCommit(branch, fileToCommit.CommitTreeType);

      if (selectedTree != null)
      {
        return CheckFolderNameSimilarities(selectedTree, fileToCommit.FileName);
      }
      return false;

    }



    private bool CheckFileSimilarities(Tree commitTree, FileToCommit fileToCommit)
    {
      if (commitTree == null)
      {
        return false;
      }

      foreach (var entry in commitTree)
      {
        if (entry.TargetType == TreeEntryTargetType.Tree)
        {
          continue;
        }

        var equalFileName = entry.Name.Equals(fileToCommit.FileName);
        var fileHash = entry.Target.Sha;
        var equalFileContent = (fileHash.Equals(fileToCommit.BinaryFileHash) ||
                                fileHash.Equals(fileToCommit.TextFileHash));

        if (equalFileName && equalFileContent)
        {
          Errors.Add(new FileAlreadySaved(fileToCommit.FileName, fileToCommit.FilePath));
          return true;
        }
        if (equalFileName)
        {
          Errors.Add(new FileNameInUse(fileToCommit.FileName, fileToCommit.FilePath));
          return true;
        }
        if (equalFileContent)
        {
          Errors.Add(new FileContentSaved(fileToCommit.FileName, fileToCommit.FilePath));
          return true;
        }
      }
      return false;
    }



    private bool CheckFolderNameSimilarities(Tree commitTree, string newFolderName)
    {

      if (commitTree == null)
      {
        return false;
      }


      foreach (var entry in commitTree)
      {
        if (entry.TargetType != TreeEntryTargetType.Tree)
        {
          continue;
        }

        var treeName = entry.Name;
        if (treeName == newFolderName)
        {
          Errors.Add(new FolderNameAlreadyInUse(treeName));
          return true;
        }
      }

      return false;
    }



    private Tree SelectTreeOfLastCommit(Branch branch, CommitType treeType)
    {
      var lastCommit = branch.Tip;
      if (treeType.Equals(CommitType.PublicData))
      {
        return lastCommit.Tree;
      }
      else if (treeType.Equals(CommitType.MeasurementData))
      {
        return PrivateDataCommit.GetPrivateDataTree
          (ConfigValue.MEASUREMENT_DATA_TREE_NAME, branch);
        //return MeasurementDataCommit.GetMeasurementDataTree(branch);
      }
      else if (treeType.Equals(CommitType.UserDefinedData))
      {
        return PrivateDataCommit.GetPrivateDataTree
          (ConfigValue.USER_DATA_TREE_NAME, branch);
        //return UserDataCommit.GetUserDataTree(branch);
      }
      //foreach (var entry in lastCommit.Tree)
      //{

      //  if (treeType.Equals(CommitType.PublicData))
      //  {
      //    return lastCommit.Tree;
      //  }
      //  else if (treeType.Equals(CommitType.MeasurementData) &&
      //    entry.Name.Equals(ConfigValue.MEASUREMENT_DATA_TREE_NAME))
      //  {
      //    return (Tree)entry.Target;
      //  }
      //  else if (treeType.Equals(CommitType.UserDefinedData) &&
      //    entry.Name.Equals(ConfigValue.USER_DATA_TREE_NAME))
      //  {
      //    return (Tree)entry.Target;
      //  }
      //}
      return null;
    }


    public bool GitIdAvailable(string tagName)
    {
      if (string.IsNullOrEmpty(tagName))
        throw new ArgumentException("Not a valid git id tag name");


      if (SearchTagbyName(tagName) == null)
      {
        Errors.Add(new GitIdNotAvailable(tagName));
        return false;
      }
      return true;
    }


    public bool CertificateNumberAvailable(string tagName)
    {
      if (string.IsNullOrEmpty(tagName))
        throw new ArgumentException("Not a valid certifictaion number tag name");

      if (SearchTagbyName(tagName) == null)
      {
        Errors.Add(new CertificateNumberNotAvailable(tagName));
        return false;
      }
      return true;
    }

    private Tag SearchTagbyName(string tagName)
    {
      return repository.Tags.First(obj => obj.CanonicalName.Equals(tagName));
    }

  }
}
