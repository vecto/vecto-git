﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VECTO_GIT.ComponentNaming;


namespace VECTO_GIT.ComponentCommit
{
  public class ComponentDataCommit : ComponentCoreCommit
  {
    private CertificateNumberHandler certificateHandler;
    private PublicBranchNaming branchNameHandler;

    private readonly Branch standardValuesBranch;
    private readonly string standardValueFilePath;
    private readonly string componentDataFilePath;
    private readonly Blob componentDataBlob;
    private readonly XmlContentReader xmlContentReader = new XmlContentReader();
    
    private DateTime certificationDate;

    public ComponentDataCommit(Repository repository, string componentDataFilePath,
      Branch standardValuesBranch, string standardValueFilePath): base(repository)
    {
      this.componentDataFilePath = componentDataFilePath.CheckFilePath();
      this.standardValuesBranch = standardValuesBranch;
      this.standardValueFilePath = standardValueFilePath;

      ReadXmlContent();
    }

    public ComponentDataCommit(Repository repository, Branch standardValuesBranch,
      Blob componentDataBlob) : base(repository)
    {
      this.componentDataBlob = componentDataBlob.CheckNull(nameof(componentDataBlob));
      this.standardValuesBranch = standardValuesBranch.CheckNull(nameof(standardValuesBranch));

      ReadBlobContent();
    }
    
    public override bool VerificationCheckBeforeCommit()
    {
      verificationCheckResult = VerificationChecks();
      return verificationCheckResult;
    }
    

    public override Branch ExecuteCommit()
    {
      if (!verificationCheckResult)
        return null;

      var stdBranch = GetStandardBranch();
      var compBlob = GetComponentDataBlob();
      var commitDateHandler = new CommitDateHandler(certificationDate, CommitDateType.ComponentData);

      Commit commit;
      if (stdBranch != null)
      {
        commit = GetCompDataCommitWithStdValue(stdBranch, compBlob, commitDateHandler);
      }
      else
      {
        commit = GetDefaultCompDataCommit(compBlob, commitDateHandler);
      }

      var branch = CreateOrphanBranch(commit, branchNameHandler.GetGitPathBranchName());

      var gitIdHandler = new GitIdHandler(compBlob.Sha, false);
      CreateTag(commit, branchNameHandler.GetJsonRepesentation(), gitIdHandler.GetTagGitPathName());
      CreateTag(commit, branchNameHandler.GetJsonRepesentation(), certificateHandler.GetTagGitPathName());

      if (stdBranch != null)
      {
        repository.Branches.Remove(stdBranch);
      }

      verificationCheckResult = false;
      return branch;
    }


    protected override bool VerificationChecks()
    {
      var verification = new CommitVerification(repository);
      if (verification.BranchNameInUse(branchNameHandler.GetGitPathBranchName(), componentDataFilePath))
      {
        verificationErrors = verification.Errors;
        return false;
      }
      return true;
    }

    private Commit GetDefaultCompDataCommit(Blob compBlob, CommitDateHandler commitDateHandler)
    {
      var tree = CreateTree(compBlob, ConfigValue.COMPONENT_DATA_FILE_NAME);
      return CreateCommit(tree, commitDateHandler.GetJsonRepresentation());
    }

    private Commit GetCompDataCommitWithStdValue(Branch stdBranch, Blob compBlob, CommitDateHandler commitDateHandler )
    {
      var stdCommit = StandardDataCommit.GetStandardDataCommit(stdBranch);
      var stdBlob = StandardDataCommit.GetStandardDataBlob(stdCommit);

      var blobsToCommit = new Dictionary<string, Blob>
      {
          { ConfigValue.STANDARD_VALUES_FILE_NAME , stdBlob},
          { ConfigValue.COMPONENT_DATA_FILE_NAME , compBlob}
      };

      var tree = CreateTree(blobsToCommit);
      return CreateCommit(null, stdCommit, tree, commitDateHandler.GetJsonRepresentation());
    }

    private void ReadBlobContent()
    {
      branchNameHandler = GetPublicBranchNaming(componentDataBlob);
      certificateHandler = GetCertificateNumberHandler(componentDataBlob);
      certificationDate = GetCertificateDate(componentDataBlob);
    }
    
    private void ReadXmlContent()
    {
      branchNameHandler = GetBranchNameHandler(componentDataFilePath);
      certificateHandler = GetCertificateNumberHandler(componentDataFilePath);
      certificationDate = GetCertificateDate(componentDataFilePath);
    }

    private PublicBranchNaming GetBranchNameHandler(string filePath)
    {
      return xmlContentReader.ReadOutBranchNameData(filePath);
    }

    private PublicBranchNaming GetPublicBranchNaming(Blob compDataBlob)
    {
      using (var streamReader = new StreamReader(compDataBlob.GetContentStream(), Encoding.Default))
      {
        return xmlContentReader.ReadOutBranchNameData(streamReader);
      }
    }
    
    private CertificateNumberHandler GetCertificateNumberHandler(string filePath)
    {
      return xmlContentReader.ReadOutCertificateId(filePath);
    }

    private CertificateNumberHandler GetCertificateNumberHandler(Blob compDataBlob)
    {
      using (var streamReader = new StreamReader(compDataBlob.GetContentStream(), Encoding.Default))
      {
        return xmlContentReader.ReadOutCertificateId(streamReader);
      }
    }

    private DateTime GetCertificateDate(Blob compDataBlob)
    {
      return xmlContentReader.ReadOutDateOfFile(compDataBlob);
    }

    private DateTime GetCertificateDate(string filePath)
    {
      return xmlContentReader.ReadOutDateOfFile(filePath);
    }
    
    private Branch GetStandardBranch()
    {
      if (standardValuesBranch != null)
        return standardValuesBranch;

      if (standardValueFilePath != null)
      {
        var stdBranchName = GetBranchNameHandler(standardValueFilePath);
        return FetchBranchByName(stdBranchName.GetGitPathBranchName());
      }

      return null;
    }

    private Blob GetComponentDataBlob()
    {
      if (componentDataBlob != null)
        return componentDataBlob;

      return CreateBlob(componentDataFilePath);
    }


    public static Commit GetComponentDataCommit(Branch branch)
    {
      branch.CheckNull("branch argument");

      if (!branch.FriendlyName.StartsWith(PublicBranchNaming.TREE_NAME) &&
          !branch.FriendlyName.StartsWith(ConfigValue.REMOTE_NAME + "/" + PublicBranchNaming.TREE_NAME))
      {
        return null;
      }

      foreach (var commit in branch.Commits)
      {
        var compDataBlob = GetComponentDataBlob(commit);
        if (compDataBlob != null)
        {
          return commit;
        }
      }
      return null;
    }


    public static Blob GetComponentDataBlob(Commit commit)
    {
      if (commit == null)
      {
        return null;
      }

      if (!commit.Message.Contains(CommitDateHandler.COMP_DATA_DATE_NAME))
      {
        return null;
      }

      var tree = commit.Tree;
      if (tree == null || !(tree.Count <= 2))
        return null;
     
      foreach (var treeEntry in tree)
      {
        if(treeEntry.Name == ConfigValue.COMPONENT_DATA_FILE_NAME)
        {
          return treeEntry.Target as Blob;
        } 
      }
      return null;
    }


    public static Blob GetComponentDataBlob(Branch branch)
    {
      var componentCommit = GetComponentDataCommit(branch);

      return (componentCommit == null) ? null : GetComponentDataBlob(componentCommit);
    }

  }
}
