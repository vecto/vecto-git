﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VECTO_GIT.GUI;
using Ninject;
using VECTO_GIT.GUI.DI;
using System.Reflection;
using TUGraz.VectoCore;
using VECTO_GIT.View;

namespace VECTO_GIT
{  
  public partial class App : Application
  {
    public IKernel iocKernel { get; set; }

    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);

      iocKernel = new StandardKernel();
      iocKernel.Load(new InjectModule());
	  iocKernel.Load(new VectoNinjectModule());
      iocKernel.Inject(this);


      Current.MainWindow = iocKernel.Get<MainWindow>();
      Current.MainWindow.Show();

      
    }
  }
}
