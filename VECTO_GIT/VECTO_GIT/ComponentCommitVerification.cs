﻿using LibGit2Sharp;
using System.Collections.Generic;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT
{
  public class ComponentCommitVerification
  {
    private readonly ComponentDataManagement compDataManagement;

    public List<ComponentCoreCommit> commitsToExecute { get; private set; }
    public List<List<CommitVerificationError>> verificationErrors { get; private set; }

    public ComponentCommitVerification(ComponentDataManagement compDataManagement)
    {
      this.compDataManagement = compDataManagement;
    }

    public bool VerifyCommitsBeforeSave(ComponentToSave componentToSave)
    {
      commitsToExecute = new List<ComponentCoreCommit>();
      verificationErrors = new List<List<CommitVerificationError>>();

      if (!VerifyManufacturerModelName(componentToSave))
      {
        return false;
      }

      if (componentToSave.StandardValuesFilePath != null)
      {
        VerifyStandardValues(componentToSave);
      }

      if (componentToSave.ComponentDataFilePath != null)
      {
        VerifyComponentData(componentToSave);
      }

      if (componentToSave.CertificatetDataFilePath != null)
      {
        VerifyCertificateData(componentToSave);
      }

      if (componentToSave.SelectedMeasurementData.Count > 0)
      {
        VerifyMeasurementData(componentToSave);
      }

      if (componentToSave.SelectedUserData.Count > 0)
      {
        VerifyUserDefinedData(componentToSave);
      }

      return !(verificationErrors.Count > 0);
    }

    private bool VerifyManufacturerModelName(ComponentToSave componentToSave)
    {
      var xmlReader = new XmlContentReader();

      PrivateBranchNaming prvBranchNaming = null;
      if (componentToSave.PrivateDataBranch != null)
      {
        prvBranchNaming = new PrivateBranchNaming(componentToSave.PrivateDataBranch.FriendlyName);
      }
      
      var stdBranchNaming = ReadPublicBranchNaming(xmlReader, componentToSave.StandardValuesFilePath);
      var compBranchNaming = ReadPublicBranchNaming(xmlReader, componentToSave.ComponentDataFilePath);

      if (componentToSave.StandardValuesBranch != null)
      {
        var stdBlob = StandardDataCommit.GetStandardDataBlob(componentToSave.StandardValuesBranch);
        stdBranchNaming = ReadPublicBranchNaming(xmlReader, stdBlob);
      }
      if (componentToSave.ComponentDataBranch != null)
      {
        var compBlob = ComponentDataCommit.GetComponentDataBlob(componentToSave.ComponentDataBranch);
        compBranchNaming = ReadPublicBranchNaming(xmlReader, compBlob);
      }
      
      var verificationResult = VerifyNamings(stdBranchNaming, compBranchNaming, prvBranchNaming);
      if (!verificationResult)
        SetNamingVerificationError(prvBranchNaming != null);

      return verificationResult;
    }

    private void SetNamingVerificationError(bool anyPrivateBranch)
    {
      if(anyPrivateBranch)
        verificationErrors.Add(new List<CommitVerificationError>() { new PrvManufacturerModelNaming()});
      else
        verificationErrors.Add(new List<CommitVerificationError>() { new PubManufacturerModelNaming()});

    }


    private bool VerifyNamings(PublicBranchNaming stdBranchNaming, PublicBranchNaming compBranchNaming, PrivateBranchNaming prvBranchNaming)
    {
      if (prvBranchNaming != null)
      {
        if (stdBranchNaming != null && compBranchNaming != null)
        {
          if (stdBranchNaming.Manufacturer == prvBranchNaming.Manufacturer &&
              stdBranchNaming.Model == prvBranchNaming.Model &&
              compBranchNaming.Manufacturer == stdBranchNaming.Manufacturer &&
              compBranchNaming.Model == stdBranchNaming.Model)
            return true;
          return false;
        }

        if (stdBranchNaming != null)
        {
          if (stdBranchNaming.Manufacturer == prvBranchNaming.Manufacturer &&
              stdBranchNaming.Model == prvBranchNaming.Model)
            return true;
          return false;
        }

        if (compBranchNaming != null)
        {
          if (compBranchNaming.Manufacturer == prvBranchNaming.Manufacturer &&
              compBranchNaming.Model == prvBranchNaming.Model)
            return true;
          return false;
        }
      }
      
      if (stdBranchNaming != null && compBranchNaming != null)
      {
        if (stdBranchNaming.Manufacturer == compBranchNaming.Manufacturer &&
            stdBranchNaming.Model == compBranchNaming.Model)
          return true;
      }

      if (stdBranchNaming == null || compBranchNaming == null)
        return true;

      return false;
    }



    private void VerifyStandardValues(ComponentToSave componentToSave)
    {
      var result = compDataManagement.VerifySaveStandardValueFile(componentToSave.StandardValuesFilePath, false);

      SetVerificationErrors(result);
    }

    private void VerifyComponentData(ComponentToSave componentToSave)
    {
      var result = compDataManagement.VerifySaveComponentData(
        componentToSave.ComponentDataFilePath,
        componentToSave.StandardValuesBranch,
        componentToSave.StandardValuesFilePath,
        false);

      SetVerificationErrors(result);
    }

    private void VerifyCertificateData(ComponentToSave componentToSave)
    {
      var result = compDataManagement.VerifySaveCertificateByComponentFile(componentToSave.CertificatetDataFilePath,
        componentToSave.ComponentDataFilePath, componentToSave.ComponentDataBranch);

      SetVerificationErrors(result);
    }

    private void VerifyMeasurementData(ComponentToSave componentToSave)
    {
      var result = compDataManagement.VerifySaveMeasurementData(componentToSave);
      SetVerificationErrors(result);
    }

    private void VerifyUserDefinedData(ComponentToSave componentToSave)
    {
      var result = compDataManagement.VerifySaveUserData(componentToSave);
      SetVerificationErrors(result);
    }

    private void SetVerificationErrors(ComponentCoreCommit result)
    {
      if (result.IsNull())
      {
        verificationErrors.Add(compDataManagement.CommitVerificationErrors);
      }
      else
      {
        commitsToExecute.Add(result);
      }
    }

    #region Read XML Content

    private PublicBranchNaming ReadPublicBranchNaming(XmlContentReader xmlContentReader, string filePath)
    {
      return filePath != null ? xmlContentReader.ReadOutBranchNameData(filePath) : null;
    }

    private PublicBranchNaming ReadPublicBranchNaming(XmlContentReader xmlContentReader, Blob blob)
    {
      if (blob != null)
      {
        using (var stream = blob.GetContentStream())
        {
          return xmlContentReader.ReadOutBranchNameData(stream);
        }
      }

      return null;
    }

    #endregion



  }
}
