﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

using LibGit2Sharp;
using VECTO_GIT;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.Commands;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT.ComponentTransfer
{
  public class TransferHandler
  {
    #region Members
    private string transferRepositoryPath;

    #endregion

    #region Properties

    public IEnumerable<string> ErrorMessages { private set; get; }
    public string CreatedArchivePath { private set; get; }

    public List<KeyValuePair<string, bool>> MergeMessages { get; set; }

    #endregion
    


    #region Merge Methods  
    public List<MergePrivateBranchModel> MergeExtractedArchiveIntoRepository(Repository destRepository, string extractedArchivePath)
    {
      if (!extractedArchivePath.IsExistingFolderPath() || destRepository == null)
      {
        return null;
      }

      MergeMessages = null;

      var pullCommand = new PullRemoteCommand(destRepository, extractedArchivePath);

      SetMergeMessages(pullCommand.PublicMergeMessages);
      SetMergeMessages(pullCommand.PrivateMergeMessages);

      return pullCommand.Result() ? null : pullCommand.PrivateMergeConflicts;
    }

    public bool MergeConflictsIntoRepository(Repository destRepository, string extractedArchivePath, List<MergePrivateBranchModel> conflicts)
    {
      if (!extractedArchivePath.IsExistingFolderPath() || destRepository == null || conflicts == null)
      {
        return false;
      }

      var pullCommand = new PullRemoteCommand(destRepository, extractedArchivePath, conflicts);
    
      SetMergeMessage(new KeyValuePair<string, bool>("Private data conflicts successfully merged", true));

      return pullCommand.Result();
    }

    private void SetMergeMessage(KeyValuePair<string, bool> mergeMessage)
    {
      if (MergeMessages == null)
        MergeMessages = new List<KeyValuePair<string, bool>>();

      MergeMessages.Add(mergeMessage);
    }


    private void SetMergeMessages(List<KeyValuePair<string, bool>> importMessages)
    {
      if (importMessages == null)
        return;

      if(MergeMessages == null)
        MergeMessages = new List<KeyValuePair<string, bool>>();

      MergeMessages.AddRange(importMessages);
    }


    #endregion


    #region Download Transfer Archive

    public bool DownloadArchiveForTransfer(Repository srcRepository, IList<ComponentForTransfer> components, string downloadDest)
    {
      if (srcRepository.IsNull() || components.IsNullOrEmpty() || downloadDest.IsExistingFolderPath())
        return false;

      var branchesForTransfer = GetAllBranchNamesForTransfer(components);

      List<Branch> newStdBranches = null;
      if (branchesForTransfer.Count > 0)
      {
        var stdBranches = FetchTransferStandardBranches(components);
        if (stdBranches.Count > 0)
        {
          newStdBranches = CreateMissingStdValueBranches(srcRepository, stdBranches);
        }
      }

      var archivePath = CreateTransferArchive(srcRepository, branchesForTransfer, downloadDest);
      if (archivePath == null)
      {
        RemoveBranches(srcRepository, newStdBranches);
        return false;
      }

      RemoveBranches(srcRepository, newStdBranches);
      CreatedArchivePath = archivePath;
      return RemoveTransferRepository(); //ToDo add Error messages ?!?      
    }


    private List<string> GetAllBranchNamesForTransfer(IList<ComponentForTransfer> components)
    {
      var branchNames = new List<string>();
      for (int i = 0; i < components.Count; i++)
      {
        var component = components[i];
        if (component.PublicBranch != null && component.PublicBranch.TransferBranch)
        {
          branchNames.Add(component.PublicBranch.BranchName);
        }

        if (component.PrivatBranch != null && component.PrivatBranch.TransferBranch)
        {
          branchNames.Add(component.PrivatBranch.BranchName);
        }

        if (component.StandardBranch != null && component.StandardBranch.TransferBranch)
        {
          branchNames.Add(component.StandardBranch.BranchName);
        }
      }
      return branchNames;
    }


    private List<Branch> CreateMissingStdValueBranches(Repository repository, List<ComponentStandardBranch> stdBranches)
    {
      var branches = new List<Branch>();

      foreach (var stdBranch in stdBranches)
      {
        var commit = repository.Lookup(stdBranch.StandardCommitId) as Commit;
        if (commit != null)
        {
          var createdBranch = new CreateOrphanBranchCommand(repository, stdBranch.BranchName, commit).Result();
          branches.Add(createdBranch); //ToDo Handle error if no branch is created!!!!
        }
      }

      return branches;
    }


    private List<ComponentStandardBranch> FetchTransferStandardBranches(IList<ComponentForTransfer> components)
    {
      var stdValueStdBranches = new List<ComponentStandardBranch>();

      for (int i = 0; i < components.Count; i++)
      {
        var component = components[i];
        if (TransferStdValueBranchNeeded(component))
        {
          stdValueStdBranches.Add(component.StandardBranch);
        }
      }

      return stdValueStdBranches;
    }


    private bool TransferStdValueBranchNeeded(ComponentForTransfer component)
    {
      if (component.StandardBranch != null && component.StandardBranch.TransferBranch)
      {
        if (component.PublicBranch.BranchName != null &&
            component.PublicBranch.BranchName != component.StandardBranch.BranchName)
        {
          return true;
        }
      }
      return false;
    }


    private string CreateTransferArchive(Repository srcRepository, List<string> selectedBranches,
      string destArchivePath)
    {

      transferRepositoryPath = GetFolderPath(srcRepository.Info.Path, ConfigValue.TRANSFER_ARCHIVE_FOLDER_NAME);
      var pushCommand = new PushBranchCommand(srcRepository, transferRepositoryPath, selectedBranches);
      transferRepositoryPath = pushCommand.Result();

      if (transferRepositoryPath != null)
      {
        return CreateArchive(transferRepositoryPath, destArchivePath);
      }
      ErrorMessages = pushCommand.PushErrors;

      return null;
    }

    private string CreateArchive(string srcTransferPath, string destArchivePath)
    {
      try
      {
        ZipFile.CreateFromDirectory(srcTransferPath, destArchivePath);
      }
      catch (Exception ex)
      {
        ErrorMessages = new []
        {
          $"Creation of archive failed : {ex.Message}"
        };
        return null;
      }
      return destArchivePath;
    }



    #endregion


    public string ExtractArchiveIntoFolder(string destFolderPath, string archiveFilePath)
    {
      var destPath = GetFolderPath(destFolderPath, ConfigValue.EXTRACT_ARCHIVE_FOLDER_NAME);

      try
      {
        ZipFile.ExtractToDirectory(archiveFilePath, destPath);
      }
      catch (Exception ex)
      {
        ErrorMessages = new []
        {
          $"Extraction of archive failed : {ex.Message}"
        };
        return null;
      }
      return destPath;
    }

    private string GetFolderPath(string repositoryPath, string folderName)
    {
      var dirPath = Path.Combine(repositoryPath, folderName);
      Helpers.CreateDirectory(dirPath);
      return dirPath;
    }

    private void RemoveBranches(Repository repository, List<Branch> branches)
    {
      if (repository == null || branches == null)
        return;

      for (int i = 0; i < branches.Count; i++)
      {
        repository.Branches.Remove(branches[i]);
      }
    }


    private bool RemoveTransferRepository()
    {
      if (transferRepositoryPath == null)
      {
        return false;
      }

      Helpers.DeleteDirectory(transferRepositoryPath);

      return true;
    }
  }
}
