﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VECTO_GIT
{
  public class StringCharacterLookup
  {
    private Dictionary<char, char> replacementLookup;
    private Dictionary<char, char> originLookup;

    private static StringCharacterLookup instance;
    public static StringCharacterLookup Instance
    {
      get
      {
        if (instance == null)
        {
          instance = new StringCharacterLookup();
        }
        return instance;
      }

      set
      {
        instance = value;
      }
    }
    
    private StringCharacterLookup()
    {
      SetUpLookupList();
    }

    //ToDo maybe a faster implementation
    //https://stackoverflow.com/a/1321366/3556684


    private void SetUpLookupList()
    {
      replacementLookup = new Dictionary<char, char>()
      {
        { '\\' , '(' },
        { '/'  , ')' },
        { ':'  , ';' },
        { '*'  , '#' },
        { '?'  , '$' },
        { '"'  , ',' },
        { '<'  , ']' },
        { '>'  , '[' },
        { '|'  , '+' },
        { ' '  , '_' },
        { '.'  , '%' }
      };

      originLookup = new Dictionary<char, char>();

      foreach(var replacement in replacementLookup)
      {
        originLookup[replacement.Value] = replacement.Key;
      }
    }

    public string ConvertToFileSystemCompatible(string target)
    {
      return ExchangeCharacters(replacementLookup, target);
    }
    public string ConvertToOrigin(string target)
    {
      return ExchangeCharacters(originLookup, target);
    }

    private string ExchangeCharacters(Dictionary<char, char> replacementList, string target)
    {
      if (string.IsNullOrEmpty(target))
      {
        return target;
      }

      var allowed = target.ToCharArray();

      for (int i = 0; i < allowed.Length; i++)
      {
        char value;
        if (replacementList.TryGetValue(allowed[i], out value))
        {
          allowed[i] = value;
        }
      }
      return new string(allowed);
    }



  }
}
