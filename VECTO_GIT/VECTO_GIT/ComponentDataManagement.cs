﻿using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VECTO_GIT.ComponentCommit;
using VECTO_GIT.ComponentNaming;
using VECTO_GIT.ComponentSearch;
using VECTO_GIT.GUI.Model;

namespace VECTO_GIT
{
  public class ComponentDataManagement : IComponentDataManagement, IDisposable
  {
    private const string XSD_FILE_NAME = "VectoComponent.xsd";
    private const string XSD_NAMESPACE = "urn:tugraz:ivt:VectoAPI:DeclarationComponent:v1.0";
    private const bool IS_BARE = true;

    public Repository CurrentRepository { get; private set; }
    public List<string> XmlErrors { get; private set; }
    public List<string> XmlWarnings { get; private set; }

    public List<CommitVerificationError> CommitVerificationErrors { get; private set; }

    private readonly XmlValidator xmlValidator;
    private readonly XmlContentReader xmlReader;
    public VirtualDrive VirtualDrive { get; }
    
    private bool disposed;
    
    public ComponentDataManagement()
    {
      xmlValidator = GetComponentDataValidator().CheckNull(nameof(xmlValidator));
      xmlReader = GetXmlReader().CheckNull(nameof(xmlReader));
      VirtualDrive = new VirtualDrive();
    }

    public Repository SetCurrentRepository(string repositoryPath)
    {
      if (repositoryPath == null || !Directory.Exists(repositoryPath))
      {
        throw new ArgumentException(nameof(repositoryPath));
      }

      string repoPath;
      if (VirtualDrive.AnyExistingVirtualDrive())
      {
        repoPath = VirtualDrive.ChangeVirtualDrive(repositoryPath);
      }
      else
      {
        repoPath = VirtualDrive.MapDrive(repositoryPath);
      }
      
      CurrentRepository = Helpers.CreateOrLoadRepository(repoPath, IS_BARE);
      return CurrentRepository;
    }

    private XmlValidator GetComponentDataValidator()
    {
      var xsdPath = Path.Combine(Helpers.XSDFolderPath, XSD_FILE_NAME);
      return new XmlValidator(xsdPath, XSD_NAMESPACE);
    }

    private XmlContentReader GetXmlReader()
    {
      return new XmlContentReader();
    }


    #region Component data save method

    public ComponentDataCommit VerifySaveComponentData(string compDataFilePath, Branch stdValuesBranch, string stdValuesFilePath, bool validateXml)
    {
      if (compDataFilePath == null || (validateXml && !IsValidXml(compDataFilePath)))
      {
        return null;
      }

      try
      {
        var compDataCommit = new ComponentDataCommit(CurrentRepository, compDataFilePath, stdValuesBranch, stdValuesFilePath);
        if (!compDataCommit.VerificationCheckBeforeCommit())
        {
          CommitVerificationErrors = compDataCommit.GetVerificationErrors();
          return null;
        }
        return compDataCommit;
      }
      catch (Exception ex)
      {
        return null;
      }
    }

    #endregion

    #region Standard Values save method

    public StandardDataCommit VerifySaveStandardValueFile(string filePath, bool validateXml)
    {
      if (filePath == null || (validateXml && !IsValidXml(filePath)))
      {
        return null;
      }

      try
      {
        var stdDataCommit = new StandardDataCommit(CurrentRepository, filePath);
        if (!stdDataCommit.VerificationCheckBeforeCommit())
        {
          CommitVerificationErrors = stdDataCommit.GetVerificationErrors();
          return null;
        }
        return stdDataCommit;
      }
      catch (Exception ex)
      {
        return null;
      }
    }

    #endregion

    #region Certificate save methods

    public CertificateDataCommit VerifySaveCertificateByComponentFile(string certFilePath, string compFilePath, Branch pubBranch)
    {
      if (certFilePath == null || (compFilePath == null && pubBranch == null))
        return null;

      PublicBranchNaming branchNaming;
      CertificateNumberHandler certNumberHandler;
      if (compFilePath != null)
      {
        branchNaming = ReadPublicBranchNaming(compFilePath);
        certNumberHandler = ReadCertificateNumberHandler(compFilePath);
      }
      else
      {
        var compBlob = ComponentDataCommit.GetComponentDataBlob(pubBranch);
        if (compBlob != null)
        {
          branchNaming = ReadPublicBranchNaming(compBlob);
          certNumberHandler = ReadCertificateNumberHandler(compBlob);
        }
        else
          return null;//if not an branch with componentdata
      }

      try
      {
        var certCommit = new CertificateDataCommit(CurrentRepository, certFilePath, branchNaming, pubBranch, certNumberHandler);
        if (!certCommit.VerificationCheckBeforeCommit())
        {
          CommitVerificationErrors = certCommit.GetVerificationErrors();
          return null;
        }
        return certCommit;
      }
      catch (Exception ex)
      {
        return null;
      }
    }

    #endregion

    #region Measurement Data save methods

    public MeasurementDataCommit VerifySaveMeasurementData(ComponentToSave componentToSave)
    {
      if (componentToSave == null)
        return null;

      var privateBranchName = GetPrivateBranchName(componentToSave);
      if (privateBranchName == null)
        return null; //ToDo error message

      try
      {
        var mDataCommit = new MeasurementDataCommit(CurrentRepository, componentToSave.SelectedMeasurementData, privateBranchName);
        if (!mDataCommit.VerificationCheckBeforeCommit())
        {
          CommitVerificationErrors = mDataCommit.GetVerificationErrors();
          return null;
        }
        return mDataCommit;
      }
      catch (Exception ex)
      {
        return null;
      }
    }

    #endregion

    #region User defined data save methods

    public UserDataCommit VerifySaveUserData(ComponentToSave componentToSave)
    {
      if (componentToSave == null)
        return null;

      var privateBranchName = GetPrivateBranchName(componentToSave);
      if (privateBranchName == null)
        return null; //ToDo error message

      try
      {
        var uDataCommit = new UserDataCommit(CurrentRepository, componentToSave.SelectedUserData, privateBranchName);
        if (!uDataCommit.VerificationCheckBeforeCommit())
        {
          CommitVerificationErrors = uDataCommit.GetVerificationErrors();
          return null;
        }
        return uDataCommit;
      }
      catch (Exception ex)
      {
        return null;
      }
    }

    #endregion

    #region Search vecto components

    public IEnumerable<VectoComponent> SearchVectoComponentsByGivenTerms(VectoSearchTerms searchTerms)
    {
      var searchCommand = new SearchVectoComponent(CurrentRepository);
      return searchCommand.SearchVectoComponentWithGivenTerms(searchTerms);
    }

    public IEnumerable<VectoComponent> SearchAllVectoComponents()
    {
      var searchCommand = new SearchVectoComponent(CurrentRepository);
      return searchCommand.SearchAllVectoComponents();
    }

    #endregion

    private string GetPrivateBranchName(ComponentToSave componentToSave)
    {
      if (componentToSave.PrivateDataBranch != null)
        return componentToSave.PrivateDataBranch.FriendlyName;

      var publicXmlFile = GetPublicXmlFilePath(componentToSave);

      PublicBranchNaming pubBranchNaming = null;
      if (publicXmlFile != null)
      {
        pubBranchNaming = ReadPublicBranchNaming(publicXmlFile);
      }
      else
      {
        var pubBranch = GetPublicBranch(componentToSave);
        if (pubBranch == null)
          return null; // ToDo error message

        var cmpBlob = ComponentDataCommit.GetComponentDataBlob(pubBranch);
        if (cmpBlob != null)
        {
          pubBranchNaming = ReadPublicBranchNaming(cmpBlob);
        }
        else
        {
          var stdBlob = StandardDataCommit.GetStandardDataBlob(pubBranch);
          if (stdBlob != null)
          {
            pubBranchNaming = ReadPublicBranchNaming(stdBlob);
          }
        }
      }

      if (pubBranchNaming != null)
        return pubBranchNaming.GetGitPrivateBranchName();
      
      return null;
    }


    private string GetPublicXmlFilePath(ComponentToSave componentToSave)
    {
      if (componentToSave.ComponentDataFilePath != null)
        return componentToSave.ComponentDataFilePath;

      return componentToSave.StandardValuesFilePath;
    }

    private Branch GetPublicBranch(ComponentToSave componentToSave)
    {
      if (componentToSave.ComponentDataBranch != null)
        return componentToSave.ComponentDataBranch;
      
      return componentToSave.StandardValuesBranch;
    }
    
    private bool IsValidXml(string filePath)
    {
      if (!xmlValidator.ValidateXml(filePath))
      {
        XmlErrors = xmlValidator.XmlValidationResult.Errors;
        XmlWarnings = xmlValidator.XmlValidationResult.Warnings;
        return false;
      }
      return true;
    }
    
    private CertificateNumberHandler ReadCertificateNumberHandler(Blob componentDataBlob)
    {
      using (var streamReader = new StreamReader(componentDataBlob.GetContentStream(), Encoding.Default))
      {
        return xmlReader.ReadOutCertificateId(streamReader);
      }
    }

    private CertificateNumberHandler ReadCertificateNumberHandler(string componentDataFile)
    {
      return xmlReader.ReadOutCertificateId(componentDataFile);
    }

    private PublicBranchNaming ReadPublicBranchNaming(string componentDataFile)
    {
      return xmlReader.ReadOutBranchNameData(componentDataFile);
    }

    private PublicBranchNaming ReadPublicBranchNaming(Blob componentDataBlob)
    {
      using (var streamReader = new StreamReader(componentDataBlob.GetContentStream(), Encoding.Default))
      {
        return xmlReader.ReadOutBranchNameData(streamReader);
      }
    }


    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }


    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        if (disposing)
        {
          if (CurrentRepository != null)
          {
            CurrentRepository.Dispose();
            VirtualDrive.UnmapDrive();
            CurrentRepository = null;
          }
        }
      }
      disposed = true;
    }
  }
}
